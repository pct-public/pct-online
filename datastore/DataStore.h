
/* Local Variables:  */
/* mode: c++         */
/* End:              */

/**
 * @class datastore
 *
 * Check also:
 * - Boost PropertyTree
 *
 * An abstract data store, different implementations optimized for
 * different applications, e.g. linear sequential access, fast read,
 * access grid
 */
template<typename DataObject>
class SimpleDataStore {
  using value_type = DataObject;
  using index_type = typename DataObject::index_type;
 public:
  SimpleDataStore();
  ~SimpleDataStore();

  /// TODO: design the io adaptor functions
  template<typename Adaptor>
  operator<<();

  /// manipulation 

  DataObject& operator[](index_type const& index);
  DataObject const& operator[](index_type const& index) const;

 private:
};

/// alternative
#include <map>

// not sure if the object should know the index at all, or even the index type
template<typename DataObject>
using DataStore = std::map<typename DataObject::index_type, DataObject>;
