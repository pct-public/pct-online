/// @file   run-pru-parser.cxx
/// @author Matthias Richter
/// @since  2019-02-14
/// @brief  Standalone pRU parser application

#include "readout/ReadoutSession.h"
#include "io-adaptors/FileFormat.h"
#include "io-adaptors/ASCIINumberReader.h"
#include "io-adaptors/Alphanumeric.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <string>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/endian/conversion.hpp>

#include "readout/pRUParser.h"
#include "readout/alpideDecoder.h"
#include "rootio/pRUExporter.h"

namespace bpo = boost::program_options;

using namespace bpct; // the Bergen proton CT

int main(int argc, char* argv[])
{
  // definition of the available command line options
  // note: because of some shortcoming in boost program option we can not set a default
  // value in the option description for type FileFormat, so we build defaults beore
  auto buildDefaultHelp = [](const char* description, auto defaultValue, auto const& supportedValues) -> std::string {
    std::string result(description);
    for (auto const& value : supportedValues) {
      std::stringstream option;
      option << " " << value << (value == defaultValue ? " (default)" : "") << ",";
      result += option.str();
    }
    result.pop_back();
    return result;
  };
  constexpr io::FileFormat defaultInputFormat = io::FileFormat::BINARY;
  constexpr io::FileFormat defaultOutputFormat = io::FileFormat::ROOT;
  auto inputModeHelp = buildDefaultHelp("input format:", defaultInputFormat, std::initializer_list<io::FileFormat>{io::FileFormat::BINARY, io::FileFormat::ASCII});
  auto outputModeHelp = buildDefaultHelp("output format:", defaultOutputFormat, std::initializer_list<io::FileFormat>{io::FileFormat::BINARY, io::FileFormat::ROOT});

  // clang-format off
  boost::program_options::options_description od{
    "Standalone pRU data format parser\n"
    "(c) 2019 Bergen pCT Collaboration\n\n"
    "Usage:\n"
    "  run-pru-parser --ifile data.raw --ofile sorted-data.bin\n"
  };
  od.add_options()
    ("ifile,i", bpo::value<std::string>(), "input file name")
    ("input-mode", bpo::value<io::FileFormat>(), inputModeHelp.c_str())
    ("ofile,o", bpo::value<std::string>(), "output file name")
    ("output-mode", bpo::value<io::FileFormat>(), outputModeHelp.c_str())
    ("treename", bpo::value<std::string>()->default_value("alpide_data"), "tree name in the ROOT output file")
    ("treetitle", bpo::value<std::string>()->default_value("Alpide data"), "tree title")
    ("verbosity,v", bpo::value<int>()->default_value(1), "verbosity level")
    ("help,h", "Hilfe!");
  // clang-format on

  std::stringstream cmdLineError;
  auto printhelp = [&cmdLineError, &od](int exitvalue = 0) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(exitvalue);
  };
  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error parsing command line arguments: " << e.what() << std::endl
              << std::endl;
    printhelp(1);
  }

  if (varmap.count("help") > 0) {
    printhelp();
  }
  if (varmap.count("ifile") == 0 || varmap.count("ofile") == 0) {
    cmdLineError << "missing parameter, please specify input and output files";
    printhelp(1);
  }

  auto ifilename = varmap["ifile"].as<std::string>();
  auto ofilename = varmap["ofile"].as<std::string>();
  auto treename = varmap["treename"].as<std::string>();
  auto treetitle = varmap["treetitle"].as<std::string>();
  auto verbosity = varmap["verbosity"].as<int>();
  auto inputFormat = defaultInputFormat;
  if (varmap.count("input-mode") > 0) {
    inputFormat = varmap["input-mode"].as<io::FileFormat>();
  }
  auto outputFormat = defaultOutputFormat;
  if (varmap.count("output-mode") > 0) {
    outputFormat = varmap["output-mode"].as<io::FileFormat>();
  }
  size_t wordsize = 16; // size of pRU words

  // Using ReadoutSession to create a processing pipeline with input, filter and output policies
  // Naturally, the pRU parser runs as filter policy, the basic buffer used for data exchange
  // between the policies is std::vector<char>
  // InputPolicy: a lambda function reading from file
  // ForwardPolicy: the pRUParser
  // OutputPolicy: an std function taking a buffer by move
  //
  // By use of policies, we provide different implementations which can mutually provide different
  // algorithms and interfaces.
  // Note that generic functions are used as forward and output policies instead of specific classes.
  // this allows to use a simple lambda function as policy.
  using BufferT = std::vector<char>;
  using Processor = readout::ReadoutSession<std::function<BufferT()>,
                                            std::function<BufferT(BufferT &&)>,
                                            std::function<void(BufferT &&)>>;

  std::mutex mutex;
  std::condition_variable readyToQuit;

  // run pipeline which might might be of different type depending on
  // configured policies
  auto runPipeline = [&readyToQuit, &mutex, verbosity](auto& input, auto& forward, auto& output) {
    Processor pipeline(input, forward, output);
    if (verbosity > 0) {
      std::cout << "Starting pipeline ... " << std::endl;
    }

    pipeline.control(Processor::Command::START);
    std::unique_lock<std::mutex> lock(mutex);
    readyToQuit.wait(lock);
    pipeline.control(Processor::Command::STOP);
    if (verbosity > 0) {
      std::cout << "... done " << std::endl;
    }
  };

  // input policy: a file reader
  // a lambda function which reads the file into a buffer and send the buffer out
  auto fileReader = [&ifilename, &inputFormat, wordsize, verbosity]() -> BufferT {
    std::istream* input = &std::cin;
    std::ifstream inputFile;
    if (ifilename != "-") {
      inputFile.open(ifilename);
      if (!inputFile.is_open()) {
        std::cout << "can not open file '" << ifilename << "' for reading" << std::endl;
        exit(1);
      }
      input = &inputFile;
    }

    BufferT buffer;
    if (inputFormat == io::FileFormat::BINARY) {
      // the size of one read cycle is fixed for now, but in the future we might allow
      // partial reads
      const size_t cycleSize = 64 * 1024;
      size_t bytesRead = 0;
      while (input->good() && !input->eof()) {
        if (bytesRead + cycleSize > buffer.size()) {
          // reserve space for the next cycle
          buffer.resize(bytesRead + cycleSize);
        }
        auto size = (input->read(buffer.data() + bytesRead, cycleSize)).gcount();
        bytesRead += size;
        if (size == 0) {
          break;
        }
      }
      buffer.resize(bytesRead);
    } else if (inputFormat == io::FileFormat::ASCII) {
      *input >> io::ASCIINumberReader(buffer, wordsize, verbosity);
      // reverse the data words if the native endianness does not match big endian
      // format of pRU words
      if (boost::endian::order::native == boost::endian::order::little) {
        for (size_t i = 0; i < buffer.size(); i += wordsize) {
          for (int low = i, high = i + wordsize - 1; low < high; low++, high--) {
            std::swap(buffer[low], buffer[high]);
          }
        }
      }
    } else {
      std::stringstream errmsg("invalid file format specified: ");
      errmsg << inputFormat;
      throw std::runtime_error(errmsg.str());
    }
    return buffer;
  };

  // processing policy
  readout::pRUParser parser = readout::pRUParser();
  // --------- pRU parser Filter -----
  auto pRUFilter = [&parser, verbosity](BufferT&& buffer) -> BufferT {
    if (verbosity > 0) {
      std::cout << "Filter policy [pRU parser]: buffer of " << buffer.size() << " byte(s)" << std::endl;
    }
    return parser.parse_pRU_data(std::move(buffer));
  };

  if (outputFormat == io::FileFormat::BINARY) {
    ofilename.append(".bin");
    std::ofstream ofile(ofilename);
    if (!ofile.good()) {
      throw std::runtime_error("can not open file " + ofilename + " for writing output");
    }
    // output policy: write to file
    auto fileWriter = [&ofilename, &ofile, &readyToQuit, verbosity](BufferT&& buffer) {
      if (buffer.size() > 0) {
        if (verbosity > 0) {
          std::cout << "Output policy [File writer]: write to file " << ofilename << " size " << buffer.size() << std::endl;
        }
        ofile.write(reinterpret_cast<const char*>(buffer.data()), buffer.size() * sizeof(typename BufferT::value_type));
        readyToQuit.notify_all();
      }
    };

    runPipeline(fileReader, pRUFilter, fileWriter);
    ofile.flush();
    ofile.close();
  } else if (outputFormat == io::FileFormat::ROOT) {
    // Decoder extracts the alpide coordinate data
    readout::alpideDecoder decoder = readout::alpideDecoder();
    // TODO: Separate root functionality from rest of the system.
    rootio::pRUExporter exporter = rootio::pRUExporter(ofilename, treename, treetitle);

    auto rootWriter = [&ofilename, &decoder, &exporter, &readyToQuit, verbosity](BufferT&& buffer) {
      readyToQuit.notify_all();
      if (buffer.size() > 0) {
        if (verbosity > 0) {
          std::cout << "Output policy  [Root tree Writer]: write to file " << ofilename << ".root" << std::endl;
        }
        exporter.writeToTree(decoder.convert(std::move(buffer)));
      }
    };

    runPipeline(fileReader, pRUFilter, rootWriter);
  } else {
    std::stringstream msg;
    msg << "Output mode '" << outputFormat << "' not yet supported";
    throw std::runtime_error(msg.str());
  }

  // Gives a summary of errors found.
  parser.print_errors();
  
  // Prints the binary content of all errors found:
  //parser.inspect_error_vector();

}
