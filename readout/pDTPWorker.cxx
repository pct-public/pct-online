#include "readout/pDTPWorker.h"
#include <string>
#include <stdexcept>

namespace bpct
{
namespace readout
{

/// Calculate the number of expected bytes of the server reply
/// The server reply depends on the request.
size_t pDTPWorker::getExpectedServerReplySize(data_model::pDTPClientRequest const& request) const
{
  // TODO: we might want to get this from header file
  // the server reply header is followed by a time stamp of the same size
  constexpr size_t size_of_pru_word = 8;
  size_t replySize = 0;
  using ClientOpcode = data_model::pDTPClientRequest::ClientOpcode;
  auto opcode = static_cast<ClientOpcode>(request.opcode);
  if (opcode == ClientOpcode::CLIENT_RQR || opcode == ClientOpcode::CLIENT_RQT || opcode == ClientOpcode::CLIENT_RQS || opcode == ClientOpcode::CLIENT_RQFS) {
    replySize = sizeof(data_model::pDTPServerReply) + request.sizeofpacket * size_of_pru_word;
  } else if (opcode == ClientOpcode::CLIENT_ERR || opcode == ClientOpcode::CLIENT_ACK || opcode == ClientOpcode::CLIENT_ABRT) {
    replySize = 0;
  } else if (opcode == ClientOpcode::CLIENT_GS) {
    replySize = sizeof(data_model::pDTPServerReply);
  } else {
    throw std::runtime_error("invalid pDTP client request opcode " + std::to_string(static_cast<unsigned>(request.opcode)));
  }

  return replySize;
}

} // namespace readout
} // namespace bpct
