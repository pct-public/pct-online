/// @file   ez-network-listener.cxx
/// @author Matthias Richter
/// @since  2019-02-14
/// @brief  An easy network listener example
/// An example for using the ReadoutSession to listen on a port and write the data to
/// stdout or file
/// TODO:
/// - implement the control via terminal input
/// - implement protocol option
/// - implement timeout option

#include "readout/ReadoutSession.h"
#include "readout/NetworkReadout.h"
#include <iostream>
#include <cstdint> // uint8_t
#include <vector>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <thread>
#include <sstream>
#include <stdio.h> // system (terminal control)
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

namespace bpo = boost::program_options;

using namespace bpct; // the Bergen proton CT

int main(int argc, char* argv[])
{
  // definition of the available command line options
  boost::program_options::options_description od{
    //
    "Easy network listener example\n"                                                                                          //
    "(c) 2019 Bergen pCT Collaboration\n\n"                                                                                    //
    "Send data to the listener with e.g.\n"                                                                                    //
    "echo -n \"hello\" | nc localhost 29070\n"                                                                                 //
  };                                                                                                                           //
  od.add_options()                                                                                                             //
    ("port,p", bpo::value<int>(), "port number")                                                                               //
    ("timeout", bpo::value<int>()->default_value(0), "timeout in sec after which the server automatically terminates")         //
    ("protocol", bpo::value<std::string>()->default_value("tcp"), "protocol type: tcp or udp\nnot yet implemented")            //
    ("read-mode", bpo::value<std::string>()->default_value("once"), "read mode: once, cont\nnot yet implemented")              //
    ("output-mode,m", bpo::value<std::string>()->default_value("binary"), "output mode: binary or ascii\nnot yet implemented") //
    ("output-file,o", bpo::value<std::string>(), "output file\nnot yet implemented")("help,h", "Hilfe!");                      //

  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error: " << e.what() << std::endl;
    exit(1);
  }

  bool printHelp = varmap.count("help");
  std::stringstream cmdLineError;
  if (!printHelp && varmap.count("port") == 0) {
    cmdLineError << "invalid argument: missing parameter 'port'";
    printHelp = true;
  }
  if (printHelp) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(0);
  }

  auto port = varmap["port"].as<int>();
  auto timeout = varmap["timeout"].as<int>();

  // define the server with input, processing and output policies
  // The basic buffer used for data exchange between the policies is std::vector<uint8_t>
  // InputPolicy: the NetworkReadout policy is used as input policy
  // ForwardPolicy: an std function which takes a buffer by move and returns a buffer
  // OutputPolicy: an std function taking a buffer by move
  //
  // Note that generic functions are used as forward and output policies instead of specific classes.
  // this allows to use a simple lambda function as policy.
  using byte = uint8_t;
  using BufferT = std::vector<byte>;
  using Protocol = boost::asio::ip::tcp;
  using Server = readout::ReadoutSession<readout::NetworkReadout<BufferT, Protocol>,
                                         std::function<BufferT(BufferT &&)>,
                                         std::function<void(BufferT &&)>>;

  // processing policy: a simple forward filter
  // this is a lambda function, it is defined in the current scope (i.e. the current function)
  // and has access to the function variables if specified in the catch-clause []
  // the function parameter definition is as usual, the return type of the lambda is either
  // deduced automatically or specified with the '->' syntax.
  // this lambda function is a simple forward, we can later on optimize the Networker class
  // to simply drop to step if a void forward policy is specified.
  auto forwardFilter = [](BufferT&& buffer) -> BufferT {
    return std::move(buffer);
  };

  // output policy: write to standard output or file
  // this is a lambda function which has access to the local variable 'outputStream'
  std::ostream& outputStream = std::cout;
  auto output = [&outputStream](BufferT&& buffer) {
    outputStream.write(reinterpret_cast<const char*>(buffer.data()), buffer.size() * sizeof(typename BufferT::value_type));
    outputStream.flush();
  };

  // a guard to set the terminal mode to raw and restore back the standard
  // TODO: would in principle need to store the state
  class TerminalRawMode
  {
   public:
    TerminalRawMode()
    {
      system("stty raw");
    }
    ~TerminalRawMode()
    {
      system("stty cooked");
    }
  };
  // setup keyboard control
  // keyboard control is supposed to act directly onto keys without the enter key
  // the raw terminal mode is supposed to be right approach for this but this still
  // needs some development, can be even a separate tool because we might need this
  // in different applications
  std::condition_variable readyToQuit;
  [[maybe_unused]] auto keyboardControl = [&readyToQuit]() {
    // extremely simple right now, look out for pressed 'q' key
    TerminalRawMode guard;
    char key = 0;
    while (key != 'q') {
      sleep(1);
      key = getchar();
    }
    std::cout << " - quitting" << std::endl;
    readyToQuit.notify_all();
  };
  std::thread keyboardListener;
  // this is not yet working reliably
  // disable keyboard control until better tested
  //keyboardListener = std::thread(keyboardControl);

  Server server(Server::InputPolicy(BufferT(16), port), forwardFilter, output);
  std::mutex mutex;
  server.control(Server::Command::START);
  std::unique_lock<std::mutex> lock(mutex);
  if (timeout > 0) {
    readyToQuit.wait_for(lock, std::chrono::seconds(timeout));
  } else {
    readyToQuit.wait(lock);
  }
  server.control(Server::Command::STOP);
  keyboardListener.detach();
}
