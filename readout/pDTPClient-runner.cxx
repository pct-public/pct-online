//
// Created by alf on 12.09.2019.
//
#include "datamodel/pDTP-protocol.h"
#include "readout/comService.h"
#include "readout/pDTPClient.h"
#include <boost/log/trivial.hpp>
#include <boost/asio.hpp>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include "datamodel/pDTP-helpers.h"
#include "datamodel/pDTP-protocol.h"
#include "readout/clientconfig.h"
#include "readout/iComService.h"
//#include <cstring>
#include <shared_mutex>

#include <boost/lockfree/spsc_queue.hpp>
#include <boost/program_options.hpp>
namespace bpo = boost::program_options;
using namespace bpct;

std::atomic<bool> exitProgram = false;
std::atomic<bool> exitFileWriter = false;

bpct::readout::pDTPClient* client;
boost::lockfree::spsc_queue<std::vector<char>>* spscQueue;

using pDTPClientRequest = data_model::pDTPClientRequest;
using pDTPServerReply = data_model::pDTPServerReply;
using ClientOpcode = data_model::pDTPClientRequest::ClientOpcode;
using ServerOpcode = data_model::pDTPServerReply::ServerOpcode;
using pDTPHelpers = data_model::pDTPHelpers;

bpct::readout::ClientConfig conf;

/**
 * @Brief Function that handles getting data from a queue and writing it to a
 * file.
 */
void fileWriter()
{
  /*File to write test results in.*/
  if (conf.verbosity > 0) {
    std::cout << "File writer thread started \n";
  }
  long long int totalByteWriten = 0;
  long long int numWrite = 0;
  std::fstream fileOut;
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream oss;
  oss << std::put_time(&tm, "%d-%m-%Y-%H-%M-%S");
  auto str = oss.str();
  std::string filename = str + "-testData10gb.txt";
  fileOut.open(filename, std::ios::out | std::ios::binary);
  while (client->isRunning() || client->hasDataInQueue()) {
    std::vector<char> buf = {'0'};
    if (client->getDataFromQueue(buf) && buf.size() > 1) {
      fileOut.write(buf.data(), buf.size());
      numWrite++;
      totalByteWriten += buf.size();
      if (conf.verbosity > 0) {
        if (numWrite % 1000 == 0) {
          t = std::time(nullptr);
          tm = *std::localtime(&t);
          oss.str("");
          oss << std::put_time(&tm, "%d-%m-%Y-%H-%M-%S");
          auto str = oss.str();

          std::cout << str << " -- Packet number:" << numWrite
                    << " -- Package size writen: " << buf.size()
                    << " -- Total bytes writen to file: " << totalByteWriten
                    << '\n';
        }
      }

      buf = {'0'};
    }
    if (exitFileWriter) {
      fileOut.close();
      std::cout << "Number of packages writen to file: " << numWrite << '\n';
    }
  }
  if (conf.verbosity > 0) {
    std::cout << "File writer thread stopped \n";
  }
}
/**
 * @brief Function that handles input from console and sends signals to the
 * client
 */
void commandListener()
{
  bool run = false;
  while (!run) {
    char cmd;
    std::cin >> cmd;
    switch (cmd) {
      case 'q':
        client->stopClient();
        exitProgram = true;
        run = true;
        break;
      case 's':

        std::cout << "Speed: " << client->getBytesPrSec() << " MB/s"
                  << "\nNumber of bytes received:"
                  << client->getNumberOfBytesReceived() << '\n';
        break;
      default:
        break;
    }
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
}
/**
 * @brief Function that handles a timer if the client is set to run for a given
 * time.
 * @param time an int with seconds for how long to run the client.
 */
void timedRunner(int time)
{
  if (conf.verbosity > 0) {
    std::cout << "Timer thread started \n";
  }
  auto sleep = 0;
  if (time > 0) {
    if (conf.verbosity > 0) {
      std::cout << "Timer running for time: " << time << "s \n";
    }
    while (time != sleep && !exitProgram) {
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));
      sleep++;
    }
    std::cout << "Timer expired\n";
    exitProgram = true;
    client->stopClient(); /// Send a signal to stop the pDTPClient
  }

  if (conf.verbosity > 0) {
    std::cout << "Timer thread stopped \n";
  }
}

/**
 * @brief Reads the config file and sets the struct with the data for the client
 * @param argc integer, number of arguments sent
 * @param argv pointer to a char array with the arguments
 * @return returns true if settings are set
 */
bool readConfig(int argc, char* argv[])
{
  // Get config data from file
  boost::program_options::options_description generalOptions{
    "pRU readout application using the pDTP protocol\n"
    "(c) 2019 Bergen pCT Collaboration\n\n"
    "Usage:\n"
    "  pdtpclient --config settings.conf \n"
    "Config file is located in the tuning-scripts folder\n"
    "Remember to run other tuning scripts for improved perfomance of OS"};

  generalOptions.add_options()("help,h", "Help screen")(
    "config", boost::program_options::value<std::string>(), "Config file");

  boost::program_options::options_description fileOptions{"File"};
  /// Options for the config file
  fileOptions.add_options()("IP", boost::program_options::value<std::string>(),
                            "IP");
  fileOptions.add_options()("PORT", boost::program_options::value<short>(),
                            "PORT");
  fileOptions.add_options()(
    "SSIZE", boost::program_options::value<unsigned short>(), "SSIZE");
  fileOptions.add_options()(
    "PSIZE", boost::program_options::value<unsigned short>(), "PSIZE");
  fileOptions.add_options()(
    "MODE", boost::program_options::value<std::string>(), "MODE");
  fileOptions.add_options()("RTIME", boost::program_options::value<int>(),
                            "RTIME");
  fileOptions.add_options()("UDPTIMEO", boost::program_options::value<int>(),
                            "UDPTIMEO");
  fileOptions.add_options()("NUMREQ", boost::program_options::value<int>(),
                            "NUMREQ");
  fileOptions.add_options()("VERB", boost::program_options::value<int>(),
                            "VERB");
  fileOptions.add_options()("SPSCSIZE", boost::program_options::value<int>(),
                            "SPSCSIZE");
  fileOptions.add_options()("POLL", boost::program_options::value<int>(),
                            "POLL");
  fileOptions.add_options()("ENABLEFILE", boost::program_options::value<int>(),
                            "ENABLEFILE");
  fileOptions.add_options()("SMODE", boost::program_options::value<int>(),
                            "SMODE");
  fileOptions.add_options()("NOWAIT", boost::program_options::value<int>(),
                            "NOWAIT");
  fileOptions.add_options()("MAXIMIZE", boost::program_options::value<int>(),
                            "MAXIMIZE");
  fileOptions.add_options()("TIMESTAMP", boost::program_options::value<int>(),
                            "TIMESTAMP");

  bpo::variables_map vm;
  bpo::store(parse_command_line(argc, argv, generalOptions), vm);
  if (vm.count("config")) {
    std::ifstream ifs{vm["config"].as<std::string>().c_str()};
    if (ifs)
      store(parse_config_file(ifs, fileOptions), vm);
  }
  notify(vm);

  if (vm.count("help")) {
    std::cout << generalOptions << '\n';
    return false;
  } else if (vm.count("IP")) {
    conf.ipAddress = vm["IP"].as<std::string>();
    std::cout << "---------------------------CONFIG FROM FILE: "
                 "---------------------------\n"
              << "SERVER IP: " << conf.ipAddress << "\n";
  }
  if (vm.count("PORT")) {
    conf.portNumber = vm["PORT"].as<short>();
    std::cout << "PORT: " << conf.portNumber << "\n";
  }

  if (vm.count("SSIZE")) {
    conf.streamSize = vm["SSIZE"].as<unsigned short>();
    std::cout << "STREAM SIZE: " << conf.streamSize << "\n";
  }
  if (vm.count("PSIZE")) {
    conf.packageSize = vm["PSIZE"].as<unsigned short>();
    std::cout << "PACKAGE SIZE: " << conf.packageSize << "\n";
  }
  if (vm.count("MODE")) {
    conf.startUpMode = vm["MODE"].as<std::string>();
    std::cout << "MODE: " << conf.startUpMode << "\n";
  }
  if (vm.count("RTIME")) {
    conf.clientRunTime = vm["RTIME"].as<int>();
    std::cout << "Time in secounds to run client for(0 is disabled): "
              << conf.clientRunTime << "\n";
  }
  if (vm.count("UDPTIMEO")) {
    conf.clientUDPTimeout = vm["UDPTIMEO"].as<int>();
    std::cout << "UDP timeout in microsecounds(0 is disabled): "
              << conf.clientUDPTimeout << "\n";
  }
  if (vm.count("NUMREQ")) {
    conf.requestNumberOfWords = vm["NUMREQ"].as<int>();
    std::cout << "Number of words to request (0 is disabled): "
              << conf.requestNumberOfWords << "\n";
  }
  if (vm.count("VERB")) {
    conf.verbosity = vm["VERB"].as<int>();
    std::cout << "Verboisity for logger: " << conf.verbosity << "\n";
  }
  if (vm.count("SPSCSIZE")) {
    conf.spscQueueSize = vm["SPSCSIZE"].as<int>();
    std::cout << "Size of the spsc queue: " << conf.spscQueueSize << "\n";
  }

  if (vm.count("POLL")) {
    conf.pollMode = vm["POLL"].as<int>();
    std::cout << "Poll mode is: " << conf.pollMode << "\n";
  }

  if (vm.count("ENABLEFILE")) {
    conf.fileWriterEnabled = vm["ENABLEFILE"].as<int>();
    std::cout << "Filwriter is: " << conf.fileWriterEnabled << "\n";
  }
  if (vm.count("SMODE")) {
    conf.handleBufferStatus = vm["SMODE"].as<int>();
    std::cout << "Stream mode: " << conf.handleBufferStatus << "\n";
  }
  if (vm.count("NOWAIT")) {
    conf.noWaitFlag = vm["NOWAIT"].as<int>();
    std::cout << "NOWAIT flag: " << conf.noWaitFlag << "\n";
  }
  if (vm.count("TIMESTAMP")) {
    conf.noWaitFlag = vm["TIMESTAMP"].as<int>();
    std::cout << "TIMESTAMP flag: " << conf.timeStampFile << "\n";
  }
  if (vm.count("MAXIMIZE")) {
    conf.maximizeFlag = vm["MAXIMIZE"].as<int>();
    std::cout << "MAXIMIZE flag: " << conf.maximizeFlag << "\n";
    return true;
  }
  return false;
}

int main(int argc, char* argv[])
{
  try {
    if (argc < 2) {
      perror("Need to input 2 parameters");
      return -1;
    }
    if (readConfig(argc, argv)) {
      std::cout << "---------------------------CONFIG FILE "
                   "LOADED---------------------------\n";

    } else {
      perror("Error Loading config file");
      return -1;
    }

  } catch (std::exception& ex) {
    std::cerr << ex.what() << '\n';
    return -1;
  }
  std::mutex mutex;
  std::condition_variable readyToQuit;
  std::unique_lock<std::mutex> lock(mutex);

  std::thread cmdThread([&readyToQuit, &mutex]() {
    std::cout << "starting cmd thread";
    boost::asio::io_context my_io_context;
    ::unlink("/tmp/PCTDOMAINSOCK"); // Remove previous binding.
    boost::asio::local::datagram_protocol::endpoint ep("/tmp/PCTDOMAINSOCK");
    boost::asio::local::datagram_protocol::socket socket(my_io_context, ep);

    char buffer[1024];
    boost::system::error_code ignored_error;
    auto length = socket.receive_from(boost::asio::buffer(buffer), ep);
    if (length > 0) {
      // TODO: do we want to have a check here?
    }
    client->stopClient();
    readyToQuit.notify_all();
    socket.close();
  });

  ComService* coms = new ComService();
  client = new bpct::readout::pDTPClient(conf, coms);

  std::this_thread::sleep_for(std::chrono::milliseconds(10));

  std::thread fileWriterThread(fileWriter);
  std::thread fsmThread(&readout::pDTPClient::handleStates, client);
  //std::thread commandThread(commandListener);
  readyToQuit.wait(lock);
  if (conf.clientRunTime != 0) {
    std::thread timedThread(timedRunner, conf.clientRunTime);
    timedThread.join();
  }

  cmdThread.join();
  fileWriterThread.join();
  fsmThread.join();

  //commandThread.detach();
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  std::cout << "Client has been running for: \n"
            << client->getUpTime() << " ms " << (client->getUpTime() * 0.001)
            << " s\n"
            << "Number of bytes recivived: "
            << client->getNumberOfBytesReceived()
            << "\n"
            << "Number of packet id errors: "
            << client->getMissingPacketErrors()
            ///        << mellom
            << "\n";
  delete (coms);
  delete (client);
  return 0;
}
