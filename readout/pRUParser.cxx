/// @file   pRUParser.cxx
/// @author Øistein Jelmert Skjolddal
/// @since  2019-11-11
/// @brief  Parser implemenation for pRU words

#include "readout/pRUParser.h"
#include "readout/pRUEnums.h"
#include "readout/pRUError.h"

#include <bitset>
#include <iostream>
#include <thread>
#include <stdexcept>

namespace bpct
{
namespace readout
{

pRUParser::pRUParser()
{
  stat_errors = std::vector<int>(NUMBER_OF_ERRORS, 0);
}

pRUParser::~pRUParser()
{
}

/// @brief Split the input buffer and send each 16-byte pRU word to be analyzed
/// @param binary vector
/// @return "Binary" buffer of all complete frames found in the input buffer.
std::vector<char> pRUParser::parse_pRU_data(std::vector<char>&& data)
{ // TODO: detatch as thread

  // the parser is using member variable frame_vector to store the parsed data
  // we observe accumulation of data if the parser method is called multiple times
  // in principle, the vector should be empty after returning by move, need to
  // investigate why that isn't the case, probably a matter of the return type
  // definition.
  // Anyhow clear the vector
  frame_vector.clear();

  // TODO: Bad form, need better error handling
  if (data.size() % 16 != 0) {
    std::cout << "wrong input size " << std::endl;
    stat_errors[INPUT_SIZE_ERROR]++;
    return {}; // return nothing
    //TODO: save this data(dump to disk) or do something else with it?
  }

  // Analyze all pRUWords - Endianness problems should be handled here!
  for (unsigned int i = 0; i < data.size(); i += 16) { //extract 16 words
    check_pRU_word(reinterpret_cast<unsigned char const*>(data.data() + i));
  }

  //----- All frames are now parsed and stored in frame_vector -----//

  // ---------- Return result ----------- //
  // We move, because we want to leave the frame_vector empty!
  return std::move(frame_vector);
}

/// @brief Checks word type of incomming word
/// @param Data from a text file
/// @return none
void pRUParser::check_pRU_word(unsigned char const pRUWord[16])
{
  // the PRU Word type is defined by the 2 most significant bits
  unsigned char pRUType = pRUWord[0] & data_model::pRUTypeMask;

  if (pRUType == data_model::pRUTDataWord) { // case DATA_WORD:
    insert_data(pRUWord);
  } else if (pRUType == data_model::pRUTHeaderWord) { // case TAG_HEADER_WORD:
    insert_header(pRUWord);
  } else if (pRUType == data_model::pRUTTrailerWord) { // case TAG_TRAILER_WORD:
    insert_trailer(pRUWord);
  } else { // case TAG_EMPTY_WORD or TAG_DELIMITER_WORD:
    // if empty word (reserved bit number 111 is set to 0)
    // mask 0b1000'0000 &
    if ((0x80 & pRUWord[2]) == 0) { // this is not the pRU trailer flag but the 8th bit in the 3rd octet of the PRU word.
      // Add frame to the error vector
      for (int i = 0; i < 16; ++i) {
        empty_words.push_back(static_cast<char>(pRUWord[i]));
      }
      // Extract elements and concatenate to a number then add to sum of empty words
      number_of_empty_words += (static_cast<unsigned long int>(0x07 & pRUWord[2])) << 13 | // 0b00000111
                               (static_cast<unsigned long int>(pRUWord[3])) << 5 |
                               (static_cast<unsigned long int>(0xF8 & pRUWord[4]) >> 3); // 0b11111000
    }                                                                                    // else Delimiter Word (alternative check: pRUWord[2] == 255)
    else {
      // delimiter words contain no data, count only
      number_of_delimiter_words++;
    }
  } // End case Empty or Delimiter word

} // End Check_pRU_Word

/// @brief Inserts a header word into an frame
/// @param Word and a list of unfinished frames
/// @return none
/// @note This function creates a temporary frame which will be complete when
/// the trailer word arrives
void pRUParser::insert_header(unsigned char const pRUWord[16])
{
  pRUFrame pRU_frame;
  pRUWord >> pRU_frame; /**< the binary header word*/

  auto itr = pRUError::check_existing_header(temp_frames, pRUWord[0], pRUWord[1]);
  if (itr != temp_frames.end()) {
    // Save unfinished frame to error_vector
    error_vector << *itr;

    //TODO: Check busy flag in header: If a busy on_is the previous frame from the same chip
    // has a full buffer and might be baad (ie, consider moving the frame it to the error vector)
    // Since still stored in temp vec, no trailer has arrived
    temp_frames.erase(itr);
    stat_errors[MULTIPLE_PRU_HEADER_ERROR]++;
  }
  temp_frames.push_back(std::move(pRU_frame));
}

/// @brief Inserts a data word into an unfinished frame
/// @param Word and a list of unfinished frames
/// @return none
void pRUParser::insert_data(unsigned char const pRUWord[16])
{
  // masked prefix is turned in to a header word with the same RU
  // Change prefix 00 to 01
  const unsigned char maskedPrefix = data_model::pRUTHeaderWord | pRUWord[0];
  //0b01'000000 | pRUWord[0];
  //Find coresponding header & frame
  for (auto& frame : temp_frames) {
    // check if chip id, stave id, ru id match by comparing
    // first 16 bytes of the words buffer, which are the pRU header
    if (frame.words[0] == maskedPrefix &&
        frame.words[1] == pRUWord[1]) {
      /**< add the the binary data word to frame*/
      pRUWord >> frame;
      return;
    }
  }
  {
    // Add to errors
    for (int i = 0; i < 16; ++i) { // TODO: check if compiler unwraps this?
      error_vector.push_back(static_cast<char>(pRUWord[i]));
    }
    stat_errors[DATA_WORD_MISSING_HEADER_ERROR]++;
  }
}

/// @brief Inserts a trailer word into a frame
/// @param Word and a list of unfinished frames
/// @return none
/// @note The trailer word is thrown away if it cannot be added to the correct frame
void pRUParser::insert_trailer(unsigned char const pRUWord[16])
{
  // masked prefix is turned in to a header word with the same RU
  // Change prefix 10 to 01
  unsigned char maskedPrefix = data_model::pRUTypeMask ^ pRUWord[0];

  //Find coresponding header & frame
  for (auto it = temp_frames.begin(); it != temp_frames.end(); it++) {
    // if chip id, stave id, ru id matches and it is not on the end
    if (it->words[0] == maskedPrefix &&
        it->words[1] == pRUWord[1]) {

      pRUWord >> *it; /**< add the binary trailer word to the frame*/

      analyze_data(*it);     // TODO: detatch as tread
      temp_frames.erase(it); //Remove the iterators object/ the object the iterator points to
      return;
    }
  }

  {
    stat_errors[TRAILER_WORD_MISSING_HEADER_ERROR]++;
  }
}

//#------------------------------------------------------------------------------------------------#
/// @brief Takes a complete frame and analyses it.
/// @param An iterator pointing to a complete frame
/// @return none
/// @note The frame is checked and is moved to the return vector or the error storage.
void pRUParser::analyze_data(frame_list::value_type& it)
{
  bool error_found = false;

  if (pRUError::check_trailer_flags(it) == true) {
    stat_errors[PRU_TRAILER_FLAGS_ERROR]++;
    error_found = true;
  } // checks if data.fields == 0 (no data in frame)
  else if (it.words.size() < 48) {
    stat_errors[NO_DATA_WORD_ERROR]++;
    error_found = true;
  } else {
    // Must be called before check_frame_size
    // checks the the alpide binary data and finds number of alpide data bytes recived.
    if (!check_data(it)) {
      error_found = true;
    }
    if (!error_found && (pRUError::check_frame_size((it)) == false)) {
      stat_errors[FRAME_SIZE_ERROR]++;
      error_found = true;
    }
  }
  if (!error_found) {
    number_of_frames++;
    // ---- Store sorted binaries ------ //
    // -- insert header, data, and trailer
    frame_vector << it;
  } else { // error found
    //Store the faulty frame
    error_vector << it;
  }
}

/// @brief	Checks if the alpide data fields are according to the pRU data format
/// @param 	A reference to an frame
/// @return None
bool pRUParser::check_data(pRUFrame& frame)
{
  if (frame.words.size() < 48) {
    throw std::logic_error("must have header, trailer, and at least one data word");
  }
  bool found_trailer = false;
  unsigned int buzycounter = 0;
  /** <byte element iterator*/
  // data is starting at the second word, byte offset 16, the position of the trailer
  // is the end for the data iterator
  std::vector<unsigned char>::const_iterator fields_begin = frame.words.cbegin() + 16;
  std::vector<unsigned char>::const_iterator fields_end = frame.words.cend() - 16;
  std::vector<unsigned char>::const_iterator it_fields = fields_begin;
  bool isFirst = true;
  // the end position of the current word
  auto word_end = it_fields;
  std::advance(word_end, 16);
  do {
    if (it_fields == word_end) {
      std::advance(word_end, 16);
    }
    // --------------- Once per pRU Word -------------- //
    // Remove pRU word header (Strip to data payload)
    std::advance(it_fields, 2);

    // If this is the first dataword.
    if (isFirst) {
      isFirst = false;
      // it_fields should now be at position 2 in the pru frame and here should be a chip header
      if ((*it_fields & data_model::MASK_CHIP) != data_model::ALPIDE_CHIP_HEADER) {
        //Chip header not found
        stat_errors[MISSING_CHIP_HEADER_ERROR]++;
        return false;

      }                             // We did find a chip header
      frame.alpide_data_bytes += 2; // count the bytes
      std::advance(it_fields, 2);   // move 2, inherently safe

      //Now at position 4 and we should find a region header
      if ((*it_fields & data_model::MASK_REGION_HEADER) != data_model::ALPIDE_REGION_HEADER) {
        unsigned char chip_word_check = (*it_fields) & data_model::MASK_CHIP;
        if (chip_word_check == data_model::ALPIDE_CHIP_TRAILER) {
          unsigned char trailer_flags = (*it_fields) & 0x0F; // 0b0000'1111
          if (trailer_flags == 8) {                          //Check if for buzy violation
            stat_errors[ALPIDE_TRAILER_FLAGS_ERROR]++;
            return false;
          }
        }

        //Region header not found
        stat_errors[MISSING_REGION_HEADER_ERROR]++;
        return false;

      } // Region header found, count the bytes and move 1, inherently safe
      frame.alpide_data_bytes += 1;
      pRUParser::REGION = *it_fields & 0x1F; // 0b0001'1111
      std::advance(it_fields, 1);
    }

    // ----------------- For each remaining unsigned char in the word -----------------//
    while (it_fields != word_end) {
      unsigned char chip_word_check = (*it_fields) & data_model::MASK_CHIP;
      unsigned char data_word_check = (*it_fields) & data_model::MASK_DATA;
      unsigned char region_word_check = (*it_fields) & data_model::MASK_REGION_HEADER;

      auto advance_it = [&frame, &it_fields, &word_end, &fields_end](size_t length) -> bool {
        frame.alpide_data_bytes += length;
        size_t capacity = std::distance(it_fields, word_end);
        if (capacity < length) {
          if (word_end != fields_end) {
            it_fields = word_end;
            std::advance(word_end, 16);
            std::advance(it_fields, length - capacity);
          }
          return false;
        }
        std::advance(it_fields, length);
        return true;
      };

      // Allready passed and counted chip header, more headers is a fatal error
      if (chip_word_check == data_model::ALPIDE_CHIP_HEADER) {
        stat_errors[MULTIPLE_CHIP_HEADER_ERROR]++;
        return false;
      } else if (chip_word_check == data_model::ALPIDE_CHIP_TRAILER) {
        unsigned char trailer_flags = (*it_fields) & 0x0F; // 0b0000'1111

        if (trailer_flags > 3) { // Check the trailer flags (2 most significant bits are fatal)
          stat_errors[ALPIDE_TRAILER_FLAGS_ERROR]++;
          return false;
        }
        // The two least significant bits are not fatal, count only
        if (trailer_flags == 1) {
          busy_transition++;
        } else if (trailer_flags == 2) {
          strobe_extended++;
        } else if (trailer_flags == 3) {
          busy_transition++;
          strobe_extended++;
        }

        frame.alpide_data_bytes += 1;
        found_trailer = true;

        // Distance from end of vector to last data element of the data
        // there should not be any more data after the trailer
        unsigned int expected_padding = ((fields_end)-1) - (it_fields);
        unsigned int actual_padding = pRUError::check_padding_error(it_fields, word_end);
        if (expected_padding != actual_padding) {
          stat_errors[PADDING_ERROR]++;
          return false;
        }
        break;
      }

      else if (data_word_check == data_model::ALPIDE_DATA_LONG) {
        if (!advance_it(3)) {
          break;
        }
      }

      else if (data_word_check == data_model::ALPIDE_DATA_SHORT) {
        if (!advance_it(2)) {
          break;
        }
      }

      else if (region_word_check == data_model::ALPIDE_REGION_HEADER) {
        pRUParser::REGION = *it_fields & 0x1F; // 0b0001'1111
        advance_it(1);
      } else if (data_word_check == data_model::ALPIDE_BUSY_ON) {
        std::cout << "BUSY ON " << ++buzycounter << '\n';
        advance_it(1);
      } else if (data_word_check == data_model::ALPIDE_BUSY_OFF) {
        std::cout << "BUSY OFF " << buzycounter << '\n';
        advance_it(1);
      }
      // Catch missing trailers
      else if ((*it_fields) == data_model::ALPIDE_PADDING) {
        stat_errors[MISSING_CHIP_TRAILER_ERROR]++;
        return false;
      } else {
        stat_errors[UNKNOWN_ALPIDE_WORD_ERROR]++;
        break;
      }
    }
  } while (it_fields != fields_end && !found_trailer);
  if (found_trailer == false) {
    stat_errors[MISSING_CHIP_TRAILER_ERROR]++;
  }
  return true;
}

unsigned int pRUParser::get_number_of_errors()
{
  int total = 0;
  for (auto err : stat_errors) {
    total += err;
  }
  return total;
}

void pRUParser::print_errors()
{
  std::cout << '\n'
            << "SUMMARY ------------- " << '\n'
            << "Total number of pRU frames parsed: " << number_of_frames << '\n'
            << "Total number of pRU empty words found: " << number_of_empty_words << '\n'
            << "Total number of pRU delimiter words found: " << number_of_delimiter_words << '\n'
            << "Unfinished pRU words in buffer: " << temp_frames.size() << '\n';

  // ---------- Errors ----------- //
  if ((number_of_empty_words != 0)) {
    std::cout << '\n'
              << "EMPTY FRAMES FOUND: " << number_of_empty_words << '\n';
  }
  if (get_number_of_errors() == 0) {
    std::cout << '\n'
              << "No errors found. " << std::endl;
  } else {
    std::cout << '\n'
              << "INPUT_SIZE_ERROR: " << stat_errors[INPUT_SIZE_ERROR] << ", "
              << "MULTIPLE_PRU_HEADER_ERROR: " << stat_errors[MULTIPLE_PRU_HEADER_ERROR] << ", "
              << "DATA_WORD_MISSING_HEADER_ERROR: " << stat_errors[DATA_WORD_MISSING_HEADER_ERROR] << ", " << '\n'
              << "TRAILER_WORD_MISSING_HEADER_ERROR: " << stat_errors[TRAILER_WORD_MISSING_HEADER_ERROR] << ", "
              << "PRU_TRAILER_FLAGS_ERROR: " << stat_errors[PRU_TRAILER_FLAGS_ERROR] << ", "
              << "NO_DATA_WORD_ERROR: " << stat_errors[NO_DATA_WORD_ERROR] << ", " << '\n'
              << "FRAME_SIZE_ERROR: " << stat_errors[FRAME_SIZE_ERROR] << ", "
              << "MISSING_CHIP_HEADER_ERROR: " << stat_errors[MISSING_CHIP_HEADER_ERROR] << ", "
              << "MISSING_REGION_HEADER_ERROR: " << stat_errors[MISSING_REGION_HEADER_ERROR] << ", " << '\n'
              << "MISSING_CHIP_TRAILER_ERROR: " << stat_errors[MISSING_CHIP_TRAILER_ERROR] << ", "
              << "MULTIPLE_CHIP_HEADER_ERROR: " << stat_errors[MULTIPLE_CHIP_HEADER_ERROR] << ", "
              << "ALPIDE_TRAILER_FLAGS_ERROR: " << stat_errors[ALPIDE_TRAILER_FLAGS_ERROR] << ", " << '\n'
              << "PADDING_ERROR: " << stat_errors[PADDING_ERROR] << ", "
              << "UNKNOWN_ALPIDE_WORD_ERROR: " << stat_errors[UNKNOWN_ALPIDE_WORD_ERROR];

    if (busy_transition > 0 && strobe_extended > 0) {
      std::cout << '\n'
                << '\n'
                << "ALPIDE Busy Transition: " << busy_transition << ", "
                << "ALPIDE Strobe Extended: " << strobe_extended;
    }
    std::cout << '\n'
              << std::endl;
  }
}

void pRUParser::inspect_error_vector()
{
  if (error_vector.size() % 16 != 0 || error_vector.size() < 16) {
    std::cout << "The error vector had wrong size. Do Panic! " << std::endl;
    return;
  }
  //std::cout << '\n';
  std::vector<char>::const_iterator it = error_vector.cbegin();
  while (it != error_vector.cend()) { // for each byte element
    // print the 3 first element as hexadesimal
    // Is masking with 1111 1111 nececary?
    std::cout << std::hex << (0xFF & it[0]) << " " << (0xFF & it[1]) << " " << (0xFF & it[2]) << std::endl;

    /*
    // Print all 16 elements as binary
    std::cout <<
      std::bitset<8>(static_cast<unsigned char>(it[0])) << " " << std::bitset<8>(static_cast<unsigned char>(it[1])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[2])) << " " << std::bitset<8>(static_cast<unsigned char>(it[3])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[4])) << " " << std::bitset<8>(static_cast<unsigned char>(it[5])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[6])) << " " << std::bitset<8>(static_cast<unsigned char>(it[7])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[8])) << " " << std::bitset<8>(static_cast<unsigned char>(it[9])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[10])) << " " << std::bitset<8>(static_cast<unsigned char>(it[11])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[12])) <<  " " << std::bitset<8>(static_cast<unsigned char>(it[13])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[14])) << " " << std::bitset<8>(static_cast<unsigned char>(it[15])) <<
      '\n' ;
    */
    std::advance(it, 16);
  }
}

} // namespace readout
} // namespace bpct
