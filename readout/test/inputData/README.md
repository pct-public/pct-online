# Test data generated from WP3

## How to modify data
These files can be generated from the wp3 repo by using
by using the scripts available there. 

    wp3/software/simulation/test_cases.py

## How to generate .bin files from the .txt files
On the command line run the command:

    xxd -r -p input_name.txt output_name.bin
    
    
## The files present are just a "plain" generation from test_casses.py
These descriptions are collected from the comments in that file.
### Test case 0
Purpose: To check alpide_data event handling, and that byte counter is correct when clustering is off
Test the byte counter with 100 events with incrementing size, no clustering of pixels
A good chunch of leading and trailing commas

### Test case 1
 Check that byte count is correct when clustering is on

### Test case 2
Check back-to-back data

### Test case 3
Check ALPIDE trailer flags

###Test case 4
Check random idle and comma with filtering

### Test case 5
Check random idle and comma with no filtering

###Test case 6 WARNING THIS MIGHT BE BROKEN!
Check wrong chip id with check_id on

### Test case 7
Check wrong chip id with check_id off

### Test case 8
Random Empty frames without compression

### Test case 9
Random Empty frames with full compression

### Test case 10
Random Empty frames with some compression

### Test case 11
Random frames (normal and empty) with random IDs with check ID on, also set values on the other fields Random Empty frames with some compression

### Test case 12
Max Size and Max Wait Time set to zero i.e. no checking

### Test case 13
Max Size Error

### Test case 14
Max Wait Time Error

### Test case 15
Random busys

### Test case 16
Empty Region

### Test case 17
Protocol errors

### Test case 18
Buffer Overflow





