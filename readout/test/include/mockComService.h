/**
 * @file ${FILE}
 * @author Alf K. Herland 
 * @date 21.10.2019.
 * @brief
*/

#ifndef PCT_BERGEN_READOUT_TEST_INCLUDE_MOCKCOMSERVICE_H_
#define PCT_BERGEN_READOUT_TEST_INCLUDE_MOCKCOMSERVICE_H_

#include "../../include/readout/iComService.h"
#include <vector>
#include <shared_mutex>
#include <atomic>
namespace bpct
{
namespace readout
{
namespace mock_helpers
{
// helper to create an array initialized with incremental values
template <typename T, size_t N>
constexpr std::array<T, N> make_response()
{
  std::array<T, N> array{0};
  for (size_t i = 0; i < N; i++) {
    array[i] = i;
  }
  return array;
}
} // namespace mock_helpers

class MockComService : public IComService
{
 public:
  MockComService(){};
  virtual ~MockComService(){};
  int virtual transmit(std::vector<char>& cmd) override;
  int virtual receive(std::vector<char>& buf);
  //virtual MockComService& operator= (const IComClient& a);
  void setDataOutput(std::vector<std::vector<char>> dataOut);
  int openSocket(std::string ip, short port, int timeo);
  void closeSocket();
  void nextData();

 protected:
  void assign(const MockComService& b);

 private:
  void prepareReply(std::vector<char> const& rawReply);
  // TODO: check if this is obsolete
  std::vector<std::vector<char>> data;
  std::vector<std::vector<char>> mRawFromServerList;
  std::atomic<bool> mStepCheck = false;
  uint16_t mPackId = 0;
  unsigned int mOutputCounter = 0;
  unsigned int mRequestCounter = 0;
  // the response data is generated at compile time
  static constexpr std::array<char, 256> sResponse = mock_helpers::make_response<char, 256>();
};

} // namespace readout
} // namespace bpct
#endif //PCT_BERGEN_READOUT_TEST_INCLUDE_MOCKCOMSERVICE_H_
