/// @file test_ComService.cxx
/// @brief Unit test for class @a ComService.cxx

#define BOOST_TEST_MODULE Test Bergen pCT project
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include <algorithm>
#include "readout/comService.h"
#include <boost/asio/io_service.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/read.hpp>
#include "util/logging-tools.h"

namespace bpct
{
namespace readout
{

namespace testserver
{

/// simple test server runner to be executed in a thread
template <typename Protocol>
void run(int port)
{
  boost::asio::io_service ioservice;
  auto socket = std::make_shared<typename Protocol::socket>(ioservice, typename Protocol::endpoint(Protocol::v4(), port));
  auto remote = std::make_shared<typename Protocol::endpoint>();
  auto rb = std::make_shared<std::array<char, 1024>>();

  auto handle_send = [](const boost::system::error_code& /*ec*/, std::size_t /*length*/) {
    // TODO: can check that the write op was successful
  };

  std::function<void(const boost::system::error_code&, std::size_t)> handle_receive = [socket, remote, rb, &handle_receive, &handle_send](const boost::system::error_code& ec, std::size_t length) {
    if (length == 0) {
      sleep(1);
      socket->async_receive_from(boost::asio::buffer(*rb), *remote, handle_receive);
    } else if (!ec || ec == boost::asio::error::message_size) {
      std::cout << "sending echo of length " << length << std::endl;
      socket->async_send_to(boost::asio::buffer(*rb, length), *remote, handle_send);
    }
  };

  auto schedule_listen = [socket, remote, rb, &handle_receive]() {
    socket->async_receive_from(boost::asio::buffer(*rb), *remote, handle_receive);
  };

  schedule_listen();
  ioservice.run();
}

} // namespace testserver

BOOST_AUTO_TEST_CASE(test_ComService)
{
  constexpr int port = 24052;

  // start the test server, wait a bit because there is no synchronization in UDP
  std::thread testserver(testserver::run<boost::asio::ip::udp>, port);
  sleep(1);

  ComService com;
  com.openSocket("localhost", port, 1);

  // we expect an echo server
  std::string callout = "Anybody out there?\r\n";
  std::vector<char> request(callout.length() + 1);
  std::copy(callout.data(), callout.data() + callout.length(), request.begin());
  std::vector<char> reply;

  // currently ComService only sends the first 32 bit of the buffer. This corresponds
  // to the size of pDTPClientRequest
  const size_t expectedLen = 4;
  com.transmit(request);
  size_t len = com.receive(reply);
  BOOST_CHECK_MESSAGE(len == expectedLen, "expecting reply length " << expectedLen << ", got " << len);
  BOOST_CHECK(std::string(reply.data(), len) == callout.substr(0, expectedLen));

  // TODO: implement timeout
  testserver.join();
  com.closeSocket();
}

} // namespace readout
} // namespace bpct
