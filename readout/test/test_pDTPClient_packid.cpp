/// @file test_pDTPClient_rqs_mockCom
/// @author Alf K. Herland
/// @date 15.02.2020
/// @brief Unit test for checking packet id response with the pDTPClient
/// Using the mockComService to emulate the server response

//#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include "include/mockComService.h"
#include "readout/pDTPClient.h"
#include "readout/clientconfig.h"

#define BOOST_TEST_MODULE fixture_01
namespace bpct
{
namespace readout
{
bpct::readout::ClientConfig conf;
int testCaseNum = 0;
int testCaseTot = 3;
struct F {
  MockComService* mockPtr;
  pDTPClient* client;
  F()
  { //m.start(
    conf.ipAddress = "127.0.0.1";
    conf.clientRunTime = 0;
    conf.clientUDPTimeout = 0;
    conf.spscQueueSize = 10;
    conf.fileWriterEnabled = 1;
    conf.verbosity = 1;
    conf.pollMode = 0;

    switch (testCaseNum) {
      case 0:
        conf.startUpMode = "RQR";
        conf.streamSize = 1;
        conf.packageSize = 100;
        conf.streamSize = 65535;
        conf.handleBufferStatus = 0;
        break;
      case 1:
        conf.startUpMode = "RQS";
        conf.streamSize = 1;
        conf.packageSize = 100;
        conf.streamSize = 65535;
        conf.handleBufferStatus = 0;
        break;
      case 2:
        conf.startUpMode = "RQS";
        conf.streamSize = 1;
        conf.packageSize = 100;
        conf.streamSize = 65535;
        conf.handleBufferStatus = 0;
        break;
      default:
        break;
    }

    testCaseNum++;

    mockPtr = new MockComService();
    client = new bpct::readout::pDTPClient(conf, mockPtr);
    BOOST_TEST_MESSAGE("MSG: SETUP FIXTURE");
    std::cout << "\n----------RQ New fixture---------" << std::endl;
  }

  ~F()
  {
    //std::cout << "F destructor" << std::endl;
    delete mockPtr;
    delete client;
    BOOST_TEST_MESSAGE("MSG: TEARDOWN FIXTURE");
  }
};
BOOST_FIXTURE_TEST_SUITE(s, F)

/**
 * @brief Test to see if the requested stream size matches the number of elements
 * of data is returned.
 */

BOOST_AUTO_TEST_CASE(test_packid_1)
{
  BOOST_TEST_MESSAGE("Starting test case to check if after two first packets are id 1");
  BOOST_TEST_PASSPOINT();
  std::this_thread::sleep_for(std::chrono::milliseconds(1));
  conf.startUpMode = "RQR";
  conf.streamSize = 1;
  //client->setConfigForClient(conf);
  std::thread fsmThread(&readout::pDTPClient::handleStates, client);
  std::vector<char> buffer;
  BOOST_TEST_PASSPOINT();
  fsmThread.join();
  BOOST_TEST_PASSPOINT();
  BOOST_TEST(client->getPacketIdRecved() == 1);

  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  std::cout << "Test is done " << '\n';
}
BOOST_AUTO_TEST_CASE(test_packid_65535)
{
  conf.startUpMode = "RQS";
  conf.streamSize = 1;
  BOOST_TEST_MESSAGE("Starting test case to check if packet id wraps around");
  BOOST_TEST_PASSPOINT();
  std::this_thread::sleep_for(std::chrono::milliseconds(1));
  std::thread fsmThread(&readout::pDTPClient::handleStates, client);

  std::vector<char> buffer;
  BOOST_TEST_PASSPOINT();
  fsmThread.join();
  BOOST_TEST_PASSPOINT();
  BOOST_TEST(client->getPacketIdRecved() == 65535);

  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  std::cout << "Test is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_packid_65536)
{
  BOOST_TEST_MESSAGE("Starting test case RQR request match elements out");
  BOOST_TEST_PASSPOINT();
  std::this_thread::sleep_for(std::chrono::milliseconds(1));
  std::thread fsmThread(&readout::pDTPClient::handleStates, client);

  std::vector<char> buffer;
  BOOST_TEST_PASSPOINT();
  fsmThread.join();
  BOOST_TEST_PASSPOINT();
  BOOST_TEST(client->getPacketIdRecved() == 65535);

  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  std::cout << "Test is done " << '\n';
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace readout
} // namespace bpct
