#define BOOST_TEST_MODULE Test Bergen pCT project
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/endian/conversion.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include "readout/pDTPWorker.h"
#include "datamodel/pDTP-protocol.h"
#include "util/logging-tools.h"

namespace bpct
{
namespace readout
{

using pDTPClientRequest = data_model::pDTPClientRequest;
BOOST_AUTO_TEST_CASE(test_pDTPWorker)
{
#if 0
  // this is the raw sender function provided to the pDTP worker, but the we hold the
  // communication flexible
  auto sender = [](auto request) {
    // automatic conversion to boost::asio::buffer reqires the request object
    // to support data() and size() methods
    util::hexDump ("SEND REQUEST", request.data(), request.size(), request.size());
    // TODO: implement some basic emulation
  };

  // this is the raw reader function provided to the pDTP worker
  auto reader = [](auto& target) {
    
    util::hexDump ("Creating REPLY", target.data(), target.size(), target.size());
    return target.size();
  };

  // make an instance of the worker class
  readout::pDTPWorker worker;

  // get one data set
  auto target = worker.get(sender, reader, pDTPClientRequest::ClientOpcode::CLIENT_RQR, 64);

  // handle the result
  size_t offset = sizeof(data_model::pDTPServerReply);
  if (target.size() > offset) {
    util::hexDump ("Data", target.data() + offset, target.size() - offset, target.size() - offset);
  } else {
    std::cout << "failed to get data from worker" << std::endl;
    if (target.size() == offset) {
      std::cout << *reinterpret_cast<data_model::pDTPServerReply const*>(target.data()) << std::endl;
    }
  }
#endif
}

} // namespace readout
} // namespace bpct
