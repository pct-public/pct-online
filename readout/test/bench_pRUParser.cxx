/// @file bench_pRUParser.cxx
/// @brief Benchmark test for the pRUParser

#include <benchmark/benchmark.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include <stdlib.h>
#include "readout/pRUParser.h"
#include "datamodel/pRUFormatEmulator.h"

namespace bpct
{
namespace readout
{

std::vector<char> emulateData()
{
  int nDataWords = 0;
  using pRUEmulatorDevice = data_model::pRUEmulatorDevice;
  using State = pRUEmulatorDevice::State;
  pRUEmulatorDevice::DataLengthDistribution dataLengthDistribution = [&nDataWords]() {
    // a simple alternating distribution
    return (++nDataWords % 2) + 2;
  };
  pRUEmulatorDevice::FrameSizeDistribution frameSizeDistribution = []() {
    return 5;
  };
  pRUEmulatorDevice::BurstLengthDistribution burstLengthDistribution = []() {
    return 10;
  };
  pRUEmulatorDevice emulator1(1, 2, 3, dataLengthDistribution, frameSizeDistribution);
  pRUEmulatorDevice emulator2(1, 5, 7, dataLengthDistribution, frameSizeDistribution, burstLengthDistribution);

  std::vector<char> buffer;
  int cycle = 1000;
  bool active = true;
  do {
    active = false;
    if (emulator1 != State::IDLE || cycle-- > 0) {
      buffer << emulator1;
      active = true;
    }
    if (emulator2 != State::IDLE || cycle-- > 10) {
      buffer << emulator2;
      active = true;
    }
  } while (active);

  return buffer;
}

static void BM_pRUParser(benchmark::State& state)
{
  auto data = emulateData();
  readout::pRUParser parser = readout::pRUParser();
  auto runParser = [&parser](std::vector<char>&& input) {
    auto sortedBuffer = parser.parse_pRU_data(std::move(input));
  };

  for (auto _ : state) {
    std::vector<char> input(data.begin(), data.end());
    runParser(std::move(input));
  }
}

BENCHMARK(BM_pRUParser);
} // namespace readout
} // namespace bpct

BENCHMARK_MAIN();
