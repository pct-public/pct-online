/**
 * @file test_pRUParser_input_data.cxx
 * @author Alf K. Herland
 * @date 05.06.2020.
 * @brief Test suite that loads input data from files generated from the simulation
 * folder in the wp3 repo. See the readme file in the inputData folder for more information.
*/


//#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <iostream>
#include <readout/pRUParser.h>
#include <fstream>
#include "cmakesourcepath.h" // generated to define current source dir
#define BOOST_TEST_MODULE fixture_01
namespace bpct
{
namespace readout
{
/// Global Test Counter - Is used as an index the input data
int testCaseNum = 0;

struct BeforeTest {
 public:
  readout::pRUParser parser = readout::pRUParser();

  /**
   *  This struct will run before each test in this file.
   */
  BeforeTest()
  {
    std::cout << "\n\n\n--------------------New fixture-------------------\n"
              << std::endl;
    int verbosity = 1;
    std::string path = DEFINED_CMAKE_CURRENT_SOURCE_DIR;
    ///Add or remove filenames in this vector to add or remove input data for test cases.
    std::vector<std::string> ifilenames = {path + "/inputData/testcase0_pruformat.bin",
                                           path + "/inputData/testcase1_pruformat.bin",
                                           path + "/inputData/testcase2_pruformat.bin",
                                           path + "/inputData/testcase3_pruformat.bin",
                                           path + "/inputData/testcase4_pruformat.bin",
                                           path + "/inputData/testcase5_pruformat.bin",
                                           path + "/inputData/testcase7_pruformat.bin",
                                           path + "/inputData/testcase8_pruformat.bin",
                                           path + "/inputData/testcase9_pruformat.bin",
                                           path + "/inputData/testcase10_pruformat.bin",
                                           path + "/inputData/testcase11_pruformat.bin",
                                           path + "/inputData/testcase12_pruformat.bin",
                                           path + "/inputData/testcase13_pruformat.bin",
                                           path + "/inputData/testcase14_pruformat.bin",
                                           path + "/inputData/testcase15_pruformat.bin",
                                           path + "/inputData/testcase16_pruformat.bin",
                                           path + "/inputData/testcase17_pruformat.bin",
                                           path + "/inputData/testcase18_pruformat.bin"};
    auto fileReader = [&ifilenames, verbosity]() {
      std::ifstream ifile(ifilenames.at(testCaseNum), std::ifstream::binary);
      if (!ifile.good()) {
        throw std::runtime_error("can not open file " + ifilenames.at(testCaseNum) + " for reading");
      }
      // get length of file:
      ifile.seekg(0, ifile.end);
      size_t length = ifile.tellg();
      ifile.seekg(0, ifile.beg);
      std::vector<char> buffer(length);
      ifile.read(buffer.data(), buffer.size());
      ifile.close();
      if (verbosity > 0) {
        std::cout << "Data input [File reader]: file " << ifilenames.at(testCaseNum) << " size " << buffer.size() << std::endl;
      }
      return buffer;
    };

    /// Moves the vector with test data in to the parser.
    parser.parse_pRU_data(std::move(fileReader()));


    BOOST_TEST_MESSAGE("MSG: SETUP FIXTURE");
  }
  ~BeforeTest()
  {
    /// Increase the index for the test data after a test have been completed.
    testCaseNum++;
  }
};

BOOST_FIXTURE_TEST_SUITE(s, BeforeTest)
BOOST_AUTO_TEST_CASE(test_case0)
{
  std::string testName = "Test Case 0";
  std::string discription = "# Purpose: To check alpide_data event handling, and that byte counter is correct when clustering is off\n"
    "# Test the byte counter with 100 events with incrementing size, no clustering of pixels\n"
    "# A good chunch of leading and trailing commas";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(parser.get_number_of_frames() == 1);
  BOOST_CHECK(parser.get_number_of_errors() == 0);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case1)
{
  std::string testName = "Test Case 1";
  std::string discription = "# Check that byte count is correct when clustering is on";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case2)
{
  std::string testName = "Test Case 2";
  std::string discription ="# Check back-to-back data";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case3)
{
  std::string testName = "Test Case 3";
  std::string discription = "# Check ALPIDE trailer flags";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case4)
{
  std::string testName = "Test Case 4";
  std::string discription = "# Check random idle and comma with filtering";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case5)
{
  std::string testName = "Test Case 5 ";
  std::string discription = "\nUnable to analyze datastream if idle ";
  discription += "(FF) is thrown in the mix, needs masking to handle this.\n";
  discription += "Expected output shall be 1 frame parsed with out any errors";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  if (parser.get_number_of_errors() != 0)
    parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case7)
{
  std::string testName = "Test Case 7";
  std::string discription = "# Check wrong chip id with check_id off";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case8)
{
  std::string testName = "Test Case 8";
  std::string discription ="# Random Empty frames without compression";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case9)
{
  std::string testName = "Test Case 9";
  std::string discription = "Random Empty frames with full compression. Mistakenly throws missing region header error when we have a frame consisting of only ";
  discription += "header+trailer with alpide trailer set to b8, which should be a busy violation.";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case10)
{
  std::string testName = "Test Case 10";
  std::string discription = "# Random Empty frames with some compression";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case11)
{
  std::string testName = "Test Case 11";
  std::string discription = "# Random frames (normal and empty) with random IDs with check ID on, also set values on the other fields Random Empty frames with some compression";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case12)
{
  std::string testName = "Test Case 12";
  std::string discription = "# Max Size and Max Wait Time set to zero i.e. no checking";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case13)
{
  std::string testName = "Test Case 13";
  std::string discription = "# Max Size Error";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case14)
{
  std::string testName = "Test Case 14";
  std::string discription = "# Max Wait Time Error";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case15)
{
  std::string testName = "Test Case 15";
  std::string discription = "Parser reports more pru frame errors than there are frames when ";
  discription += "busies are thrown in randomly. \nReports missing trailer errors when all";
  discription += "trailers are present.";
  discription += "Check how parser determines the end of the alpide data stream.";
  discription += "Reports alpide trailer errors, when there are none. \n";
  discription += "Expected output shall be 150 frames parsed";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case16)
{
  std::string testName = "Test Case 16";
  std::string discription = "# Empty Region";
    BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case17)
{
  std::string testName = "Test Case 17";
  BOOST_TEST_MESSAGE(testName);
  std::string discription = "# Protocol errors";
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_CASE(test_case18)
{
  std::string testName = "Test Case 18";
  std::string discription = "# Buffer Overflow";
  BOOST_TEST_MESSAGE(testName);
  BOOST_TEST_MESSAGE(discription);
  BOOST_TEST_PASSPOINT();
  BOOST_CHECK(true);
  BOOST_TEST_PASSPOINT();
  parser.print_errors();
  std::cout << testName << " is done " << '\n';
}

BOOST_AUTO_TEST_SUITE_END()
} // namespace readout
} // namespace bpct
