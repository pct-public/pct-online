/// @file bench_pDTPClient_rqs_mockCom
/// @brief Benchmark test for the pDTPClient RQS operation
/// Using the mockComService to emulate the server response

#include <benchmark/benchmark.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include <stdlib.h>
#include "include/mockComService.h"
#include "readout/pDTPClient.h"
#include "readout/clientconfig.h"
#include "datamodel/pDTP-protocol.h"
#include "datamodel/pDTP-helpers.h"

namespace bpct
{
namespace readout
{
using pDTPClientRequest = data_model::pDTPClientRequest;
using pDTPServerReply = data_model::pDTPServerReply;

void runClientbenchmark(pDTPClient& client)
{
  std::thread fsmThread(&pDTPClient::handleStates, &client);

  fsmThread.join();
  std::vector<char> buf;
  while (client.hasDataInQueue()) {
    if (client.getDataFromQueue(buf)) {
    }
    buf.clear();
  }
}

static void BM_pDTPClient_RQS(benchmark::State& state)
{
  ClientConfig conf;
  conf.ipAddress = "127.0.0.1";
  conf.clientRunTime = 0;
  conf.clientUDPTimeout = 0;
  conf.spscQueueSize = 20;
  conf.fileWriterEnabled = 1;
  conf.packageSize = 1000;
  conf.streamSize = 16;
  conf.startUpMode = "RQS";
  conf.verbosity = 0;
  conf.pollMode = 0;
  conf.handleBufferStatus = 0;
  // TODO: for the moment, the emulation of the server response in the
  // mockComService is dominating the test. Needs to be set up once in
  // the beginning.
  MockComService service;
  pDTPClient client(conf, &service);

  for (auto _ : state) {
    runClientbenchmark(client);
  }
}

BENCHMARK(BM_pDTPClient_RQS);
} // namespace readout
} // namespace bpct

BENCHMARK_MAIN();
