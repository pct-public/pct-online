/**
 * @file test_pRUParser.cxx
 * @author Øistein J. Skjolddal
 * @date 10.10.2019
 * @brief Testing for the modifications to the pRUParser
 */

#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <algorithm>  //for copy
#include <iostream>  // for cout
#include <iterator>  //for ostream_iterator
#include <vector>

#include "readout/pRUParser.h"     // Test object
#include "datamodel/pRUFormatEmulator.h"
#include "io-adaptors/Alphanumeric.h"
#include "pRUWords.h"     // Contains datasets for testing


namespace bpct {
namespace readout {

// TODO
// Test with generated input
// Test for large vallues of each datatype greater than 8 bits
// Make test set able to have changable verbosity level
// MULTIPLE_REGION_HEADER_ERROR, // TODO: Implement?


// ----------------- Test case: correct output ------------------- //
BOOST_AUTO_TEST_CASE(correctOutput) { // Test 1
  std::cout << '\n' 
    << "------------- Test 1 -------------" << '\n'
    << "Testing: Correct output." << '\n';
  // --------- Test setup --------- //
  // Suboptimal, mauall managment. Legacy dependency.
  //readout::pRUParser* parser = new readout::pRUParser();
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input = std::vector<char>(regInput.begin(), regInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(regOutput.begin(), regOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  // normal input/output test and error count check
  BOOST_CHECK(expectedResult == actualResult 
    && parser.get_number_of_errors() == 0
    && parser.get_number_of_frames() == 2);  
  parser.print_errors();
  
  // ---------------- Test teardown ---------------- //
  //parser->~pRUParser(); // This leaks!!!!!!!
  //delete parser; // Dealocate object
}

// --------------- Test case: multiple input, multiple readout -------------------//
BOOST_AUTO_TEST_CASE(multipleInputAndReadoutVectors) { // Test 2
  std::cout << '\n' 
    << "------------- Test 2 -------------" << '\n'
    << "Testing: Multiple input vectors, multiple readout." << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input0 = std::vector<char>(regInput.begin(), regInput.end());
  std::vector<char> input1 = std::vector<char>(regInput.begin(), regInput.end());

  std::vector<char> expectedResult0 = std::vector<char>(regOutput.begin(), regOutput.end());
  std::vector<char> expectedResult1 = std::vector<char>(regOutput.begin(), regOutput.end());

  // Parse data
  std::vector<char> actualResult0 = parser.parse_pRU_data(std::move(input0));
  std::vector<char> actualResult1 = parser.parse_pRU_data(std::move(input1));
  // Check result
  BOOST_CHECK(expectedResult0 == actualResult0 
    && expectedResult1 == actualResult1 
    && parser.get_number_of_errors() == 0);
  parser.print_errors();
}

// --------------- Test case: missing Trailer ------------------- //
BOOST_AUTO_TEST_CASE(missingTrailer) { // Test 3
  std::cout << '\n' 
    << "------------- Test 3 -------------" << '\n'
    << "Testing: Missing trailer." << '\n';
    //<< "Uncomplete frame gives no error" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(missingTrailerInput.begin(), missingTrailerInput.end());
  std::vector<char> expectedResult = std::vector<char>(
      missingTrailerOutput.begin(), missingTrailerOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult 
    && parser.get_number_of_errors() == 0); 
  parser.print_errors();
  
}

// ---------------- Test case: No header data test ------------------ //
BOOST_AUTO_TEST_CASE(MissingHeader) { // Test 4
  std::cout << '\n' 
    << "------------- Test 4 -------------" << '\n'
    << "Testing: Data with no header." << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(missingHeaderInput.begin(), missingHeaderInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(missingHeaderOutput.begin(), missingHeaderOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult && parser.get_number_of_errors() == 4 
    && parser.get_error_vector()[DATA_WORD_MISSING_HEADER_ERROR] == 3 
    && parser.get_error_vector()[TRAILER_WORD_MISSING_HEADER_ERROR] == 1); /* 3 data missing header error and 1 trailer missing header error */
  parser.print_errors();
}

// ----------------- Test case: double Header -------------------- //
BOOST_AUTO_TEST_CASE(doubleHeader) { // Test 5
  std::cout << '\n' 
    << "------------- Test 5 -------------" << '\n'
    << "Testing: Double header." << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(doubleHeaderInput.begin(), doubleHeaderInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(doubleHeaderOutput.begin(), doubleHeaderOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult 
    && parser.get_number_of_errors() == 1 
    && parser.get_error_vector()[MULTIPLE_PRU_HEADER_ERROR] == 1);
  parser.print_errors();
  //parser->get_error_vector();
}

// ------------ Test case: empty Frame---------------- //
BOOST_AUTO_TEST_CASE(emptyFrame) { // Test 6
  std::cout << '\n' 
    << "------------- Test 6 -------------" << '\n'
    << "Testing: Empty frame" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(emptyFrameInput.begin(), emptyFrameInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(emptyFrameOutput.begin(), emptyFrameOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult  
    && parser.get_number_of_errors() == 1 
    && parser.get_error_vector()[NO_DATA_WORD_ERROR] == 1);  
  parser.print_errors();
} 

// ------------ Test case: Frame size error (Protocoll error)---------------- //
BOOST_AUTO_TEST_CASE(frameSizeError) { // Test 7
  std::cout << '\n' 
    << "------------- Test 7 -------------" << '\n'
    << "Testing: Frame size error (Protocoll error)" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(sizeErrorInput.begin(), sizeErrorInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(sizeErrorOutput.begin(), sizeErrorOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult  
    && parser.get_number_of_errors() == 1 
    && parser.get_error_vector()[FRAME_SIZE_ERROR] == 1);  
  parser.print_errors();
} 

// --------------- Test case: pRU trailer error Flags -------------------//
// TODO: Test for several flags sett simmeltaneously
BOOST_AUTO_TEST_CASE(pRUTrailerFlagError) { // Test 8
  std::cout << '\n' 
    << "------------- Test 8 -------------" << '\n'
    << "Testing: Trailer error flags." << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  // flag 0
  std::vector<char> input0 = std::vector<char>(decodeErrorInput.begin(), decodeErrorInput.end());
  // flag 1
  std::vector<char> input1 = std::vector<char>(frameErrorInput.begin(), frameErrorInput.end());
  std::vector<char> input2 = std::vector<char>(emptyRegionInput.begin(), emptyRegionInput.end());
  std::vector<char> input3 = std::vector<char>(doubleBusyOnInput.begin(), doubleBusyOnInput.end());
  std::vector<char> input4 = std::vector<char>(doubleBusyOffInput.begin(), doubleBusyOffInput.end());
  std::vector<char> input5 = std::vector<char>(bufferOverflowInput.begin(), bufferOverflowInput.end());
  std::vector<char> input6 = std::vector<char>(maxSizeErrorInput.begin(), maxSizeErrorInput.end());
  std::vector<char> input7 = std::vector<char>(doubleBusyOnInput.begin(), doubleBusyOnInput.end());

  std::vector<char> expectedResult0 = std::vector<char>(decodeErrorOutput.begin(), decodeErrorOutput.end());
  std::vector<char> expectedResult1 = std::vector<char>(frameErrorOutput.begin(), frameErrorOutput.end());
  std::vector<char> expectedResult2 = std::vector<char>(emptyRegionOutput.begin(), emptyRegionOutput.end());
  std::vector<char> expectedResult3 = std::vector<char>(doubleBusyOnOutput.begin(), doubleBusyOnOutput.end());
  std::vector<char> expectedResult4 = std::vector<char>(doubleBusyOffOutput.begin(), doubleBusyOffOutput.end());
  std::vector<char> expectedResult5 = std::vector<char>(bufferOverflowOutput.begin(), bufferOverflowOutput.end());
  std::vector<char> expectedResult6 = std::vector<char>(maxSizeErrorOutput.begin(), maxSizeErrorOutput.end());
  std::vector<char> expectedResult7 = std::vector<char>(maxWaitTimeOutput.begin(), maxWaitTimeOutput.end());

  // Parse data
  std::vector<char> actualResult0 = parser.parse_pRU_data(std::move(input0));
  std::vector<char> actualResult1 = parser.parse_pRU_data(std::move(input1));
  std::vector<char> actualResult2 = parser.parse_pRU_data(std::move(input2));
  std::vector<char> actualResult3 = parser.parse_pRU_data(std::move(input3));
  std::vector<char> actualResult4 = parser.parse_pRU_data(std::move(input4));
  std::vector<char> actualResult5 = parser.parse_pRU_data(std::move(input5));
  std::vector<char> actualResult6 = parser.parse_pRU_data(std::move(input6));
  std::vector<char> actualResult7 = parser.parse_pRU_data(std::move(input7));
  // Check result
  BOOST_CHECK(expectedResult0 == actualResult0 && expectedResult1 == actualResult1 
  && expectedResult2 == actualResult2 && expectedResult3 == actualResult3
  && expectedResult4 == actualResult4 && expectedResult5 == actualResult5
  && expectedResult6 == actualResult6 && expectedResult7 == actualResult7
  && parser.get_number_of_errors() == 8 
  && parser.get_error_vector()[PRU_TRAILER_FLAGS_ERROR] == 8);
  parser.print_errors();
}

// ------------ Test case: missing alpide chip header (a Data loss)---------------- //
BOOST_AUTO_TEST_CASE(missingAlpideChipHeader) { // Test 9
  std::cout << '\n' 
    << "------------- Test 9 -------------" << '\n'
    << "Testing: missing alpide chip header(Data loss)" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(missingAlpideChipHeaderInput.begin(), missingAlpideChipHeaderInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(missingAlpideChipHeaderOutput.begin(), missingAlpideChipHeaderOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult
    && parser.get_number_of_errors() == 1
    && parser.get_error_vector()[MISSING_CHIP_HEADER_ERROR] == 1);  
  parser.print_errors();
} 
 
// ------------ Test case: missing alpide region header (a Data loss)---------------- //
BOOST_AUTO_TEST_CASE(missingAlpideRegionHeader) { // Test 10
  std::cout << '\n' 
    << "------------- Test 10 -------------" << '\n'
    << "Testing: missing alpide region header(Data loss)" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(missingAlpideRegionHeaderInput.begin(), missingAlpideRegionHeaderInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(missingAlpideRegionHeaderOutput.begin(), missingAlpideRegionHeaderOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult
    && parser.get_number_of_errors() == 1
    && parser.get_error_vector()[MISSING_REGION_HEADER_ERROR] == 1);  
  parser.print_errors();
}

// ------------ Test case: More than one alpide chip header (Protocoll error)---------------- //
BOOST_AUTO_TEST_CASE(multipleAlpideChipHeader) { // Test 11
  std::cout << '\n' 
    << "------------- Test 11 -------------" << '\n'
    << "Testing: multiple alpide chip headers" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(multipleAlpideChipHeaderInput.begin(), multipleAlpideChipHeaderInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(multipleAlpideChipHeaderOutput.begin(), multipleAlpideChipHeaderOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult 
    && parser.get_number_of_errors() == 1
    && parser.get_error_vector()[MULTIPLE_CHIP_HEADER_ERROR] == 1);
  parser.print_errors();
}

// ------------ Test case: missing alpide chip trailer (a Data loss)---------------- //
BOOST_AUTO_TEST_CASE(missingAlpideChipTrailer) { // Test 12
  std::cout << '\n' 
    << "------------- Test 12 -------------" << '\n'
    << "Testing: missing alpide chip trailer" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(missingAlpideChipTrailerInput.begin(), missingAlpideChipTrailerInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(missingAlpideChipTrailerOutput.begin(), missingAlpideChipTrailerOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult 
    && parser.get_number_of_errors() == 1
    && parser.get_error_vector()[MISSING_CHIP_TRAILER_ERROR] == 1);  
  parser.print_errors();
}

// ------------ Test case: missing alpide region header (a Data loss)---------------- //
BOOST_AUTO_TEST_CASE(alpideChipPaddingError) { // Test 13
  std::cout << '\n' 
    << "------------- Test 13 -------------" << '\n'
    << "Testing: alpide chip padding error" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>(alpideChipPaddingErrorInput.begin(), alpideChipPaddingErrorInput.end());
  std::vector<char> expectedResult =
      std::vector<char>(alpideChipPaddingErrorOutput.begin(), alpideChipPaddingErrorOutput.end());
  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  // Check result
  BOOST_CHECK(expectedResult == actualResult 
    && parser.get_number_of_errors() == 1
    && parser.get_error_vector()[PADDING_ERROR] == 1);  
  parser.print_errors();
}

// ------------ Test case: alpide trailer error flags ---------------- //
BOOST_AUTO_TEST_CASE(alpideTrailerFlagsError) { // Test 14
  std::cout << '\n' 
    << "------------- Test 14 -------------" << '\n'
    << "Testing: alpide trailer error flags" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input1 =
      std::vector<char>( alpideBusyTransactionInput.begin(), alpideBusyTransactionInput.end());
  std::vector<char> input2 =
      std::vector<char>(alpideStrobeExtendedInput.begin(), alpideStrobeExtendedInput.end());
  std::vector<char> input3 =
      std::vector<char>( alpideFlushedIncompleteInput.begin(), alpideFlushedIncompleteInput.end());
  std::vector<char> input4 =
      std::vector<char>( alpideBussyViolationInput.begin(), alpideBussyViolationInput.end());
  std::vector<char> input5 =
      std::vector<char>( alpideTransitionAndStrobeInput.begin(), alpideTransitionAndStrobeInput.end());

  std::vector<char> expectedResult1 =
      std::vector<char>( alpideBusyTransactionOutput.begin(), alpideBusyTransactionOutput.end());
  std::vector<char> expectedResult2 =
      std::vector<char>( alpideStrobeExtendedOutput.begin(), alpideStrobeExtendedOutput.end());
  std::vector<char> expectedResult3 =
      std::vector<char>( alpideFlushedIncompleteOutput.begin(), alpideFlushedIncompleteOutput.end());
  std::vector<char> expectedResult4 =
      std::vector<char>( alpideBussyViolationOutput.begin(), alpideBussyViolationOutput.end());      
  std::vector<char> expectedResult5 =
      std::vector<char>( alpideTransitionAndStrobeOutput.begin(), alpideTransitionAndStrobeOutput.end());

  // Parse data
  std::vector<char> actualResult1 = parser.parse_pRU_data(std::move(input1));
  std::vector<char> actualResult2 = parser.parse_pRU_data(std::move(input2));
  std::vector<char> actualResult3 = parser.parse_pRU_data(std::move(input3));
  std::vector<char> actualResult4 = parser.parse_pRU_data(std::move(input4));
  std::vector<char> actualResult5 = parser.parse_pRU_data(std::move(input5));

  // Check result
  BOOST_CHECK(expectedResult1 == actualResult1
    && expectedResult2 == actualResult2
    && expectedResult3 == actualResult3
    && expectedResult4 == actualResult4 
    && expectedResult5 == actualResult5 
    && parser.get_number_of_errors() == 2
    && parser.get_error_vector()[ALPIDE_TRAILER_FLAGS_ERROR] == 2);  
  parser.print_errors();
}

// ------------ Test case: pRU empty and delimiter words ---------------- //
BOOST_AUTO_TEST_CASE(pRUEmptyAndDelimiterWords) { // Test 15
  std::cout << '\n' << "------------- Testing: pRU empty and delimiter words -------------" << '\n';
  // --------- Test setup --------- //
  readout::pRUParser parser = readout::pRUParser();
  // Input Conversion
  std::vector<char> input =
      std::vector<char>( emptyAndDelimiterInput.begin(), emptyAndDelimiterInput.end());
  std::vector<char> expectedResult =
      std::vector<char>( emptyAndDelimiterOutput.begin(), emptyAndDelimiterOutput.end());

  // Parse data
  std::vector<char> actualResult = parser.parse_pRU_data(std::move(input));
  
  // Check result
  BOOST_CHECK(expectedResult == actualResult
    && parser.get_number_of_errors() == 0
    && parser.get_number_of_delimiter() == 1
    && parser.get_number_of_empty() == 91746);  
  parser.print_errors();
}

BOOST_AUTO_TEST_CASE(emulator)
{
  int nDataWords = 0;
  using pRUEmulatorDevice = data_model::pRUEmulatorDevice;
  using State = pRUEmulatorDevice::State;
  pRUEmulatorDevice::DataLengthDistribution dataLengthDistribution = [&nDataWords]() {
    // a simple alternating distribution
    return (++nDataWords % 2) + 2;
  };
  pRUEmulatorDevice::FrameSizeDistribution frameSizeDistribution = []() {
    return 5;
  };
  pRUEmulatorDevice::BurstLengthDistribution burstLengthDistribution = []() {
    return 10;
  };
  pRUEmulatorDevice emulator1(1, 2, 3, dataLengthDistribution, frameSizeDistribution);
  pRUEmulatorDevice emulator2(1, 5, 7, dataLengthDistribution, frameSizeDistribution, burstLengthDistribution);

  std::vector<char> buffer;
  int cycle = 100;
  bool active = true;
  do {
    active = false;
    if (emulator1 != State::IDLE || cycle-- > 0) {
      buffer << emulator1;
      active = true;
    }
    if (emulator2 != State::IDLE || cycle-- > 10) {
      buffer << emulator2;
      active = true;
    }
  } while (active);
  std::cout << io::Alphanumeric(buffer, 16);
  std::cout << "--------------------------------" << std::endl;

  readout::pRUParser parser = readout::pRUParser();
  auto sortedBuffer = parser.parse_pRU_data(std::move(buffer));
  std::cout << io::Alphanumeric(sortedBuffer, 16);
  std::cout << "--------------------------------" << std::endl;

  BOOST_CHECK_MESSAGE(buffer.size() == sortedBuffer.size(), "output size has to be identical to input size");
  BOOST_CHECK(parser.get_number_of_errors() == 0);
  if (parser.get_number_of_errors() == 0) {
    // check whether the data is sorted
    char noFrame = ~(unsigned char)0;
    char stavechip = noFrame;
    // the markers for the pRU word types
    char headerMarker = 0x41;
    char dataMarker = 0x01;
    char trailerMarker = 0x81;
    for (size_t wordno = 0; wordno < sortedBuffer.size(); wordno += 16) {
      if (stavechip == noFrame) {
        // a new frame should start here
        BOOST_CHECK_MESSAGE(sortedBuffer[wordno] == headerMarker, "header word expected at position " << wordno);
        if (sortedBuffer[wordno] == headerMarker) {
          stavechip = sortedBuffer[wordno + 1];
        } else {
          break;
        }
      } else {
        // either a data or trailer word, but all from the same chip
        BOOST_CHECK_MESSAGE(sortedBuffer[wordno] == dataMarker || sortedBuffer[wordno] == trailerMarker, "data or trailer word expected at position " << wordno << ", got 0x" << std::hex << (unsigned int)sortedBuffer[wordno]);
        BOOST_CHECK(sortedBuffer[wordno + 1] == stavechip);
        if (sortedBuffer[wordno + 1] != stavechip) {
          break;
        }
        if (sortedBuffer[wordno] == trailerMarker) {
          stavechip = noFrame;
        } else if (sortedBuffer[wordno] != trailerMarker) {
          break;
        }
      }
    }
  } else {
    parser.print_errors();
  }
}

}  // namespace readout
}  // namespace bpct

/*
 // ------- Malual checking of result: -------- //
  std::cout << '\n' << "Expected result: " << expectedResult.size();
  for (auto j = 0; j < expectedResult.size(); j++) {
    if (j % 16 == 0) std::cout << '\n';
    std::cout << std::bitset<8>(expectedResult[j]).to_string() << ' ';
  }
  std::cout << '\n';
  std::cout << '\n' << "result data: " << actualResult.size();
  for (auto k = 0; k < actualResult.size(); k++) {
    if (k % 16 == 0) std::cout << '\n';
    std::cout << std::bitset<8>(actualResult[k]).to_string() << ' ';
  }
  std::cout << '\n';
*/
