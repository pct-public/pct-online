/// @file test_pDTPClient_rqs_mockCom
/// @author Alf K. Herland
/// @date 21.10.2019
/// @brief Unit test for the pDTPClient RQS operation
/// Using the mockComService to emulate the server response

//#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include "include/mockComService.h"
#include "readout/pDTPClient.h"
#include "readout/clientconfig.h"

#define BOOST_TEST_MODULE fixture_01
namespace bpct {
namespace readout {
using pDTPClientRequest = data_model::pDTPClientRequest;
using pDTPServerReply = data_model::pDTPServerReply;
ClientConfig conf;

struct F {
  MockComService *mockPtr;
  pDTPClient *client;
  F() { //m.start(
    conf.ipAddress = "127.0.0.1";
    conf.clientRunTime = 0;
    conf.clientUDPTimeout = 0;
    conf.spscQueueSize = 10;
    conf.fileWriterEnabled = 1;
    // this is misleading, the client calculates the number of RU words from this
    // by dividing by 16. For RQR it rounds up, for RQS it truncates.
    conf.packageSize = 100;
    conf.streamSize = 3;
    conf.startUpMode = "RQS";
    conf.verbosity = 4;
    conf.pollMode = 0;
    conf.handleBufferStatus = 0;
    mockPtr = new MockComService();
    client = new bpct::readout::pDTPClient(conf, mockPtr);
    BOOST_TEST_MESSAGE("MSG: SETUP FIXTURE");
    std::cout << "\n----------RQS New fixture---------" << std::endl;
    
  }
  
  ~F() {
//std::cout << "F destructor" << std::endl;
    delete mockPtr;
    delete client;
    BOOST_TEST_MESSAGE("MSG: TEARDOWN FIXTURE");
  }
  
};
BOOST_FIXTURE_TEST_SUITE(s, F
)

/**
 * @brief Test to see if the requested stream size matches the number of elements
 * of data is returned.
 */

BOOST_AUTO_TEST_CASE(test_case_rqs_num_elements) {
  BOOST_TEST_MESSAGE("Starting test case RQS request match elements out");
  BOOST_TEST_PASSPOINT();
  std::this_thread::sleep_for(std::chrono::milliseconds(1));
  std::thread fsmThread(&readout::pDTPClient::handleStates, client);
  
  std::vector<char> buffer;
  BOOST_TEST_PASSPOINT();
  fsmThread.join();
  BOOST_TEST_PASSPOINT();
  // the client is executing an additional RQR after the actual operation
  // thus we expect conf.streamSize+1 packets
  // not sure why this needs to be there. Of course there can be more data
  // in the FIFO but since the client was configured to read 3 stream packets
  // it should do this and nothing else
  BOOST_REQUIRE(client->numberOfElementsInQueue() == conf.streamSize + 1);
  int count = 0;
  while ( client->hasDataInQueue()) {
    std::vector<char> buf = {'0'};
    if (client->getDataFromQueue(buf) && buf.size() > 0) {
      /*
      for (int i = 0; i < buf.size(); ++i) {
        std::bitset<8> b1(buf.at(i));
        std::cout << std::hex << (b1.to_ulong()) << " ";
      }
      std::cout << '\n';
      */
      // MockComService simply fills the data buffer with incremented numbers
      BOOST_CHECK(buf[0] == 0);
      BOOST_CHECK((unsigned char)buf[buf.size() - 1] == (buf.size() - 1) % 256);
      if (count < conf.streamSize) {
        // check size of the actual stream packages, the client calculates the
        // number of RU words by deviding by 16 and simply truncates, so the
        // size specified in the configuration is misleading
        BOOST_CHECK((int)buf.size() == (conf.packageSize / 16) * 16);
      } else {
        // this is the extra RQR and here the packageSize is calculated using
        // ceil, so one more package
        BOOST_CHECK((int)buf.size() == ((conf.packageSize + 15) / 16) * 16);
      }
      std::cout << "Buffer size: " << std::dec << buf.size() << std::endl;
    }
    count++;
    buf = {'0'};
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  std::cout << "Test is done "<<'\n';

}

BOOST_AUTO_TEST_SUITE_END()

} // namespace readout
} // namespace bpct
