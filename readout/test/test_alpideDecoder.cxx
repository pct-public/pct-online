/**
 * @file test_pRUParser.cxx
 * @author Øistein J. Skjolddal
 * @date 10.10.2019
 * @brief Testing for the modifications to the pRUParser
 */

#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <algorithm>  //for copy
#include <iostream>  // for cout
#include <iterator>  //for ostream_iterator
#include <vector>

#include "readout/alpideDecoder.h"     // Test object
#include "pRUWords.h"     // Contains datasets for testing
#include "datamodel/AlpideHit.h"
#include "datamodel/RUDataHeader.h"


namespace bpct {
namespace readout {

// ----------------- Test case: correct output ------------------- //
BOOST_AUTO_TEST_CASE(correctOutput) { // Test 1
  std::cout << '\n' 
    << "------------- Test 1 -------------" << '\n'
    << "Testing: Correct output." << '\n';
  // --------- Test setup --------- //
  readout::alpideDecoder decoder = readout::alpideDecoder();
  // Input Conversion
  std::vector<char> input = std::vector<char>(regOutput.begin(), regOutput.end());
  //std::vector<char> input = std::vector<char>(testCasesOutput.begin(), testCasesOutput.end());
  std::vector<char> input2 = std::vector<char>(regOutput.begin(), regOutput.end());

  //data_model::RUDataHeader dataHeader1;
  //std::vector<data_model::AlpideHit> hitlist1;
  // std::vector<std::pair<data_model::RUDataHeader, std::vector<data_model::AlpideHit>>> expectedResult =
    
  // decode data
  //std::vector<std::pair<data_model::RUDataHeader, std::vector<data_model::AlpideHit>>> actualResult 
  //std::vector<data_model::RUDataHeader> actualResult = decoder.convert(std::move(input));
  decoder.convert(std::move(input));
  // Check result
  // normal input/output test and error count check
  // BOOST_CHECK(expectedResult == actualResult);
  BOOST_CHECK(true);
  // ------- Malual checking of result: -------- //
  //std::cout << '\n';
  //std::cout << '\n' << "result data: " << actualResult.size() << '\n';
  //for (auto k = 0; k < actualResult.size(); k++) {
  //  std::cout << "frame id: " << actualResult[k].first.frameid << '\n';
  //  std::cout << "readout unit: " << actualResult[k].first.layer << '\n';
  //  std::cout << "number of hits: " << actualResult[k].first.nofHits << '\n';
  //}
  //decoder.inspect(std::move(input2));
  //std::cout << '\n';
}


}  // namespace readout
}  // namespace bpct

/*
 // ------- Malual checking of result: -------- //
  std::cout << '\n' << "Expected result: " << expectedResult.size();
  for (auto j = 0; j < expectedResult.size(); j++) {
    if (j % 16 == 0) std::cout << '\n';
    std::cout << std::bitset<8>(expectedResult[j]).to_string() << ' ';
  }
  std::cout << '\n';
  std::cout << '\n' << "result data: " << actualResult.size();
  for (auto k = 0; k < actualResult.size(); k++) {
    if (k % 16 == 0) std::cout << '\n';
    std::cout << std::bitset<8>(actualResult[k]).to_string() << ' ';
  }
  std::cout << '\n';
*/
