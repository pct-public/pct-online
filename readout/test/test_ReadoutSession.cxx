#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/endian/conversion.hpp>
#include <iostream>
#include <iomanip>
#include <cstdint>    // uint8_t
#include <functional> // std::function
#include <vector>
#include <mutex>
#include <condition_variable>
#include <unistd.h> // usleep
#include "readout/ReadoutSession.h"

namespace bpct
{
namespace readout
{

BOOST_AUTO_TEST_CASE(test_ReadoutSession)
{
  using byte = uint64_t;
  using BufferT = std::vector<byte>;
  using Server = ReadoutSession<std::function<BufferT()>, std::function<BufferT(BufferT &&)>, std::function<void(BufferT &&)>>;

  const unsigned int nLoops = 16;
  unsigned int roll = 0;
  std::mutex mutex;
  std::condition_variable terminationCondition;

  // the input policy, publish a vector of 64bit numbers with one entry
  auto inputHandler = [nLoops, &roll, &terminationCondition, &mutex]() -> BufferT {
    std::cout << "publishing roll " << std::dec << roll << std::endl;
    usleep(250000);
    if (roll < nLoops) {
      BufferT buffer;
      buffer.emplace_back(++roll);
      return buffer;
    }
    return BufferT();
  };

  // revert endianess using the boost endian library
  auto forwardReverse = [](BufferT&& buffer) {
    for (auto& val : buffer) {
      val = boost::endian::endian_reverse(val);
    }
    return std::move(buffer);
  };

  // the output policy, check the value of the received data
  auto outputHandler = [nLoops, &roll, &terminationCondition, &mutex](BufferT&& buffer) {
    if (buffer.size() == 0) {
      return;
    }
    std::cout << "got data: size " << buffer.size() << "   value 0x" << std::hex << buffer[0] << std::endl;
    BOOST_CHECK((buffer[0] >> 56) == roll);
    if (roll >= nLoops) {
      std::cout << "done" << std::endl;
      // tell the main thread that we terminate
      terminationCondition.notify_all();
    }
  };

  Server server(inputHandler, forwardReverse, outputHandler);
  server.control(Server::Command::START);
  std::unique_lock<std::mutex> lock(mutex);
  terminationCondition.wait(lock);
  server.control(Server::Command::STOP);
}

} // namespace readout
} // namespace bpct
