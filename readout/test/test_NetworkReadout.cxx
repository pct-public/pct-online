#define BOOST_TEST_MODULE Test Bergen pCT readout
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/endian/conversion.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <unistd.h> // usleep
#include "readout/NetworkReadout.h"
#include "util/logging-tools.h"

namespace bpct
{
namespace readout
{

BOOST_AUTO_TEST_CASE(test_NetworkReadout)
{
  using Protocol = boost::asio::ip::tcp;
  NetworkReadout<std::vector<uint8_t>, Protocol> readout(std::vector<uint8_t>(16));
  std::mutex mtx;
  std::condition_variable ready;
  bool readyToQuit = false;
  auto worker = [&readout, &ready, &readyToQuit]() {
    readout.start(29070);
    // TODO: have to implement some smarter control
    int count = 0;
    while (count < 5 && !readyToQuit) {
      // TODO: think about asynchronous implementation and implement at least the timeout
      auto buffer = readout();
      if (buffer.size() > 0) {
        bpct::util::hexDump("DATA: ", buffer.data(), buffer.size(), buffer.size());
        count++;
      }
    }
    ready.notify_all();
  };

  std::thread workerThread(worker);
  std::unique_lock<decltype(mtx)> guard(mtx);
  if (ready.wait_for(guard, std::chrono::seconds(20)) == std::cv_status::timeout) {
    // no special handling so far
  }
  readyToQuit = true;
  readout.stop();
  std::cout << "shutting down network readout" << std::endl;
  workerThread.join();
}

} // namespace readout
} // namespace bpct
