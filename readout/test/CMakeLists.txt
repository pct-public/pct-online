# cmake setup for unit tests of module 'readout'

set(TEST_SRCS
        test_ReadoutSession.cxx
        test_NetworkReadout.cxx
        test_pDTPWorker.cxx
        test_pRUParser.cxx
        test_pDTPClient_rqr_mockCom.cpp
        test_pDTPClient_rqs_mockCom.cpp
        test_pDTPClient_packid.cpp
        test_pRUParser_input_data.cxx
        test_ComService.cxx
        )

set(MOCK_SRCS
        MockComService.cpp
        )

add_library(mocks ${MOCK_SRCS})
target_include_directories(mocks PRIVATE ${Boost_INCLUDE_DIRS})

# configure the include file containing CMAKE_CURRENT_SOURCE_DIR as
# define DEFINED_CMAKE_CURRENT_SOURCE_DIR
configure_file(include/cmakesourcepath.h.in cmakesourcepath.h @ONLY)

foreach (test ${TEST_SRCS})
    string(REGEX REPLACE ".*/" "" test_name ${test})
    string(REGEX REPLACE "\\..*" "" test_name ${test_name})
    add_executable(${test_name} ${test})
    target_include_directories(${test_name}
            PUBLIC
            $<INSTALL_INTERFACE:include>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../include>
            PRIVATE
            pctUtil
	    ${CMAKE_CURRENT_BINARY_DIR}
            )
    target_link_libraries(${test_name} Boost::unit_test_framework pthread pctReadout pctIO mocks)
    add_test(NAME ${test_name} COMMAND ${test_name})
endforeach ()

set(BENCH_SRCS
    bench_pDTPClient_rqs_mockCom.cpp
    bench_pRUParser.cxx
    )

if (benchmark_FOUND)
foreach (bench ${BENCH_SRCS})
    string(REGEX REPLACE ".*/" "" bench_name ${bench})
    string(REGEX REPLACE "\\..*" "" bench_name ${bench_name})
    add_executable(${bench_name} ${bench})
    target_include_directories(${bench_name}
            PUBLIC
            $<INSTALL_INTERFACE:include>
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../include>
            PRIVATE
            pctUtil
            )
    target_link_libraries(${bench_name} pthread pctReadout mocks benchmark::benchmark)
    add_test(NAME ${bench_name} COMMAND ${bench_name})
endforeach ()
endif (benchmark_FOUND)
