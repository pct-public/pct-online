/**
 * @file ${FILE}
 * @author Alf K. Herland 
 * @date 21.10.2019.
 * @brief
*/
#include <iostream>
#include <chrono>
#include <thread>
#include <boost/endian/conversion.hpp>
#include "../../datamodel/include/datamodel/pDTP-protocol.h"
#include "include/mockComService.h"
#include <bitset>

namespace bpct
{
namespace readout
{
using pDTPClientRequest = data_model::pDTPClientRequest;
using pDTPServerReply = data_model::pDTPServerReply;
using ClientOpcode = data_model::pDTPClientRequest::ClientOpcode;
using ServerOpcode = data_model::pDTPServerReply::ServerOpcode;
int MockComService::transmit(std::vector<char>& cmd)
{
  mStepCheck = false;
  prepareReply(cmd);
  return 1;
}

int MockComService::receive(std::vector<char>& buf)
{
  //std::cout << "------------------Got mock data: "
  //        << mRequestCounter << " of " << mRawFromServerList.size() << "--------\n";

  //if (mOutputCounter < data.size()) {
  //buf = data.at(mOutputCounter);
  //} else {
  //return -1; //Error
  //}
  //mOutputCounter++;
  //int iter = 0;
  /*
  while (!mStepCheck) {
    std::cout << "waiting for input\n";
    std::this_thread::sleep_for(std::chrono::microseconds(100));
    iter++;
    if(iter > 1000){
      return -2;
    }
  }
   */

  //copy(mRawFromServerList.at(mOutputCounter-1).begin(), mRawFromServerList.at(mOutputCounter-1).end(), back_inserter(buf));
  if (mRequestCounter < mRawFromServerList.size()) {
    buf = std::move(mRawFromServerList.at(mRequestCounter++));
    return buf.size();
  }
  std::cout << "<><><><><<><><><><>ERROR\n";
  return -1;
}

void MockComService::setDataOutput(std::vector<std::vector<char>> dataOut)
{
  data = dataOut;
}

int MockComService::openSocket(std::string, short , int )
{
  std::cout << "mock OpenSocket called \n";
  return 1;
}

void MockComService::closeSocket()
{
}

void MockComService::nextData()
{
  if (mStepCheck) {
    mStepCheck = false;
  } else {
    mStepCheck = true;
  }
}

void MockComService::prepareReply(std::vector<char> const& rawReply)
{
  pDTPClientRequest const& toServer = *reinterpret_cast<pDTPClientRequest const*>(rawReply.data());
  auto opcode = static_cast<ClientOpcode>(toServer.opcode);
  const size_t sizeOfReturnPackage = toServer.sizeofpacket * 16;
  std::vector<char> rawFromServer;
  auto copyReply = [&rawFromServer, &sizeOfReturnPackage]() {
    size_t bytesCopied = 0;
    do {
      size_t copy = bytesCopied + sResponse.size() < sizeOfReturnPackage ? sResponse.size() : sizeOfReturnPackage - bytesCopied;
      memcpy(rawFromServer.data() + sizeof(pDTPServerReply) + bytesCopied, sResponse.data(), copy);
      bytesCopied += copy;
    } while (bytesCopied < sizeOfReturnPackage);
  };
  switch (opcode) {
    case ClientOpcode::CLIENT_RQR: {
      rawFromServer.resize(sizeof(pDTPServerReply) + sizeOfReturnPackage);
      pDTPServerReply& fromServer = *reinterpret_cast<pDTPServerReply*>(rawFromServer.data());
      ///Prepare reply for RQR
      ///Generate random data for requested package
      fromServer.empty = 1;
      fromServer.opcode = static_cast<uint32_t>(ServerOpcode::SERVER_WRITE);
      fromServer.packetid = boost::endian::endian_reverse(uint16_t(mPackId));
      fromServer.size = toServer.sizeofpacket;
      mPackId++;
      copyReply();
      mRawFromServerList.emplace_back(std::move(rawFromServer));
    } break;
    case ClientOpcode::CLIENT_RQS:
      std::cout << "Size req: " << boost::endian::endian_reverse(uint16_t(toServer.nofpackets)) << '\n';
      for (int i = 0; i < boost::endian::endian_reverse(uint16_t(toServer.nofpackets)); i++) {
        {
          rawFromServer.resize(sizeof(pDTPServerReply) + sizeOfReturnPackage);
          pDTPServerReply& fromServer = *reinterpret_cast<pDTPServerReply*>(rawFromServer.data());
          fromServer.opcode = static_cast<uint32_t>(ServerOpcode::SERVER_STREAM);
          //std::cout << "opcode: " << fromServer.opcode << '\n';
          fromServer.packetid = boost::endian::endian_reverse(uint16_t(mPackId));
          fromServer.size = toServer.sizeofpacket;
          mPackId++;
          //std::bitset<32> x(fromServer.raw);//(boost::endian::endian_reverse((fromServer.raw)));
          //std::cout << x << '\n';
          //fromServer.raw = boost::endian::endian_reverse(fromServer.raw);
          //std::cout << "Comservice size of data: " << rawFromServer.size() << '\n';
          copyReply();
          mRawFromServerList.emplace_back(std::move(rawFromServer));
          /*
          std::bitset<8> b1(rawFromServer.at(0));
          std::bitset<8> b2(rawFromServer.at(1));
          std::bitset<8> b3(rawFromServer.at(2));
          std::bitset<8> b4(rawFromServer.at(3));
          std::bitset<8> b5(rawFromServer.at(4));
          std::cout << "bitset: " << b1 << " " << b2 << " " << b3 << " " << b4 << " " << b5 << '\n';
           */
        }
        //std::cout << "Size list of packages: " << mRawFromServerList.size()<< '\n';
      }
      {
        rawFromServer.resize(sizeof(pDTPServerReply));
        pDTPServerReply& fromServer = *reinterpret_cast<pDTPServerReply*>(rawFromServer.data());
        fromServer.opcode = static_cast<uint32_t>(ServerOpcode::SERVER_EOS);
        mRawFromServerList.emplace_back(std::move(rawFromServer));
        //std::cout << mOutputCounter << " " << mRawFromServerList.size() << '\n';
      }
      break;
    case ClientOpcode::CLIENT_RQFS:
      break;
    case ClientOpcode::CLIENT_ABRT: //fromServer.raw = 0;
      //mOutputCounter--;
      break;
    default:
      std::cout << "Error in test request\n";
  }
}

} // namespace readout
} // namespace bpct
