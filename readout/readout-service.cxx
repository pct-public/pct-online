// runFairMQDevice.h contains all the boilerplate code including the main function
// only the two functions addCustomOptions and getDevice need to be defined.
#include "runFairMQDevice.h"
#include "readout/ReadoutService.h"

namespace bpo = boost::program_options;

void addCustomOptions(bpo::options_description& /*options*/)
{
  // the custom options defined in this function are accessible during Init
}

FairMQDevicePtr getDevice(const FairMQProgOptions& /*config*/)
{
  return new bpct::readout::ReadoutService;
}
