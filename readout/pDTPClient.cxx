/// @file pDTPClient.cxx
/// @brief The worker class for the pDTP protocol readout client
/// FIXME: as this is more the worker class and state machine for the protocol
/// we should probably rename it

#include "readout/pDTPClient.h"
#include <boost/endian/conversion.hpp>
#include <thread>
#include <bitset>
#include <cassert>

namespace bpct
{
namespace readout
{
namespace bpo = boost::program_options;
using ClientOpcode = data_model::pDTPClientRequest::ClientOpcode;

pDTPClient::pDTPClient(ClientConfig confp, IComService* coms)
  : com(coms), conf(confp)

{
  com->openSocket(conf.ipAddress, conf.portNumber, conf.clientUDPTimeout);
  setOpCode(conf.startUpMode);
  startTime = std::chrono::system_clock::now();
}

void pDTPClient::handleStates()
{
  if (conf.verbosity > 0) {
    std::cout << "Starting client thread\n";
  }
  isClientRunning = true;
  while (!exitProgram) {
    sendOpCodeToReadOut();
    if (currentOpCode != ClientOpcode::CLIENT_ABRT) {
      receiveDataFromReadOut();
    }
    if (conf.requestNumberOfWords > 0 &&
        conf.requestNumberOfWords <= (stats.numberOfBytesReceived * 0.0625)) {
      exitProgram = true;
      conf.streamSize = 1;
    }
    if (conf.pollMode == 0) {
      exitProgram = true;
      if (conf.verbosity > 1) {
        std::cout << "Poll mode is: " << conf.pollMode << " stopping\n";
      }
    } else {
      /// timeout set opcode back.
      setOpCode(conf.startUpMode); /// In case of a abrt due to error or
    }
  }
  if (conf.verbosity > 0) {
    std::cout << "Stopping client thread\n";
    std::cout << "Missing packet errors: " << stats.missingPacketErrors << "\n";
  }

  /// If the current mode is RQS or RQFS we need to send a abort signal to the server.
  if (currentOpCode == ClientOpcode::CLIENT_RQS || currentOpCode == ClientOpcode::CLIENT_RQFS) {
    currentOpCode = ClientOpcode::CLIENT_ABRT;
    sendOpCodeToReadOut();
    if (!gotEOS) {
      receiveDataFromReadOut(); /// Get EOS packet.
    }
  }
  isClientRunning = false;
  /// Always send a RQR to make sure the fifo buffer on the RU is empty before
  /// stopping the client.
  currentOpCode = ClientOpcode::CLIENT_RQR;
  sendOpCodeToReadOut();
  receiveDataFromReadOut();
  com->closeSocket();
  stopTimePoint.now();
}

void pDTPClient::setOpCode(std::string opCode)
{
  if (conf.verbosity > 1) {
    std::cout << "Setting opCode: " << opCode << "\n";
  }
  conf.startUpMode.shrink_to_fit();
  std::string rqr = "RQR";
  if (conf.startUpMode.compare(0, 3, "RQT", 3) == 0) {
    if (conf.verbosity > 1) {
      std::cout << "RQT";
    }
    currentOpCode = ClientOpcode::CLIENT_RQT;
  } else if (conf.startUpMode.compare(0, 3, "RQR", 3) == 0) {
    if (conf.verbosity > 1) {
      std::cout << "RQR";
    }
    currentOpCode = ClientOpcode::CLIENT_RQR;
  } else if (conf.startUpMode.compare(0, 3, "RQS", 3) == 0) {
    if (conf.verbosity > 1) {
      std::cout << "RQS";
    }
    currentOpCode = ClientOpcode::CLIENT_RQS;
  } else if (conf.startUpMode.compare(0, 4, "RQFS", 4) == 0) {
    if (conf.verbosity > 1) {
      std::cout << "RQFS";
    }
    currentOpCode = ClientOpcode::CLIENT_RQFS;
  } else if (conf.startUpMode.compare(0, 2, "GS", 2) == 0) {
    if (conf.verbosity > 1) {
      std::cout << "GS";
    }
    currentOpCode = ClientOpcode::CLIENT_GS;
  } else if (conf.startUpMode.compare(0, 2, "ABRT", 2) == 0) {
    std::cout << "ABRT";
    currentOpCode = ClientOpcode::CLIENT_ABRT;
  } else {
    std::cout << "Error setting opcode \n";
  }
  if (conf.verbosity > 1) {
    std::cout << '\n';
  }
}

size_t pDTPClient::getStreamSize(uint16_t nofpackets)
{
  int nopackets = 0;
  char temp[2] = {0, 0};
  temp[0] = (nofpackets >> 8);
  temp[1] = ((nofpackets << 4) >> 4);
  std::memcpy(&nopackets, temp, sizeof(temp));
  return nopackets;
}

void pDTPClient::sendOpCodeToReadOut()
{
  std::vector<char> rawreq(sizeof(pDTPClientRequest));
  pDTPClientRequest& request =
    *reinterpret_cast<pDTPClientRequest*>(rawreq.data());
  auto opcode = static_cast<ClientOpcode>(currentOpCode);
  uint8_t sizeForPackage = ceil(float(conf.packageSize) / 16);
  switch (opcode) {
    case ClientOpcode::CLIENT_RQS:
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_RQS);
      gotEOS = false; ///Reset EOS flag
      request.sizeofpacket =
        boost::endian::endian_reverse(uint8_t(conf.packageSize / 16));
      request.nofpackets = getStreamSize(conf.streamSize);
      request.min_req = 1;
      request.no_wait = conf.noWaitFlag;
      request.maxi = conf.maximizeFlag;
      if (conf.verbosity > 1) {
        std::cout << request;
      }
      com->transmit(rawreq);
      rawreq.clear();
      break;
    case ClientOpcode::CLIENT_RQT: ///
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_RQT);
      request.no_ack = 1;
      request.sizeofpacket = sizeForPackage;
      if (conf.verbosity > 1) {
        std::cout << request;
      }
      com->transmit(rawreq);
      rawreq.clear();
      break;
    case ClientOpcode::CLIENT_RQR: ///
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_RQR);
      request.no_ack = 1;
      request.sizeofpacket = sizeForPackage;
      request.nofpackets = 0x0000;
      request.no_wait = conf.noWaitFlag;
      request.maxi = conf.maximizeFlag;
      if (conf.verbosity > 1) {
        std::cout << request;
      }
      com->transmit(rawreq);
      rawreq.clear();
      break;

    case ClientOpcode::CLIENT_RQFS:
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_RQFS);
      gotEOS = false; ///Reset EOS flag
      request.sizeofpacket = conf.packageSize / 16;
      request.min_req = 1;
      request.no_wait = conf.noWaitFlag;
      request.maxi = conf.maximizeFlag;
      if (conf.verbosity > 1) {
        std::cout << request;
      }
      com->transmit(rawreq);
      rawreq.clear();
      break;
    case ClientOpcode::CLIENT_ABRT:
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_ABRT);
      if (conf.verbosity > 1) {
        std::cout << request;
      }
      com->transmit(rawreq);
      rawreq.clear();
      exitProgram = true;
      break;
    case ClientOpcode::CLIENT_GS:
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_GS);
      if (conf.verbosity > 1) {
        std::cout << request;
      }
      com->transmit(rawreq);
      rawreq.clear();
      break;
    default:
      std::cout << "ERROR in send request state \n";
      break;
  }
}
void pDTPClient::receiveDataFromReadOut()
{
  std::vector<char> buf = {0};
  auto nrecv = com->receive(buf);
  if (conf.verbosity > 1) {
    std::bitset<8> b1(buf.at(0));
    std::cout << "pDTPClient package size: " << nrecv << "\n";
  }
  bool streaming = true;
  if (buf.size() >= sizeof(pDTPServerReply)) {
    pDTPServerReply& reply = *reinterpret_cast<pDTPServerReply*>(buf.data());
    setBufferStatus(reply);
    auto opcode = static_cast<ServerOpcode>(reply.opcode);
    switch (opcode) {
      case ServerOpcode::SERVER_ERROR:

        if (conf.verbosity > 1) {
          std::cout << reply << "\n Buffer fill; " << reply.fill_count << '\n';
        }

        if (reply.empty == 1) {
          if (conf.pollMode == 0) {
            currentOpCode = ClientOpcode::CLIENT_RQR;
          }
          stats.numberOfPacketsWithData++;
        } else {
          stats.numberOfErrors++;
        }

        break;
      case ServerOpcode::SERVER_WRITE: //
        stats.numberOfPacketsRecv++;
        if (conf.verbosity > 1) {
          std::cout << reply << "\nPacket ID: " << boost::endian::endian_reverse(uint16_t(reply.packetid));
          std::cout << " Data size: " << nrecv << '\n';
        }
        if (conf.pollMode == 1 || bufferStatus == 'A' || bufferStatus == 'F') {
          currentOpCode = ClientOpcode::CLIENT_RQR;
        }
        // TODO: the following lines can be implemented in a lambda to avoid repeated code lines
        // in the other branches
        // we have checked already the buffer size to be at least the size of the pDTPHeader, so this is
        // obsolete, the check for the receive result can be moved up to handle error conditions centrally,
        // similar places further down
        if (nrecv > 8) {
          if (!packetIdCheck(boost::endian::endian_reverse(uint16_t(reply.packetid))) && conf.verbosity > 0) {
            std::cout << "\n\nid=" << boost::endian::endian_reverse(uint16_t(reply.packetid)) << " Packets out of sequence \n\n";
          }
          stats.numberOfBytesReceived += nrecv - sizeof(pDTPServerReply);
          stats.numberOfPacketsWithData++;
          spscQueue.push(std::move(buf));
          stats.dataInQueue++;
        }
        /// Handle the buffer fill level by switching to RQS
        if (conf.handleBufferStatus == 1) {
          if (reply.almost_full == 0x1 || reply.full == 0x1) {
            if (conf.verbosity > 1) {
              std::cout << "--switching to RQS\n";
            }
            currentOpCode = ClientOpcode::CLIENT_RQS;
          }
          break;
        }
        /// Handle the buffer fill level by switching to RQS or RQFS
        if (conf.handleBufferStatus == 2) {
          if (reply.almost_full && !reply.full) {
            if (conf.verbosity > 1) {
              std::cout << "in full - switching to RQS\n";
            }
            currentOpCode = ClientOpcode::CLIENT_RQS;
            break;
          }
          if (reply.full) {
            if (conf.verbosity > 1) {
              std::cout << "switching to RQFS\n";
            }
            currentOpCode = ClientOpcode::CLIENT_RQFS;

            break;
          }
        }
        break;
      case ServerOpcode::SERVER_STREAM:
        if (conf.verbosity > 1) {
          std::cout << reply;
          std::cout << "Data size in stream package: " << nrecv << '\n';
        }
        // see comment above on error condition checking
        if (nrecv > 8) {
          stats.numberOfBytesReceived += nrecv - sizeof(pDTPServerReply);
          stats.numberOfPacketsWithData++;
          spscQueue.push(std::move(buf));
          stats.dataInQueue++;
        } else if (nrecv == -2) { /// Check if there is a timeout, send abrt
          /// and try again
          currentOpCode = ClientOpcode::CLIENT_ABRT;
        }
        if (!packetIdCheck(boost::endian::endian_reverse(uint16_t(reply.packetid))) && conf.verbosity > 0) {
          std::cout << "\n\nid=" << boost::endian::endian_reverse(uint16_t(reply.packetid)) << " Packets out of sequence \n\n";
        }
        streaming = true;
        while (streaming) {
          auto nrecv2 = com->receive(buf);
          if (nrecv2 ==
              -2) { /// Check if there is a timeout, send abrt and try again
            streaming = false;
            currentOpCode = ClientOpcode::CLIENT_ABRT;
          }
          if (buf.size() >= sizeof(pDTPServerReply)) {
            pDTPServerReply& reply = *reinterpret_cast<pDTPServerReply*>(buf.data());
            if (conf.verbosity > 1) {
              std::cout << reply;
              std::cout << "Data size: " << nrecv2 << '\n';
            }
            auto opcodeS = static_cast<ServerOpcode>(reply.opcode);
            // should be covered by the buffer size check above
            if (nrecv2 > 8) { //Check that the pDTP package contains data.
              stats.numberOfPacketsRecv++;
              stats.numberOfBytesReceived += nrecv2 - sizeof(pDTPServerReply);
              stats.numberOfPacketsWithData++;
              spscQueue.push(std::move(buf));
              stats.dataInQueue++;
              if (!packetIdCheck(boost::endian::endian_reverse(uint16_t(reply.packetid))) && conf.verbosity > 0) {
                std::cout << "\n\nid=" << boost::endian::endian_reverse(uint16_t(reply.packetid)) << " Packets out of sequence \n\n";
              }
            }
            if (opcodeS == ServerOpcode::SERVER_EOS) {
              gotEOS = true;
              if (conf.pollMode == 0) {
                if (conf.verbosity > 1) {
                  std::cout << "Got EOS and poll mode is 0, stopping client \n";
                }
                this->stopClient();
                currentOpCode = ClientOpcode::CLIENT_RQR;
              }
              streaming = false;
            }
          }
        }

        break;
      case ServerOpcode::SERVER_EOS:
        gotEOS = true;
        if (conf.verbosity > 0) {
          std::cout << reply;
        }
        if (conf.pollMode == 1) {
          setOpCode(conf.startUpMode);
          if (conf.verbosity > 0) {
            std::cout << "polling again";
          }
          break;
        } else {
          std::cout << "Got EOS \n";
        }
        break;
      case ServerOpcode::SERVER_STATUS:
        std::cout << reply << std::endl;
        break;
      default:
        break;
    }
  } else {
    // FIXME: error handling, also other places where data is pushed to the queue
  }
}

void pDTPClient::stopClient() { exitProgram = true; }

bool pDTPClient::isRunning() { return isClientRunning; }

size_t pDTPClient::getNumberOfBytesReceived()
{
  return stats.numberOfBytesReceived;
}

void pDTPClient::setBufferStatus(pDTPServerReply& reply)
{
  if (reply.empty == 1) {
    bufferStatus = 'E';
    if (conf.verbosity > 1) {
      std::cout << "Buffer status " << bufferStatus << '\n';
    }
  }
  if (reply.empty == 0) {
    bufferStatus = 'N';

    if (conf.verbosity > 1) {
      std::cout << "Buffer status " << bufferStatus << '\n';
    }
  }
  if (reply.almost_full == 1) {
    bufferStatus = 'A';

    if (conf.verbosity > 1) {
      std::cout << "Buffer status " << bufferStatus << '\n';
    }
  }
  if (reply.full == 1) {
    bufferStatus = 'F';

    if (conf.verbosity > 1) {
      std::cout << "Buffer status " << bufferStatus << '\n';
    }
  }
}

auto pDTPClient::getBufferStatus() { return bufferStatus; }

ClientOpcode pDTPClient::getCurrentMode() { return currentOpCode; }

bool pDTPClient::getDataFromQueue(std::vector<char>& buf)
{
  // TODO: we want to get the data by move, but boost::lockfree::spsc_queue
  // does not seem to support movable objects at pop, needs to be checked
  bool retVal = spscQueue.pop(buf);
  if (retVal == false) {
    // no data in queue
    // also the couter should be 0 in this case
    assert(stats.dataInQueue == 0);
    return false;
  }
  // strip the 8 bytes of the pDTP header
  constexpr size_t offset = sizeof(pDTPServerReply);
  // presence of the pDTPHeader has been checked before adding data to queue
  assert(buf.size() >= offset);
  std::move(buf.cbegin() + offset, buf.cend(), buf.begin());
  buf.resize(buf.size() - offset);
  // FIXME: the queue is thread-safe but not the operation on the counters
  stats.dataInQueue--;
  if (conf.verbosity > 3) {
    std::cout << "Data left in queue: " << stats.dataInQueue << '\n';
  }
  return retVal;
}

size_t pDTPClient::getUpTime()
{
  auto stop = std::chrono::time_point_cast<std::chrono::milliseconds>(
    nowTimePoint.now());

  auto duration = stop - startTime;

  return duration.count() * 0.001 * 0.001;
}

bool pDTPClient::hasDataInQueue() { return stats.dataInQueue > 0; }

int pDTPClient::numberOfElementsInQueue()
{
  return stats.dataInQueue;
}

float pDTPClient::getBytesPrSec()
{
  auto prevBytesRecv = stats.numberOfBytesReceived;
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  return (stats.numberOfBytesReceived - prevBytesRecv) / 1000.0f / 1000.0f;
}

bool pDTPClient::packetIdCheck(uint16_t packId)
{

  if ((packId == 0 && stats.packetIdRecved == 65535) || (packId == 0 && stats.packetIdRecved == 0) || stats.packetIdRecved == 0) {
    stats.packetIdRecved = packId;
    return true;
  } else if (stats.packetIdRecved + 1 == packId) {

    stats.packetIdRecved = packId;
    return true;
  } else {
    if (conf.verbosity > 0) {
      std::cout << ">>>>>>>>>>> id=" << packId << " prev id:" << stats.packetIdRecved << " Packets out of sequence \n";
    }
    stats.packetIdRecved = packId;
    stats.missingPacketErrors++;
    return false;
  }
}

uint16_t pDTPClient::getPacketIdRecved()
{
  return stats.packetIdRecved;
}

int pDTPClient::getMissingPacketErrors()
{
  return stats.missingPacketErrors;
}
} // namespace readout
} // namespace bpct
