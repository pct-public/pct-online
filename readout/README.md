# The pCT Bergen project: on-line readout software

## Introduction
This module implements the pDTP Client software prototype and a binary parser prototype
for pRU and ALPIDE format

- pDTP client [run-pdtp-pru-parser](#pDTP client)
- PTB readout server [ptb-readout](#PTB readout server)
- PTB test client [ptb-file-publisher](#PTB file publisher)
- pRU parser [run-pru-parser](#pRU parser)
    
### pDTP client
implemented in executable `run-pdtp-pru-parser`

#### Usage
In your build directory run the command:

    ./readout/run-pdtp-pru-parser --config ../tuning-scripts/pDTPClient-settings.conf

Options are passed to the client via a configuration file, a sample file can be found in
[../tuning-scripts/pDTPClient-settings.conf](../tuning-scripts/pDTPClient-settings.conf).

#### Using emulator process for pRU data
*To be added*

### PTB readout server
Readout server for the pCT Test Box system is implemented in executable `pru-readout`, for
backward compatibility also the old version `run-ptb-pru-parser` is supported

The readout expects data in little endian format and reverses the order of the 16 bytes of
the pRU word.

Try option `--help` for usage information

### PTB file publisher
A client to publish data from a file to the PTB readout server, implemented in executable
`ptb-file-publisher`. It is also supporting endiannes conversion.

Try option `--help` for usage information

### Control
Currently, only a very limited control is implemented for the readout server/client. Both
open a UNIX-domain socket `/tmp/PCTDOMAINSOCK`. Any data received on this socket will terminate
the applications.

From the command line, the netcat tool can be used

    echo quit | nc -q 2 -uUN /tmp/PCTDOMAINSOCK

### pRU parser
#### Usage
In build/readout/ run the command:

    ./run-pru-parser -i iputFile -o outputFile -v verbosityLevel

### Other Worker classes
- `ReadoutSession`
  A host class for a readout session, implements a very simple state machine and serves
  a processing loop. The actual data handling depends on the defined policies for input,
  processing, and output.
- `NetworkReadout`
  Input policy for receiving data via network
- `ReadoutService`
  A service application for the readout building on FairMQ (very first sketch)
- `NetworkSession`
  handler for a network socket

### Other Example Applications
These applications have been added only as examples in the beginning and will be removed in
the future.
- ez-network-listener: a very simple network listener using NetworkSession with
  input policy NetworkReadout

## Testing (probably move to a little network HOWTO)
The 'netcat' tool can be used to easily send data to the server, e.g.
```
echo -n "0xffffffffffffffffff" | nc localhost 29070
```
Note the *-n* flag in order to skip the line feed.

or using some fancy terminal stuff
```
echo -n "0xffffffffffffffffff" > /dev/tcp/127.0.0.1/29070
```

A UDP echo server using `ncat`
```
ncat -e /bin/cat -k -u -l 29070
```
