/// @file   ez-pdtp-readout.cxx
/// @author Matthias Richter
/// @since  2019-09-12
/// @brief  Stand-alone application to read data from pRU using the pDTP protocol
/// The underlying network comminucation is provided by the NetworkSession class
/// TODO:
///   - we might want to move more functionality of the bare communication (which is
///     provided here as lambda functions using NetworkSession) the the class

#include "network/NetworkSession.h"
#include "readout/pDTPWorker.h"
#include "util/logging-tools.h"
#include "datamodel/pDTP-protocol.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <type_traits>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

namespace bpo = boost::program_options;

using namespace bpct; // the Bergen proton CT

int main(int argc, char* argv[])
{
  // definition of the available command line options
  boost::program_options::options_description od{
    //
    "Easy pRU readout application using the pDTP protocol\n"                                                                   //
    "(c) 2019 Bergen pCT Collaboration\n\n"                                                                                    //
    "Usage:\n"                                                                                                                 //
    "  ez-pdtp-readout --length 64 --output-file pru_data.dat\n"                                                               //
  };                                                                                                                           //
  od.add_options()                                                                                                             //
    ("port,p", bpo::value<int>()->default_value(30000), "port number")                                                         //
    ("host", bpo::value<std::string>()->default_value("localhost"), "host name")                                               //
    ("timeout", bpo::value<int>()->default_value(0), "timeout in sec for establishing the connection\nnot yet implemented")    //
    ("length,l", bpo::value<int>(), "number of pRU word to read")                                                              //
    ("output-mode,m", bpo::value<std::string>()->default_value("binary"), "output mode: binary or ascii\nnot yet implemented") //
    ("output-file,o", bpo::value<std::string>(), "output file")                                                                //
    ("verbosity,v", bpo::value<int>()->default_value(1), "verbosity level")                                                    //
    ("help,h", "Hilfe!");                                                                                                      //

  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error: " << e.what() << std::endl;
    exit(1);
  }

  bool printHelp = varmap.count("help");
  std::stringstream cmdLineError;
  if (!printHelp && varmap.count("output-file") == 0) {
    cmdLineError << "invalid argument: missing parameter 'output-file'";
    printHelp = true;
  }
  if (!printHelp && varmap.count("length") == 0) {
    cmdLineError << "invalid argument: missing parameter 'length'";
    printHelp = true;
  }
  if (printHelp) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(0);
  }

  auto port = varmap["port"].as<int>();
  auto host = varmap["host"].as<std::string>();
  auto timeout = varmap["timeout"].as<int>();
  auto wordsToRead = varmap["length"].as<int>();
  auto ofilename = varmap["output-file"].as<std::string>();
  auto verbosity = varmap["verbosity"].as<int>();

  if (timeout > 0) {
    std::cout << "Warning: the timeout option is not yet implemented" << std::endl;
  }
  if (wordsToRead > 255) {
    std::cout << "For the moment, only the single read command RQR is implemented, which can not read more than 255 words" << std::endl;
    exit(1);
  }

  network::NetworkSession<boost::asio::ip::udp> session;

  auto notifier = [port]() {
    std::cout << "connected to port " << port << std::endl;
  };
  // Note that there is no check in UDP whether the port is really open/has an open endpoint
  // That's why the method will always succeed.

  // TODO: error handling for failed session setup
  [[maybe_unused]] auto result = session.open(host.c_str(), port, notifier);

  // this is the raw sender function provided to the pDTP worker, but the we hold the
  // communication flexible
  auto sender = [&session](auto request) {
    // automatic conversion to boost::asio::buffer reqires the request object
    // to support data() and size() methods
    util::hexDump("SEND REQUEST", request.data(), request.size(), request.size());
    session.getSocket().send(boost::asio::buffer(request));
  };

  // this is the raw reader function provided to the pDTP worker
  auto reader = [&session, verbosity](auto& target) {
    boost::system::error_code ec;
    // keep this function general and extract the value type of the container from the deduced type of the parameter
    using value_type = typename std::remove_reference<decltype(target)>::type::value_type;

    auto len = session.getSocket().receive(boost::asio::buffer(target.data(), target.size() * sizeof(value_type)), 0, ec);
    if (!ec) {
      util::hexDump("GOT REPLY", target.data(), target.size(), len);
      if (verbosity > 0) {
        std::cout << "length " << len << std::endl;
      }
    } else {
      std::cerr << "receive operation returned error " << ec << std::endl;
    }
    return len;
  };

  auto write_to_file = [&ofilename, verbosity](auto&& buffer, size_t offset) {
    if (buffer.size() > 0) {
      std::ofstream ofile(ofilename);
      if (!ofile.good()) {
        throw std::runtime_error("can not open file " + ofilename + " for writing output");
      }
      size_t bufferSize = buffer.size() * sizeof(typename std::remove_reference<decltype(buffer)>::type::value_type);
      if (bufferSize > offset) {
        ofile.write(reinterpret_cast<const char*>(buffer.data() + offset), bufferSize - offset);
        ofile.flush();
        if (verbosity > 0) {
          std::cout << "writing data to file " << ofilename << " size " << buffer.size() - offset << std::endl;
        }
      }
    }
  };

  // make an instance of the worker class
  readout::pDTPWorker worker;

  // get one data set
  // TODO: use other opcodes as well
  using pDTPClientRequest = data_model::pDTPClientRequest;
  auto target = worker.get(sender, reader, pDTPClientRequest::ClientOpcode::CLIENT_RQR, wordsToRead);

  // handle the result
  size_t offset = sizeof(data_model::pDTPServerReply);
  if (target.size() > offset) {
    if (verbosity > 0) {
      util::hexDump("Data", target.data() + offset, target.size() - offset, target.size() - offset);
    }
    write_to_file(std::move(target), offset);
  } else {
    std::cerr << "failed to get data from worker" << std::endl;
    if (target.size() == offset) {
      std::cerr << *reinterpret_cast<data_model::pDTPServerReply const*>(target.data()) << std::endl;
    }
  }
}
