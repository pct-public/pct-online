/// @file   alpideDecoder.cxx
/// @author Øistein Jelmert Skjolddal
/// @since  2020-02-22
/// @brief  This class extracts and calculates the x,y coordinate 
/// hit information from a sorted and error-checked list of pRU frames.

#include "readout/alpideDecoder.h"

#include <iostream>
#include <bitset>

namespace bpct
{
namespace readout
{

using RUDataHeader = data_model::RUDataHeader;
using AlpideHit = data_model::AlpideHit;

alpideDecoder::alpideDecoder() {}

//! Parser destructor
/*! \param None
    \return None
*/
alpideDecoder::~alpideDecoder() {}

// @Breif: Extracts and converts the spatial coordinate data.
// @Param: A sorted and conttrolled list of pRUwords
// @Return: Vector of spatial data ellements
std::vector<char> alpideDecoder::convert(std::vector<char>&& data)
{
  int nofFrames = 0;
  // Control elements
  bool first_data_word;
  unsigned int pre_advance;
  int data_word_position;

  // position of the next data in the output buffer
  size_t position = 0;
  // position of the current frame in the output buffer
  size_t framePosition = 0;
  // the output vector, initial allocation from input size
  std::vector<char> output(data.size());
  RUDataHeader* currentFrame = nullptr;

  auto nextFrame = [&output, &position, &framePosition]() -> RUDataHeader* {
    // we need space for one RUDataHeader and reserve space for 10 Alpide hits
    size_t required = position + sizeof(RUDataHeader) + 10 * sizeof(AlpideHit);
    if (output.size() < required) {
      output.resize(required);
    }
    framePosition = position;
    auto* ptr = reinterpret_cast<RUDataHeader*>(output.data() + position);
    *ptr = RUDataHeader(); // initialize to default with layer set to 0xff
    position += sizeof(RUDataHeader);
    return ptr;
  };

  auto nextHit = [&output, &position, &framePosition, &currentFrame]() -> AlpideHit& {
    if (output.size() < position + sizeof(AlpideHit)) {
      output.resize(position + 10 * sizeof(AlpideHit));
      currentFrame = reinterpret_cast<RUDataHeader*>(output.data() + framePosition);
    }
    auto& ref = *reinterpret_cast<AlpideHit*>(output.data() + position);
    ref = AlpideHit(); // initialize
    position += sizeof(ref);
    return ref;
  };

  // pRU Header word temp values
  uint8_t RU = 0;       // 6 bit
  uint8_t stave_id = 0; // 4 bit
  uint8_t chip_id = 0;  // 4 bit
  uint8_t bc = 0;       // 8 bit
  //uint16_t spill_id = 0; // 16 bit
  uint32_t frame_id = 0; // 32 bit
  uint32_t abs_time = 0; // 32 bit

  uint8_t busy_on = 0;
  uint8_t busy_off = 0;

  // Alpide word elements
  uint8_t region;
  uint16_t data_short; // Encoder ID is used by x,y converter so it needs the whole word.
  unsigned char hit_map = 0;

  // char (byte) element iterator
  std::vector<char>::iterator it = data.begin(); 
  while (it != data.end()) { // for each pru frame

    // -------------------------- Extract header info ------------------------------

    // Check that the first element in a pRU Frame is a pRU Header.
    // Mask and check that the pRU element is a Header flag 0b01000000
    if ( ((*it) & pRUTypeMask) != static_cast<unsigned char>(0x40)) {
      std::cout << "Fatal error: pRU header word expected" << '\n'
      << "Recived: " ;
      printPRUWord(it); // Print the element that was recived
      it = data.end();  // Stop analysis.
     return {}; // Return nothing.
    }

    // get RU (readout unit) [0] and mask = layer
    RU = (static_cast<uint8_t>(it[0]) & 0X3F); // 0b00'111111
    // strip by bit shift (only interested in 4 most significant bits)
    stave_id = (static_cast<uint8_t>(it[1]) >> 4); // fills with 0s
    // strip by masking (only interested in the 4 least significant bits)
    chip_id = (static_cast<uint8_t>(it[1]) & 0x0F);  // 0b0000'1111

    busy_on = (static_cast<uint8_t>(it[5]) & 0x10); // 0b0001'0000
    busy_off = (static_cast<uint8_t>(it[5]) & 0x08); // 0b0000'1000 

    // get frame_id [8:11]
    // casting introduces a bug when a bit pattern with the most significant bit is sett is cast
    // Nececary to mask
    frame_id = (
      (static_cast<uint32_t>(it[8]) << 24) | // all potential errors are removed, no masking nececary
      ((static_cast<uint32_t>(it[9]) << 16 & 0x00'FF'00'00)) | // mask 0b00000000'11111111'00000000'00000000 a mask of 0b00000000'11111111'11111111'11111111 would have the same effect 
      ((static_cast<uint32_t>(it[10]) << 8) & 0x00'00'FF'00) | // mask 0b00000000'00000000'11111111'00000000
      ((static_cast<uint32_t>(it[11])  & 0x00'00'00'FF)) // mask 0b00000000'00000000'00000000'11111111
      );   
    // get abs_time [12:15]
    abs_time = (
      (static_cast<uint32_t>(it[12]) << 24) | // all potential errors are removed, no masking nececary
      ((static_cast<uint32_t>(it[13]) << 16) & 0x00'FF'00'00)) | // mask 0b00000000'11111111'00000000'00000000 
      ((static_cast<uint32_t>(it[14]) << 8) & 0x00'00'FF'00)  | // mask 0b00000000'00000000'11111111'00000000
      ((static_cast<uint32_t>(it[15]) & 0x00'00'00'FF) // mask 0b00000000'00000000'00000000'11111111
    );

    // Finished prosesing the pRU header word (16 characters)  
    //printPRUWord(it);
    std::advance(it, 16);
    currentFrame = nextFrame();

    // -------------------------- Finished with this header!: Get the data ------------------------------
    if ( (*it & data_model::pRUTypeMask) != data_model::pRUTDataWord) { // pRU Data Word: 0x0
      std::cout << "Fatal error: pRU data word expected" << '\n'
      << "Recived: ";
      printPRUWord(it); // Print the element that was recived
      it = data.end(); // Stop analysis.
      return {}; // Return nothing.
    } 

    first_data_word = true; // the next data word found will be the first

    pre_advance = 0; //Reset needed because compiler optimalisation creates a situation where segmentation fault might happen.

    while ( (*it & data_model::pRUTypeMask) != data_model::pRUTTrailerWord) { // Check pRU flag and run untill a trailer is found
      // Not a trailer, so it must be a data word 
      // Remove pRU word header (Strip to data payload).
      data_word_position = 2;

      // The bounch counter from the chip header, if this is the first dataword (16 chars for pRU header + 2 chars for pRU data).
      // ------- Chip Header -------
      // ------- First Region Header -------
      if (first_data_word) {
        // Should be at position 2 in the frame, should be a chip header.
        bc = (it[(data_word_position + 1)]);

        data_word_position += 2;
        // At position 4, a region header
        region = (it[data_word_position] & 0x1F); // dereference and mask to extract region 5 bits 0b0001'1111
        data_word_position += 1;
        first_data_word = false;
      }
      
      // Check if there was a partial alpide word at the end of last pRUData word
      // No need to pre advance on the first pRU word
      else if (pre_advance != 0) { 
        data_word_position += pre_advance;
        pre_advance = 0;
      }

      // ----------------- For each remaining elements in the pRU word ----------------- //
      while (data_word_position < 16) { // last ecessibel element is 15
        unsigned char chip_word_check = static_cast<unsigned char>(it[data_word_position]) & data_model::MASK_CHIP;
        unsigned char data_word_check = static_cast<unsigned char>(it[data_word_position]) & data_model::MASK_DATA;
        unsigned char region_word_check = static_cast<unsigned char>(it[data_word_position]) & data_model::MASK_REGION_HEADER;

        // ------- Data Long -------
        if (data_word_check == data_model::ALPIDE_DATA_LONG) {
          data_short = static_cast<uint16_t>(it[data_word_position]) << 8; // safe since shifted to end of type and it position not out of bounds
          
          if (data_word_position <= 13 ) { // if data_word_position would not excede the pru word ( data_word_position + 2 <= 15), advance 3 ok
            // Extract data: Mask shift and concatenate the two parts of the 10 bit adress.
            // Promotion bugg, same as abs time_and frame_id. Must mask!
            data_short |= (static_cast<uint16_t>(it[data_word_position + 1]) & 0x00FF); // unsafe, must mask! & 0b00000000'11111111
            hit_map =it[data_word_position + 2]; // ok since the most significant bit is 0
            // Advance
            data_word_position += 3;
          }
          // Check for end of list and advance acordingly
          else if (data_word_position == 14 ) { // data_word_position + 2 = 16 and will excede the pru word by 1
            // Extract data: Mask shift and concatenate the two parts of the 10 bit adress.
            data_short |= (static_cast<uint16_t>(it[data_word_position + 1]) & 0x00FF); // unsafe, must mask! & 0b00000000'11111111
            hit_map = it[data_word_position + 4];                                       // 2 + 2 since this is located in the next pru word and is it preceded by to two pru elements
            // Advance
            data_word_position += 2;
            pre_advance = 1;
          } else { // Must be at position 15
            data_short |= (static_cast<uint16_t>(it[data_word_position + 3]) & 0x00FF); // & 0b00000000'11111111 
            hit_map = it[data_word_position + 4]; // same relative adress as above as above
            data_word_position += 1;
            pre_advance = 2;
          }

          //Have data, convert and store
          std::vector<std::pair<int,int>> xy_list = DecodeDataWord(data_short, hit_map, region);
          for(auto it = xy_list.begin(); it != xy_list.end(); it++){
            AlpideHit& hit = nextHit();
            hit.x = (*it).first;
            hit.y = (*it).second;
            hit.chip = chip_id;
            hit.stave = stave_id;
            currentFrame->nofHits++;
          }
        }
        // ------- Data Short -------
        else if (data_word_check == data_model::ALPIDE_DATA_SHORT) { 
          hit_map = 0;
          data_short = static_cast<uint16_t>(it[data_word_position]) << 8; // safe since shifted to end of type and it position not out of bounds
          
          if (data_word_position <= 14 ) { // data_word_position + 1 = 15 
            data_short |= (static_cast<uint16_t>(it[data_word_position + 1]) & 0x00FF); // unsafe, must mask! &  0b00000000'11111111
            data_word_position += 2;
          }
          // Check for end of list and advance acordingly
          else{ // Must be at position 15
            data_short |= (static_cast<uint16_t>(it[data_word_position + 3]) & 0x00FF); // unsafe, must mask! & 0b00000000'11111111
            data_word_position += 1;
            pre_advance = 1;
          }
          std::vector<std::pair<int,int>> xy_list = DecodeDataWord(data_short, hit_map, region);
          for(auto it = xy_list.begin(); it != xy_list.end(); it++){
            AlpideHit& hit = nextHit();
            hit.x = (*it).first;
            hit.y = (*it).second;
            hit.chip = chip_id;
            hit.stave = stave_id;
            currentFrame->nofHits++;
          }
        }
        // ------- Region Header -------
        else if (region_word_check == data_model::ALPIDE_REGION_HEADER) {
          region = it[data_word_position] & 0x1F; // & 0b0001'1111
          data_word_position += 1;
        }
        // ------- Chip Trailer -------
        else if (chip_word_check == data_model::ALPIDE_CHIP_TRAILER) {
          // move to end of data word(skip anny padding)
          data_word_position = 16;
        }
        // ------- Unknown word, Error -------
        else {
          std::cout << " Fatal error! found unknown alpide element" << '\n';
          // << " at , " <<  std::bitset<8>(static_cast<unsigned char>(it[0]));
          it = data.end(); // Stop analysis.
          data_word_position = 16; // Stop ALPIDE Data analysis.
          return{}; // Return nothing.
        }
      } // END pRU data word extraction
      std::advance(it, 16);
    }// END for all data words

    // -------------------------- Finished with all data get pRU trailer  ------------------------------
    if ( (*it & data_model::pRUTypeMask) != data_model::pRUTTrailerWord) {
      std::cout << "Fatal error: pRU trailer word expected" << '\n'
      << "Recived: ";
      printPRUWord(it); // Print the element that was recived
      it = data.end(); // Stop analysis.
      return {}; // Return nothing.
    } 
    // The trailer is not needed, we have allready all nececary data
    // Advance to next frame
    std::advance(it, 16);
    
    // -------------------------- Frame wrap up ------------------------------
    currentFrame->layer = RU;
    currentFrame->frameid = frame_id;
    currentFrame->bc = bc;
    currentFrame->time = abs_time;

    if(busy_on != 0){
      currentFrame->busy = 1;
    } if (busy_off != 0) {
      currentFrame->busy = 2;
    }

    // FIXME: that doesnt seem to be the number of frames but hits totally received
    mFramesReceived += currentFrame->nofHits; // update number of frames received.
    nofFrames++;
  } // END for all data

  // ---------- Return the result ----------- //
  output.resize(position);
  return output;
}

// ---------------------------------------------------------------------------------------------

typedef struct {
  int region;
  int dcol;
  int address;
} TPixHit;

// Calculates the x,y coordinate(s) of a hit or hit cluster on the alpid chip.
// All input is treated as ALPIDE Data Long (Short is a long with hit patern 0)
  std::vector<std::pair<int,int>> alpideDecoder::DecodeDataWord(uint16_t const& data, 
    unsigned char const& hit_map, uint8_t const& region) {

  std::vector<std::pair<int,int>> hit_positions = std::vector<std::pair<int,int>>();
  TPixHit hit;
  int address, hitmap_length;
  int x, y;

  //Mask out readout region 0b00000000'00000000'00000000'11111111 
  hit.region = (region & 0x00'00'00'FF); 
  // data_field allready int type, no promotion
  hit.dcol = ((data & 0x3C'00) >> 10); // apply mask 00111100 00000000 and righs shift 10 : most significant bit is 0, safe
  address = (data & 0x03'FF); // apply mask 00111100 11111111 : most significant bit 0, safe

  if (hit_map != 0) { // If not a data short
    hitmap_length = 7;
  } else { // Else is data short
    hitmap_length = 0;
  }

  // Decode the pixel element of the DataWord and convert all elements of the hitmap to Data Short
  for (int i = -1; i < hitmap_length; i++) {
    if ((i >= 0) && (!((hit_map >> i) & 0x1))) {
      continue;
    }
    hit.address = address + (i + 1);
    x = AddressToColumn(hit.region, hit.dcol, hit.address);
    y = AddressToRow(hit.address);
    hit_positions.push_back(std::make_pair(x,y));
  }
  return hit_positions;
}

int alpideDecoder::AddressToColumn(int const& ARegion, int const& ADoubleCol, int const& AAddress) {
  int Column = ARegion * 32 + ADoubleCol * 2; // Double columns before ADoubleCol
  // Left or right column within the double column
  int LeftRight;
  if ((AAddress % 4) == 0 || (AAddress % 4) == 3) {
    LeftRight = 0;
  } else {
    LeftRight = 1;
  }
  Column += LeftRight;

  return Column;
}

int alpideDecoder::AddressToRow(int const& AAddress) {
  int Row = AAddress / 2; 
  return Row;
}

// prints a 128 bit pRU word binary to screen, nicely formated.
void alpideDecoder::printPRUWord(std::vector<char>::iterator const& it){
  std::cout <<
  std::bitset<8>(static_cast<unsigned char>(it[0])) << " " << std::bitset<8>(static_cast<unsigned char>(it[1])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[2])) << " " << std::bitset<8>(static_cast<unsigned char>(it[3])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[4])) << " " << std::bitset<8>(static_cast<unsigned char>(it[5])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[6])) << " " << std::bitset<8>(static_cast<unsigned char>(it[7])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[8])) << " " << std::bitset<8>(static_cast<unsigned char>(it[9])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[10])) << " " << std::bitset<8>(static_cast<unsigned char>(it[11])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[12])) <<  " " << std::bitset<8>(static_cast<unsigned char>(it[13])) << " " <<
  std::bitset<8>(static_cast<unsigned char>(it[14])) << " " << std::bitset<8>(static_cast<unsigned char>(it[15])) <<
  '\n';
}

// checks the input and prints a pRU character vector to screen as pRU word binaries, nicely formated.
void alpideDecoder::inspect(std::vector<char> data) {
  if (data.size() % 16 != 0 || data.size() < 16) {
    std::cout << "wrong input size " << std::endl;
    return;
  }
  //std::cout << '\n';
  std::vector<char>::const_iterator it = data.begin();
  while (it != data.end()) { // for each byte element
    std::cout <<
      std::bitset<8>(static_cast<unsigned char>(it[0])) << " " << std::bitset<8>(static_cast<unsigned char>(it[1])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[2])) << " " << std::bitset<8>(static_cast<unsigned char>(it[3])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[4])) << " " << std::bitset<8>(static_cast<unsigned char>(it[5])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[6])) << " " << std::bitset<8>(static_cast<unsigned char>(it[7])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[8])) << " " << std::bitset<8>(static_cast<unsigned char>(it[9])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[10])) << " " << std::bitset<8>(static_cast<unsigned char>(it[11])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[12])) <<  " " << std::bitset<8>(static_cast<unsigned char>(it[13])) << " " <<
      std::bitset<8>(static_cast<unsigned char>(it[14])) << " " << std::bitset<8>(static_cast<unsigned char>(it[15])) <<
      '\n' ;

    std::advance(it, 16);
  }
}

} // namespace readout
} // namespace bpct
