/**
 * @file run-pdtp-pru-parser
 * @author Alf K. Herland 
 * @date 04.03.2020.
 * @brief
*/

#include "readout/ReadoutSession.h"
#include <iostream>
#include <iomanip> // std::put_time
#include <fstream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <string>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/asio.hpp>
#include "readout/pDTPClient.h"
#include "readout/comService.h"
#include "readout/pRUParser.h"
#include "readout/alpideDecoder.h"
#include "rootio/pRUExporter.h"

namespace bpo = boost::program_options;

using namespace bpct; // the Bergen proton CT
bpct::readout::ClientConfig conf;

bool readConfig(int argc, char* argv[])
{
  // Get config data from file
  boost::program_options::options_description generalOptions{
    "pRU readout application using the pDTP protocol\n"
    "(c) 2019 Bergen pCT Collaboration\n\n"
    "Usage:\n"
    "  pdtpclient --config settings.conf \n"
    "Config file is located in the tuning-scripts folder\n"
    "Remember to run other tuning scripts for improved perfomance of OS"};

  generalOptions.add_options()("help,h", "Help screen")(
    "config", boost::program_options::value<std::string>(), "Config file");

  boost::program_options::options_description fileOptions{"File"};
  /// Options for the config file
  fileOptions.add_options()("IP", boost::program_options::value<std::string>(),
                            "IP");
  fileOptions.add_options()("PORT", boost::program_options::value<short>(),
                            "PORT");
  fileOptions.add_options()(
    "SSIZE", boost::program_options::value<unsigned short>(), "SSIZE");
  fileOptions.add_options()(
    "PSIZE", boost::program_options::value<unsigned short>(), "PSIZE");
  fileOptions.add_options()(
    "MODE", boost::program_options::value<std::string>(), "MODE");
  fileOptions.add_options()("RTIME", boost::program_options::value<int>(),
                            "RTIME");
  fileOptions.add_options()("UDPTIMEO", boost::program_options::value<int>(),
                            "UDPTIMEO");
  fileOptions.add_options()("NUMREQ", boost::program_options::value<int>(),
                            "NUMREQ");
  fileOptions.add_options()("VERB", boost::program_options::value<int>(),
                            "VERB");
  fileOptions.add_options()("SPSCSIZE", boost::program_options::value<int>(),
                            "SPSCSIZE");
  fileOptions.add_options()("POLL", boost::program_options::value<int>(),
                            "POLL");
  fileOptions.add_options()("ENABLEFILE", boost::program_options::value<int>(),
                            "ENABLEFILE");
  fileOptions.add_options()("SMODE", boost::program_options::value<int>(),
                            "SMODE");
  fileOptions.add_options()("NOWAIT", boost::program_options::value<int>(),
                            "NOWAIT");
  fileOptions.add_options()("MAXIMIZE", boost::program_options::value<int>(),
                            "MAXIMIZE");
  fileOptions.add_options()("TIMESTAMP", boost::program_options::value<int>(),
                            "TIMESTAMP");

  bpo::variables_map vm;
  bpo::store(parse_command_line(argc, argv, generalOptions), vm);
  if (vm.count("config")) {
    std::ifstream ifs{vm["config"].as<std::string>().c_str()};
    if (ifs)
      store(parse_config_file(ifs, fileOptions), vm);
  }
  notify(vm);

  if (vm.count("help")) {
    std::cout << generalOptions << '\n';
    return false;
  } else if (vm.count("IP")) {
    conf.ipAddress = vm["IP"].as<std::string>();
    std::cout << "---------------------------CONFIG FROM FILE: "
                 "---------------------------\n"
              << "SERVER IP: " << conf.ipAddress << "\n";
  }
  if (vm.count("PORT")) {
    conf.portNumber = vm["PORT"].as<short>();
    std::cout << "PORT: " << conf.portNumber << "\n";
  }

  if (vm.count("SSIZE")) {
    conf.streamSize = vm["SSIZE"].as<unsigned short>();
    std::cout << "STREAM SIZE: " << conf.streamSize << "\n";
  }
  if (vm.count("PSIZE")) {
    conf.packageSize = vm["PSIZE"].as<unsigned short>();
    std::cout << "PACKAGE SIZE: " << conf.packageSize << "\n";
  }
  if (vm.count("MODE")) {
    conf.startUpMode = vm["MODE"].as<std::string>();
    std::cout << "MODE: " << conf.startUpMode << "\n";
  }
  if (vm.count("RTIME")) {
    conf.clientRunTime = vm["RTIME"].as<int>();
    std::cout << "Time in secounds to run client for(0 is disabled): "
              << conf.clientRunTime << "\n";
  }
  if (vm.count("UDPTIMEO")) {
    conf.clientUDPTimeout = vm["UDPTIMEO"].as<int>();
    std::cout << "UDP timeout in microsecounds(0 is disabled): "
              << conf.clientUDPTimeout << "\n";
  }
  if (vm.count("NUMREQ")) {
    conf.requestNumberOfWords = vm["NUMREQ"].as<int>();
    std::cout << "Number of words to request (0 is disabled): "
              << conf.requestNumberOfWords << "\n";
  }
  if (vm.count("VERB")) {
    conf.verbosity = vm["VERB"].as<int>();
    std::cout << "Verboisity for logger: " << conf.verbosity << "\n";
  }
  if (vm.count("SPSCSIZE")) {
    conf.spscQueueSize = vm["SPSCSIZE"].as<int>();
    std::cout << "Size of the spsc queue: " << conf.spscQueueSize << "\n";
  }

  if (vm.count("POLL")) {
    conf.pollMode = vm["POLL"].as<int>();
    std::cout << "Poll mode is: " << conf.pollMode << "\n";
  }

  if (vm.count("ENABLEFILE")) {
    conf.fileWriterEnabled = vm["ENABLEFILE"].as<int>();
    std::cout << "Filwriter is: " << conf.fileWriterEnabled << "\n";
  }
  if (vm.count("SMODE")) {
    conf.handleBufferStatus = vm["SMODE"].as<int>();
    std::cout << "Stream mode: " << conf.handleBufferStatus << "\n";
  }
  if (vm.count("NOWAIT")) {
    conf.noWaitFlag = vm["NOWAIT"].as<int>();
    std::cout << "NOWAIT flag: " << conf.noWaitFlag << "\n";
  }
  if (vm.count("TIMESTAMP")) {
    conf.noWaitFlag = vm["TIMESTAMP"].as<int>();
    std::cout << "TIMESTAMP flag: " << conf.timeStampFile << "\n";
  }
  if (vm.count("MAXIMIZE")) {
    conf.maximizeFlag = vm["MAXIMIZE"].as<int>();
    std::cout << "MAXIMIZE flag: " << conf.maximizeFlag << "\n";
    return true;
  }
  return false;
}

int main(int argc, char* argv[])
{
  try {
    if (argc < 2) {
      perror("Need to input 2 parameters");
      return -1;
    }
    if (readConfig(argc, argv)) {
      std::cout << "---------------------------CONFIG FILE "
                   "LOADED---------------------------\n";

    } else {
      perror("Error Loading config file");
      return -1;
    }

  } catch (std::exception& ex) {
    std::cerr << ex.what() << '\n';
    return -1;
  }

  /*
  bool printHelp = varmap.count("help");
  std::stringstream cmdLineError;
  if (!printHelp && (varmap.count("ifile") == 0 || varmap.count("ofile") == 0)) {
    cmdLineError << "missing parameter, please specify input and output files";
    printHelp = true;
  }
  if (printHelp) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(0);
  }
  auto verbosity = varmap["verbosity"].as<int>();
 */
  // Define our processing pipeline with input, filter and output policies
  // The basic buffer used for data exchange between the policies is std::vector<char>
  // InputPolicy: a lambda function reading from file
  // ForwardPolicy: the pRUParser
  // OutputPolicy: an std function taking a buffer by move
  //
  // Note that generic functions are used as forward and output policies instead of specific classes.
  // this allows to use a simple lambda function as policy.
  using BufferT = std::vector<char>;
  using Processor = readout::ReadoutSession<std::function<BufferT()>,
                                            std::function<BufferT(BufferT &&)>,
                                            std::function<void(BufferT &&)>>;

  auto verbosity = conf.verbosity;

  // processing policy: a simple forward filter
  // this is a lambda function, it is defined in the current scope (i.e. the current function)
  // and has access to the function variables if specified in the catch-clause []
  // the function parameter definition is as usual, the return type of the lambda is either
  // deduced automatically or specified with the '->' syntax.
  // this lambda function is a simple forward, we can later on optimize the Networker class
  // to simply drop to step if a void forward policy is specified.
  //
  // FIXME: the parser object needs to be created and passed to the lambda function, it follows
  // the following schema:
  // 1. we need the parser implementation, this is a type definition
  //using Parser = int; // replace with correct type, e.g. pRUParser
  // 2. we create the object of the parser
  //Parser parser;
  readout::pRUParser parser = readout::pRUParser();

  // 3. we define a lambda function which can use this parser
  // Note: the lambda function can be dropped later by implementing the operator() on the parser
  auto pRUFilter = [&parser, verbosity](BufferT&& buffer) -> BufferT {
    if (verbosity > 4) {
      std::cout << "Filter policy [pRU parser]: buffer of " << buffer.size() << " byte(s)" << std::endl;
    }
    return std::move(parser.parse_pRU_data(std::move(buffer)));
    //return parser.parse_pRU_data(std::move(buffer)); // changed to allow compiler RVO
  };

  // output policy: write to standard output or file
  // this is a lambda function which has access to the local variable 'outputStream'
  std::mutex mutex;
  std::condition_variable readyToQuit;
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream oss;
  std::string ofilename = "aFile.bin";
  if (conf.timeStampFile == 0) {
    oss << std::put_time(&tm, "%d-%m-%Y-%H-%M-%S");
    auto str = oss.str();
    ofilename = str;
    std::cout << ofilename << "\n";
  } else {
    ofilename = "aFile";
  }

  /**
   * Output policy
   * Can be both used as a output policy in the processor or used in the input policy to
   * write raw unprocessed data to disk.
   */
  auto output = [&ofilename, &readyToQuit, &mutex, verbosity](BufferT& buffer) {
    if (buffer.size() > 0) {
      std::string oofilename = ofilename;
      oofilename += ".bin";
      std::ofstream ofile;
      if (conf.timeStampFile == 0) {
        ofile.open(oofilename, std::ofstream::out | std::ofstream::app);
        if (verbosity > 4) {
          std::cout << "Open file\n";
        }
      } else {
        ofile.open(oofilename, std::ofstream::out | std::ofstream::trunc);
      }
      if (!ofile.good()) {
        throw std::runtime_error("can not open file " + ofilename + " for writing output");
      }
      ofile.write(reinterpret_cast<const char*>(buffer.data()), buffer.size() * sizeof(typename BufferT::value_type));
      ofile.flush();
      if (verbosity > 4) {
        std::cout << "Ouput policy  [File writer]: write to file " << ofilename << " size " << buffer.size() << std::endl;
      }
    }
  };

  // Decoder extracts the alpide coordinate data
  readout::alpideDecoder decoder = readout::alpideDecoder();
  // TODO: Sepperate root functionality from rest of the system.
  //rootio::pRUExporter exporter = rootio::pRUExporter("aFile", "aName", "aTitle"); // All standard names
  rootio::pRUExporter exporter = rootio::pRUExporter(ofilename, ofilename + "Tree", ofilename + "TreeTitle"); // Input varriable names

  auto rootWriter = [&ofilename, &readyToQuit, &mutex, &decoder, &exporter, verbosity](BufferT&& buffer) {
    if (buffer.size() > 0) {
      exporter.writeToTree(decoder.convert(std::move(buffer)));
      if (verbosity > 4) {
        std::cout << "Ouput policy  [Root tree Writer]: write to file " << ofilename /* "aFile" */ << ".root" << std::endl;
      }
    }
  };

  ComService* coms = new ComService();
  bpct::readout::pDTPClient* client = new bpct::readout::pDTPClient(conf, coms);
  /**
    *  pDTP Network Policy
    *  This policy handles getting data from the SPSC queue of the pDTP client
    */
  auto pPDTPReader = [&client, verbosity, &output]() {
    std::vector<char> buf(0);
    // set to false to make policy blocking until successful pop from queue.
    bool getData = false;
    do {
      if (client->getDataFromQueue(buf) && buf.size() > 1) {
        getData = true;
        if (verbosity > 3) {
          std::cout << "num of elements in queue " << client->numberOfElementsInQueue() << '\n';
        }
      }
    } while (!getData);
    BufferT buffer(buf);
    output(buffer); //Write raw data to disk, can be commented out if only processed files should be kept.
    return buffer;
  };
  /**
     *  Thread that listens for commands from another process
     */
  std::thread cmdThread([&client, &readyToQuit, &mutex]() {
    std::cout << "starting cmd thread";
    boost::asio::io_context my_io_context;
    ::unlink("/tmp/PCTDOMAINSOCK"); // Remove previous binding.
    boost::asio::local::datagram_protocol::endpoint ep("/tmp/PCTDOMAINSOCK"); //Create endpoint to a local domain socket
    boost::asio::local::datagram_protocol::socket socket(my_io_context, ep); //Set context and endpoint, opens socket

    char buffer[1024];
    boost::system::error_code ignored_error;
    socket.receive_from(boost::asio::buffer(buffer), ep);
    client->stopClient();
    readyToQuit.notify_all();
    socket.close();
    ::unlink("/tmp/PCTDOMAINSOCK"); // Remove binding so that if another user uses the application there
                                    // wont be any lingering files related to the socket.
  });
  /**
   * Start the pDTP communication thread.
   */
  std::thread fsmThread(&readout::pDTPClient::handleStates, client);

  /**
   * Start the processor
   */
  Processor pipeline(pPDTPReader, pRUFilter, rootWriter);
  if (verbosity > 0) {
    std::cout << "Starting pipeline ... " << std::endl;
  }
  pipeline.control(Processor::Command::START);
  std::unique_lock<std::mutex> lock(mutex);
  readyToQuit.wait(lock);

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  fsmThread.join(); //Join the thread that handles running the
  pipeline.control(Processor::Command::STOP);

  // Gives a summary of errors found.
  parser.print_errors();
  cmdThread.join();
  delete (coms);
  delete (client);
  return 0;
}
