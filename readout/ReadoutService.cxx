#include "readout/ReadoutService.h"
#include "utilities/logging-tools.h"
#include <memory>
#include <iostream>
#include <boost/asio/buffer.hpp>
#include <boost/asio/read.hpp>

namespace bpct
{
namespace readout
{

void ReadoutService::Init()
{
  mIoService = std::make_unique<boost::asio::io_service>();
  mSocket = std::make_unique<boost::asio::ip::tcp::socket>(*mIoService);

  int port = 29070;
  auto connection = std::make_shared<boost::asio::ip::tcp::acceptor>(*mIoService, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port));

  auto connectionChecker = [connection, this, port]() {
    std::cout << "trying RU connection at port " << port << std::endl;
    connection->accept(*(this->mSocket));
    std::cout << "established RU connection at port " << port << std::endl;
    this->mHasConnection = true;
  };
  mConnectorThread = std::make_unique<std::thread>(connectionChecker);
}

void ReadoutService::PreRun()
{
}

void ReadoutService::PostRun()
{
}

void ReadoutService::Reset()
{
}

bool ReadoutService::ConditionalRun()
{
  if (mHasConnection) {
    while (true) {
      std::vector<uint8_t> buffer(16);
      boost::system::error_code error;
      size_t len = boost::asio::read(*mSocket, boost::asio::buffer(buffer), error);

      if (error == boost::asio::error::eof) {
        std::cout << "Unexpected end of offloading stream found, should be flushed but is not!" << std::endl;
        break;
      }

      //check for flush word, all bytes need to be 0xff
      if (std::find_if(buffer.begin(), buffer.end(), [](auto const& v) { return v != 0xFF; }) == buffer.end()) {
        break;
      }
      bpct::utilities::hexDump("ALPIDE: ", buffer.data(), len, buffer.size());
    }
  } else {
    std::cout << "waiting for connected RU at port " << std::endl;
    WaitFor(std::chrono::seconds(1));
  }
  return true;
}

} // namespace readout
} // namespace bpct
