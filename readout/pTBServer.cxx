//
// Created by wac002 on 3/7/20.
//
#include "readout/ptbserver.h"

#include <algorithm>
#include <iostream>

namespace bpct
{
namespace readout
{
pTBServer::pTBServer(boost::asio::io_context& io_context,
                     const boost::asio::ip::tcp::endpoint& endpoint, boost::asio::ip::tcp::socket socket)
  : socket(std::move(socket))
  , acceptor(io_context, endpoint)
{
}
bool pTBServer::control(Command command)
{
  auto start = [this]() {
    mState = State::STARTING;
    std::cout << "STARTING PTB SERVER\n";
    mRunner = std::thread(&pTBServer::run, this);
  };
  auto stop = [this]() {
    mState = State::STOPPING;
    mRunner.join();
    std::cout << "STOPPED PTB SERVER\n";
  };
  auto checkValid = [](Command) {
    // TODO: implement the logic
    return true;
  };
  if (!checkValid(command)) {
    // invalid state for this command
    return false;
  }
  switch (command) {
    case Command::START:
      start();
      return true;
      break;
    case Command::STOP:
      stop();
      return true;
      break;
    case Command::TERMINATE:
      break;
    case Command::RESET:
      start();
      break;
  }
  return false;
}
std::vector<char> pTBServer::getData()
{
  std::vector<char> buf;
  std::vector<char> retBuffer;
  do {
    while (!spscQueue.pop(buf) && mState == State::RUNNING) {
      //Block until successful pop;
    }
    if (buf.size() != 0) {
      retBuffer.insert(retBuffer.end(), buf.begin(), buf.end());
      buf.clear();
    }
  } while (retBuffer.size() % 16 != 0 && mState == State::RUNNING);
  for (size_t i = 0; i < retBuffer.size(); i += 16) {
    for (int low = i, high = i + 15; low < high; low++, high--) {
      std::swap(retBuffer[low], retBuffer[high]);
    }
  }

  return retBuffer;
}
std::atomic<bool> pTBServer::isRunning()
{
  return (mState == State::RUNNING);
}
void pTBServer::run()
{
  if (mState != State::STARTING) {
    // this is the wrong state to start the run loop
    return;
  }
  acceptor.accept(socket);
  unsigned char data[4096];
  mState = State::RUNNING;
  while (mState == State::RUNNING) {
    auto length = socket.read_some(boost::asio::buffer(data), ignored_error);
    if (length > 0){ //Check if the read data greater than zero
      std::vector<char> vecData(data, data + length);
      spscQueue.push(std::move(vecData));
    }else{ //If zero or error sleep of some millisec
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  }
  if (mState == State::STOPPING) {
    // some cleanup if necessary
    mState = State::IDLE;
    socket.close();
  } else {
    std::cout << "PTB ---- WRONGSTATE!\n";
    // wrong state, do nothing and leave the state as is
  }
}
} // namespace readout
} // namespace bpct
