/// @file   pRUParser.h
/// @author Øistein Jelmert Skjolddal
/// @since  2019-11-11
/// @brief  Parser implemenation for pRU words

#ifndef PRUPARSER_H
#define PRUPARSER_H

#include <list>
#include <queue>
#include <vector>
#include <chrono> // Benchmarking

#include "datamodel/alpide_format.h"
#include "datamodel/pRU-format.h"
#include "readout/pRUError.h"
#include "readout/pRUFrame.h"

namespace bpct
{
namespace readout
{

/*! \class pRUParser
      \brief A parser class
      This class sorts and checks the pRU words and the containing ALPIDE data.
      It will remove words with errors and return a vector containing the resulting data, sorted.

*/
class pRUParser
{
 private:
  std::vector<char> frame_vector;  /*!< Vector of finished and checked frames */
  std::list<pRUFrame> temp_frames; /*!< Vector of uncomplete frames */

  // error frames
  std::vector<char> error_vector; /*!< Vector of frames or alpide words with errors */
  std::vector<char> empty_words;  /*!< Vector of frames or alpide words with errors */

  unsigned int number_of_frames = 0; /*!< Number of how many complete frames has been parsed without errors */

  std::vector<int> stat_errors; /*!< Statistic of errors found */

  unsigned int busy_transition = 0; /*!< Counter for set busy_transition flags in the alpide trailer */
  unsigned int strobe_extended = 0; /*!< Counter for set strobe_extended flags in the alpide trailer */

  unsigned long int number_of_empty_words = 0; /*!< Counter for how manny alpide empty words have been registered */
  unsigned int number_of_delimiter_words = 0;  /*!< Counter for how manny pRU Delimiter words have been registered */

  unsigned int REGION = 0; /*!< Current readout region */

  //! Creates a new frame for the header word
  /*!
  \param vector<unsigned char> containing a 128 bit header word.
  \return None
  \note This function creates a temporary frame which will be complete when
  the trailer word arrives and no errors have been found.
  */
  void insert_header(unsigned char const pRUWord[16]);

  //! Inserts a data word into an frame
  /*!
  \param vector<unsigned char> containg a 128 bit data word.
  \return None
  \note The data word is stored in the error vector if it cannot be added to the correct frame.
  */
  void insert_data(unsigned char const pRUWord[16]);

  //! Inserts a trailer word into an frame
  /*!
  \param vector<unsigned char>  containg a 128 bit trailer word
  \return None
  \note The trailer word is stored in the error vector if it cannot be added to the correct.
  */
  void insert_trailer(unsigned char const pRUWord[16]);

  void check_pRU_word(unsigned char const pRUWord[16]);

  //! proceses a finished frame
  /*!
  \param frame_list::iterator (An itterator pointing to a complete frame)
  \return None
  \note This functions does multiple checks. The frame is checked and is moved to the return vector or the error storage.
  */
  void analyze_data(frame_list::value_type& it);

  //! Checks all the data fields in the data word.
  /*!
  \param pRUFrame Reference to an frame
  \return None
  \note This functions does multiple checks.
  */
  bool check_data(pRUFrame& frame);

 public:
  pRUParser();
  ~pRUParser();

  //! Parser main function
  /*!
    \param  Vector of binary data in character form
    \return A vector of frames (sorted and complete sets of a pRUHeader, a
     number of pRUData and a pRUTrailer word)
  */
  std::vector<char> parse_pRU_data(std::vector<char>&& data);

  // Get number of pRUFrames parsed so far.
  unsigned int get_number_of_frames() { return number_of_frames; }

  // Get number of alpide empty words recived so far.
  unsigned int get_number_of_empty() { return number_of_empty_words; }

  // Get number of pRU delimiter words recived so far
  unsigned int get_number_of_delimiter() { return number_of_delimiter_words; }

  // Get the summ of errors
  unsigned int get_number_of_errors();

  //! Writes a summary of the errors found this session.
  /*!
    \param  none.
    \return none.
  */
  void print_errors();

  //! Prints the entire error vector as a binary, one pRU word per line.
  /*!
    \param  none.
    \return none.
  */
  void inspect_error_vector();

  //! Retrieve the vector containing the summary of found errors
  /*!
    \param  none.
    \return vector<int> of the number of each error found in the current session.
  */
  std::vector<int> get_error_vector() { return stat_errors; }

  // TODO: Need a method to acess the vector storing the faulty pRU frames and other errors
};

} // end namespace readout
} // end namespace bpct

#endif // PRUPARSER_H
