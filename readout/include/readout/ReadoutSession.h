/// @file   ReadoutSession.h
/// @author Matthias Richter
/// @since  2019-02-14
/// @brief  An instance of a readout session

#ifndef READOUTSESSION_H
#define READOUTSESSION_H

#include <thread>

namespace bpct
{
namespace readout
{

/// readout session host class
/// InputPolicy:
///    - signature: []() {return std::move(...);}
///    - implement: TCP, UDP, cin
/// FilterPolicy:
///    - signature: [](auto&& input) {return std::move(input);}
///    - implement: endianess, pRU word sorter
///    - allow tuple of filter policies to be executed in a pipeline
///    - exchange via std::move
/// OutputPolicy:
///    - signature: [](auto&& input) {}
///    - implement: cout, readout service
///    - blocking, non-blocking mode
///    - binary, text
template <typename InputPolicyType, typename FilterPolicyType, typename OutputPolicyType>
class ReadoutSession
{
 public:
  using InputPolicy = InputPolicyType;
  using FilterPolicy = FilterPolicyType;
  using OutputPolicy = OutputPolicyType;

  ReadoutSession() = delete;
  ReadoutSession(InputPolicy&& inputpolicy, FilterPolicy&& filterpolicy, OutputPolicy&& outputpolicy)
    : mInputPolicy(std::move(inputpolicy))
    , mFilterPolicy(std::move(filterpolicy))
    , mOutputPolicy(std::move(outputpolicy))
  {
  }

  enum struct State {
    IDLE,
    STARTING,
    RUNNING,
    STOPPING,
    ERROR,
  };

  enum struct Command {
    START,
    STOP,
    TERMINATE,
    RESET,
  };

  bool control(Command command)
  {
    auto start = [this]() {
      mState = State::STARTING;
      mRunner = std::thread(&ReadoutSession::run, this);
    };

    auto stop = [this]() {
      mState = State::STOPPING;
      mRunner.join();
    };

    auto checkValid = [](Command) {
      // TODO: implement the logic
      return true;
    };

    if (!checkValid(command)) {
      // invalid state for this command
      return false;
    }
    switch (command) {
      case Command::START:
        start();
        break;
      case Command::STOP:
        stop();
        break;
      case Command::TERMINATE:
        break;
      case Command::RESET:
        break;
    }
    return true;
  }

 private:
  void run()
  {
    if (mState != State::STARTING) {
      // this is the wrong state to start the run loop
      return;
    }
    mState = State::RUNNING;
    while (mState == State::RUNNING) {
      // the InputPolicy can contain a timer to control the data rate
      auto input = mInputPolicy();
      // note: pass the input by move to make sure that the same instance is used, not needed for the
      // return value and assignment as the compiler does automatically use move semantics by return value optimization
      auto processd = mFilterPolicy(std::move(input));
      // TODO: check if there needs to be a special handling for OutputPolicy in blocking mode and full
      // for the moment, we simply assume the policy does not return if it blocks
      mOutputPolicy(std::move(processd));
    }

    if (mState == State::STOPPING) {
      // some cleanup if necessary
      mState = State::IDLE;
    } else {
      // wrong state, do nothing and leave the state as is
    }
  }

  State mState = State::IDLE;
  std::thread mRunner;
  InputPolicy mInputPolicy;
  FilterPolicy mFilterPolicy;
  OutputPolicy mOutputPolicy;
};

} // namespace readout
} // namespace bpct
#endif // READOUTSESSION_H
