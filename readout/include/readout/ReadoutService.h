#ifndef READOUTSERVICE_H
#define READOUTSERVICE_H

// TODO: this is probably not an interface header and can be module internal

#include <fairmq/FairMQDevice.h>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>

namespace bpct
{
namespace readout
{

class ReadoutService : public FairMQDevice
{
 public:
  ReadoutService() = default;
  ~ReadoutService() = default;

  void Init() final;
  void PreRun() final;
  void PostRun() final;
  void Reset() final;
  bool ConditionalRun() final;

 private:
  std::unique_ptr<boost::asio::io_service> mIoService;
  std::unique_ptr<boost::asio::ip::tcp::socket> mSocket;
  bool mHasConnection = false;
  std::unique_ptr<std::thread> mConnectorThread;
};
} // namespace readout
} // namespace bpct

#endif //READOUTSERVICE_H
