/**
 * @file iComService.h
 * @author Alf K. Herland 
 * @date 21.10.2019.
 * @brief Interface class for the ComService that transmit and receive data
 *
*/

#ifndef PCT_BERGEN_READOUT_INCLUDE_READOUT_ICOMSERVICE_H_
#define PCT_BERGEN_READOUT_INCLUDE_READOUT_ICOMSERVICE_H_
#include <netinet/in.h>
#include <string>
#include <vector>
#include <array>
class IComService {
 private:
  int sock, n;
  int reuse;
  unsigned int sockLenght;
  char buffer[4096];
  struct sockaddr_in server, from;
  struct hostent *hp;
  struct timeval tv;

 public:
   virtual ~IComService() = default;
  /**
   * @brief Virtual function for transmit
   * @param Refrence to a vector of chars
   * @return Integer
   * @throw nullptr if used
   */
  virtual int transmit(std::vector<char> &) {
    throw nullptr;
  };
  /**
   * @brief Virtual function for receive
   * @param Refrence to a vector of chars
   * @return Integer
   * @throw nullptr if used
   */
  virtual int receive(std::vector<char> &) {
    throw nullptr;
  };
  /**
   * @brief Virtual function for closeSocket
   * @throw nullptr if used
   */
  virtual void closeSocket() {
    throw nullptr;
  };

  /**
   * @brief Virtual function for openSocket
   * @param String with the ip address to open
   * @param Short with the port number to open
   * @param integer with time in milli seconds for timeout
   * @return Integer
   * @throw nullptr if used
   */
  virtual int openSocket(std::string , short , int ) {
    throw nullptr;
  };


};
#endif //PCT_BERGEN_READOUT_INCLUDE_READOUT_ICOMSERVICE_H_
