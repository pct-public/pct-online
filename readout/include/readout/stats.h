/**
 * @file ${FILE}
 * @author Alf K. Herland 
 * @date 23.11.2019.
 * @brief
*/

#ifndef PCT_BERGEN_READOUT_INCLUDE_READOUT_STATS_H_
#define PCT_BERGEN_READOUT_INCLUDE_READOUT_STATS_H_
namespace bpct
{
namespace readout
{
struct pDTPClientStats {
  int numberOfPacketsSent = 0;
  size_t numberOfPacketsRecv = 0;
  size_t numberOfBytesReceived = 0;
  int numberOfPacketsWithData = 0;
  int numberOfErrors = 0;
  uint16_t packetIdRecved = 0;
  std::atomic<int> dataInQueue = 0;
  float bytesPrSec = 0;
  int missingPacketErrors = 0;
};

} // namespace readout
} // namespace bpct
#endif //PCT_BERGEN_READOUT_INCLUDE_READOUT_STATS_H_
