#ifndef PDTPWORKER_H
#define PDTPWORKER_H

/// @file pDTPWorker.h
/// @brief Encoder/Decoder worker for the pDTP protocol

#include "datamodel/pDTP-protocol.h"
#include "datamodel/pDTP-helpers.h"
#include <cstddef>
#include <vector>
#include <iostream>
#include <cassert>
#include <tuple>
#include <unistd.h> // sleep

namespace bpct
{
namespace readout
{

/// @class pDTPWorker
class pDTPWorker
{
 public:
  pDTPWorker() = default;
  ~pDTPWorker() = default;

  // we might want to define the return type of get() as template parameter later
  using ReturnT = std::vector<char>;

  size_t getExpectedServerReplySize(data_model::pDTPClientRequest const& request) const;

  // TODO: pass sender and reader by move or reference to avoid copy
  // request to read number of pRU words specified by length using specific operation
  template <typename Sender, typename Reader>
  ReturnT get(Sender sender, Reader reader, data_model::pDTPClientRequest::ClientOpcode operation, size_t length)
  {
    using pDTPClientRequest = data_model::pDTPClientRequest;
    using pDTPServerReply = data_model::pDTPServerReply;
    using ClientOpcode = pDTPClientRequest::ClientOpcode;
    using ServerOpcode = pDTPServerReply::ServerOpcode;
    using pDTPHelpers = data_model::pDTPHelpers;

    // read data from server
    // TODO: implement in separate thread for non-blocking and to allow abort
    auto server_read = [&reader](size_t len) {
      ReturnT rawtgt(len);
      size_t result = reader(rawtgt);
      if (result < rawtgt.size()) {
        rawtgt.resize(result);
      }
      if (rawtgt.size() >= sizeof(pDTPServerReply)) {
        pDTPServerReply& reply = *reinterpret_cast<pDTPServerReply*>(rawtgt.data());
        std::cout << reply << std::endl;
      }
      return rawtgt;
    };

    // send a request
    // TODO: more flaxible argument list
    auto send_request = [&sender, &server_read](ClientOpcode oc, size_t packetSize = 1, size_t nofPackets = 0, bool noAck = true) {
      std::vector<char> rawreq(sizeof(pDTPClientRequest));
      pDTPClientRequest& request = *reinterpret_cast<pDTPClientRequest*>(rawreq.data());
      request.opcode = static_cast<uint32_t>(oc);
      request.sizeofpacket = packetSize;
      // TODO: check where to do the swap, maybe we make the members private and implement
      // setters which can cover also the swap
      request.nofpackets = ((nofPackets & 0xff00) >> 8) + ((nofPackets & 0x00ff) << 8);
      request.no_ack = noAck;
      sender(rawreq);
      // return the request header so that the called can use this for further runtime adoption
      return request;
    };

    pDTPServerReply lastreply;
    enum struct ServerStatus {
      DOWN,
      LIMBO,
      RUNNING,
    };
    // request status of the server
    auto request_status = [&send_request, &server_read, this, &lastreply]() -> std::tuple<ServerStatus, size_t> {
      auto request = send_request(ClientOpcode::CLIENT_GS);
      size_t replySize = getExpectedServerReplySize(request);
      assert(replySize > 0);
      if (replySize == 0) {
        return {ServerStatus::DOWN, 0};
      }
      auto reply = server_read(replySize);
      if (reply.size() < sizeof(pDTPServerReply)) {
        // this means that there is probably no connection to the board
        return {ServerStatus::DOWN, 0};
      }
      pDTPServerReply& header = *reinterpret_cast<pDTPServerReply*>(reply.data());
      if (static_cast<ServerOpcode>(header.opcode) == ServerOpcode::SERVER_STATUS) {
        // FIXME: have to swap bytes, need to find a better and generic solution
        size_t fill_count = (size_t(header.fill_count & 0xff00) >> 8) + (size_t(header.fill_count & 0xff) << 8);
        if (header.raw != lastreply.raw) {
          std::cout << "Server (version " << header.version << "): status is "
                    << (header.full ? "full" : (header.almost_full ? "almost full" : "empty"))
                    << ": " << fill_count << " word(s) available"
                    << std::endl;
        }
        lastreply = header;
        return {ServerStatus::RUNNING, fill_count};
      }
      std::cout << "Invalid server reply to status request, got " << static_cast<pDTPHelpers::ServerStringID>(header.opcode) << std::endl;
      return {ServerStatus::LIMBO, 0};
    };

    [[maybe_unused]] auto request_single_read = [&sender, &server_read]() {
      std::vector<char> rawreq(sizeof(pDTPClientRequest));
      pDTPClientRequest& request = *reinterpret_cast<pDTPClientRequest*>(rawreq.data());
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_RQS);
      request.sizeofpacket = 1;
      request.nofpackets = 0x0100;
      request.no_ack = 1;
      std::cout << request << std::endl;
      sender(rawreq);
    };

    // send error
    [[maybe_unused]] auto send_error = [&sender]() {
      std::vector<char> rawreq(sizeof(pDTPClientRequest));
      pDTPClientRequest& request = *reinterpret_cast<pDTPClientRequest*>(rawreq.data());
      request.opcode = static_cast<uint32_t>(ClientOpcode::CLIENT_ERR);
      // TODO: allows other errors
      request.uninterpretable = 1;
    };

    // send acknowledge
    [[maybe_unused]] auto send_ack = [&send_request]() {
      [[maybe_unused]] auto request = send_request(ClientOpcode::CLIENT_ACK);
    };

    ServerStatus status = ServerStatus::DOWN;
    size_t words = 0;
    // TODO: implement timeout
    std::cout << "Connecting to server" << std::endl;
    do {
      std::tie(status, words) = request_status();
    } while (status != ServerStatus::RUNNING && words == 0 && (sleep(1) || true));
    if (length > words) {
      // limit to the available data
      std::cout << "warning: only " << words << " pRU words available, shrinking request" << std::endl;
      length = words;
    }

    // TODO: calculate packet size and number of packets for stream request
    auto header = send_request(operation, length, 0);
    //size_t byteLen = getExpectedServerReplySize(header);
    // TODO:
    // - consistency check of the server reply header
    // - stripping of the data
    // - assembly of stream data into contiguous buffer, maybe this is not necessary if we anyhow persue
    //   a streaming model in the processing pipeline
    // - Idea: pDTPData helper class which provides access methods to the different parts of the raw buffer
    return server_read(getExpectedServerReplySize(header));
  }

 private:
};

} // namespace readout
} // namespace bpct
#endif
