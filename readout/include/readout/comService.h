//
// Created by alf on 01.09.2019.
//
#pragma once

#ifndef ComService_H
#define ComService_H

#include <netinet/in.h>
#include <string>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "iComService.h"

class ComService : public IComService {
 private:
  // the maximum number to be read at once. The pDTP protocol supports up to
  // 255 pRU words of size 16; the maximum pDTP server reply is thus
  // 255 * 16 + 8 = 288
  static constexpr unsigned int RcvBufferSize = 4096;
  int mSock = -1;
  int mReuse = 1;
  unsigned int mSockLenght = 0;
  struct sockaddr_in mServer;
  // TODO: the counters seem to be unused
  int mRecvByte = 0;
  int mRecvNum = 0;
  int mTransByte = 0;
  int mTransNum = 0;

 public:
  ComService();
  ~ComService();
  int transmit(std::vector<char> &cmd);
  int receive(std::vector<char> &buf);
  int openSocket(std::string ip, short port, int timeo);
  void closeSocket();
};

#endif //ComService_H
