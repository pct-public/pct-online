#ifndef PRUFRAME_H
#define PRUFRAME_H

#include <vector>

#include "readout/pRUEnums.h"

namespace bpct {
namespace readout {

/*! \struct pRUframe
        \brief frame struct

        This struct is built up by a header word, data word and trailer word \n
        It also contains a bitset to identify different errors that has been
   discovered by pRUError functions.
*/
typedef struct pRUFrame {
  std::vector<unsigned char> words; /**< the binary pRU words*/

  // default constructor
  pRUFrame()
    : words()
    , alpide_data_bytes(0)
  {
  }

  // constructor from the header word
  pRUFrame(unsigned char const word[16])
    : words()
    , alpide_data_bytes(0)
  {
    append(word);
  }

  // append a word to the data buffer
  void append(unsigned char const word[16])
  {
    words.insert(words.end(), word, word + 16);
  }

  // stream a word into the data buffer
  friend auto operator>>(unsigned char const word[16], pRUFrame& frame)
  {
    frame.append(word);
    return word;
  }

  // stream data buffer to target
  friend auto& operator<<(std::vector<char>& target, pRUFrame const& frame)
  {
    target.insert(target.end(), frame.words.begin(), frame.words.end());
    return target;
  }

  unsigned long int alpide_data_bytes = 0;
} pRUFrame;

}  // namespace readout
}  // end namespace bpct

#endif  // PRUFRAME_H
