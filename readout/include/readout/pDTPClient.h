/// @file pDTPClient.h
/// @brief The worker class for the pDTP protocol readout client
/// FIXME: as this is more the worker class and state machine for the protocol
/// we should probably rename it

#ifndef PCT_BERGEN_PDTPCLIENT_H
#define PCT_BERGEN_PDTPCLIENT_H

#include <boost/lockfree/spsc_queue.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <shared_mutex>
#include "clientconfig.h"
#include "iComService.h"
#include "stats.h"
#include "datamodel/pDTP-helpers.h"
#include "datamodel/pDTP-protocol.h"

namespace bpct
{
namespace readout
{

/// @class pDTPClient
class pDTPClient
{
  using pDTPClientRequest = data_model::pDTPClientRequest;
  using pDTPServerReply = data_model::pDTPServerReply;
  using ClientOpcode = data_model::pDTPClientRequest::ClientOpcode;
  using ServerOpcode = data_model::pDTPServerReply::ServerOpcode;
  using pDTPHelpers = data_model::pDTPHelpers;

 private:
  std::atomic<bool> pollAgain = false;
  std::atomic<bool> exitProgram = false;
  std::atomic<bool> abortStreamSignal = false;
  std::atomic<bool> fullAuto = false;
  std::atomic<bool> semiAuto = false;

  std::chrono::high_resolution_clock nowTimePoint;
  std::chrono::high_resolution_clock stopTimePoint;
  std::chrono::system_clock::time_point startTime;
  size_t upTime;
  IComService* com;
  bpct::readout::ClientConfig conf;
  bpct::readout::pDTPClientStats stats;
  char bufferStatus = 'U';
  size_t getStreamSize(uint16_t nofpackets);
  ClientOpcode currentOpCode;
  boost::lockfree::spsc_queue<std::vector<char>> spscQueue{conf.spscQueueSize};
  bool isClientRunning = false;
  bool gotEOS = false;

 private:
  /**
   * @brief Check if the packet id is in sequence
   * @param packId
   * @return bool true if in seq, false if out of seq.
    */
  bool packetIdCheck(uint16_t packId);

 public:
  pDTPClient(ClientConfig confp, IComService* coms);
  ~pDTPClient() = default;
  /**
   * @brief Handle the transmissions of the requests to the PRU
   */
  void sendOpCodeToReadOut();

  /**
   * @brief Handle the receive of data from the PRU
   */
  void receiveDataFromReadOut();

  /**
   * @brief Set the configuration for the client.
   * @param conf a struct containing all the config parameters
   */
  void setConfigForClient(bpct::readout::ClientConfig pconf);

  /**
   * @brief Change the Opcode of a running client.
   * @param opCode a clientOpcode defined in the "datamodel/pDTP-protocol.h"
   */
  void setOpCode(std::string opCode);

  /**
   * @brief Change the Opcode of a running client.
   */
  void handleStates();

  /**
   * @brief Interface for the external signals to stop the client
   */
  void stopClient();

  /**
   * @brief Interface for the external threads to see if the client is running
   * @return An boolean indicating if the client is running.
   */
  bool isRunning();

  /**
   * @brief Get number of bytes received
   * @return An integer number with the bytes recevied by the client
   */

  size_t getNumberOfBytesReceived();

  /**
   * @brief Get number of packets with data received
   * @return An integer number of packets with data recevied by the client
   */

  size_t getNumberOfPackagesReceived();

  /**
   * @brief Get the errors that have accured in the client.
   * @return An integer of the number of errors tha that have accured in the
   * client.
   */
  int getErrorsReceived();

  /**
   * @brief Get the buffer status of the PRU
   * @return an char with the buffer status: F == FULL, A == ALMOST FULL, N ==
   * NOT EMPTY, E == EMPTY
   */
  auto getBufferStatus();

  /**
   * @brief Set the buffer status of the last packet received
   * @param pDTPServerReply
   */
  void setBufferStatus(pDTPServerReply& reply);

  /**
   * @brief Get the current running mode
   * @return A client op code
   */
  ClientOpcode getCurrentMode();

  /**
   * @brief Get data from the queue
   * @param ref to a vector of chars
   * @return bool if pop is successful
   */
  bool getDataFromQueue(std::vector<char>& buf);

  /**
   * @brief Get the time the client has been running
   * @return a int with the time in milli seconds.
   */
  size_t getUpTime();

  /**
   * @brief Check if there are more data in the queue
   * @return bool that is true if there are more data in the queue
   * False if it is empty.
   */
  bool hasDataInQueue();

  /**
   * @breif Check to see how many elements that is in the queue.
   * @return Returns the number of elements that is in the queue.
   */
  int numberOfElementsInQueue();

  /**
  * @brief Get megabytes pr second recv
  * @return float with the number of megabytes received in one second
  */
  float getBytesPrSec();

  /**
   * @brief Get the id of the last packet recved
   * @return uint16 containing the id of the last packet recved
   */
  uint16_t getPacketIdRecved();

  /**
   * @brief Gets the missing packet error value
   * @return int
   */
  int getMissingPacketErrors();

  /**
 * @brief Reads the config file and sets the struct with the data for the client
 * @param argc integer, number of arguments sent
 * @param argv pointer to a char array with the arguments
 * @return returns true if settings are set
 */
  bool readConfig(int argc, char* argv[]);
};

} // namespace readout
} // namespace bpct

#endif // PCT_BERGEN_PDTPCLIENT_H
