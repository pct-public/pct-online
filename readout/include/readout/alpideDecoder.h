/// @file   alpideDecoder.h
/// @author Øistein Jelmert Skjolddal
/// @since  2020-02-22
/// @brief  An instance of a hit coordinate decoder.

#ifndef ALPIDEDECODER_H
#define ALPIDEDECODER_H

#include <vector>
#include <chrono>  // Benchmarking

#include "datamodel/AlpideHit.h"
#include "datamodel/alpide_format.h"
#include "datamodel/pRU-format.h"
#include "datamodel/RUDataHeader.h"

namespace bpct {
namespace readout {

/*! 
  \class alpideDecoder.
  \brief Converts sorted pRU frames.
    This class will take a list of sorted pRU words and return a list of.
*/
class alpideDecoder {
 private:
  unsigned long long mFramesReceived = 0; /*!< number pRU frames received so far */

  // Mask
  //constexpr unsigned char pRUTypeMask = {0xC0}; //  Maks the two most significant bits 0b1100'0000
  const unsigned char pRUTypeMask = {0xC0}; //  Maks the two most significant bits 0b1100'0000

  //! Extracts the x and y hit coordinates from a alpide hit (data long or data short).
  /*!
    \param the data short structure data & data long hit_map(0 in case of data short) and readout region.
    \return the x,y hit coordinate pair.
  */
  std::vector<std::pair<int,int>> DecodeDataWord(uint16_t const& data, 
  unsigned char const& hit_map, uint8_t const& region);
  
  //! Extracts the x hit coordinates.
  /*!
    \param the alpide readout region, double coullumn and the hit address.
    \return the x coordinate for an hit on an alpide chip.
  */
  int AddressToColumn(int const& ARegion, int const& ADoubleCol, int const& AAddress);
  
  //! Extracts the y coordinates.
  /*!
    \param the hit address.
    \return the y coordinate for an hit on an alpide chip.
  */
  int AddressToRow(int const& AAddress);

  void printPRUWord(std::vector<char>::iterator const& it);
 public:
  alpideDecoder();

  ~alpideDecoder();

  //! Decoder
  /*!
    \param  Vector of sorted and checked binary data in character form.
    \brief Extracts the hit posittion from the alpide words and
      converts them in to x,y coordinates on the alpide chip.
    \return A vector of extracted alpide coordinate data in the RUDataHeaderVec format.
  */
  std::vector<char> convert(std::vector<char>&& data);

  //! Inspect a vector of pRU data.
  /*!
    \param  Vector of binary pRU data in character form.
    \brief prints the binary information, nicely formated, to terminal for inspection.
    \return None.
  */
  void inspect(std::vector<char> data);

  //! Get the number of alpide hits registered so far.
  /*!
    \param  none.
    \brief the number of registerd hits on all alpide chips over all staves and all layers.
    \return the number of registered hits.
  */
  int get_number_of_alpide_hits() { return mFramesReceived; }
};

}  // end namespace readout
}  // end namespace bpct

#endif  // ALPIDEDECODER_H
