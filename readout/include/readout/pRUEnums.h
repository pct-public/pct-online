#ifndef PRUENUMS_H
#define PRUENUMS_H

namespace bpct {
namespace readout {

/*! Enum with two different readout modes.\n
Used to identify alpide mode in the #pRUTagHeaderWord::mode field */
enum ALPIDE_MODE { TRIGGERED, CONTINUOUS };

/*! Enum with three different trigger sources.\n
Used to identify trigger source in the #pRUTagHeaderWord::trigger_source field
*/
enum TRIGGER_SOURCE {
  ALPIDE_INTERNAL_STROBE_SEQUENCER = 0x0,
  EXTERNAL_PRU_HARDWARE_SIGNAL = 0x1,
  SOFTWARE_TRIGGER = 0x2
};

/*! Enum with eight possible error flags.\n
Used to identify flags found in the #pRUTagTrailerWord::error_flags field*/
enum TRAILER_ERROR_FLAGS {
  /*!< 0 */ DECODE_ERROR = 0,
  /*!< 1 */ FRAME_ERROR,
  /*!< 2 */ EMPTY_REGION_ERROR,
  /*!< 3 */ DOUBLE_BUSY_ON_ERROR ,
  /*!< 4 */ DOUBLE_BUSY_OFF_ERROR,
  /*!< 5 */ BUFFER_OVERFLOW_ERROR,
  /*!< 6 */ MAX_SIZE_ERROR,
  /*!< 7 */ MAX_WAIT_TIME_ERROR 
};

/*! Number of ellements in PARSE_ERROR */
static const int NUMBER_OF_ERRORS = 14;

/*! Enum with all errors that can occure during parsing and error checking */
typedef enum PARSE_ERROR {
  //pRU Errors
  INPUT_SIZE_ERROR = 0,   // Input byte size is not mod 16 /**< data is droped, no alternative behaviour*/
  /**< Delimiter or empty word. Data is stored seperately, no alternative behaviour*/
  MULTIPLE_PRU_HEADER_ERROR, // More than on header from same chip  /**< data is stored seperately, no alternative behaviour*/
  DATA_WORD_MISSING_HEADER_ERROR, // Could not find the header to this data word /**< data is stored seperately, no alternative behaviour*/
  TRAILER_WORD_MISSING_HEADER_ERROR,  // Could not find the header to this trailer /**< data is stored seperately, no alternative behaviour*/
  PRU_TRAILER_FLAGS_ERROR,  // Error flags set in the trailer /**< data is stored seperately, no alternative behaviour*/
  NO_DATA_WORD_ERROR, // This fame had no data /**< data is stored seperately, no alternative behaviour*/
  FRAME_SIZE_ERROR, // Specified size does not match recived data /**< data is stored seperately, no alternative behaviour*/
  
  //Alpide errors
  MISSING_CHIP_HEADER_ERROR, // The alpide chip header is either missing completely or not in correct possition /**< data is stored seperately, no alternative behaviour*/
  MISSING_REGION_HEADER_ERROR, // The first alpide region header is either missing completely or not in correct possition /**< data is stored seperately, no alternative behaviour*/
  MULTIPLE_CHIP_HEADER_ERROR, // More than one alpide chip header detected /**< data is stored seperately, no alternative behaviour*/
  MISSING_CHIP_TRAILER_ERROR, // Could not find the alpide chip trailer word /**< data is stored seperately, no alternative behaviour*/
  ALPIDE_TRAILER_FLAGS_ERROR, // Error flags were set in the alpide chip trailer
  PADDING_ERROR,  // Something not pading found after chip trailer /**< data is stored seperately, no alternative behaviour*/
  UNKNOWN_ALPIDE_WORD_ERROR // Could not recognice alpide word /**< data is stored seperately, no alternative behaviour*/
  
  /* TODO: CURRENT UNHANDLED ERRORS:
  Multiple region headers
    -> region header with no data (//REGION_HEADER_WITHOUT_DATA_ERROR,)
    -> same region header twice
  pRU chip id != alpide header chip id
  */
} PARSE_ERROR;

}  // namespace readout
}  // end namespace bpct
#endif  // PRUENUMS_H