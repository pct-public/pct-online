
#ifndef CLIENTCONFIG_H
#define CLIENTCONFIG_H

#include<string>

namespace bpct {
namespace readout {


struct ClientConfig {
  short portNumber = 0;
  /// The size of the stream in RQS mode.
  int streamSize = 0;
  /// The size of packets in bytes to be requested from the PRU.
  unsigned short packageSize = 0;
  /// The time in seconds that you want the client to run.
  /// 0 to disable.
  int clientRunTime = 0;
  ///The UDP timeout setting in milli seconds. 0 to disable.
  int clientUDPTimeout = 0;
  /// Set a request with a spesific number of words before
  /// the client aborts and exits.
  int requestNumberOfWords = 0;
  ///Set the level of log messages to be writen to console.
  int verbosity = 0;
  ///Set the size of the SPSC buffer queue.
  size_t spscQueueSize = 10;
  ///Enable or disable the file writer output.
  int fileWriterEnabled = 0;
  ///To poll the PRU again if client receives server error
  /// with buffer empty flag.
  int pollMode = 0;
  ///IP address of the PRU.
  std::string ipAddress = "";
  ///What OpCode the client should start with.
  std::string startUpMode = "";
  ///How to handle the buffer fill level, 0 = no RQS,RQFS, 1 = RQS, no RQFS or
  ///2 = RQS and RQFS.
  int handleBufferStatus = 0;
  /// Server will not wait for buffer to be filled with data if buffer is empty or is filled with less
  ///than MIN_RQ. Result is EOS in both RQS and RQFS.
  int noWaitFlag = 0;
  /// Maximize the number of pRU words transmitted, based on the data available. Can be
  /// used together with MIN_RQ.
  int maximizeFlag = 0;
  /// Turn on timestamping of files or just give the file "aFile.bin"
  int timeStampFile = 0;
};


}  // namespace readout

}  // namespace bpct

#endif  // PCT_BERGEN_CLIENTCONFIG_H