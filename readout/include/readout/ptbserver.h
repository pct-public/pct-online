//
// Created by wac002 on 3/7/20.
//

#ifndef PCT_BERGEN_PTBSERVER_H
#define PCT_BERGEN_PTBSERVER_H
#include <vector>
#include <boost/asio.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <thread>
#include <iostream>

namespace bpct
{
namespace readout
{

/// @class pTBClient
class pTBServer
{
 public:
  pTBServer(boost::asio::io_context& io_context,
            const boost::asio::ip::tcp::tcp::endpoint& endpoint, boost::asio::ip::tcp::socket socket);
  ~pTBServer() = default;

  enum class State {
    IDLE,
    STARTING,
    RUNNING,
    STOPPING,
    ERROR,
  };

  enum struct Command {
    START,
    STOP,
    TERMINATE,
    RESET,
  };

  bool control(Command command);

  std::vector<char> getData();

  std::atomic<bool> isRunning();

 private:
  boost::lockfree::spsc_queue<std::vector<char>> spscQueue{100};
  State mState = State::IDLE;
  std::thread mRunner;
  boost::asio::io_context io_context;
  boost::asio::ip::tcp::socket socket;
  boost::asio::ip::tcp::acceptor acceptor;
  boost::asio::ip::tcp::endpoint endpoint;
  boost::system::error_code ignored_error;
  void run();
};
} // namespace readout
} // namespace bpct

#endif //PCT_BERGEN_PTBSERVER_H
