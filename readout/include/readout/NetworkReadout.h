#ifndef NETWORKREADOUT_H
#define NETWORKREADOUT_H

#include "network/NetworkSession.h"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <cstddef> // size_t
#include <memory>
#include <chrono>
#include <iostream> // TODO: temporary for some logging, to be removed
#include <stdexcept>
#include <boost/asio/io_service.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/read.hpp>

namespace bpct
{
namespace readout
{

/// @class NetworkReadout
/// @brief readout server input policy using network
/// Function object to be invoked by the host class, returning allocated
/// buffer by move/return type optimization
template <typename T, typename Protocol>
class NetworkReadout
{
 public:
  using SessionType = network::NetworkSession<Protocol>;

  NetworkReadout() = delete;
  NetworkReadout(T&& templateBuffer, int port = 0)
    : mTemplateBuffer(std::move(templateBuffer))
    , mPort(port)
  {
  }

  // need move constructor in order to be used as a policy for the ReadoutServer
  NetworkReadout(NetworkReadout<T, Protocol>&& other)
    : mListenerExecutor(std::move(other.mListenerExecutor))
    , mTemplateBuffer(std::move(other.mTemplateBuffer))
    , mSession(std::move(other.mSession))
    , mState(other.mState)
    , mPort(other.mPort)
  {
    // we can actually not pass a preinitialized object by move contruct as the
    // thread resources might be invalid, have to check in more detail
    if (mState != State::UNDEFINED || mSession) {
      throw std::logic_error("NetworkReadout must not be started");
    }
  }
  ~NetworkReadout()
  {
  }

  enum struct State {
    UNDEFINED,
    LISTENING,
    CONNECTED,
    TERMINATING,
  };

  /// start listening
  bool start(int port)
  {
    auto listener = [this, port]() {
      while (mState != State::TERMINATING) {
        // delete an existing session
        this->mSession.reset();
        // create a new session
        this->mSession = std::make_unique<SessionType>();

        auto notifier = [this]() {
          this->mState = State::CONNECTED;
          this->mHaveConnection.notify_all();
        };

        if (mSession->open(port, notifier)) {
          std::unique_lock<decltype(mMutex)> lock(this->mMutex);
          // we have an active session, wait for the connection to be closed
          mIdleListening.wait(lock);
          // delete the existing session and start listening again
          this->mSession.reset();
        } else {
          break;
        }
      }
    };
    std::cout << "listening for connection at port " << port << std::endl;
    mState = State::LISTENING;
    mListenerExecutor = std::thread(listener);
    return true;
  }

  bool stop()
  {
    // flush a pending read operation by shutting down the socket
    if (mSession) {
      mSession->close();
    }
    mState = State::TERMINATING;
    mIdleListening.notify_all();
    mListenerExecutor.join();
    mHaveConnection.notify_all();

    return true;
  }

  /// function object entry point
  T operator()()
  {
    if (mState == State::UNDEFINED) {
      // start the listener for the port specified at constructor level
      start(mPort);
    }
    T buffer(mTemplateBuffer.size());
    boost::system::error_code error;
    {
      std::unique_lock<decltype(mMutex)> lock(mMutex);
      //if (!lock.try_lock()) {
      //  // TODO: probably need to check if we end up here repetitively
      //  return buffer;
      //}
      // wait for the connection if not yet connected
      if (mState == State::LISTENING) {
        // the lock object has locked the mutex, but the other threads do not attempt to lock
        // it before  sending notification on mHaveConnection
        // TODO: wait_for to implement timeout
        mHaveConnection.wait(lock);
        if (mState != State::CONNECTED) {
          // not connected, skipping
          return T();
        }
      }
      // lock gets out of scope and mutex is unlocked
    }
    if (!mSession) {
      return T();
    }
    auto& socket = mSession->getSocket();
    // TODO: check timeout possibility
    size_t len = boost::asio::read(socket, boost::asio::buffer(buffer.data(), buffer.size() * sizeof(typename T::value_type)), error);
    // TODO: find a generic way for handling the readout length, resizing the buffer is not
    // supported by e.g. std::array
    buffer.resize(len / sizeof(typename T::value_type) + (len % sizeof(typename T::value_type) > 0 ? 1 : 0));

    if (error == boost::asio::error::eof) {
      // TODO: the connection is closed, default mode is to activate listening again, but
      // the actual mode should be implemented
      std::unique_lock<decltype(mMutex)> lock(mMutex);
      //if (!lock.try_lock()) {
      //  // TODO: have to study this in detail
      //  throw std::runtime_error("unhandled race condition");
      //}
      mState = State::LISTENING;
      mIdleListening.notify_all();
    }

    // with return value optimization, the compiler will use the content of the buffer
    // directly as long as the type is move assignable
    return buffer;
  }

 private:
  /// the data receiver thread waits until the listener has a connection
  std::condition_variable mHaveConnection;
  /// the listener goes to idle once found an active connection
  std::condition_variable mIdleListening;
  /// listener thread
  std::thread mListenerExecutor;
  /// mutex to be used together with the condition variables
  std::mutex mMutex;
  /// the template for the construction of the readout buffer
  T mTemplateBuffer;
  /// session instance
  std::unique_ptr<network::NetworkSession<Protocol>> mSession;
  /// state of the class
  State mState = State::UNDEFINED;
  /// port number
  int mPort = 0;
};
} // namespace readout
} // namespace bpct

#endif // NETWORKREADOUT_H
