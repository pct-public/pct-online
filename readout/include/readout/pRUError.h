
#ifndef PRUERROR_H
#define PRUERROR_H

#include <list>

#include "readout/pRUFrame.h"

namespace bpct {
namespace readout {

struct pRUFrame;

typedef std::list<pRUFrame> frame_list;

/*! \class pRUError
        \brief Checks for errors with the format in incomplete frames

        Uses a bitset to keep track of what errors are found for any given
   word.\n Checks if there are any data words\n Checks if there was no trailer
   word recieved.\n Checks for consecutive headers from a single chip
*/
class pRUError {
 public:
  pRUError();
  ~pRUError();

  //! Checks for consecutive headers from one chip
  /*!
   * \param   temp_frames list of frames that are not completed
   * \param   chip_id the id of the chip whose word we recieved from the parser
   * \return  either an iterator to the object that is errorful or
   * temp_frames.end() should the frame be healthy
   */
  static frame_list::const_iterator check_existing_header(frame_list const& temp_frames,
                                                    unsigned char const& typeAndRU, unsigned char const& staveAndChipID);

  //! Counts number of bytes that lies in the data word fields and compare to
  //! frame size in trailer word field
  /*! \param  temp_frames list of frames that are not completed
   *  \param  chip_id the id of the chip whose word we recieved from the parser
   *  \param  frame_size the frame_size as given to us by the trailer word
   *  \return either an iterator to the object that is errorful or
   *temp_frames.end() should the frame be healthy \note   padded fields after 8
   *bit trailer word are not counted (0xFF)
   **/
  static bool check_frame_size(pRUFrame const& frame);

  //! Checks if any data word has been recieved for the frame
  /*!
   * \param   temp_frames list of frames that are not completed
   * \param   chip_id the id of the chip whose word we recieved from the parser
   * \return  either an iterator to the object that is errorful or
   * temp_frames.end() should the frame be healthy
   */
  static bool check_missing_data(pRUFrame const& frame);

  static unsigned int check_padding_error(std::vector<unsigned char>::const_iterator& cit_fields,
                                          std::vector<unsigned char>::const_iterator const& cit_end);

  static bool check_trailer_flags(pRUFrame const& frame);

};

}  // namespace readout
}  // end namespace bpct

#endif  // PRUERROR_H
