//
// Created by alf on 01.09.2019.
//
#include "readout/comService.h"
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

ComService::ComService()
{
}

ComService::~ComService()
{
}

int ComService::openSocket(std::string ip, short port, int timeo)
{
  mReuse = 1;
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) {
    perror("error creating socket");
    return -1;
  }
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const char*)&mReuse,
                 sizeof(mReuse)) < 0) {
    perror("Set sock reuse failed");
  }
  int sendbuff = 2040 * 1024 * 1024;

  printf("sets the send buffer to %d\n", sendbuff);
  int res =
      setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &sendbuff, sizeof(sendbuff));
  if (res == -1) printf("Error setsockopt");

  if (timeo > 0) {
    struct timeval tv;
    tv.tv_sec = timeo;
    tv.tv_usec = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
      perror("Error setting timeout");
    }
  }
  mServer.sin_family = AF_INET;
  struct hostent* hp = gethostbyname(ip.c_str());
  if (hp == nullptr) {
    // that is a fatal error
    perror("Unknown host");
    close(sock);
    return -1;
  }

  bcopy((char*)hp->h_addr, (char*)&mServer.sin_addr, hp->h_length);
  mServer.sin_port = htons(port);
  mSockLenght = sizeof(struct sockaddr_in);
  mSock = sock;
  return 1;
}

int ComService::transmit(std::vector<char>& cmd)
{
  if (mSock < 0) {
    perror("Socket is not open");
    return -1;
  }

  // std::cout << "base\n";
  mTransNum++;
  mTransByte += sizeof(cmd);
  int nsent = sendto(mSock, cmd.data(), 4, 0, (const struct sockaddr*)&mServer,
                     mSockLenght);
  if (nsent < 0)
    perror("Com client Sendto");
  // std::cout << "base1\n";
  return 1;
}

int ComService::receive(std::vector<char>& buf)
{
  if (mSock < 0) {
    perror("Socket is not open");
    return -1;
  }

  buf.resize(RcvBufferSize);
  struct sockaddr_in from;
  int nrecv = recvfrom(mSock, buf.data(), buf.size(), 0, (struct sockaddr*)&from,
                       &mSockLenght);
  if (nrecv < 0) {
    int errosv = errno;
    if(errosv == EWOULDBLOCK || errosv == EAGAIN){ //Check if the error is a timeout
      mRecvNum++;
      return -2;
    }
    std::cout << nrecv << " error in recv\n";
    perror("error in recvfrom");
    // close(sock);
  } else {
    buf.resize(nrecv);
    // std::cout << "Bytes recv: " << n << std::endl;
    mRecvByte += nrecv;
  }
  mRecvNum++;
  return nrecv;
}

void ComService::closeSocket()
{
  if (mSock < 0) {
    return;
  }
  close(mSock);
}
