/// @file   run-ptb-pru-parser.cxx
/// @author Alf K. Herland
/// @date   2020-07-03
/// @brief  Readout application for the pCT Test Box (pTB)
///
/// The pCT Test Box (pTB) is a dedicated hardware solution and embedded system
/// for testing ALPIDE chips.
///
/// This application implements a server which the pTB can connect to for data
/// offload. pRU words are sent without further encapsulation into a protocol.
/// Because the embedded system of the pTB sends pRU words in reverse byte order with
/// respect to hardware devices using pDTP protocol, the server also inverts byte order.
///
/// For further processing, data can be written to file in binary and alphanumeric
/// format, or processed by the pRUParser with subsequent export to a ROOT tree object
/// stored in a ROOT file.
///
/// Note: binary pRU data are stored in big endian format, which is also the native
/// endianness of the pDTP protocol.

#include "readout/ReadoutSession.h"
#include "readout/pRUParser.h"
#include "readout/alpideDecoder.h"
#include "readout/ptbserver.h"
#include "rootio/pRUExporter.h"
#include "io-adaptors/FileFormat.h"
#include "io-adaptors/Alphanumeric.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <string>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/asio.hpp>

namespace bpo = boost::program_options;

using namespace bpct; // the Bergen proton CT project

int main(int argc, char* argv[])
{
  // definition of the available command line options
  // the default output file name is created from time stamp
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream ofileDefault;
  ofileDefault << std::put_time(&tm, "%d-%m-%Y-%H-%M-%S");
  const char* odirDefault = "../results";

  // note: because of some shortcoming in boost program option we can not set a default
  // value in the option description
  constexpr io::FileFormat defaultOutputFormat = io::FileFormat::ROOT;
  std::string outputModeHelp = "output format:";
  for (auto const& id : io::FileFormatIDs) {
    std::stringstream option;
    option << " " << id << (id == defaultOutputFormat ? " (default)" : "") << ",";
    outputModeHelp += option.str();
  }
  outputModeHelp.pop_back();

  // clang-format off
  boost::program_options::options_description od{
    "Readout application for the pCT Test Board\n"
    "(c) 2020 Bergen pCT Collaboration\n\n"
    "Usage:\n"
    "  ptb-readout [OPTION]\n\n"
    "Note: the input policy is not configurable at the moment\n"
    "Available options"
  };
  od.add_options()("ipolicy", bpo::value<int>(), "input policy, args: file=1 or network=2")
    ("ifile,i", bpo::value<std::string>(), "input file name")
    ("ofile,o", bpo::value<std::string>()->default_value(ofileDefault.str()), "output file name\n(timestamp-generated default)")
    ("output-mode", bpo::value<io::FileFormat>(), outputModeHelp.c_str())
    ("odir", bpo::value<std::string>()->default_value(odirDefault), "directory for output file")
    ("verbosity,v", bpo::value<int>()->default_value(1), "verbosity level")
    ("port,p", bpo::value<int>()->default_value(29070), "server port")
    ("treename", bpo::value<std::string>()->default_value("alpide_pixel"), "tree name in the ROOT output file")
    ("treetitle", bpo::value<std::string>()->default_value("Alpide pixel data"), "tree title")
    ("help,h", "Hilfe!");
  // clang-format on

  std::stringstream cmdLineError;
  auto printhelp = [&cmdLineError, &od](int exitvalue = 0) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(exitvalue);
  };
  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error parsing command line arguments: " << e.what() << std::endl
              << std::endl;
    printhelp(1);
  }
  if (varmap.count("help") > 0) {
    printhelp();
  }
  if (varmap.count("ipolicy") > 0 || varmap.count("ifile") > 0) {
    cmdLineError << "Input policy is fixed to network server at the moment,\ndo not use options '--ipolicy', '--ifile'";
    printhelp();
  }
  // now we can check if all required parameters are available
  auto outputMode = defaultOutputFormat;
  if (varmap.count("output-mode") > 0) {
    outputMode = varmap["output-mode"].as<io::FileFormat>();
  }
  auto ofilename = varmap["ofile"].as<std::string>();
  auto odirectory = varmap["odir"].as<std::string>();
  auto verbosity = varmap["verbosity"].as<int>();
  auto port = varmap["port"].as<int>(); //The port the ptb box will connect to on the pc.
  auto treename = varmap["treename"].as<std::string>();
  auto treetitle = varmap["treetitle"].as<std::string>();

  // Define our processing pipeline with input, filter and output policies
  // The basic buffer used for data exchange between the policies is std::vector<char>
  // InputPolicy: a lambda function reading from file
  // ForwardPolicy: the pRUParser
  // OutputPolicy: an std function taking a buffer by move
  //
  // Note that generic functions are used as forward and output policies instead of specific classes.
  // this allows to use a simple lambda function as policy.
  using BufferT = std::vector<char>;

  /// We are using the ReadoutSession controller to define a processing pipeline
  /// serving the three policies input, filter, and output.
  using Processor = readout::ReadoutSession<std::function<BufferT()>,
                                            std::function<BufferT(BufferT &&)>,
                                            std::function<void(BufferT &&)>>;

  /**
   * Input policy : get data from the network server for PTB via a SPSC queue
   */
  // FIXME: move the io context to the server, so that we initialize with the port number only
  // FIXME 2: pass verbosity to server
  boost::asio::io_context io_context;
  boost::asio::ip::tcp::socket socket(io_context);
  readout::pTBServer ptb(io_context,
                         boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port),
                         boost::asio::ip::tcp::socket(io_context));
  if (verbosity > 0) {
    std::cout << "sending start command to server, using port " << port << std::endl;
  }
  ptb.control(readout::pTBServer::Command::START);

  // Policy for retrieving data from the PTB server
  auto ptbData = [&ptb, verbosity]() {
    return std::move(ptb.getData());
  };
  // processing policy: a simple forward filter
  // this is a lambda function, it is defined in the current scope (i.e. the current function)
  // and has access to the function variables if specified in the catch-clause []
  // the function parameter definition is as usual, the return type of the lambda is either
  // deduced automatically or specified with the '->' syntax.
  // this lambda function is a simple forward, we can later on optimize the Networker class
  // to simply drop to step if a void forward policy is specified.
  //
  // FIXME: the parser object needs to be created and passed to the lambda function, it follows
  // the following schema:
  // 1. we need the parser implementation, this is a type definition
  //using Parser = int; // replace with correct type, e.g. pRUParser
  // 2. we create the object of the parser
  //Parser parser;
  readout::pRUParser parser = readout::pRUParser();

  // 3. we define a lambda function which can use this parser
  // Note: the lambda function can be dropped later by implementing the operator() on the parser
  auto pRUFilter = [&parser, verbosity](BufferT&& buffer) -> BufferT {
    if (verbosity > 4) {
      std::cout << "Filter policy [pRU parser]: buffer of " << buffer.size() << " byte(s)" << std::endl;
    }
    return std::move(parser.parse_pRU_data(std::move(buffer)));
    //return parser.parse_pRU_data(std::move(buffer)); // changed to allow compiler RVO
  };

  // simple forward policy used with the binary file writer
  auto forwardPolicy = [verbosity](BufferT&& buffer) -> BufferT {
    if (verbosity > 4) {
      std::cout << "Filter policy [pRU parser]: buffer of " << buffer.size() << " byte(s)" << std::endl;
    }
    return std::move(buffer);
  };

  std::mutex mutex;
  std::condition_variable readyToQuit;

  if (!odirectory.empty() && odirectory.rfind('/') != odirectory.length() - 1) {
    // append if not ending with a slash
    odirectory += "/";
  }
  ofilename.insert(0, odirectory);
  if (verbosity > 0) {
    std::cout << "Output target: " << ofilename << "\n";
  }

  /**
   *  Thread that listens for commands from another process
   */
  std::thread cmdThread([&readyToQuit, &mutex, &ptb, verbosity]() {
    // command line: echo quit | nc -uUN /tmp/PCTDOMAINSOCK
    if (verbosity > 1) {
      std::cout << "starting cmd thread" << std::endl;
    }
    boost::asio::io_context my_io_context;
    ::unlink("/tmp/PCTDOMAINSOCK");                                           // Remove previous binding.
    boost::asio::local::datagram_protocol::endpoint ep("/tmp/PCTDOMAINSOCK"); //Create endpoint to a local domain socket
    boost::asio::local::datagram_protocol::socket socket(my_io_context, ep);  //Set context and endpoint, opens socket

    char buffer[1024];
    boost::system::error_code ignored_error;
    [[maybe_unused]] auto length = socket.receive_from(boost::asio::buffer(buffer), ep);
    readyToQuit.notify_all();
    socket.close();
    ::unlink("/tmp/PCTDOMAINSOCK"); // Remove binding so that if another user uses the application there
    // wont be any lingering files related to the socket.
  });

  /**
   * Start the processor
   */
  auto runPipeline = [&readyToQuit, &mutex, &ptb](auto& pipeline) {
    pipeline.control(Processor::Command::START);
    std::unique_lock<std::mutex> lock(mutex);
    readyToQuit.wait(lock);
    ptb.control(readout::pTBServer::Command::STOP);
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    pipeline.control(Processor::Command::STOP);
  };

  if (verbosity > 4) {
    std::cout << "Output mode: " << outputMode << std::endl;
  }
  if (outputMode == io::FileFormat::BINARY) {
    ofilename.append(".bin");
    std::ofstream ofile(ofilename);
    if (!ofile.good()) {
      throw std::runtime_error("can not open file " + ofilename + " for writing output");
    }
    // output policy: write to file
    auto fileWriter = [&ofilename, &ofile, verbosity](BufferT&& buffer) {
      if (buffer.size() > 0) {
        ofile.write(reinterpret_cast<const char*>(buffer.data()), buffer.size() * sizeof(typename BufferT::value_type));
        if (verbosity > 4) {
          std::cout << "Output policy [File writer]: write to file " << ofilename << " size " << buffer.size() << std::endl;
        }
      }
    };

    Processor pipeline(ptbData, forwardPolicy, fileWriter);
    if (verbosity > 0) {
      std::cout << "Starting pipeline with forward and binary filewriter to " << ofilename << std::endl;
    }
    runPipeline(pipeline);
    ofile.flush();
    ofile.close();
  } else if (outputMode == io::FileFormat::ROOT) {
    /**
     * Policy for writing root files
     */
    // Decoder extracts the alpide coordinate data
    readout::alpideDecoder decoder = readout::alpideDecoder();
    // TODO: Separate root functionality from rest of the system.
    rootio::pRUExporter exporter = rootio::pRUExporter(ofilename, treename, treetitle);

    auto rootWriter = [&ofilename, &decoder, &exporter, verbosity](BufferT&& buffer) {
      if (buffer.size() > 0) {
        exporter.writeToTree(decoder.convert(std::move(buffer)));
        if (verbosity > 4) {
          std::cout << "Output policy  [Root tree Writer]: write to file " << ofilename << ".root" << std::endl;
        }
      }
    };

    Processor pipeline(ptbData, pRUFilter, rootWriter);
    if (verbosity > 0) {
      std::cout << "Starting pipeline with parser and root writer... " << std::endl;
    }
    runPipeline(pipeline);
    parser.print_errors();
  } else if (outputMode == io::FileFormat::ASCII) {
    ofilename.append(".txt");
    std::ofstream ostream(ofilename);
    if (!ostream.good()) {
      throw std::runtime_error("can not open file " + ofilename + " for writing output");
    }
    // output policy: write to file
    auto fileWriter = [&ofilename, &ostream, verbosity](BufferT&& buffer) {
      if (buffer.size() == 0) {
        return;
      }
      if (verbosity > 2) {
        std::cout << "Writing buffer of size " << buffer.size() << std::endl;
      }

      ostream << io::Alphanumeric(buffer, 16);
    };
    Processor pipeline(ptbData, forwardPolicy, fileWriter);
    if (verbosity > 0) {
      std::cout << "Starting pipeline with forward and ascii filewriter to " << ofilename << std::endl;
    }
    runPipeline(pipeline);
    ostream.flush();
    ostream.close();
  } else {
    std::stringstream msg;
    msg << "Output mode '" << outputMode << "' not yet supported";
    throw std::runtime_error(msg.str());
  }

  // Gives a summary of errors found.

  cmdThread.join();
  std::cout << "End of program... " << std::endl;
  return 0;
}
