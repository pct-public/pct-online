/// @file   ptb-file-publisher.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-14
/// @brief  Client implementation to publish data to the pTB readout
///
/// This client can be used to connect to the pTB readout server instead of
/// the real hardware and publish test files.

#include "io-adaptors/FileFormat.h"
#include "io-adaptors/ASCIINumberReader.h"
#include "util/logging-tools.h"
#include "network/NetworkSession.h"
#include <initializer_list>
#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/asio.hpp>
#include <boost/endian/conversion.hpp>

namespace bpo = boost::program_options;

using namespace bpct; // the Bergen proton CT project

int main(int argc, char* argv[])
{
  // clang-format off
  boost::program_options::options_description od{
    "Test client for pCT Test Board (pTB) readout\n"
    "(c) 2020 Bergen pCT Collaboration\n\n"
    "Usage:\n"
    "  ptb-file-publisher --ifile inputfile [--port portnumber]\n\n"
    "By default, byte order of binary data words is expected to be big endian, and is\n"
    "reversed before sending to match little endian byte order of pTB data offload.\n\n"
    "Available options"
  };

  // note: because of some shortcoming in boost program option we can not set a default
  // value in the option description
  const io::FileFormat defaultInputFormat = io::FileFormat::BINARY;
  const std::initializer_list<io::FileFormat> supportedFileFormats{io::FileFormat::BINARY, io::FileFormat::ASCII};
  std::string inputModeHelp = "input format:";
  for (auto const& id : supportedFileFormats) {
    std::stringstream option;
    option << " " << id << (id == defaultInputFormat ? " (default)" : "") << ",";
    inputModeHelp += option.str();
  }
  inputModeHelp.pop_back();
  od.add_options()
    ("ifile,i", bpo::value<std::string>(), "input file name")
    ("input-mode", bpo::value<io::FileFormat>(), inputModeHelp.c_str())
    ("verbosity,v", bpo::value<int>()->default_value(1), "verbosity level")
    ("host,h", bpo::value<std::string>()->default_value("localhost"), "host name")
    ("port,p", bpo::value<int>()->default_value(29070), "server port to connect to")
    ("wordsize", bpo::value<size_t>()->default_value(16), "size of words")
    ("input-endianness", bpo::value<std::string>(), "picked automatically: big for binary, little for ascii")
    ("output-endianness", bpo::value<std::string>()->default_value("little"), "endianness of the published data")
    ("help,h", "Hilfe!");
  // clang-format on

  std::stringstream cmdLineError;
  auto printhelp = [&cmdLineError, &od](int exitvalue = 0) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(exitvalue);
  };
  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error parsing command line arguments: " << e.what() << std::endl
              << std::endl;
    printhelp(1);
  }

  if (varmap.count("help") > 0) {
    printhelp();
  }
  if (varmap.count("ifile") == 0) {
    cmdLineError << "Error: No input file specified, aborting ...";
    printhelp(1);
  }

  auto ifileName = varmap["ifile"].as<std::string>();
  auto host = varmap["host"].as<std::string>();
  auto port = varmap["port"].as<int>();
  auto wordsize = varmap["wordsize"].as<size_t>();
  auto verbosity = varmap["verbosity"].as<int>();
  auto inputFormat = defaultInputFormat;
  if (varmap.count("input-mode") > 0) {
    inputFormat = varmap["input-mode"].as<io::FileFormat>();
  } else if (verbosity > 0) {
    std::cout << "Using default file mode '" << inputFormat << "'" << std::endl;
  }
  auto getEndianness = [&varmap](const char* option) -> boost::endian::order {
    auto byteOrder = boost::endian::order::little;
    if (varmap[option].as<std::string>() == "big") {
      byteOrder = boost::endian::order::big;
    } else if (varmap[option].as<std::string>() != "little") {
      throw std::runtime_error(std::string("invalid argument for option --") + option);
    }
    return byteOrder;
  };

  boost::endian::order inByteOrder = boost::endian::order::little;
  if (varmap.count("input-endianness") > 0) {
    // pick the value from the option if specified
    inByteOrder = getEndianness("input-endianness");
  } else if (inputFormat == io::FileFormat::BINARY) {
    // binary pRU data words are stored in big endian format
    inByteOrder = boost::endian::order::big;
  } else if (inputFormat == io::FileFormat::ASCII) {
    // ASCII pRU data words are extracted in the native byte order
    inByteOrder = boost::endian::order::native;
  }
  boost::endian::order outByteOrder = getEndianness("output-endianness");

  std::istream* input = &std::cin;
  std::ifstream inputFile;
  if (ifileName != "-") {
    inputFile.open(ifileName);
    if (!inputFile.is_open()) {
      std::cout << "can not open file '" << ifileName << "' for reading" << std::endl;
      exit(1);
    }
    input = &inputFile;
  }

  std::vector<char> buffer;
  if (inputFormat == io::FileFormat::BINARY) {
    // the size of one read cycle is fixed for now, but in the future we might allow
    // partial reads
    const size_t cycleSize = 64 * 1024;
    size_t bytesRead = 0;
    while (input->good() && !input->eof()) {
      if (bytesRead + cycleSize > buffer.size()) {
        // reserve space for the next cycle
        buffer.resize(bytesRead + cycleSize);
      }
      auto size = (input->read(buffer.data() + bytesRead, cycleSize)).gcount();
      bytesRead += size;
      if (size == 0) {
        break;
      }
    }
    buffer.resize(bytesRead);
  } else if (inputFormat == io::FileFormat::ASCII) {
    *input >> io::ASCIINumberReader(buffer, wordsize, verbosity);
  } else {
    std::stringstream errmsg("invalid file format specified: ");
    errmsg << inputFormat;
    throw std::runtime_error(errmsg.str());
  }

  // reverse the data words if the native endianness does not match the output endianness
  if (inByteOrder != outByteOrder) {
    for (size_t i = 0; i < buffer.size(); i += wordsize) {
      for (int low = i, high = i + wordsize - 1; low < high; low++, high--) {
        std::swap(buffer[low], buffer[high]);
      }
    }
  }

  if (verbosity > 1) {
    util::hexDump("Publishing buffer", buffer.data(), buffer.size(), buffer.size());
  } else if (verbosity > 0) {
    std::cout << "Publishing buffer of size " << buffer.size() << std::endl;
  }

  if (inputFile.is_open()) {
    inputFile.close();
  }

  // create a NetworkSession object for TCP protocol
  network::NetworkSession<boost::asio::ip::tcp> session;

  auto notifier = [host, port, verbosity]() {
    if (verbosity > 0) {
      std::cout << "publisher client connected to port " << port << " on host " << host << std::endl;
    }
  };
  // Note: if that would be an UDP session the method will always succeed,
  // because there is no check in UDP whether the port is really open/has an open endpoint
  auto result = session.open(host.c_str(), port, notifier);
  if (result) {
    session.getSocket().send(boost::asio::buffer(buffer));
  } else {
    std::cout << "Could not connect to port " << port << " on host " << host << std::endl;
  }

  return 0;
}
