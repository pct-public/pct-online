#include "readout/pRUError.h"

#include "datamodel/alpide_format.h"
#include "readout/pRUEnums.h"  // WORD_TYPE, ERROR_FLAGS etc.
#include <stdexcept>

namespace bpct {
namespace readout {

frame_list::const_iterator pRUError::check_existing_header(
    frame_list const& temp_list, unsigned char const& typeAndRU,
    unsigned char const& staveAndChipID) {
  for (auto itr = temp_list.cbegin(); itr != temp_list.cend(); itr++) {
    // if chip id & stave id & ru matches existing frame, ellement acess
    if ((*itr).words[0] == typeAndRU &&
        (*itr).words[1] == staveAndChipID) {
      return itr;
    }
  }
  return temp_list.cend();
}

bool pRUError::check_missing_data(pRUFrame const& frame) {
  return frame.words.size() < 48;
}

bool pRUError::check_frame_size(pRUFrame const& frame) {
  // Extract frame size elements and concatenate to a number
  if (frame.words.size() < 32) {
    throw std::logic_error("should have both header and trailer word");
  }
  unsigned char const* trailer_word = frame.words.data() + (frame.words.size() - 16);
  unsigned long int frameSize =
    (static_cast<unsigned long int>(trailer_word[12])) << 24 |
    (static_cast<unsigned long int>(trailer_word[13])) << 16 |
    (static_cast<unsigned long int>(trailer_word[14])) << 8 |
    (static_cast<unsigned long int>(trailer_word[15]));

  if (frameSize == frame.alpide_data_bytes) { 
    return true;
  }
  return false;
}

unsigned int pRUError::check_padding_error(
  std::vector<unsigned char>::const_iterator& cit_fields,
  std::vector<unsigned char>::const_iterator const& cit_end)
{
  unsigned int actual_padding = 0;

  while (cit_fields != cit_end) {
    if (*cit_fields == data_model::ALPIDE_PADDING) actual_padding++;
    ++cit_fields;
  }
  return actual_padding;
}

bool pRUError::check_trailer_flags(pRUFrame const& frame) {
  if (frame.words.size() < 32) {
    throw std::logic_error("should have both header and trailer word");
  }
  // Error flags are in the 8th octet in the trailer.
  // Optimized error block without posibility for differentiated handling
  if (*(frame.words.cend() - 9) == 0) { // if 0, no errors exists
    return false;
  } else { // if flags is not 0 there is an error
    return true;
  }
}

}  // namespace readout
} // end namespace bpct
