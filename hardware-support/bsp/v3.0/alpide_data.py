class ALPIDE_DATA_H:

    """ Register: run_settings """
    RUN_SETTINGS_OFFSET = 0x0
    RUN_SETTINGS_RESET = 0x1800

    """ Field: run """
    RUN_SETTINGS_RUN_OFFSET = 0
    RUN_SETTINGS_RUN_WIDTH = 1
    RUN_SETTINGS_RUN_RESET = 0x0
    RUN_SETTINGS_RUN_MASK = 0x1

    """ Field: test_mode """
    RUN_SETTINGS_TEST_MODE_OFFSET = 1
    RUN_SETTINGS_TEST_MODE_WIDTH = 1
    RUN_SETTINGS_TEST_MODE_RESET = 0x0
    RUN_SETTINGS_TEST_MODE_MASK = 0x2

    """ Field: stave_id """
    RUN_SETTINGS_STAVE_ID_OFFSET = 2
    RUN_SETTINGS_STAVE_ID_WIDTH = 4
    RUN_SETTINGS_STAVE_ID_RESET = 0x0
    RUN_SETTINGS_STAVE_ID_MASK = 0x3c

    """ Field: chip_id """
    RUN_SETTINGS_CHIP_ID_OFFSET = 6
    RUN_SETTINGS_CHIP_ID_WIDTH = 4
    RUN_SETTINGS_CHIP_ID_RESET = 0x0
    RUN_SETTINGS_CHIP_ID_MASK = 0x3c0

    """ Field: prbs_test """
    RUN_SETTINGS_PRBS_TEST_OFFSET = 10
    RUN_SETTINGS_PRBS_TEST_WIDTH = 1
    RUN_SETTINGS_PRBS_TEST_RESET = 0x0
    RUN_SETTINGS_PRBS_TEST_MASK = 0x400

    """ Field: hs_rate """
    RUN_SETTINGS_HS_RATE_OFFSET = 11
    RUN_SETTINGS_HS_RATE_WIDTH = 2
    RUN_SETTINGS_HS_RATE_RESET = 0x3
    RUN_SETTINGS_HS_RATE_MASK = 0x1800

    """ Register: run_status """
    RUN_STATUS_OFFSET = 0x1
    RUN_STATUS_RESET = 0x1

    """ Field: data_format_ver """
    RUN_STATUS_DATA_FORMAT_VER_OFFSET = 0
    RUN_STATUS_DATA_FORMAT_VER_WIDTH = 8
    RUN_STATUS_DATA_FORMAT_VER_RESET = 0x1
    RUN_STATUS_DATA_FORMAT_VER_MASK = 0xff

    """ Field: last_sample_word """
    RUN_STATUS_LAST_SAMPLE_WORD_OFFSET = 8
    RUN_STATUS_LAST_SAMPLE_WORD_WIDTH = 10
    RUN_STATUS_LAST_SAMPLE_WORD_RESET = 0x0
    RUN_STATUS_LAST_SAMPLE_WORD_MASK = 0x3ff00

    """ Field: last_decode_word """
    RUN_STATUS_LAST_DECODE_WORD_OFFSET = 18
    RUN_STATUS_LAST_DECODE_WORD_WIDTH = 8
    RUN_STATUS_LAST_DECODE_WORD_RESET = 0x0
    RUN_STATUS_LAST_DECODE_WORD_MASK = 0x3fc0000

    """ Field: data_formatter_status """
    RUN_STATUS_DATA_FORMATTER_STATUS_OFFSET = 26
    RUN_STATUS_DATA_FORMATTER_STATUS_WIDTH = 2
    RUN_STATUS_DATA_FORMATTER_STATUS_RESET = 0x0
    RUN_STATUS_DATA_FORMATTER_STATUS_MASK = 0xc000000

    """ Field: buffer_status """
    RUN_STATUS_BUFFER_STATUS_OFFSET = 28
    RUN_STATUS_BUFFER_STATUS_WIDTH = 3
    RUN_STATUS_BUFFER_STATUS_RESET = 0x0
    RUN_STATUS_BUFFER_STATUS_MASK = 0x70000000

    """ Register: run_status_2 """
    RUN_STATUS_2_OFFSET = 0x2
    RUN_STATUS_2_RESET = 0x0

    """ Field: loss_of_signal """
    RUN_STATUS_2_LOSS_OF_SIGNAL_OFFSET = 0
    RUN_STATUS_2_LOSS_OF_SIGNAL_WIDTH = 1
    RUN_STATUS_2_LOSS_OF_SIGNAL_RESET = 0x0
    RUN_STATUS_2_LOSS_OF_SIGNAL_MASK = 0x1

    """ Field: fifo_empty """
    RUN_STATUS_2_FIFO_EMPTY_OFFSET = 1
    RUN_STATUS_2_FIFO_EMPTY_WIDTH = 1
    RUN_STATUS_2_FIFO_EMPTY_RESET = 0x0
    RUN_STATUS_2_FIFO_EMPTY_MASK = 0x2

    """ Field: enable_alignment """
    RUN_STATUS_2_ENABLE_ALIGNMENT_OFFSET = 2
    RUN_STATUS_2_ENABLE_ALIGNMENT_WIDTH = 1
    RUN_STATUS_2_ENABLE_ALIGNMENT_RESET = 0x0
    RUN_STATUS_2_ENABLE_ALIGNMENT_MASK = 0x4

    """ Field: data_aligned """
    RUN_STATUS_2_DATA_ALIGNED_OFFSET = 3
    RUN_STATUS_2_DATA_ALIGNED_WIDTH = 1
    RUN_STATUS_2_DATA_ALIGNED_RESET = 0x0
    RUN_STATUS_2_DATA_ALIGNED_MASK = 0x8

    """ Register: reset_counters """
    RESET_COUNTERS_OFFSET = 0x3
    RESET_COUNTERS_RESET = 0x0

    """ Register: num_error_0 """
    NUM_ERROR_0_OFFSET = 0x4
    NUM_ERROR_0_RESET = 0x0

    """ Register: num_error_1 """
    NUM_ERROR_1_OFFSET = 0x5
    NUM_ERROR_1_RESET = 0x0

    """ Register: num_correct_0 """
    NUM_CORRECT_0_OFFSET = 0x6
    NUM_CORRECT_0_RESET = 0x0

    """ Register: num_correct_1 """
    NUM_CORRECT_1_OFFSET = 0x7
    NUM_CORRECT_1_RESET = 0x0

    """ Register: num_prbs_error """
    NUM_PRBS_ERROR_OFFSET = 0x8
    NUM_PRBS_ERROR_RESET = 0x0

    """ Register: num_decode_error_0 """
    NUM_DECODE_ERROR_0_OFFSET = 0x9
    NUM_DECODE_ERROR_0_RESET = 0x0

    """ Register: num_decode_error_1 """
    NUM_DECODE_ERROR_1_OFFSET = 0xa
    NUM_DECODE_ERROR_1_RESET = 0x0

    """ Register: num_protocol_error """
    NUM_PROTOCOL_ERROR_OFFSET = 0xb
    NUM_PROTOCOL_ERROR_RESET = 0x0

    """ Register: num_frame_error """
    NUM_FRAME_ERROR_OFFSET = 0xc
    NUM_FRAME_ERROR_RESET = 0x0

    """ Register: num_frames """
    NUM_FRAMES_OFFSET = 0xd
    NUM_FRAMES_RESET = 0x0

    """ Register: num_empty_frames """
    NUM_EMPTY_FRAMES_OFFSET = 0xe
    NUM_EMPTY_FRAMES_RESET = 0x0

    """ Register: num_busy """
    NUM_BUSY_OFFSET = 0xf
    NUM_BUSY_RESET = 0x0

    """ Register: num_busy_violation """
    NUM_BUSY_VIOLATION_OFFSET = 0x10
    NUM_BUSY_VIOLATION_RESET = 0x0

    """ Register: num_flushed_incomplete """
    NUM_FLUSHED_INCOMPLETE_OFFSET = 0x11
    NUM_FLUSHED_INCOMPLETE_RESET = 0x0

    """ Register: num_strobe_extended """
    NUM_STROBE_EXTENDED_OFFSET = 0x12
    NUM_STROBE_EXTENDED_RESET = 0x0

    """ Register: num_busy_transition """
    NUM_BUSY_TRANSITION_OFFSET = 0x13
    NUM_BUSY_TRANSITION_RESET = 0x0

    """ Register: num_data_overrun """
    NUM_DATA_OVERRUN_OFFSET = 0x14
    NUM_DATA_OVERRUN_RESET = 0x0

    """ Register: num_fatal_condition """
    NUM_FATAL_CONDITION_OFFSET = 0x15
    NUM_FATAL_CONDITION_RESET = 0x0

    """ Register: num_buffer_overflow """
    NUM_BUFFER_OVERFLOW_OFFSET = 0x16
    NUM_BUFFER_OVERFLOW_RESET = 0x0

    """ Register: num_frames_dropped """
    NUM_FRAMES_DROPPED_OFFSET = 0x17
    NUM_FRAMES_DROPPED_RESET = 0x0

    """ Register: num_fake_frames """
    NUM_FAKE_FRAMES_OFFSET = 0x18
    NUM_FAKE_FRAMES_RESET = 0x0

    """ Register: test_vectors """
    TEST_VECTORS_OFFSET = 0x19
    TEST_VECTORS_RESET = 0xbcbcbc

    """ Field: test_vector_0 """
    TEST_VECTORS_TEST_VECTOR_0_OFFSET = 0
    TEST_VECTORS_TEST_VECTOR_0_WIDTH = 8
    TEST_VECTORS_TEST_VECTOR_0_RESET = 0xbc
    TEST_VECTORS_TEST_VECTOR_0_MASK = 0xff

    """ Field: test_vector_1 """
    TEST_VECTORS_TEST_VECTOR_1_OFFSET = 8
    TEST_VECTORS_TEST_VECTOR_1_WIDTH = 8
    TEST_VECTORS_TEST_VECTOR_1_RESET = 0xbc
    TEST_VECTORS_TEST_VECTOR_1_MASK = 0xff00

    """ Field: test_vector_2 """
    TEST_VECTORS_TEST_VECTOR_2_OFFSET = 16
    TEST_VECTORS_TEST_VECTOR_2_WIDTH = 8
    TEST_VECTORS_TEST_VECTOR_2_RESET = 0xbc
    TEST_VECTORS_TEST_VECTOR_2_MASK = 0xff0000

