<?xml version="1.0" encoding="ISO-8859-1"?>
<node id="power_control">
    <node id="config" address="0x00000000" permission="rw" description="Power Control configuration." parameters="reset=0x2">
        <node id="mode" mask="0x1" description="0 \(\to\)~ configuration mode. 1 \(\to\)~ continuous monitoring mode. In configuration mode, the I2C bus is controlled by the AXI bus." parameters="reset=0x0"/>
        <node id="auto_shutoff" mask="0x2" description="If set, monitors the INA226 alert pin and disables power supply if alerts occur or current exceeds critical level as defined by register." parameters="reset=0x1"/>
    </node>
    <node id="i2c_status" address="0x00000001" permission="r" description="I2C bus status." parameters="reset=0x0">
        <node id="transfer_in_progress" mask="0x1" description="An I2C-transfers is in progress." parameters="reset=0x0"/>
        <node id="transmitting_byte" mask="0x2" description="An I2C byte transmit is in progress." parameters="reset=0x0"/>
        <node id="receiving_byte" mask="0x4" description="An I2C byte receiving is in progress." parameters="reset=0x0"/>
        <node id="command_completed" mask="0x8" description="Last I2C command was completed." parameters="reset=0x0"/>
        <node id="error" mask="0x10" description="Some kind of error occurred." parameters="reset=0x0"/>
        <node id="command_aborted" mask="0x20" description="I2C operation was aborted by user." parameters="reset=0x0"/>
        <node id="arbitration_lost" mask="0x40" description="I2C arbitration lost." parameters="reset=0x0"/>
        <node id="bus_busy" mask="0x80" description="I2C bus is busy." parameters="reset=0x0"/>
    </node>
    <node id="i2c_write" address="0x00000002" permission="rw" description="I2C operation bytes." parameters="reset=0x0">
        <node id="slave_address" mask="0x7f" description="Slave-address byte." parameters="reset=0x0"/>
        <node id="register_pointer" mask="0x7f80" description="Register-pointer byte." parameters="reset=0x0"/>
        <node id="data" mask="0x7fff8000" description="Data to be written to device." parameters="reset=0x0"/>
    </node>
    <node id="i2c_read" address="0x00000003" permission="r" description="I2C operation bytes." parameters="reset=0x0">
        <node id="data" mask="0xffff" description="Data read from device." parameters="reset=0x0"/>
    </node>
    <node id="i2c_command" address="0x00000004" permission="w" description="Control the I2C bus." parameters="reset=0x0;pulse_cycles=1">
        <node id="write_word" mask="0x1" description="Start I2C write procedure. Including register pointer and 2 bytes of data." parameters="reset=0x0"/>
        <node id="read_word" mask="0x2" description="Start I2C read procedure. Does not include writing of register pointer." parameters="reset=0x0"/>
        <node id="write_register_pointer" mask="0x4" description="Start I2C write register pointer procedure. Required to read a different register than what is already stored on the INA chips." parameters="reset=0x0"/>
    </node>
    <node id="dvdd_enable" address="0x00000005" permission="rw" description="Turn on 1.8V DVDD. Bit 0 = stave 0, etc." parameters="reset=0x0"/>
    <node id="avdd_enable" address="0x00000006" permission="rw" description="Turn on 1.8V AVDD. Bit 0 = stave 0, etc." parameters="reset=0x0"/>
    <node id="avdd_current_limits" address="0x00000007" permission="rw" description="The current limits for the ALPIDE AVDD per stave. The resolution is set by settings on the INA226 chips. Is ignored if the value is 0x0." parameters="reset=0x0">
        <node id="warning_threshold" mask="0xffff" description="Current above this threshold triggers a warning." parameters="reset=0x0"/>
        <node id="critical_threshold" mask="0xffff0000" description="Current above this threshold disables power regulator and triggers critical level." parameters="reset=0x0"/>
    </node>
    <node id="dvdd_current_limits" address="0x00000008" permission="rw" description="The current limits for the ALPIDE AVDD per stave. The resolution is set by settings on the INA226 chips. Is ignored if the value is 0x0." parameters="reset=0x0">
        <node id="warning_threshold" mask="0xffff" description="Current above this threshold triggers a warning." parameters="reset=0x0"/>
        <node id="critical_threshold" mask="0xffff0000" description="Current above this threshold disables power regulator and triggers critical level." parameters="reset=0x0"/>
    </node>
    <node id="minimum_current" address="0x00000009" permission="rw" description="The minimum current of a stave. The resolution is set by settings on the INA226 chips. Is ignored if the value is 0x0." parameters="reset=0x0">
        <node id="dvdd" mask="0xffff" description="Current below this threshold triggers a warning." parameters="reset=0x0"/>
        <node id="avdd" mask="0xffff0000" description="Current below this threshold triggers a warning." parameters="reset=0x0"/>
    </node>
    <node id="current_stave0" address="0x0000000A" permission="r" description="Supply current measured by INA226. Updated only in monitoring mode." parameters="reset=0x0">
        <node id="avdd_current" mask="0xffff" description="Analog supply current." parameters="reset=0x0"/>
        <node id="dvdd_current" mask="0xffff0000" description="Digital supply current." parameters="reset=0x0"/>
    </node>
    <node id="current_stave1" address="0x0000000B" permission="r" description="Supply current measured by INA226. Updated only in monitoring mode." parameters="reset=0x0">
        <node id="avdd_current" mask="0xffff" description="Analog supply current." parameters="reset=0x0"/>
        <node id="dvdd_current" mask="0xffff0000" description="Digital supply current." parameters="reset=0x0"/>
    </node>
    <node id="current_stave2" address="0x0000000C" permission="r" description="Supply current measured by INA226. Updated only in monitoring mode." parameters="reset=0x0">
        <node id="avdd_current" mask="0xffff" description="Analog supply current." parameters="reset=0x0"/>
        <node id="dvdd_current" mask="0xffff0000" description="Digital supply current." parameters="reset=0x0"/>
    </node>
    <node id="current_stave3" address="0x0000000D" permission="r" description="Supply current measured by INA226. Updated only in monitoring mode." parameters="reset=0x0">
        <node id="avdd_current" mask="0xffff" description="Analog supply current." parameters="reset=0x0"/>
        <node id="dvdd_current" mask="0xffff0000" description="Digital supply current." parameters="reset=0x0"/>
    </node>
    <node id="current_stave4" address="0x0000000E" permission="r" description="Supply current measured by INA226. Updated only in monitoring mode." parameters="reset=0x0">
        <node id="avdd_current" mask="0xffff" description="Analog supply current." parameters="reset=0x0"/>
        <node id="dvdd_current" mask="0xffff0000" description="Digital supply current." parameters="reset=0x0"/>
    </node>
    <node id="current_stave5" address="0x0000000F" permission="r" description="Supply current measured by INA226. Updated only in monitoring mode." parameters="reset=0x0">
        <node id="avdd_current" mask="0xffff" description="Analog supply current." parameters="reset=0x0"/>
        <node id="dvdd_current" mask="0xffff0000" description="Digital supply current." parameters="reset=0x0"/>
    </node>
    <node id="stave_dvdd_status" address="0x00000010" permission="r" description="Status of DVDD supply for each stave. 0 \(\to\)~ OFF, 1 \(\to\)~ OK, 2 \(\to\)~ WARNING, 3 \(\to\)~ CRITICAL, 4 \(\to\)~ ALERT." parameters="reset=0x0">
        <node id="stave0" mask="0x7" description="Stave0 DVDD status." parameters="reset=0x0"/>
        <node id="stave1" mask="0x38" description="Stave1 DVDD status." parameters="reset=0x0"/>
        <node id="stave2" mask="0x1c0" description="Stave2 DVDD status." parameters="reset=0x0"/>
        <node id="stave3" mask="0xe00" description="Stave3 DVDD status." parameters="reset=0x0"/>
        <node id="stave4" mask="0x7000" description="Stave4 DVDD status." parameters="reset=0x0"/>
        <node id="stave5" mask="0x38000" description="Stave5 DVDD status." parameters="reset=0x0"/>
    </node>
    <node id="stave_avdd_status" address="0x00000011" permission="r" description="Status of AVDD supply for each stave. 0 \(\to\)~ OFF, 1 \(\to\)~ OK, 2 \(\to\)~ WARNING, 3 \(\to\)~ CRITICAL, 4 \(\to\)~ ALERT." parameters="reset=0x0">
        <node id="stave0" mask="0x7" description="Stave0 AVDD status." parameters="reset=0x0"/>
        <node id="stave1" mask="0x38" description="Stave1 AVDD status." parameters="reset=0x0"/>
        <node id="stave2" mask="0x1c0" description="Stave2 AVDD status." parameters="reset=0x0"/>
        <node id="stave3" mask="0xe00" description="Stave3 AVDD status." parameters="reset=0x0"/>
        <node id="stave4" mask="0x7000" description="Stave4 AVDD status." parameters="reset=0x0"/>
        <node id="stave5" mask="0x38000" description="Stave5 AVDD status." parameters="reset=0x0"/>
    </node>
    <node id="reset_stave_status" address="0x00000012" permission="w" description="Resets staves num status." parameters="reset=0x0;pulse_cycles=1"/>
    <node id="bias_control" address="0x00000013" permission="rw" description="Set the BIAS VOLTAGE for all staves. 00 \(\to\)~ 0V, 01 \(\to\)~ -2V1, 10 \(\to\)~ -3V, 11 \(\to\)~ -6V" parameters="reset=0x0"/>
</node>