class PTB_REGS_H:

    """ Register: top_chip_id """
    TOP_CHIP_ID_OFFSET = 0x0
    TOP_CHIP_ID_RESET = 0x0

    """ Register: bot_chip_id """
    BOT_CHIP_ID_OFFSET = 0x1
    BOT_CHIP_ID_RESET = 0x0

