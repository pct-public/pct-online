class OFFLOAD_H:

    """ Register: enable_offload """
    ENABLE_OFFLOAD_OFFSET = 0x0
    ENABLE_OFFLOAD_RESET = 0x0

    """ Register: reset_offload_fifo """
    RESET_OFFLOAD_FIFO_OFFSET = 0x1
    RESET_OFFLOAD_FIFO_RESET = 0x0

    """ Register: frame_based_offload """
    FRAME_BASED_OFFLOAD_OFFSET = 0x2
    FRAME_BASED_OFFLOAD_RESET = 0x1

    """ Register: idle """
    IDLE_OFFSET = 0x3
    IDLE_RESET = 0x0

    """ Register: pDTP_TX_status """
    PDTP_TX_STATUS_OFFSET = 0x4
    PDTP_TX_STATUS_RESET = 0x0

    """ Field: idle """
    PDTP_TX_STATUS_IDLE_OFFSET = 0
    PDTP_TX_STATUS_IDLE_WIDTH = 1
    PDTP_TX_STATUS_IDLE_RESET = 0x0
    PDTP_TX_STATUS_IDLE_MASK = 0x1

    """ Field: busy_stream """
    PDTP_TX_STATUS_BUSY_STREAM_OFFSET = 1
    PDTP_TX_STATUS_BUSY_STREAM_WIDTH = 1
    PDTP_TX_STATUS_BUSY_STREAM_RESET = 0x0
    PDTP_TX_STATUS_BUSY_STREAM_MASK = 0x2

    """ Field: busy_data """
    PDTP_TX_STATUS_BUSY_DATA_OFFSET = 2
    PDTP_TX_STATUS_BUSY_DATA_WIDTH = 1
    PDTP_TX_STATUS_BUSY_DATA_RESET = 0x0
    PDTP_TX_STATUS_BUSY_DATA_MASK = 0x4

    """ Field: complete """
    PDTP_TX_STATUS_COMPLETE_OFFSET = 3
    PDTP_TX_STATUS_COMPLETE_WIDTH = 1
    PDTP_TX_STATUS_COMPLETE_RESET = 0x0
    PDTP_TX_STATUS_COMPLETE_MASK = 0x8

    """ Field: throttle_value """
    PDTP_TX_STATUS_THROTTLE_VALUE_OFFSET = 4
    PDTP_TX_STATUS_THROTTLE_VALUE_WIDTH = 24
    PDTP_TX_STATUS_THROTTLE_VALUE_RESET = 0x0
    PDTP_TX_STATUS_THROTTLE_VALUE_MASK = 0xffffff0

    """ Register: test_mode """
    TEST_MODE_OFFSET = 0x5
    TEST_MODE_RESET = 0x0

    """ Field: enable_test_mode """
    TEST_MODE_ENABLE_TEST_MODE_OFFSET = 0
    TEST_MODE_ENABLE_TEST_MODE_WIDTH = 1
    TEST_MODE_ENABLE_TEST_MODE_RESET = 0x0
    TEST_MODE_ENABLE_TEST_MODE_MASK = 0x1

    """ Field: test_word_interval """
    TEST_MODE_TEST_WORD_INTERVAL_OFFSET = 1
    TEST_MODE_TEST_WORD_INTERVAL_WIDTH = 31
    TEST_MODE_TEST_WORD_INTERVAL_RESET = 0x0
    TEST_MODE_TEST_WORD_INTERVAL_MASK = 0xfffffffe

    """ Register: num_test_words """
    NUM_TEST_WORDS_OFFSET = 0x6
    NUM_TEST_WORDS_RESET = 0x400

    """ Register: num_wait """
    NUM_WAIT_OFFSET = 0x7
    NUM_WAIT_RESET = 0x0

    """ Register: tlast_threshold """
    TLAST_THRESHOLD_OFFSET = 0x8
    TLAST_THRESHOLD_RESET = 0xffff

    """ Register: assert_tlast_when_empty """
    ASSERT_TLAST_WHEN_EMPTY_OFFSET = 0x9
    ASSERT_TLAST_WHEN_EMPTY_RESET = 0x0

    """ Register: flush_buffer """
    FLUSH_BUFFER_OFFSET = 0xa
    FLUSH_BUFFER_RESET = 0x0

