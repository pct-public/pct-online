class TRIGGER_MANAGER_H:

    """ Register: status """
    STATUS_OFFSET = 0x0
    STATUS_RESET = 0x1

    """ Field: idle """
    STATUS_IDLE_OFFSET = 0
    STATUS_IDLE_WIDTH = 1
    STATUS_IDLE_RESET = 0x1
    STATUS_IDLE_MASK = 0x1

    """ Field: error """
    STATUS_ERROR_OFFSET = 1
    STATUS_ERROR_WIDTH = 1
    STATUS_ERROR_RESET = 0x0
    STATUS_ERROR_MASK = 0x2

    """ Register: mode """
    MODE_OFFSET = 0x1
    MODE_RESET = 0x0

    """ Register: num_triggers """
    NUM_TRIGGERS_OFFSET = 0x2
    NUM_TRIGGERS_RESET = 0x1

    """ Register: pre_cmd_delay """
    PRE_CMD_DELAY_OFFSET = 0x3
    PRE_CMD_DELAY_RESET = 0x0

    """ Register: trigger_delay """
    TRIGGER_DELAY_OFFSET = 0x4
    TRIGGER_DELAY_RESET = 0xE

    """ Register: pulse_delay """
    PULSE_DELAY_OFFSET = 0x5
    PULSE_DELAY_RESET = 0xE

    """ Register: pulse_trigger_delay """
    PULSE_TRIGGER_DELAY_OFFSET = 0x6
    PULSE_TRIGGER_DELAY_RESET = 0xE

    """ Register: trigger_init """
    TRIGGER_INIT_OFFSET = 0x7
    TRIGGER_INIT_RESET = 0x0

    """ Register: num_trains """
    NUM_TRAINS_OFFSET = 0x8
    NUM_TRAINS_RESET = 0x1

    """ Register: trains_delay """
    TRAINS_DELAY_OFFSET = 0x9
    TRAINS_DELAY_RESET = 0xE

    """ Register: alpide_grst """
    ALPIDE_GRST_OFFSET = 0xa
    ALPIDE_GRST_RESET = 0x0

    """ Register: alpide_prst """
    ALPIDE_PRST_OFFSET = 0xb
    ALPIDE_PRST_RESET = 0x0

    """ Register: alpide_bcrst """
    ALPIDE_BCRST_OFFSET = 0xc
    ALPIDE_BCRST_RESET = 0x0

    """ Register: alpide_rorst """
    ALPIDE_RORST_OFFSET = 0xd
    ALPIDE_RORST_RESET = 0x0

    """ Register: trigger_source """
    TRIGGER_SOURCE_OFFSET = 0xe
    TRIGGER_SOURCE_RESET = 0x3

    """ Register: alpide_mode """
    ALPIDE_MODE_OFFSET = 0xf
    ALPIDE_MODE_RESET = 0x1

    """ Register: absolute_time """
    ABSOLUTE_TIME_OFFSET = 0x10
    ABSOLUTE_TIME_RESET = 0x0

    """ Register: reset_time """
    RESET_TIME_OFFSET = 0x11
    RESET_TIME_RESET = 0x0

    """ Register: spill_id """
    SPILL_ID_OFFSET = 0x12
    SPILL_ID_RESET = 0x0

    """ Register: increment_spill_id """
    INCREMENT_SPILL_ID_OFFSET = 0x13
    INCREMENT_SPILL_ID_RESET = 0x0

    """ Register: reset_spill_id """
    RESET_SPILL_ID_OFFSET = 0x14
    RESET_SPILL_ID_RESET = 0x0

