#include "rootio/CsvImporter.h"
#include <iostream>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <TFile.h>

namespace bpct
{
namespace rootio
{

int CsvImporter::init(std::string branchConfiguration)
{
  using BranchType = CsvImporter::BranchType;
  std::string token;
  std::stringstream streaminput(branchConfiguration);
  while ((std::getline(streaminput, token, ';')) && !token.empty()) {
    //std::cout << "scanning branch configuration: " << token << std::endl;
    std::stringstream tokenstream(token);
    std::string name, typestr;
    BranchType type = BranchType::kUndefinedValue;
    if (std::getline(tokenstream, name, '/') && (tokenstream >> typestr)                      //
        && ((typestr == "I" && (type = BranchType::kIntValue) != BranchType::kUndefinedValue) //
            || (typestr == "F" && (type = BranchType::kFloatValue) != BranchType::kUndefinedValue))) {
      //std::cout << "  name: " << name << std::endl;
      //std::cout << "  type: " << typestr << " " << type << std::endl;
    } else {
      std::cerr << "format error in branch definition '" << token << "' (expected format 'branchname/[I,F]')" << std::endl;
      return -1;
    }
    mBranchConfigurations.push_back(BranchConfiguration(token, name, type));
  }

  // now we create branches in a separate loop once the vector of configurations is
  // fixed to make sure the the pointers used as branch addresses are constant.
  mTree = std::make_unique<TTree>(mTreeName.c_str(), mTreeTitle.c_str());
  if (!mTree) {
    return -1;
  }

  for (auto& bc : mBranchConfigurations) {
    std::cout << "adding branch: " << bc.configuration << " type " << bc.type << " " << ((void*)&bc.intval) << std::endl;
    switch (bc.type) {
      case BranchType::kIntValue:
        mTree->Branch(bc.name.c_str(), &bc.intval, bc.configuration.c_str());
        break;
      case BranchType::kFloatValue:
        mTree->Branch(bc.name.c_str(), &bc.floatval, bc.configuration.c_str());
        break;
      default:
        throw std::runtime_error("undefined branch type");
    }
  }
  return 0;
}

CsvImporter& CsvImporter::operator<<(std::string const& data)
{
  if (!mTree) {
    throw std::runtime_error("Importer not initialized");
  }
  // scan ine line of data
  int index = 0;
  try {
    std::string token;
    std::stringstream linestream(data);
    //std::cout << "scanning: " << linestream.str() << std::endl;
    auto bc = mBranchConfigurations.begin();
    std::getline(linestream, token, ';');
    while (!token.empty()) {
      switch (bc->type) {
        case BranchType::kIntValue:
          bc->intval = std::stoi(token);
          //std::cout << "  branch " << bc->name << " value " << bc->intval << " " << ((void*)&bc->intval) << std::endl;
          break;
        case BranchType::kFloatValue:
          bc->floatval = std::stof(token);
          //std::cout << "  branch " << bc->name << " value " << bc->floatval << " " << ((void*)&bc->floatval) << std::endl;
          break;
        default:
          throw std::runtime_error("undefined branch type");
      }
      std::getline(linestream, token, ';');
      if (++bc == mBranchConfigurations.end()) {
        break;
      }
      ++index;
    }
  }

  catch (std::exception& e) {
    std::cout << e.what()
              << "format error in data string '" << data << "' at position " << index << std::endl;
  }

  mTree->Fill();
  return *this;
}

CsvImporter const& CsvImporter::operator>>(TFile& file)
{
  file.cd();
  if (mTree) {
    mTree->Write();
  }
  mTree->SetDirectory(nullptr);
  return *this;
}

} // namespace rootio
} // namespace bpct

std::ostream& operator<<(std::ostream& os, bpct::rootio::CsvImporter::BranchType type)
{
  using BranchType = decltype(type);
  switch (type) {
    case BranchType::kIntValue:
      os << "I";
      break;
    case BranchType::kFloatValue:
      os << "F";
      break;
    default:
      os << "<undefined>";
  }
  return os;
}
