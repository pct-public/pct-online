/// @file   test_pRUExporter.cxx
/// @author Øistein Jelmert Skjolddal
/// @since  2020-02-26
/// @brief  Unit test for the pRUExporter

#define BOOST_TEST_MODULE Test Bergen pCT rootio adaptors
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include "rootio/pRUExporter.h"
#include "datamodel/AlpideHit.h"
#include "datamodel/RUDataHeader.h"

#include <iostream>
#include <memory>
#include <TFile.h>
#include <TTree.h>

using namespace bpct;
using RUDataHeader = data_model::RUDataHeader;
using AlpideHit = data_model::AlpideHit;

/// construct a set of RUDataHeader information
/// can produce frames with data from different chips if flag is true
auto makeData(int count = 10, bool incrementChipId = false)
{
  size_t position = 0;
  // choose buffer big enough to avoid resize later
  std::vector<char> data(count * sizeof(RUDataHeader) + count * 5 * sizeof(AlpideHit));

  auto nextFrame = [&data, &position]() -> RUDataHeader& {
    // we need space for one RUDataHeader and reserve space for 10 Alpide hits
    size_t required = position + sizeof(RUDataHeader) + 5 * sizeof(AlpideHit);
    BOOST_REQUIRE(data.size() >= required);
    auto& ref = *reinterpret_cast<RUDataHeader*>(data.data() + position);
    ref = RUDataHeader(); // initialize to default with layer set to 0xff
    position += sizeof(RUDataHeader);
    return ref;
  };

  auto nextHit = [&data, &position]() -> AlpideHit& {
    BOOST_REQUIRE(data.size() >= position + sizeof(AlpideHit));
    auto& ref = *reinterpret_cast<AlpideHit*>(data.data() + position);
    ref = AlpideHit(); // initialize
    position += sizeof(AlpideHit);
    return ref;
  };
  for (int it = 0; it < count; it++) { //For each frame
    data_model::RUDataHeader& frame = nextFrame();
    frame.layer = it;
    frame.frameid = it + 5;
    frame.nofHits = 5;
    frame.time = (it + 25);
    frame.bc = (it + 30);

    for (int i = 0; i < 5; i++) { // for each hit
      auto& hit = nextHit();
      hit.x = i + 35 + it;
      hit.y = i + 40 + it;
      hit.chip = (it + 1 + (incrementChipId ? i : 0)) % 5;
      hit.stave = (it + 8) % 16;
    }
  }
  data.resize(position);
  return data;
}

template <typename T, typename... Args>
void exportData(T&& data, Args&&... args)
{
  rootio::pRUExporter worker(std::forward<Args>(args)...);
  worker.writeToTree(std::move(data));
}

bool checkTree(const char* filename, const char* treename, int nExpectedEntries)
{
  auto file = std::make_unique<TFile>(filename);
  BOOST_REQUIRE(file.get() != nullptr && !file->IsZombie());
  TTree* tree = reinterpret_cast<TTree*>(file->GetObjectChecked(treename, "TTree"));
  BOOST_REQUIRE(tree != nullptr);
  int nEntries = tree->GetEntries();
  BOOST_CHECK(nExpectedEntries == nEntries);

  uint16_t ru_id = 0;
  uint16_t stave_id = 0;
  uint16_t chip_id = 0;
  std::vector<uint16_t>* column = nullptr;
  std::vector<uint16_t>* row = nullptr;
  tree->SetBranchAddress("ru_id", &ru_id);
  tree->SetBranchAddress("stave_id", &stave_id);
  tree->SetBranchAddress("chip_id", &chip_id);
  tree->SetBranchAddress("column", &column);
  tree->SetBranchAddress("row", &row);
  for (int entry = 0; entry < nEntries; entry++) {
    tree->GetEntry(entry);
    BOOST_REQUIRE(ru_id == entry);
    BOOST_REQUIRE_MESSAGE(stave_id == (entry + 8) % 16, "wrong stave id, expecting " << (entry + 8) % 16 << ", got " << (int)stave_id);
    BOOST_REQUIRE_MESSAGE(chip_id == (entry + 1) % 5, "wrong chip id, expecting " << (entry + 1) % 5 << ", got " << (int)chip_id);
    BOOST_REQUIRE(column != nullptr && column->size() == 5);
    BOOST_REQUIRE(row != nullptr && row->size() == 5);
    BOOST_REQUIRE(column->size() == row->size());
    for (size_t i = 0; i < column->size(); i++) {
      BOOST_REQUIRE((*column)[i] == ru_id + i + 35);
      BOOST_REQUIRE((*row)[i] == ru_id + i + 40);
    }
  }
  file->Close();
  boost::filesystem::remove(filename);
  return nEntries == nExpectedEntries;
}

BOOST_AUTO_TEST_CASE(test_pRUExporter)
{
  int nEntries = 10;
  auto data = makeData(nEntries);
  BOOST_REQUIRE_MESSAGE(data.size() > 0, "can not test with empty data");
  exportData(std::move(data), "test_pRUExporter.root", "treename", "Tree Title");
  checkTree("test_pRUExporter.root", "treename", nEntries);
}

BOOST_AUTO_TEST_CASE(test_pRUExporter_filetype_appended)
{
  int nEntries = 20;
  // pRUExporter should append .root if not yet in the filename
  auto data = makeData(nEntries);
  BOOST_REQUIRE_MESSAGE(data.size() > 0, "can not test with empty data");
  exportData(std::move(data), "pRUExporter.test", "treename", "Tree Title");
  checkTree("pRUExporter.test.root", "treename", nEntries);
}

BOOST_AUTO_TEST_CASE(test_pRUExporter_runtime_error)
{
  // make an inconsistent data set with changing chip ids within one frame
  // exporter should throw
  auto data = makeData(5, true);
  BOOST_CHECK_THROW(exportData(std::move(data), "test_pRUExporter.root", "treename", "Tree Title"), std::runtime_error);
}
