#ifndef CSVIMPORTER_H
#define CSVIMPORTER_H

#include <string>
#include <memory>
#include <vector>
#include <TTree.h>

class TFile;

namespace bpct
{
namespace rootio
{

class CsvImporter
{
 public:
  enum struct BranchType {
    kUndefinedValue = 0,
    kIntValue,
    kFloatValue,
  };

  // helper struct to combine branch information in one object
  struct BranchConfiguration {
    BranchConfiguration(std::string& _configuration, std::string& _name, BranchType _type)
      : configuration(_configuration), name(_name), type(_type), intval(0) {}

    std::string configuration;
    std::string name;
    BranchType type;
    union {
      int intval;
      float floatval;
    };
  };

  using TreeConfiguration = std::vector<BranchConfiguration>;

  CsvImporter() = delete;
  CsvImporter(std::string fileName, std::string treeName, std::string treeTitle)
    : mFileName(std::move(fileName)), mTreeName(std::move(treeName)), mTreeTitle(std::move(treeTitle))
  {
  }

  /// Scan branch configuration argument and extract properties, and create
  /// tree with corresponding branches.
  int init(std::string branchConfiguration);

  /// Scan input data, assign values to branch variables and fill tree
  CsvImporter& operator<<(std::string const&);

  /// Write tree to file
  CsvImporter const& operator>>(TFile& file);

 private:
  std::string mFileName;
  std::string mTreeName;
  std::string mTreeTitle;
  std::unique_ptr<TTree> mTree;
  TreeConfiguration mBranchConfigurations;
};

} // namespace rootio
} // namespace bpct

std::ostream& operator<<(std::ostream&, bpct::rootio::CsvImporter::BranchType);

#endif //CSVIMPORTER_H
