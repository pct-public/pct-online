/// @file   pRUExporter.h
/// @author Øistein Jelmert Skjolddal
/// @since  2020-02-27
/// @brief  Exporter imlementation of RUDataHeader list to ROOT tree format srored in file

#ifndef PRUEXPORTER_H
#define PRUEXPORTER_H

#include <string>
#include <memory>
#include <vector>

class TTree;
class TFile;

#include "datamodel/AlpideHit.h"
#include "datamodel/RUDataHeader.h"

class TFile;

namespace bpct
{
namespace rootio
{

class pRUExporter
{
 private:
  struct Frame {
    uint16_t ru_id;
    uint16_t stave_id;
    uint16_t chip_id;
    uint32_t frame_id;
    uint16_t spill_id;
    uint32_t abs_time;
    uint16_t bunch_counter;

    uint16_t busy; //busy on/of

    std::vector<int16_t> column; // x axis: 10 bits (0 - 1023)
    std::vector<int16_t> row;    // y axis: 9 bits (0 - 511)
  };

  /// output file name
  std::string mFileName;
  /// tree name
  std::string mTreeName;
  /// tree title
  std::string mTreeTitle;
  /// the storage object
  Frame mFrame;

  std::unique_ptr<TTree> mTree;
  std::unique_ptr<TFile> mFile;

 public:
  pRUExporter() = delete;
  pRUExporter(std::string const& inpFileNam, std::string const& inpTreeName, std::string const& inpTreeTitle);

  ~pRUExporter();

  /// consume input and write to tree
  void writeToTree(std::vector<char>&& data);
};

} // namespace rootio
} // namespace bpct

#endif //PRUEXPORTER_H
