/// @file   pRUExporter.cxx
/// @author Øistein Jelmert Skjolddal
/// @since  2020-02-27
/// @brief  This class takes a vector of extracted pRU and alpide pixel hit information
//          in the format of  RUDataHeaders and exports this information to a
//          root tree for human consumption and data analysis.

#include "rootio/pRUExporter.h"

#include <iostream>
#include <string>
#include <vector>
#include <TFile.h>
#include <TTree.h>

namespace bpct
{
namespace rootio
{

pRUExporter::pRUExporter(std::string const& inpFileNam, std::string const& inpTreeName, std::string const& inpTreeTitle)
  : mFileName(inpFileNam)
  , mTreeName(inpTreeName)
  , mTreeTitle(inpTreeTitle)
{
  if (mFileName.length() > 4 && mFileName.rfind(".root") != mFileName.length() - 5) {
    //Correct file type guarantee.
    mFileName.append(".root");
  }

  //-------- Create a new root file: --------
  mFile = std::make_unique<TFile>(mFileName.c_str(), "RECREATE");

  //-------- Create new root tree: --------
  mTree = std::make_unique<TTree>(mTreeName.c_str(), mTreeTitle.c_str());
  mTree->SetDirectory(mFile.get()); // conect file and tree

  //-------- Create branch names: --------
  mTree->Branch("ru_id", &mFrame.ru_id);
  mTree->Branch("frame_id", &mFrame.frame_id);
  mTree->Branch("stave_id", &mFrame.stave_id);
  mTree->Branch("chip_id", &mFrame.chip_id);
  mTree->Branch("abs_time", &mFrame.abs_time);
  mTree->Branch("spill_id", &mFrame.spill_id);
  mTree->Branch("bunch_counter", &mFrame.bunch_counter);

  mTree->Branch("busy", &mFrame.busy); //busy on/of

  mTree->Branch("column", &mFrame.column);
  mTree->Branch("row", &mFrame.row);
}

pRUExporter::~pRUExporter()
{
  mFile->Write();
  // disconect file and tree, closing and deleting file would delete
  // the tree also, making the pointer invalid which we have in a unique_ptr
  mTree->SetDirectory(nullptr);
  mFile->Close();
}

void pRUExporter::writeToTree(std::vector<char>&& data)
{
  std::cout << "processing buffer of size " << data.size() << '\n';

  auto fillFrame = [this](auto const& frame) {
    mFrame.ru_id = frame.layer;
    mFrame.frame_id = frame.frameid;
    mFrame.abs_time = frame.time;
    mFrame.bunch_counter = frame.bc;
    //mFrame.spill_id = frame.spill_id;
    mFrame.busy = frame.busy;
  };

  auto fillHits = [this](auto const hits[], size_t size) {
    for (unsigned int i = 0; i < size; i++) { // for each hit
      mFrame.column.push_back(hits[i].x);
      mFrame.row.push_back(hits[i].y);
      if (i == 0) {
        mFrame.stave_id = hits[i].stave;
        mFrame.chip_id = hits[i].chip;
      } else if (hits[i].stave != hits[0].stave || hits[i].chip != hits[0].chip) {
        // FIXME: we are strict for the moment, but can relax later provided we keep
        // track of the data error
        throw std::runtime_error("inconsistent frame data, all hits within one frame must come from one single chip");
      }
    } // END for each hit
  };

  auto processFrame = [&fillFrame, &fillHits, this](auto const* ptr) -> size_t {
    auto const& header = *reinterpret_cast<data_model::RUDataHeader const*>(ptr);
    fillFrame(header);
    fillHits(header.hits, header.nofHits);

    //-------- Write frame info to tree: --------
    mTree->Fill();

    //-------- Reset frame: --------
    mFrame.column.clear();
    mFrame.row.clear();

    return sizeof(data_model::RUDataHeader) + header.nofHits * sizeof(data_model::AlpideHit);
  };

  size_t position = 0;
  while (position < data.size()) {
    position += processFrame(data.data() + position);
  }

  //-------- Inspection options: --------
  //mTree->Print(); // outputs the file structure.
  //mTree->Show(); // outputs the last ellement to be added
  //tFile->Print(); // same as TTree::Print() but ads the file info and aditional trees

  // See: TTree::Changefile()
  // https://root.cern.ch/doc/master/classTTree.html#a4680b0dfd17292acc29ba9a8f33788a3
  //tFile = mTree->GetCurrentFile(); // to handle large files see: TTree::Changefile()
  //tFile->Write();

  //This might be problematic if many smaler sets of frames are being stored consequtively
  std::cout << "Finished processing, Root tree created" << '\n';
}

} // namespace rootio
} // namespace bpct
