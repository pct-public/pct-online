#include "rootio/CsvImporter.h"
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <iostream>
#include <memory>
#include <TFile.h>

int main(int argc, char* argv[])
{
  // definition of the available command line options
  namespace bpo = boost::program_options;
  boost::program_options::options_description od{
    "CSV format to ROOT tree converter\n"
    "(c) 2020 Bergen pCT Collaboration\n\n"
    "Reads csv format from standard input line by line and fills values\n"
    "into tree. The branch configuration can be specified in the corresponding\n"
    "argument or first line if argument NULL.\n"
    "\n"
    "Branch configuration format: type is either 'I' or 'F'\n"
    "branchname1/type;branchname2/type;...;branchnameN/type\n"
    "\n"
    "Value line format: value is either integer or float\n"
    "value1;value2;...;valueN\n"};
  od.add_options()("treename", bpo::value<std::string>()->default_value("csv"), "name of tree") //
    ("treetitle", bpo::value<std::string>()->default_value(""), "title of tree")                //
    ("filename", bpo::value<std::string>()->default_value("csv.root"), "name of output file")   //
    ("branch-config", bpo::value<std::string>()->default_value(""), "branch configuration")     //
    ("help,h", "Hilfe!");

  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error: " << e.what() << std::endl;
    exit(1);
  }

  bool printHelp = varmap.count("help");
  std::stringstream cmdLineError;
  if (!printHelp && varmap.count("treename") == 0) {
    cmdLineError << "invalid argument: missing parameter 'treename'";
    printHelp = true;
  }
  if (printHelp) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(0);
  }

  auto filename = varmap["filename"].as<std::string>();
  auto treename = varmap["treename"].as<std::string>();
  auto treetitle = varmap["treetitle"].as<std::string>();
  auto config = varmap["branch-config"].as<std::string>();
  if (treetitle.empty()) {
    treetitle = treename;
  }
  using Importer = bpct::rootio::CsvImporter;
  auto importer = std::make_unique<Importer>(filename, treename, treetitle);
  if (config.empty() == false) {
    if (importer->init(config) <= 0) {
      std::cerr << "format error in branch configuration '" << config << "'" << std::endl;
    }
  }

  std::string line;
  while ((std::getline(std::cin, line))) {
    *importer << line;
  }

  auto file = std::make_unique<TFile>(filename.c_str(), "RECREATE");
  if (!file) {
    std::cerr << "failed to open output file '" << filename << "'" << std::endl;
    exit(1);
  }

  *importer >> *file;
  file->Close();
}
