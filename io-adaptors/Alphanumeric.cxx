/// @file   Alphanumeric.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-19
/// @brief  Alphanumeric converter

#include "io-adaptors/Alphanumeric.h"
#include <iostream>

namespace bpct
{
namespace io
{

std::ostream& operator<<(std::ostream& stream, Alphanumeric const& formatter)
{
  auto writedigit = [&stream](char digit) {
    if (digit < 10) {
      digit += 0x30;
    } else if (digit < 16) {
      digit += 0x37;
    }
    stream << digit;
  };
  const size_t wordsize = formatter.mWordsize;
  auto const* buffer = formatter.mRaw;
  size_t position = 0;
  do {
    while (position < formatter.mSize) {
      writedigit((buffer[position] & 0xf0) >> 4);
      writedigit(buffer[position] & 0xf);
      if ((++position) % wordsize == 0) {
        break;
      }
    }
    stream << "\n";
  } while (position < formatter.mSize);

  return stream;
}

} // namespace io
} // namespace bpct
