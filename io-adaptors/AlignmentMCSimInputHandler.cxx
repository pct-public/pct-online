//
// Created by Rune on 15/01/2022.
//

#include "include/io-adaptors/AlignmentMCSimInputHandler.h"

AlignmentMCSimInputHandler::AlignmentMCSimInputHandler(const std::string& filepath) {
  this->filePath = filepath;
  this->headerLine = true;
  this->coordinateTransformer = bpct::CoordinateTransformer();
}

AlignmentMCSimInputHandler::~AlignmentMCSimInputHandler() = default;


bpct::data_model::AliTrack AlignmentMCSimInputHandler::getTrack(fstream &fileStream){
  //Read strings from input list into int lists and fill data vector
  int PDGEncoding;
  int eventID;
  int trackID;
  double posX;
  double posY;
  double posZ;
  bpct::data_model::AliHit localHit;



  //Read line into input vector
  std::vector<std::string> input; //holds each line from file
  std::string line; //current line

  while (getline(fileStream, line)){

    std::istringstream ss(line); // string stream
    std::string token; // buffer for current input in getline
    std::vector<std::string> hit; //holds every token read from current line

    while (std::getline(ss, token, ',')) {
      hit.push_back(token);
    }

    //skip processing header line
    if (headerLine) {
      //create lookup table with <header:ID> structure
      //get value by: hit[headers["valueName"]]
      for (unsigned long i = 0; i < hit.size(); i++) {
        headers[hit[i]] = i;
      }
      headerLine = false;
    } else {
      //No need to add hits that are not the right particle type
      //The simulation data contains trackID and eventID making it easier to add a hit as it appears in data.
      //Data also contains particle type. Limiting it to only protons for now.
      //TODO: Use muons as particle type. Should be particle code 13?
      PDGEncoding = std::stoi(hit[headers["PDGEncoding"]]);
      if (PDGEncoding != 2212) continue; // skip particles that are not proton

      eventID = std::stoi(hit[headers["eventID"]]);
      trackID = std::stoi(hit[headers["trackID"]]);
      posX = std::stod(hit[headers["posX"]]);
      posY = std::stod(hit[headers["posY"]]);
      posZ = std::stod(hit[headers["posZ"]]);

      //transform into local hit
      localHit = coordinateTransformer.globalToLocalHitCoordinateMM(posX, posY, posZ);

      tracks[trackKey(eventID, trackID)].hits.push_back(localHit);

      bpct::data_model::globalHit globalHit;
      globalHit.x = posX;
      globalHit.y = posY;
      globalHit.z = posZ;
      tracks_global[trackKey(eventID, trackID)].push_back(globalHit);


      if (trackFilter(tracks[trackKey(eventID, trackID)], tracks_global[trackKey(eventID, trackID)])) {
        auto ret = tracks[trackKey(eventID, trackID)];
        tracks.erase(trackKey(eventID, trackID));

        //ignore tracking layers for now
        ret.hits.erase(ret.hits.begin());
        ret.hits.erase(ret.hits.begin());

        return ret;
      }
    }
  }

  std::cout << "Reached end of input file." << std::endl;
  return {};
}

bool AlignmentMCSimInputHandler::trackFilter(bpct::data_model::AliTrack &track, vector<bpct::data_model::globalHit> globalTrack) {

  //base check: Does track have hit on all layers?
  if(track.hits.size() < (u_long)defaultGeometry.NUM_LAYERS) return false;

  //check that tracks has hit on all layers
  int currLayer = 0;
  for (u_long i = 1 ; i < track.hits.size() ; i++){
    if(track.hits[i].layer <= track.hits[currLayer].layer){
      return false;
    }
    currLayer = i;
  }

  auto x_diff = globalTrack.back().x - globalTrack.front().x; //Diff between start and end hit in x direction
  auto y_diff = globalTrack.back().y - globalTrack.front().y; // same for y

  //How much does each hit need to shift for each layer to be a straight track.
  auto x_step = x_diff / track.hits.size();
  auto y_step = y_diff / track.hits.size();

  //check if each hit is within step size + bounds
  double bound = 0.4;
  for(u_int i = 0; i < track.hits.size() ; i++){
    if(globalTrack[i].x > globalTrack[0].x + (x_step*i) + bound || globalTrack[i].x < globalTrack[0].x + (x_step*i) - bound){
      return false;
    }

    if(globalTrack[i].y > globalTrack[0].y + (y_step*i) + bound || globalTrack[i].y < globalTrack[0].y + (y_step*i) - bound){
      return false;
    }
  }

  return true;
}
