#define BOOST_TEST_MODULE Test Bergen pCT io - adaptors
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include "io-adaptors/Alphanumeric.h"

namespace bpct
{
namespace io
{

BOOST_AUTO_TEST_CASE(test_Alpanumeric)
{
  std::vector<uint64_t> vec{0x7766554433221100, 0xffeeddccbbaa9988};
  std::stringstream target16;
  target16 << io::Alphanumeric(vec, 16);
  BOOST_CHECK(target16.str() == "00112233445566778899AABBCCDDEEFF\n");
  std::cout << "\nresult for wordsize 16\n"
            << target16.str();

  uint64_t arr[] = {0x7766554433221100, 0xffeeddccbbaa9988};
  std::stringstream target8;
  target8 << io::Alphanumeric(arr, 8);
  BOOST_CHECK(target8.str() == "0011223344556677\n8899AABBCCDDEEFF\n");
  std::cout << "\nresult for wordsize 8\n"
            << target8.str();

  std::stringstream target4;
  target4 << io::Alphanumeric(vec.data(), vec.size(), 4);
  BOOST_CHECK(target4.str() == "00112233\n44556677\n8899AABB\nCCDDEEFF\n");
  std::cout << "\nresult for wordsize 4\n"
            << target4.str();
}

} // namespace io
} // namespace bpct
