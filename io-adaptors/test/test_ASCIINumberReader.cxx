#define BOOST_TEST_MODULE Test Bergen pCT io - adaptors
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <array>
#include <sstream>
#include <type_traits>
#include <stdexcept>
#include "io-adaptors/ASCIINumberReader.h"

namespace bpct
{

BOOST_AUTO_TEST_CASE(test_ASCIINumberReader)
{
  const int verbosity = 2;
  // stream to scan
  std::stringstream stream("0xdead\n0xbeef");
  // the expected wordsize 2 result
  std::array<unsigned char, 4> expectedWs2{0xad, 0xde, 0xef, 0xbe};
  // the expected wordsize 4 result
  std::array<unsigned char, 8> expectedWs4{0xad, 0xde, 0x0, 0x0, 0xef, 0xbe, 0x0, 0x0};

  stream.clear();
  stream.seekg(0);
  auto result1 = io::ASCIINumberReader(stream, 2, verbosity)();
  BOOST_CHECK(result1.size() == expectedWs2.size());
  BOOST_CHECK(memcmp(result1.data(), expectedWs2.data(), expectedWs2.size()) == 0);

  stream.clear();
  stream.seekg(0);
  std::vector<char> result2;
  stream >> io::ASCIINumberReader(result2, 2, verbosity);
  BOOST_CHECK(result2.size() == expectedWs2.size());
  BOOST_CHECK(memcmp(result2.data(), expectedWs2.data(), expectedWs2.size()) == 0);

  stream.clear();
  stream.seekg(0);
  auto result3 = io::ASCIINumberReader(stream, 4, verbosity)();
  BOOST_CHECK(result3.size() == expectedWs4.size());
  BOOST_CHECK(memcmp(result3.data(), expectedWs4.data(), expectedWs4.size()) == 0);

  // check the 16-byte word conversion
  stream.clear();
  stream.seekg(0);
  stream << "0x0123456789abcdeffedcba9876543210";
  stream << "\r\n";
  stream << "\n"; // an empty line should be dropped
  stream << "0xffeeddccbbaa99887766554433221100";
  stream << "\r\n";
  stream << "deadbeef";
  // the expected conversion
  // clang-format off
  std::array<unsigned char, 48> expectedWs16{
    0x10, 0x32, 0x54, 0x76, 0x98, 0xba, 0xdc, 0xfe, 0xef, 0xcd, 0xab, 0x89, 0x67, 0x45, 0x23, 0x01,
    0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
    0xef, 0xbe, 0xad, 0xde, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  // clang-format on
  auto result128 = io::ASCIINumberReader(stream, 16, verbosity)();
  BOOST_CHECK(result128.size() == expectedWs16.size());
  BOOST_CHECK(memcmp(result128.data(), expectedWs16.data(), expectedWs16.size()) == 0);

  // check that exception is thrown if the stream does not contain pure numbers
  stream.clear();
  stream << "invalid number";
  BOOST_CHECK_THROW(io::ASCIINumberReader(stream, 4, 1)(), std::runtime_error);

  // check that exception is thrown if number is too large
  stream.clear();
  stream << "0x11223344";
  BOOST_CHECK_THROW(io::ASCIINumberReader(stream, 2, 1)(), std::runtime_error);
}

} // namespace bpct
