#define BOOST_TEST_MODULE Test Bergen pCT io - adaptors
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <type_traits>
#include "io-adaptors/FileFormat.h"

#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

namespace bpo = boost::program_options;

namespace bpct
{

BOOST_AUTO_TEST_CASE(test_FileFormat)
{
  std::stringstream oss;
  auto const mode = io::FileFormat::BINARY;
  // test streaming operator
  oss << mode;
  BOOST_CHECK(oss.str() == "binary");

  // test integration into boost program options, this is realized via
  // the input streaming operator implemented for FileFormat

  // we test an option scan for the following command line options
  std::vector<const char*> args = {
    "program name", // this one is ignored by option parsing
    "--test-option",
    "root"};

  // Note: there seems to be a limitation in boost program options for strongly
  // types enums, its not possible the set a default value in the usual way
  //     ->default_value(io::FileFormat::BINARY)
  // because the operator<< is not matched, although implemented and working as
  // tested above
  // some links on the net
  //   https://stackoverflow.com/questions/24310053/using-boostprogram-options-with-enum-in-class-in-namespace
  //   https://stackoverflow.com/questions/5211988/boost-custom-validator-for-enum

  // clang-format off
  boost::program_options::options_description od;
  od.add_options()
    ("test-option", bpo::value<io::FileFormat>(), "");
  // clang-format on

  auto checkfunction = [&args, &od](const char* option, auto const& criteria) {
    bpo::variables_map varmap;
    bpo::store(bpo::parse_command_line(args.size(), args.data(), od), varmap);
    BOOST_REQUIRE(varmap.count(option) > 0);
    BOOST_CHECK(varmap[option].as<std::remove_reference_t<decltype(criteria)>>() == criteria);
  };

  checkfunction("test-option", io::FileFormat::ROOT);
  args[2] = "binary";
  checkfunction("test-option", io::FileFormat::BINARY);
  args[2] = "ascii";
  checkfunction("test-option", io::FileFormat::ASCII);

  // now check the legacy functionality, will be removed in the future
  args[2] = "1";
  checkfunction("test-option", io::FileFormat::BINARY);
  args[2] = "2";
  checkfunction("test-option", io::FileFormat::ROOT);
}

} // namespace bpct
