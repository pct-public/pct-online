/// @file   test_FileReaderPolicy.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-02-03
/// @brief  Unit test for FileReaderPolicy utility

#define BOOST_TEST_MODULE Test Bergen pCT io - adaptors
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>
#include <string_view>
#include <type_traits>
#include "io-adaptors/FileReaderPolicy.h"

#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

namespace bpo = boost::program_options;

namespace bpct
{

BOOST_AUTO_TEST_CASE(test_FileReaderPolicy)
{
  const char* testFileName = "test_FileReaderPolicy.dat";
  const char* testData = "Hello pct!";
  std::ofstream testfile(testFileName);
  BOOST_REQUIRE(testfile.good());
  testfile << testData;
  testfile.close();
  boost::program_options::options_description od;
  io::FileReaderPolicy iPolicy;
  // add options of input policy to descriptions
  od << iPolicy;
  std::cout << od << std::endl;

  std::vector<const char*> args = {
    "program name", // this one is ignored by option parsing
    "--ifile",
    testFileName};

  bpo::variables_map varmap;
  BOOST_CHECK_NO_THROW(bpo::store(bpo::parse_command_line(args.size(), args.data(), od), varmap));
  BOOST_REQUIRE(varmap.count("ifile") > 0);
  // configure input policy from command line options
  BOOST_CHECK_NO_THROW(varmap >> iPolicy);

  // read file through input policy
  auto data = iPolicy();
  std::cout << std::string_view(data.data(), data.size()) << std::endl;
  BOOST_CHECK(std::string_view(data.data(), data.size()) == std::string_view(testData, strlen(testData)));
  boost::filesystem::remove(testFileName);
}

BOOST_AUTO_TEST_CASE(test_FileReaderPolicy_configuration)
{
  DefaultVerbosity verbosity(2);
  BOOST_CHECK(verbosity() == 2);

  DefaultWordSize wordsize(16);
  BOOST_CHECK(wordsize() == 16);

  io::DefaultOutputEndianess oEndianess("big");
  BOOST_CHECK(oEndianess() == "big");

  io::DefaultInputEndianess iEndianess("little");
  BOOST_CHECK(iEndianess() == "little");

  std::initializer_list<io::FileFormat> formats{io::FileFormat::ASCII, io::FileFormat::BINARY};

  auto selectEndianess = [](io::FileFormat fmt) -> std::string {
    if (fmt == io::FileFormat::BINARY) {
      return std::string("big");
    } else if (fmt == io::FileFormat::ASCII) {
      return std::string("little");
    }
    return "";
  };
  io::DefaultInputEndianess iDependentEndianess(selectEndianess);
  BOOST_CHECK(iDependentEndianess(io::FileFormat::BINARY) == "big");
  BOOST_CHECK(iDependentEndianess(io::FileFormat::ASCII) == "little");
  BOOST_CHECK(iDependentEndianess(io::FileFormat::ROOT) == "");
  BOOST_CHECK_THROW(iDependentEndianess(), std::logic_error);

  io::FileReaderPolicy iPolicy(verbosity, wordsize, iDependentEndianess, oEndianess, formats);
  boost::program_options::options_description od;
  od << iPolicy;
  std::cout << od << std::endl;

  std::vector<const char*> args = {
    "program name", // this one is ignored by option parsing
    "--wordsize",
    "8",
    "--input-mode",
    "binary",
    "--output-endianness",
    "big",
    "--ifile",
    "somefile.dat"};

  bpo::variables_map varmap;
  BOOST_CHECK_NO_THROW(bpo::store(bpo::parse_command_line(args.size(), args.data(), od), varmap));
  BOOST_REQUIRE(varmap.count("ifile") > 0);
  // configure input policy from command line options
  BOOST_CHECK_NO_THROW(varmap >> iPolicy);

  BOOST_CHECK(iPolicy.getInputFormat() == io::FileFormat::BINARY);
  // have selected big endian for the output and the selector should also return big for binary input
  BOOST_CHECK(iPolicy.getDoByteSwap() == false);
  BOOST_CHECK(iPolicy.getWordSize() == 8);
}

BOOST_AUTO_TEST_CASE(test_FileReaderPolicy_swap)
{
  const char* testFileName = "test_FileReaderPolicy.dat";
  const char* testData = "Hello pct!";
  std::ofstream testfile(testFileName);
  BOOST_REQUIRE(testfile.good());
  testfile << testData;
  testfile.close();

  io::FileReaderPolicy iPolicy(DefaultWordSize(10));
  boost::program_options::options_description od;
  od << iPolicy;

  std::vector<const char*> args = {
    "program name", // this one is ignored by option parsing
    "--input-mode",
    "binary",
    "--output-endianness",
    "big",
    "--input-endianness",
    "little",
    "--ifile",
    testFileName};

  bpo::variables_map varmap;
  BOOST_CHECK_NO_THROW(bpo::store(bpo::parse_command_line(args.size(), args.data(), od), varmap));
  BOOST_REQUIRE(varmap.count("ifile") > 0);
  // configure input policy from command line options
  BOOST_CHECK_NO_THROW(varmap >> iPolicy);
  BOOST_CHECK(iPolicy.getWordSize() == 10);
  BOOST_CHECK(iPolicy.getDoByteSwap() == true);

  // read file through input policy
  auto data = iPolicy();
  std::string reversed(data.rbegin(), data.rend());
  std::cout << std::string_view(data.data(), data.size()) << " " << reversed <<std::endl;
  BOOST_CHECK(reversed == testData);
  boost::filesystem::remove(testFileName);
}

} // namespace bpct
