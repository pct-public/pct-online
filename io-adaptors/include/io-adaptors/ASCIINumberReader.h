/// @file   ASCIINumberReader.h
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-15
/// @brief  Read ASCII numbers from stream to buffer

#ifndef IO_ASCIINUMBERREADER_H
#define IO_ASCIINUMBERREADER_H

#include <vector>
#include <iosfwd> // forward declaration of iostream functionality
#include <stdexcept>

namespace bpct
{
namespace io
{

/// @class ASCIINumberReader
/// Read ASCII formatted numbers from a text stream into a binary buffer.
///
/// Usage:
///   // functor
///   auto buffer = ASCIINumberReader(stream, 16)();
///
///   // streaming operator
///   std::vector<unsigned char> buffer;
///   stream >> ASCIINumberReader(buffer, 16);
///
/// Current implementation:
/// - scanning of hexadecimal numbers, with or without prefix 0x
/// - one number per line
class ASCIINumberReader
{
 public:
  // implementing this for a char buffer, but can be later extended to
  // arbitrary containers with consecutive memory
  using container_type = std::vector<char>;

  /// standard constructor forbidden, need either input stream or target buffer
  ASCIINumberReader() = delete;

  /// constructor providing input stream, to be used with functor
  ASCIINumberReader(std::istream& stream, size_t wordsize, int verbosity = 0)
    : mStream(&stream)
    , mWordsize(wordsize)
    , mVerbosity(verbosity)
    , mContainer(nullptr)
  {
  }

  /// constructor providing target buffer, to be used with stream operator
  ASCIINumberReader(container_type& buffer, size_t wordsize, int verbosity = 0)
    : mStream(nullptr)
    , mWordsize(wordsize)
    , mVerbosity(verbosity)
    , mContainer(&buffer)
  {
  }

  container_type operator()()
  {
    container_type buffer;
    if (mStream) {
      scanStream(buffer, *mStream, mWordsize);
    } else {
      throw std::runtime_error("missing stream");
    }
    return buffer;
  }

  friend void operator>>(std::istream& stream, ASCIINumberReader&& reader)
  {
    if (reader.mContainer) {
      reader.scanStream(*reader.mContainer, stream, reader.mWordsize);
    } else {
      throw std::runtime_error("missing target container");
    }
  }

 private:
  void scanStream(std::vector<char>& buffer, std::istream& stream, size_t wordsize);

  std::istream* mStream = nullptr;
  size_t mWordsize;
  int mVerbosity = 1;
  container_type* mContainer = nullptr;
};

} // namespace io
} // namespace bpct

#endif // IO_ASCIINUMBERREADER_H
