/// @file   AlignmentToySimInputHandler.h
/// @author Rune Almåsbakk
/// @since  2021-10-18
/// @brief  Alignment data preprocessor.

#ifndef ALIGNMENT_INPUT_HANDLER_H
#define ALIGNMENT_INPUT_HANDLER_H

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <tuple>
#include "datamodel/AliTrack.h"


class AlignmentToySimInputHandler {
  private:

    std::unordered_map<int, bpct::data_model::AliTrack> tracks;


  public:
    /*
     * filepath where input data is located
     */
    std::string filePath;

    /*
     * Should line be treated as csv header or not
     */
    bool headerLine; //used to skip the header line

    /*
     * holds ref to name:index for csv headers
     * Get index of e.g posX by: headers["posX"]
     */
    std::unordered_map<std::string, int> headers;



    explicit AlignmentToySimInputHandler(const std::string& filepath);
    ~AlignmentToySimInputHandler();

    /*!
     * Fetch new track from input file stream.
     * @returns void
     */
    bpct::data_model::AliTrack getTrack(std::fstream &fileStream);
};

#endif //ALIGNMENT_INPUT_HANDLER_H