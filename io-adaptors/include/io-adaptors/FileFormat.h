/// @file   FileFormat.h
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-13
/// @brief  File format specifiers

#ifndef IO_FILEFORMAT_H
#define IO_FILEFORMAT_H

#include <iosfwd> // forward declaration of iostream functionality
#include <initializer_list>

namespace bpct
{
namespace io
{

/// @enum FileFormat Strong type definition of file format ids
enum struct FileFormat {
  BINARY,
  ASCII,
  ROOT,
};

constexpr std::initializer_list<FileFormat> FileFormatIDs{FileFormat::BINARY, FileFormat::ASCII, FileFormat::ROOT};

// note: the streaming operators for enum FileFormat are implemented under the
// same namespace, so the compiler/linker can find them

/// operator to hook up the specific type to boost program options
std::istream& operator>>(std::istream& in, enum bpct::io::FileFormat& id);

/// output streaming operator
std::ostream& operator<<(std::ostream& out, const enum bpct::io::FileFormat& id);

} // namespace io
} // namespace bpct

#endif // IO_FILEFORMAT_H
