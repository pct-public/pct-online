/// @file   FileReaderPolicy.h
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-02-03
/// @brief  An input policy reading data from file

#ifndef IO_FILEREADERPOLICY_H
#define IO_FILEREADERPOLICY_H

#include "io-adaptors/FileFormat.h"
#include "core/foundation.h"
#include <vector>
#include <string>
#include <stdexcept>
#include <functional>
#include <variant>
#include <initializer_list>

namespace boost::program_options
{
class options_description;
class variables_map;
} // namespace boost::program_options

namespace bpct
{
namespace io
{

// define helpers for configuring the endianess for the reader, foundation::Value
// is a helper to define unique data types selected by an integral value; the types
// are callable to return the value
using DefaultOutputEndianess = foundation::Value<128, std::string>;

// a special helper which supports a function handler to determine default endianess
// from format id
struct DefaultInputEndianess {
  using value_type = std::string;
  using DefaultInputEndianessFromFileType = std::function<value_type(io::FileFormat)>;
  std::variant<value_type, DefaultInputEndianessFromFileType> value;
  DefaultInputEndianess()
    : value(value_type(""))
  {
  }
  template <typename T>
  DefaultInputEndianess(T const& v)
    : value(v)
  {
  }

  bool hasSelector() const
  {
    return std::get_if<DefaultInputEndianessFromFileType>(&value) != nullptr;
  }

  template <typename... Args>
  value_type operator()(Args&&... args) const
  {
    // could also implement this by means of std::visit
    if (auto p = std::get_if<value_type>(&value)) {
      //static_assert(sizeof...(args) == 0);
      return *p;
    } else if (auto fp = std::get_if<DefaultInputEndianessFromFileType>(&value)) {
      if constexpr (sizeof...(args) == 1) {
        return (*fp)(std::forward<Args>(args)...);
      } else {
        throw std::logic_error("selector function must be called with FileFormat argument");
      }
    }
    return "";
  }

  operator value_type() const
  {
    // could also implement this by means of std::visit
    if (auto p = std::get_if<value_type>(&value)) {
      //static_assert(sizeof...(args) == 0);
      return *p;
    } else if (std::get_if<DefaultInputEndianessFromFileType>(&value)) {
      return "auto";
    }
    return "";
  }
};

/// @class FileReaderPolicy
/// A reader class for data from file or standard input. It is supporting the
/// input policy interface used in the readout software. Configuration from
/// command line options is supported via boost program opptions.
///
/// Configuration:
/// The class defines command line options and implements operators to add those
/// to a boost::program_options::options_description definition. It also implements
/// the input operator to load the configuration from bpo::variables_map which
/// holds the result of the boost program option parsing.
///
/// <pre>
///   boost::program_options::options_description od;
///   io::FileReaderPolicy policy;
///   // add options of input policy to descriptions through streaming operator
///   od << policy;
///   // or
///   od += policy;
///
///   // parsing of the command line options:
///   bpo::variables_map varmap;
///   bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
///   // configure input policy from command line options
///   varmap >> policy;
/// </pre>
///
/// Default configurations by means of the following helpers:
///   - DefaultWordSize(wordsize)
///   - DefaultVerbosity(verbosity)
///   - DefaultOutputEndianess("little") or "big"
///   - DefaultInputEndianess
///
/// Class is callable to support the input policy interface:
/// <pre>
///   // type of the container is FileReaderPolicy::container_type which is
///   // std::vector<char> for the moment
///   auto data = policy();
/// </pre>
///
/// Advanced configuration of the input endianness default value:
/// A callback taking FileFormat as argument can return a value depending
/// on the input file format.
class FileReaderPolicy
{
 public:
  using container_type = std::vector<char>;

  /// default constructor
  FileReaderPolicy() {}
  /// constructor
  template <typename... Args>
  FileReaderPolicy(Args&&... args)
  {
    if constexpr (sizeof...(args) > 0) {
      scanArgs(std::forward<Args>(args)...);
    }
  }
  /// destructor
  ~FileReaderPolicy() {}

  container_type operator()();

  friend boost::program_options::options_description& operator+=(boost::program_options::options_description& od, FileReaderPolicy const& me);

  friend boost::program_options::options_description& operator<<(boost::program_options::options_description& od, FileReaderPolicy const& me);

  friend boost::program_options::variables_map& operator>>(boost::program_options::variables_map& od, FileReaderPolicy& me);

  bool getDoByteSwap() const
  {
    return mDoByteSwap;
  }

  io::FileFormat getInputFormat() const
  {
    return mInputFormat;
  }

  size_t getWordSize() const
  {
    return mWordSize;
  }

 private:
  template <typename Arg, typename... Args>
  void scanArgs(Arg&& arg, Args&&... args)
  {
    using T = std::decay_t<Arg>;
    static_assert(std::is_void_v<T> == false, "invalid void type");
    if constexpr (std::is_same_v<T, DefaultWordSize>) {
      mWordSize = arg;
    } else if constexpr (std::is_same_v<T, DefaultVerbosity>) {
      mVerbosity = arg;
    } else if constexpr (std::is_same_v<T, DefaultOutputEndianess>) {
      mDefaultOutputEndianess = arg;
    } else if constexpr (std::is_same_v<T, DefaultInputEndianess>) {
      mDefaultInputEndianess = arg;
    } else if constexpr (std::is_same_v<T, io::FileFormat>) {
      mAllowedFormats.clear();
      mAllowedFormats.push_back(arg);
      mInputFormat = arg;
    } else if constexpr (std::is_same_v<T, std::initializer_list<io::FileFormat>>) {
      mAllowedFormats = arg;
      if (mAllowedFormats.size() > 0) {
        mInputFormat = mAllowedFormats[0];
      }
    } else {
      // the type-dependent trick, its always false, but we need this type-dependent
      // argument for the static_assert
      static_assert(std::is_void_v<T>, "invalid argument type");
    }

    if constexpr (sizeof...(args) > 0) {
      scanArgs(std::forward<Args>(args)...);
    }
  }

  /// input file name
  std::string mFileName;
  /// format of the input file
  io::FileFormat mInputFormat = io::FileFormat::BINARY;
  /// word size for byte swap and alphanumeric reading
  size_t mWordSize = 1;
  /// activate byte swap
  bool mDoByteSwap = false;
  /// verbosity
  int mVerbosity = 0;
  /// default output endianess
  DefaultOutputEndianess mDefaultOutputEndianess;
  /// default input endianess
  DefaultInputEndianess mDefaultInputEndianess;
  /// allowed input formats
  std::vector<io::FileFormat> mAllowedFormats = {io::FileFormat::BINARY};
};

} // namespace io
} // namespace bpct

#endif // IO_FILEREADERPOLICY_H
