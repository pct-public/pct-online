//
// Created by Rune on 15/01/2022.
//

#ifndef PCT_BERGEN_ALIGNMENTMCSIMINPUTHANDLER_H
#define PCT_BERGEN_ALIGNMENTMCSIMINPUTHANDLER_H

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <unordered_map>
#include <tuple>
#include "alignment/KeyDefs.h"
#include "datamodel/AliTrack.h"
#include "alignment/CoordinateTransformer.h"



class AlignmentMCSimInputHandler {
  private:

    //eventID, trackID
    using trackKey = tuple<int, int>;

    std::unordered_map<trackKey, bpct::data_model::AliTrack, KeyHash<trackKey>> tracks;

    std::unordered_map<trackKey, vector<bpct::data_model::globalHit>, KeyHash<trackKey>> tracks_global;

  public:
    /*
     * filepath where input data is located
     */
    std::string filePath;

    /*
     * Should line be treated as csv header or not
     */
    bool headerLine; //used to skip the header line


    /*
     * holds ref to name:index for csv headers
     * Get index of e.g posX by: headers["posX"]
     */
    std::unordered_map<std::string, int> headers;

    bpct::CoordinateTransformer coordinateTransformer;


    explicit AlignmentMCSimInputHandler(const std::string& filepath);
    ~AlignmentMCSimInputHandler();

    /*!
     * Fetch new track from input file stream.
     * @returns void
     */
    bpct::data_model::AliTrack getTrack(fstream &fileStream);

    /*!
     * Check if track is viable for usage in the alignment process
     * @param track
     * @return true if viable, false otherwise
     */
    bool trackFilter(bpct::data_model::AliTrack &track, vector<bpct::data_model::globalHit> globalTrack);
};

#endif //PCT_BERGEN_ALIGNMENTMCSIMINPUTHANDLER_H
