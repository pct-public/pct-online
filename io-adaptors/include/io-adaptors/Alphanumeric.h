/// @file   Alphanumeric.h
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-19
/// @brief  Alphanumeric converter

#ifndef IO_ALPHANUMERIC_H
#define IO_ALPHANUMERIC_H

#include <iosfwd> // forward declaration of iostream functionality
#include <string>
#include <vector>

namespace bpct
{
namespace io
{

/// class Alphanumeric
/// A formatter converting a buffer to alphanumeric stream. The formatter
/// implements the stream output operator.
///
/// The input can be provided as std::vector, static array, or buffer pointer
/// and size. The object to be converted has to be valid throughout the lifetime
/// of the formatter object.
///
/// Usage:
///   int buffer[] = {0x33221100, 0x77665544};
///   std::cout << Alphanumeric(buffer, 4);
///
/// Wishlist:
/// - configurable prefix
/// - support for hexadecimal, octal and binary
/// - handling of byte order
class Alphanumeric
{
 public:
  Alphanumeric()
  {
  }

  /// constructor for vector
  /// TODO: we can implement support for any container type with consecutive memory
  /// by means of type traits
  template <typename T>
  Alphanumeric(std::vector<T> const& cont, size_t wordsize)
    : mRaw(reinterpret_cast<char const*>(cont.data()))
    , mSize(cont.size() * sizeof(T))
    , mWordsize(wordsize)
  {
  }

  /// contructor for static array
  template <typename T, size_t N>
  Alphanumeric(T const (&arr)[N], size_t wordsize)
    : mRaw(reinterpret_cast<char const*>(arr))
    , mSize(N * sizeof(T))
    , mWordsize(wordsize)
  {
  }

  /// constructor getting pointer to buffer and size
  template <typename T>
  Alphanumeric(T const* buf, size_t size, size_t wordsize)
    : mRaw(reinterpret_cast<char const*>(buf))
    , mSize(size * sizeof(T))
    , mWordsize(wordsize)
  {
  }

  friend std::ostream& operator<<(std::ostream& stream, Alphanumeric const& formatter);

 private:
  char const* mRaw = nullptr;
  size_t mSize = 0;
  size_t mWordsize = 1;
};

} // namespace io
} // namespace bpct

#endif // IO_ALPHANUMERIC_H
