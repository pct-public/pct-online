/// @file   AlignmentToySimInputHandler.cxx
/// @author Rune Almåsbakk
/// @since  2021-10-18
/// @brief  Alignment data preprocessor.

/*
* Here input for the alignment algorithm should be fetched and structured
* so that it can be read by the alignment module.
*/

#include "include/io-adaptors/AlignmentToySimInputHandler.h"


AlignmentToySimInputHandler::AlignmentToySimInputHandler(const std::string& filepath) {
  this->filePath = filepath;
  this->headerLine = true;
}

AlignmentToySimInputHandler::~AlignmentToySimInputHandler() = default;


bpct::data_model::AliTrack AlignmentToySimInputHandler::getTrack(std::fstream &fileStream) {
  int eventID;
  int layer;
  int stave;
  int chip;
  int x;
  int y;

  //Read line into input vector
  std::vector<std::string> input; //holds each line from file
  std::string line; //current line

  while (getline(fileStream, line)){

    std::istringstream ss(line); // string stream
    std::string token; // buffer for current input in getline
    std::vector<std::string> hit; //holds every token read from current line

    while (std::getline(ss, token, ',')) {
      hit.push_back(token);
    }

    //skip processing header line
    if (headerLine) {
      //create lookup table with <header:ID> structure
      //get value by: hit[headers["valueName"]]
      for (unsigned long i = 0; i < hit.size(); i++) {
        headers[hit[i]] = i;
      }
      headerLine = false;
    } else {

      eventID = std::stoi(hit[headers["eventID"]]);
      layer = std::stoi(hit[headers["Layer"]]);
      stave = std::stoi(hit[headers["Stave"]]);
      chip = std::stoi(hit[headers["Chip"]]);
      x = std::stoi(hit[headers["X"]]);
      y = std::stoi(hit[headers["Y"]]);

      bpct::data_model::AliHit localHit{};
      localHit.layer = layer;
      localHit.stave = stave;
      localHit.chip = chip;
      localHit.x = x;
      localHit.y = y;

      tracks[eventID].hits.push_back(localHit);

      //Always 43 layers in simulation data
      if(tracks[eventID].hits.size() >= 43){
        auto ret = tracks[eventID];
        tracks.erase(eventID);
        return ret;
      }
    }
  }

  std::cout << "Reached end of input file." << std::endl;
  return {};
}
