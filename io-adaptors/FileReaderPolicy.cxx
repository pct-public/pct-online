/// @file   FileReaderPolicy.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-02-03
/// @brief  An input policy reading data from file

#include "io-adaptors/FileReaderPolicy.h"
#include "io-adaptors/FileFormat.h"
#include "io-adaptors/ASCIINumberReader.h"
#include "util/logging-tools.h"
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/endian/conversion.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

namespace bpo = boost::program_options;

namespace bpct
{
namespace io
{

FileReaderPolicy::container_type FileReaderPolicy::operator()()
{
  // the first simple implementation opens the file and reads all its content
  // TODO:
  // - partial read
  std::istream* input = &std::cin;
  std::ifstream inputFile;
  if (mFileName != "-") {
    inputFile.open(mFileName);
    if (!inputFile.is_open()) {
      throw std::runtime_error("can not open file '" + mFileName + "' for reading");
    }
    input = &inputFile;
  }

  FileReaderPolicy::container_type buffer;
  if (mInputFormat == io::FileFormat::BINARY) {
    // the size of one read cycle is fixed for now, but in the future we might allow
    // partial reads
    const size_t cycleSize = 64 * 1024;
    size_t bytesRead = 0;
    while (input->good() && !input->eof()) {
      if (bytesRead + cycleSize > buffer.size()) {
        // reserve space for the next cycle
        buffer.resize(bytesRead + cycleSize);
      }
      auto size = (input->read(buffer.data() + bytesRead, cycleSize)).gcount();
      bytesRead += size;
      if (size == 0) {
        break;
      }
    }
    buffer.resize(bytesRead);
  } else if (mInputFormat == io::FileFormat::ASCII) {
    *input >> io::ASCIINumberReader(buffer, mWordSize, mVerbosity);
  } else {
    std::stringstream errmsg("invalid file format specified: ");
    errmsg << mInputFormat;
    throw std::runtime_error(errmsg.str());
  }

  // reverse the data words if the native endianness does not match the output endianness
  if (mDoByteSwap) {
    for (size_t i = 0; i < buffer.size(); i += mWordSize) {
      for (int low = i, high = i + mWordSize - 1; low < high; low++, high--) {
        std::swap(buffer[low], buffer[high]);
      }
    }
  }
  if (mVerbosity > 1) {
    util::hexDump("[FileReaderPolicy] buffer", buffer.data(), buffer.size(), buffer.size());
  } else if (mVerbosity > 0) {
    std::cout << "[FileReaderPolicy] buffer of size " << buffer.size() << std::endl;
  }

  if (inputFile.is_open()) {
    inputFile.close();
  }

  return buffer;
}

bpo::options_description& operator+=(bpo::options_description& od, FileReaderPolicy const& rhs)
{
  // note: because of some shortcoming in boost program option we can not set a default
  // value in the option description
  std::string inputModeHelp = "input format:";
  for (auto const& id : rhs.mAllowedFormats) {
    std::stringstream option;
    option << " " << id << (id == rhs.mInputFormat && rhs.mAllowedFormats.size() > 1 ? " (default)" : "") << ",";
    inputModeHelp += option.str();
  }
  inputModeHelp.pop_back();

  std::stringstream inputEndiannessHelpStr;
  inputEndiannessHelpStr << "endianness of input data:";
  if (rhs.mDefaultInputEndianess.hasSelector()) {
    for (auto const& id : rhs.mAllowedFormats) {
      inputEndiannessHelpStr << " " << rhs.mDefaultInputEndianess(id) << " (" << id << "),";
    }
  }
  std::string inputEndiannessHelp = inputEndiannessHelpStr.str();
  inputEndiannessHelp.pop_back();

  // clang-format off
  od.add_options()
    ("ifile,i", bpo::value<std::string>(), "input file name")
    ("input-mode", bpo::value<io::FileFormat>(), inputModeHelp.c_str())
    ("wordsize", bpo::value<DefaultWordSize::value_type>()->default_value(rhs.mWordSize), "size of words")
    ("input-endianness", bpo::value<std::string>()->default_value(rhs.mDefaultInputEndianess), inputEndiannessHelp.c_str())
    ("output-endianness", bpo::value<std::string>()->default_value(rhs.mDefaultOutputEndianess), "endianness of output");
  // clang-format on
  return od;
}

bpo::options_description& operator<<(bpo::options_description& od, FileReaderPolicy const& rhs)
{
  od += rhs;
  return od;
}

bpo::variables_map& operator>>(bpo::variables_map& varmap, FileReaderPolicy& rhs)
{
  if (varmap.count("ifile") == 0) {
    throw std::runtime_error("Error: No input file specified, aborting ...");
  }
  rhs.mFileName = varmap["ifile"].as<std::string>();

  auto getArg = [&varmap](const char* key, auto const& defaultValue) {
    using ReturnType = std::invoke_result_t<decltype(defaultValue)>;
    if (varmap.count(key) > 0) {
      return varmap[key].as<ReturnType>();
    }
    return defaultValue();
  };

  // read verbosity and wordsize configuration, there are no extra members for default values,
  // so we enter with the current value
  rhs.mVerbosity = getArg("verbosity", DefaultVerbosity(rhs.mVerbosity));
  rhs.mWordSize = getArg("wordsize", DefaultWordSize(rhs.mWordSize));

  if (varmap.count("input-mode") > 0) {
    rhs.mInputFormat = varmap["input-mode"].as<io::FileFormat>();
  }

  auto getEndianess = [&getArg](const char* key, auto&& defaultValue) {
    auto arg = getArg(key, std::move(defaultValue));
    if (arg == "auto") {
      arg = defaultValue();
    }
    if (arg == "little") {
      return boost::endian::order::little;
    } else if (arg == "big") {
      return boost::endian::order::big;
    } else if (arg.empty()) {
      return boost::endian::order::native;
    }
    throw std::runtime_error("invalid endianess argument '" + arg + "'");
  };
  auto inputEndianess = getEndianess("input-endianness", [&rhs]() -> std::string { return rhs.mDefaultInputEndianess(rhs.mInputFormat); });
  auto outputEndianess = getEndianess("output-endianness", rhs.mDefaultOutputEndianess);
  rhs.mDoByteSwap = inputEndianess != outputEndianess;

  return varmap;
}

} // namespace io
} // namespace bpct
