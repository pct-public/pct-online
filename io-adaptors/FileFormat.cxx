/// @file   FileFormat.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-13
/// @brief  File format specifiers

#include "io-adaptors/FileFormat.h"
#include <iostream>
#include <string>

namespace bpct
{
namespace io
{

std::istream& operator>>(std::istream& in, enum bpct::io::FileFormat& id)
{
  using FileFormat = bpct::io::FileFormat;
  // deduce the FileFormat id from the input token, the operator is used
  // to hook up the specific type to boost program options
  std::string token;
  in >> token;
  if (token == "binary" || token == "1") {
    id = FileFormat::BINARY;
  } else if (token == "root" || token == "2") {
    id = FileFormat::ROOT;
  } else if (token == "ascii") {
    id = FileFormat::ASCII;
  } else if (token == "1") {
    // legacy, will be removed at some point when depending applications
    // have been adjusted
    id = FileFormat::BINARY;
  } else if (token == "2") {
    // legacy, will be removed at some point when depending applications
    // have been adjusted
    id = FileFormat::ROOT;
  } else {
    in.setstate(std::ios_base::failbit);
  }
  return in;
}

std::ostream& operator<<(std::ostream& out, const enum bpct::io::FileFormat& id)
{
  using FileFormat = bpct::io::FileFormat;
  switch (id) {
    case FileFormat::BINARY:
      out << "binary";
      break;
    case FileFormat::ASCII:
      out << "ascii";
      break;
    case FileFormat::ROOT:
      out << "root";
      break;
    default:
      out << "unknown file format";
      out.setstate(std::ios_base::failbit);
  }
  return out;
}

} // namespace io
} // namespace bpct
