/// @file   ASCIINumberReader.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-15
/// @brief  Read ASCII numbers from stream to buffer

#include "io-adaptors/ASCIINumberReader.h"
#include "util/logging-tools.h"
#include <string>
#include <iostream>
#include <sstream>
#include <regex>
#include <algorithm>
#include <boost/endian/conversion.hpp>

namespace bpct
{
namespace io
{

// currently we only upport little endian storage format
// this is because we are using uint64 as target type to convert to, also if the
// wordsize is smaller than 8. With little endian, 32bit numbers and smaller
// are correctly represented in the target buffer.
// It's not expected, that the native enianess is going to change, but in that case
// one would also need to swap the two 64 bit numbers making up a 128 bit pRU word.
static_assert(boost::endian::order::native == boost::endian::order::little);

void ASCIINumberReader::scanStream(std::vector<char>& buffer, std::istream& stream, size_t wordsize)
{
  const size_t initialSize = 1024;
  buffer.resize(initialSize * wordsize);
  size_t position = 0;
  std::string line;
  auto scanLine = [&buffer, &position, wordsize](auto type, auto const& line) {
    using NumberT = decltype(type);
    auto prefixOffset = line.find("0x", 0, 2);
    if (prefixOffset == 0) {
      // found the prefix
      prefixOffset = 2;
    } else {
      prefixOffset = 0;
    }
    bool isHex = std::regex_match(line.begin() + prefixOffset, line.end(), std::regex("[0-9a-fA-F]*"));
    if (!isHex) {
      // TODO: we can be smart on that if the ascii file can contain comments
      throw std::runtime_error(std::string("line '") + line + "' has invalid characters, expecting hex number");
    }

    auto nDigits = line.length() - prefixOffset;
    if (wordsize * 2 < nDigits) {
      throw std::runtime_error(std::string("line '") + line + "' has invalid number of digits (" + std::to_string(nDigits) + ") for wordsize " + std::to_string(wordsize));
    }
    if (buffer.size() < position + std::max(wordsize, sizeof(uint64_t))) {
      buffer.resize(buffer.size() + initialSize * wordsize);
    }

    size_t numberOffset = 0;
    do {
      // We could use NumberT for all word sizes but 1, i.e. the 8 bit number. In that
      // particular case the streaming operator does not convert the ascii number but
      // reads the character. In order to be consistent, we always use 64 bit number,
      // and with little endian storage format, this is working for all word sizes.
      // Note: the buffer size check takes account for that we need at least 8 byte.
      auto& number = *reinterpret_cast<uint64_t*>(buffer.data() + position + numberOffset);
      size_t cycleDigits = sizeof(NumberT) * 2 >= nDigits ? nDigits : sizeof(NumberT) * 2;
      std::stringstream(line.substr(prefixOffset + nDigits - cycleDigits, cycleDigits)) >> std::hex >> number;
      nDigits -= cycleDigits;
      numberOffset += sizeof(NumberT);
    } while (nDigits > 0);
  };
  while (std::getline(stream, line)) {
    if (mVerbosity > 1) {
      std::cout << "[ASCIINumberReader] scanning line of length " << line.length() << ": " << line << std::endl;
    }
    if (line.length() == 0) {
      continue;
    }
    while (line.rfind('\r') == line.length() - 1 || line.rfind('\n') == line.length() - 1) {
      line.pop_back();
    }
    // TODO: allow more than one number per line by iterating over tokens
    if (wordsize == 1) {
      scanLine((uint8_t)0, line);
    } else if (wordsize == 2) {
      scanLine((uint16_t)0, line);
    } else if (wordsize == 4) {
      scanLine((uint32_t)0, line);
    } else if (wordsize == 8 || wordsize == 16) {
      scanLine((uint64_t)0, line);
    } else {
      throw std::runtime_error("invalid wordsize: " + std::to_string(wordsize));
    }
    position += wordsize;
  }
  buffer.resize(position);
  if (mVerbosity > 1) {
    util::hexDump("Converted buffer", buffer.data(), buffer.size(), buffer.size());
  }
}

} // namespace io
} // namespace bpct
