/// @file   run-pct-alignment.cxx
/// @author Rune Almåsbakk
/// @since  2021-09-13
/// @brief  Standalone pct Alignment application


#include <iostream>
#include <string>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include "../io-adaptors/include/io-adaptors/AlignmentToySimInputHandler.h"
#include "../io-adaptors/include/io-adaptors/AlignmentMCSimInputHandler.h"
#include "alignment/AlignmentHandler.h"
#include "alignment/OutputHandler.h"
#include "alignment/AlignmentHost.h"

namespace bpo = boost::program_options;

using namespace bpct;

int main(int argc, char* argv[])
{
  // clang-format off
  boost::program_options::options_description od{
    "Standalone pct alignment module\n"
    "(c) 2021 Bergen pCT Collaboration\n\n"
    "Usage:\n"
    "  run-pct-alignment --ifile input_path --ofile output_path --numTracks number\n"
    "  run-pct-alignment --simulation --numTracks number\n"
  };
  od.add_options()
    ("ifile,i", bpo::value<std::string>(), "Input file name")
    ("ofile,o", bpo::value<std::string>(), "Output file name")
    ("numTracks,n", bpo::value<int>(), "How many tracks to analyze")
    ("simulation,s", "Simulation run with predefined tracks")
    ("help,h", "Help!");

  // clang-format on
  std::stringstream cmdLineError;
  auto printhelp = [&cmdLineError, &od](int exitvalue = 0) {
    std::cout << od << std::endl;
    if (!cmdLineError.str().empty()) {
      std::cout << cmdLineError.str() << std::endl
                << std::endl;
    }
    exit(exitvalue);
  };
  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    std::cout << "Error parsing command line arguments: " << e.what() << std::endl
              << std::endl;
    printhelp(1);
  }

  if(varmap.count("numTracks") == 0) {
    cmdLineError << "missing parameter, please specify number of tracks to analyze";
    printhelp(1);
  }

  auto numTracks = varmap["numTracks"].as<int>();

  if (varmap.count("simulation") > 0) {
    //Define the host class
    //If function names are renamed this host class will need refactoring.
    using AlignmentHost = AlignmentHost<AlignmentToySimInputHandler, AlignmentHandler, OutputHandler>;

    //create policy instances
    auto ih = AlignmentToySimInputHandler("../alignment/simdata/simData.csv");

    auto ah = AlignmentHandler();

    auto oh = OutputHandler("../alignment/simdata/toySimOutput.json");

    //create the host object with policy instances
    AlignmentHost host = AlignmentHost(ih, ah, oh, numTracks);

    host.run();
    return {};
  }

  if (varmap.count("help") > 0) {
    printhelp();
  }
  if (varmap.count("ifile") == 0 || varmap.count("ofile") == 0) {
    cmdLineError << "missing parameter, please specify input and output files";
    printhelp(1);
  }

  auto ifilename = varmap["ifile"].as<std::string>();
  auto ofilename = varmap["ofile"].as<std::string>();

  //Define the host class 
  using AlignmentHost = AlignmentHost<AlignmentMCSimInputHandler, AlignmentHandler, OutputHandler>;

  //create policy instances
  auto ih = AlignmentMCSimInputHandler(ifilename);

  auto ah = AlignmentHandler();

  auto oh = OutputHandler(ofilename);

  //create the host object with policy instances
  AlignmentHost host = AlignmentHost(ih, ah, oh, numTracks);

  host.run();

}
