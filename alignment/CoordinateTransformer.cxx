/// @file   coordinateTransformation.cxx
/// @author Rune Almåsbakk
/// @since  2021-09-02
/// @brief  This class transforms layer, stave, chip... into global x,y,z coordinates

#include <iostream>
#include "alignment/CoordinateTransformer.h"


namespace bpct
{

CoordinateTransformer::CoordinateTransformer() {
  geo = defaultGeometry;
}

CoordinateTransformer::~CoordinateTransformer() = default;


int32_t bpct::CoordinateTransformer::getX(uint16_t layer, data_model::AlpideHit const* hit) {

  int32_t x = 0;

  int chip = hit->chip;

  if(chip >= 8){ //since chip id 7 is not in use we decrement by one.
    chip -= 1;
  }

  if(layer % 2 == 0){
    if(hit->stave % 2 == 0){
      x = chip * geo.NUM_PIXEL_WIDTH + hit->x;
    }else{
      if(chip == 0){
        x = (geo.NUM_PIXEL_WIDTH - 1) - hit->x;
      }else{
        x = (geo.NUM_PIXEL_WIDTH * chip) + ((geo.NUM_PIXEL_WIDTH - 1) - hit->x);
      }
    }
  }else{
    auto totalPixels = geo.NUM_PIXEL_WIDTH * geo.NUM_CHIPS_PER_STAVE - 1;
    if(hit->stave % 2 == 0){
      x = chip * geo.NUM_PIXEL_WIDTH + hit->x;
    }else{
      if(chip == 0){
        x = (geo.NUM_PIXEL_WIDTH - 1) - hit->x;
      }else{
        x = (geo.NUM_PIXEL_WIDTH * chip) + ((geo.NUM_PIXEL_WIDTH - 1) - hit->x);
      }
    }
    x = totalPixels - x;
  }

  return x;
}

int32_t bpct::CoordinateTransformer::getY(data_model::AlpideHit const* hit) {

  int32_t y = 0;

  if (hit->stave % 2 == 0) {
    y = (geo.NUM_PIXEL_HEIGHT * hit->stave) + hit->y;
  } else {
    // -1 to count for 0 index. hit on y == 0 is the same as 511 on odd staves
    y = (geo.NUM_PIXEL_HEIGHT * hit->stave) + (geo.NUM_PIXEL_HEIGHT - hit->y - 1);
  }

  return y;
}


std::vector<int32_t> bpct::CoordinateTransformer::localToGlobalHitCoordinatePixel(uint32_t layer,
                                                                                  data_model::AlpideHit* hit) {
  
  //x, y, x, return 0,0,0 by default
  std::vector<int32_t> output = {0, 0, 0}; 

  //translate x value
  output[0] = getX(layer, hit);

  //translate y value
  output[1] = getY(hit);

  //translate z value
  output[2] = layer;


  if(output[0] > (geo.NUM_PIXEL_WIDTH) * geo.NUM_CHIPS_PER_STAVE || output[0] < 0)
    throw std::runtime_error("local to global hit coordinate out of bounds: " + std::to_string(output[0]) +
    " for layer:" + std::to_string(layer) + " and x:" + std::to_string(hit->x));
  if(output[1] > (geo.NUM_PIXEL_HEIGHT) * geo.NUM_STAVES_PER_LAYER || output[1] < 0)
    throw std::runtime_error("local to global hit coordinate out of bounds: " + std::to_string(output[1]) +
                             " for stave:" + std::to_string(hit->stave) + " and y:" + std::to_string(hit->y));
  if(output[2] > geo.NUM_LAYERS || output[2] < 0)
    throw std::runtime_error("local to global hit coordinate out of bounds: " + std::to_string(output[1]) +
                             " for stave:" + std::to_string(hit->stave) + " and y:" + std::to_string(hit->y));
  
  return output;
}

std::vector<double> bpct::CoordinateTransformer::localToGlobalHitCoordinateMM(uint32_t layer, data_model::AlpideHit* hit) {
  
  std::vector<double> output = {0, 0, 0};
  
  //get pixels away from (0,0,0) in x direction
  auto x = getX(layer, hit);

  // x/geo.NUM_PIXEL_WIDTH gives number of chips away from origin
  // the hit is converted to int for rounding to floor value
  double xChipGaps = int(x / geo.NUM_PIXEL_WIDTH) * (geo.CHIP_GAP + geo.CHIP_GAP_OFFSET);
  double xmm = 0.0;

  //Add pixel width and offset for each pixel in mm
  xmm += x * (geo.PIXEL_SIZE_X + geo.PIXEL_SIZE_OFFSET_X);
  //Add offsets between each chip in mm
  xmm += xChipGaps;

  output[0] = xmm;

  //transform y into millimeters
  //FIXME: should be chip distance from stave to stave, not chip distance from chip to chip on same stave. something like STAVE_detectorGeometry.CHIP_GAP
  auto y = getY(hit);
  // y/ALPIDE_PIXELS_HEIGHT_NUM gives number of chips away from origin the hit is (converted to int for rounding to floor value)
  double yChipGaps = int(y / geo.NUM_PIXEL_HEIGHT) * (geo.CHIP_GAP + geo.CHIP_GAP_OFFSET);
  double ymm = 0.0;

  //Add pixel width and offset for each pixel in mm
  ymm += y * (geo.PIXEL_SIZE_Y + geo.PIXEL_SIZE_OFFSET_Y);
  //Add offset between each stave in mm
  ymm += yChipGaps;

  output[1] = ymm;
  
  //transform z into millimeters
  double zLayerGap = (geo.LAYER_DEPTH +
                      geo.LAYER_DEPTH_OFFSET +
                      geo.LAYER_GAP +
                      geo.LAYER_GAP_OFFSET +
                      geo.ABSORBER_LAYER +
                      geo.ABSORBER_LAYER_OFFSET)
                      * layer ;

  double zmm = 0.0;

  if (hit->stave % 2 == 0) { // no extra gap
    zmm = zLayerGap;
  } else {
    zmm = zLayerGap;
  }

  output[2] = zmm;


  //Check if out of bounds
  if(xmm > (geo.CHIP_WIDTH + geo.CHIP_OFFSET) * geo.NUM_CHIPS_PER_STAVE
  || xmm < 0){
    throw std::runtime_error("Global x coordinate out of bounds: " + std::to_string(xmm) +
                             "for x=" +"chip:"+ std::to_string(hit->chip) +
                             "+" + "x:" + std::to_string(hit->x));
  }
  if(ymm > (geo.CHIP_HEIGHT + geo.CHIP_OFFSET) * geo.NUM_STAVES_PER_LAYER
  || ymm < 0){
    throw std::runtime_error("Global y coordinate out of bounds: " + std::to_string(xmm) +
                             "for y=" +"stave:"+ std::to_string(hit->stave) +
                             "+" + "y:" + std::to_string(hit->y));
  }
  if(zmm > (geo.LAYER_DEPTH + geo.LAYER_GAP + geo.ABSORBER_LAYER + geo.LAYER_GAP_OFFSET)
           * geo.NUM_LAYERS || zmm < 0){
    throw std::runtime_error("Global z coordinate out of bounds: " + std::to_string(zmm) +
                             "for z=" +"layer:"+ std::to_string(layer));
  }


  return output;
}

std::vector<int> CoordinateTransformer::globalToLocalHitCoordinatePixel(uint32_t x, uint32_t y, uint32_t z)
{
  int layer, stave, chip, local_x, local_y;

  layer = z;
  //rounding to floor values
  stave = int(y/(geo.NUM_PIXEL_HEIGHT + 1));
  chip = int(x/(geo.NUM_PIXEL_WIDTH + 1));

  if(layer % 2 == 0){
    if(chip > 6) chip++;
  } else {
    if(chip >= 2){
      chip = geo.NUM_CHIPS_PER_STAVE - chip - 1;
    } else {
      chip = geo.NUM_CHIPS_PER_STAVE - chip;
    }
  }

  if(layer % 2 == 0){
    if(stave % 2 == 0){
      local_x = x % (geo.NUM_PIXEL_WIDTH - 1);
    } else {
      local_x = (geo.NUM_PIXEL_WIDTH - 1) - (x % (geo.NUM_PIXEL_WIDTH - 1));
    }
  } else {
    int total_pixels = (geo.NUM_PIXEL_WIDTH) * geo.NUM_CHIPS_PER_STAVE - 1;
    if(stave % 2 == 0){
      local_x = (total_pixels - x) % (geo.NUM_PIXEL_WIDTH);
    } else {
      local_x = (geo.NUM_PIXEL_WIDTH - 1) - ((total_pixels - x) % (geo.NUM_PIXEL_WIDTH - 1));
    }
  }

  if(stave % 2 == 0){
    local_y = y % (geo.NUM_PIXEL_HEIGHT);
  } else {
    local_y = (geo.NUM_PIXEL_HEIGHT) - (y % (geo.NUM_PIXEL_HEIGHT));
  }

  //make sure the values are within bounds
  if(layer < 0 || layer > geo.NUM_LAYERS)
    throw std::runtime_error("LAYER out of bounds: " + std::to_string(layer) +
                             " for z = " + std::to_string(z + 225.2440) +
                             " in globalToLocalHitCoordinatePixel");
  if(stave < 0 || stave > geo.NUM_STAVES_PER_LAYER)
    throw std::runtime_error("STAVE out of bounds: "+ std::to_string(stave) +
                             " for y = " + std::to_string(y) +
                             " in globalToLocalHitCoordinatePixel");
  if(chip == 7 || chip < 0 || chip > geo.NUM_CHIPS_PER_STAVE)
    throw std::runtime_error("CHIP out of bounds: "+ std::to_string(chip) +
                             " for x = " + std::to_string(x) +
                             " in globalToLocalHitCoordinatePixel");
  if(local_x > geo.NUM_PIXEL_WIDTH || local_x < 0)
    throw std::runtime_error("X out of bounds: "+ std::to_string(local_x) +
                             " for x = " + std::to_string(x) +
                             " in globalToLocalHitCoordinatePixel");
  if(local_y > geo.NUM_PIXEL_HEIGHT || local_y < 0)
    throw std::runtime_error("Y out of bounds: "+ std::to_string(local_y) +
                             " for y = " + std::to_string(y) +
                             " in globalToLocalHitCoordinatePixel");

  //fill output vector and return
  std::vector<int> output;
  output.push_back(layer);
  output.push_back(stave);
  output.push_back(chip);
  output.push_back(local_x);
  output.push_back(local_y);

  return output;
}

data_model::AliHit CoordinateTransformer::globalToLocalHitCoordinateMM(double x, double y, double z) {

  int layer, stave, chip, local_x, local_y;
  double chipRest, staveRest;

  //layer
  //TODO: this is bad hardcoded as layer 0 starts at 225.24 in MC simulation
  //FIXME: Suddenly not 5.5 mm between layers starting at layer 34 in simulation. The number below should be 225.2440
  z -= 225.2430;
  if(z < geo.LAYER_0_TO_1_GAP){
    layer = 0;
  } else if(geo.LAYER_0_TO_1_GAP < z && z < geo.LAYER_1_TO_2_GAP + geo.LAYER_0_TO_1_GAP ) {
    layer = 1;
  }else {
    //TODO: add offsets
    layer = (z - (geo.LAYER_0_TO_1_GAP + geo.LAYER_1_TO_2_GAP)) /
            (geo.LAYER_DEPTH + geo.LAYER_GAP + geo.ABSORBER_LAYER);
    layer += geo.NUM_TRACKING_LAYERS; // add tracking layers back
  }

  //stave
  double temp;
  if(y > 0){
    temp = (y/ (geo.PIXEL_SIZE_X * geo.NUM_PIXEL_HEIGHT)) + (geo.NUM_STAVES_PER_LAYER / 2);
    stave = (int)temp;
    staveRest = fmod(y, geo.PIXEL_SIZE_X * geo.NUM_PIXEL_HEIGHT);
  }else if(y < 0){
    temp = (fabs(y) / (geo.PIXEL_SIZE_X * geo.NUM_PIXEL_HEIGHT)) + (geo.NUM_STAVES_PER_LAYER / 2);
    stave = (int)temp;
    staveRest = fmod(fabs(y), geo.PIXEL_SIZE_X * geo.NUM_PIXEL_HEIGHT);

    std::vector<int> flip{11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    stave = flip[stave];
  } else {
    //The middle of the DTC is a space between pixels. This implementation does not handle this.
    throw std::runtime_error("Global coordinate 0.0... is not possible!");
  }

  //chip
  double temp_x = fabs(x); //no need to work with negative x value
  //TODO: Had to define pixel width as chip_width/num_pixels to get the required accuracy.
  // This should probably be addressed
  double pixelWidth = geo.CHIP_WIDTH/geo.NUM_PIXEL_WIDTH;
  if(temp_x <= geo.CHIP_WIDTH / 2.0){
    chip = 4;
    if(x >= 0){
      chipRest = (geo.CHIP_WIDTH / 2.0) + temp_x;
    } else {
      chipRest = (geo.CHIP_WIDTH / 2.0) - temp_x;
    }
  }else{
    temp = (geo.NUM_CHIPS_PER_STAVE / 2.0) - (temp_x / geo.CHIP_WIDTH);
    chip = (int)temp;

    if(x >= 0){
      chipRest = fmod(temp_x + (geo.CHIP_WIDTH / 2.0), geo.CHIP_WIDTH);
    } else {
      chipRest = fmod(temp_x + (geo.CHIP_WIDTH / 2.0), geo.CHIP_WIDTH);
      chipRest = (pixelWidth * geo.NUM_PIXEL_WIDTH) - chipRest;
    }
  }

  std::vector<int> flip{9, 8, 6, 5, 4, 3, 2, 1, 0};
  if(x >= 0){
    chip = flip[chip];
  }
  //Use the rest values to calculate the local on chip hit
  local_x = chipRest / pixelWidth;
  local_y = staveRest / geo.PIXEL_SIZE_Y;


  //Add element rotations
  if(layer % 2 == 0) {
    if(stave % 2 != 0){
      local_x = geo.NUM_PIXEL_WIDTH - local_x - 1;
    }
  } else {
    if(stave % 2 == 0){
      local_x = geo.NUM_PIXEL_WIDTH - local_x - 1;
    }
    if(chip > 6) chip--;
    chip = flip[chip];
  }

  if(stave <= 5){
    if(stave % 2 == 0){
      local_y = geo.NUM_PIXEL_HEIGHT - local_y;
    }
  }else{
    if(stave % 2 != 0){
      local_y = geo.NUM_PIXEL_HEIGHT - local_y;
    }
  }

  //there are some issues with 0-indexing. This is a workaround
  if(local_x < 0) local_x = 0;

  //make sure the values are within bounds
  if(layer < 0 || layer > geo.NUM_LAYERS)
    throw std::runtime_error("LAYER out of bounds: " + std::to_string(layer) +
                                  " for z = " + std::to_string(z + 225.2440) +
                                  " in globalToLocalHitCoordinateMM");
  if(stave < 0 || stave > geo.NUM_STAVES_PER_LAYER)
    throw std::runtime_error("STAVE out of bounds: "+ std::to_string(stave) +
                                  " for y = " + std::to_string(y) +
                                  " in globalToLocalHitCoordinateMM");
  if(chip == 7 || chip < 0 || chip > geo.NUM_CHIPS_PER_STAVE)
    throw std::runtime_error("CHIP out of bounds: "+ std::to_string(chip) +
                                  " for x = " + std::to_string(x) +
                                  " in globalToLocalHitCoordinateMM");
  if(local_x > geo.NUM_PIXEL_WIDTH || local_x < 0)
    throw std::runtime_error("X out of bounds: "+ std::to_string(local_x) +
                                  " for x = " + std::to_string(x) +
                                  " in globalToLocalHitCoordinateMM");
  if(local_y > geo.NUM_PIXEL_HEIGHT || local_y < 0)
    throw std::runtime_error("Y out of bounds: "+ std::to_string(local_y) +
                                  " for y = " + std::to_string(y) +
                                  " in globalToLocalHitCoordinateMM");

  data_model::AliHit output;
  output.layer = layer;
  output.stave = stave;
  output.chip = chip;
  output.x = local_x;
  output.y = local_y;
  return output;
}
} // bpct