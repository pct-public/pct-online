//
// Created by rune on 02.11.2021.
//


#include "alignment/OutputHandler.h"

OutputHandler::OutputHandler(const string& filepath)
{
  this->filePath = filepath;
}

OutputHandler::~OutputHandler() = default;

void OutputHandler::insertValues(const unordered_map<staveKey , tuple<double,double,double,double,double,double>, KeyHash<staveKey>>& data)
{
  FileWrapper fw;

  auto ofilestream = fw.openFile(this->filePath, std::fstream::out);

  
  int layer, stave;
  double x, y, z, yaw, pitch, roll;
  //build the json object (root)
  for(auto pair: data){
    std::tie(layer,stave) = pair.first; //Layer stave chip used as key
    std::tie(x, y, z, yaw, pitch, roll) = pair.second; 

    root["Layer "+to_string(layer)]["Stave "+to_string(stave)]["x"] = to_string(x);
    root["Layer "+to_string(layer)]["Stave "+to_string(stave)]["y"] = to_string(y);
    root["Layer "+to_string(layer)]["Stave "+to_string(stave)]["z"] = to_string(z);
    root["Layer "+to_string(layer)]["Stave "+to_string(stave)]["yaw"] = to_string(yaw);
    root["Layer "+to_string(layer)]["Stave "+to_string(stave)]["pitch"] = to_string(pitch);
    root["Layer "+to_string(layer)]["Stave "+to_string(stave)]["roll"] = to_string(roll);
  }
  //write json root object to file
  ofilestream << root << endl;
  ofilestream.close();
}


