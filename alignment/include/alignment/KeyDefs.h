//
// Created by Rune on 03/01/2022.
//

#ifndef PCT_BERGEN_KEYDEFS_H
#define PCT_BERGEN_KEYDEFS_H

#include <tuple>
#include "boost/range/combine.hpp"
#include "boost/functional/hash.hpp"

using namespace  std;

//The key used in the unordered maps (layer, stave, chip)
using chipKey = tuple<int, int, int>;
using staveKey = tuple<int, int>;
using layerKey = int;

//The hash function used on the keys
template <typename T>
struct KeyHash {
    std::size_t operator()(const T& key) const
    {
        return boost::hash_value(key);
    }
};


//Alignment parameter values
//Currently representing: x, y, z, yaw, pitch, roll
using alignParam = tuple<double,double,double,double,double,double>;


#endif //PCT_BERGEN_KEYDEFS_H
