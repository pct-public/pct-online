//
// Created by rune on 02.11.2021.
//

#ifndef PCT_BERGEN_OUTPUTHANDLER_H
#define PCT_BERGEN_OUTPUTHANDLER_H

#include <string>
#include "KeyDefs.h"
#include "json/json.h"
#include "FileWrapper.h"

using namespace std;

class OutputHandler
{
  private:
    /*
     * Json object holding the content of the file
     */
    Json::Value root;

   public:
    /*
     * Holds the path to the output file
     */
    string filePath;

    explicit OutputHandler(const string& filepath);
    ~OutputHandler();

    /*!
     * @brief insert all values in the input hashmap into the output file.
     */
    void insertValues(const unordered_map<staveKey , alignParam, KeyHash<staveKey>>& data);
};

#endif //PCT_BERGEN_OUTPUTHANDLER_H
