/// @file   Regression.h
/// @author Rune Almåsbakk
/// @since  2021-10-22
/// @brief  Create a regression model based on given x,y values

#ifndef PCT_BERGEN_REGRESSION_H
#define PCT_BERGEN_REGRESSION_H

#include <cstdint>
#include <vector>
#include "boost/range/combine.hpp"
#include <iostream>
#include <cmath>


//TODO: Implement a more robust regression model, this can also be done using premade libraries
template <typename T>
class Regression
{
  private:
    T b0 = 0.0;//The intercept point
    T m = 0.0; //The slope
    bool isFit = false; //if the model is fit with data yet. Can't do predict if this is false.

    /*
     * holds the residuals for each point to the predicted line
     */
    std::vector<T> residuals;

    /*!
     * @brief produce the residuals for each point in the given dataset
     * @param x the x values to be predicted
     * @param y the original values
     */
    void produceResiduals(const std::vector<T>& x, const std::vector<T>& y);

  public:
    //Regression model Constructor
    Regression();
    ~Regression();


    [[nodiscard]] const std::vector<T>& getResiduals() const;

    /*!
     * @brief Fit the model with feature and target values.
     * @param x
     * @param y
     */
    void fit(const std::vector<T>& x, const std::vector<T>& y);

    /*!
     * Given a list of x values return the predicted y values based on the regression line.
     */
    [[nodiscard]] std::vector<T> predict(const std::vector<T>& x) const;

    /*!
     * @brief if class is used for multiple dataset this can be used to make sure the model is fit before used
     * @param value
     */
    void setIsFit(bool value);

    T getB0() const;
    T getM() const;
};

template <typename T>
Regression<T>::Regression() = default;

template <typename T>
Regression<T>::~Regression() = default;

template <typename T>
void Regression<T>::fit(const std::vector<T>& x, const std::vector<T>& y)
{
  residuals.clear();

  //Using OLS (ordinary least square) method to calculate regression line
  if(x.size() != y.size()){
    std::cout << "X and Y input to fit function has to be same size. Got: \n"
              << "x: " << x.size() << "\n"
              << "y: " << y.size() << "\n"
              << std::endl;
    return;
  }

  T sumX = 0;
  T sumY = 0;
  T sumX2 = 0;
  T sumXY = 0;
  T N = x.size();
  T m_;
  T b;

  T x_temp;
  T y_temp;
  for(auto tup: boost::combine(x, y)){
    boost::tie(x_temp, y_temp) = tup;

    sumX += x_temp;
    sumY += y_temp;
    sumX2 += x_temp * x_temp;
    sumXY += x_temp * y_temp;
  }

  m_ = ((N*sumXY)-(sumX*sumY))/((N*sumX2)-((sumX*sumX)));
  b = ((sumY)-(m_*sumX))/N;

  b0 = b;
  m = m_;

  produceResiduals(x, y);
  setIsFit(true);
}

template <typename T>
std::vector<T> Regression<T>::predict(const std::vector<T>& x) const
{
  if(!isFit){
    std::cout << "Model must be fit before predict is called!" << std::endl;
    return {};
  }
  std::vector<T> output;
  output.reserve(x.size());

  for([[maybe_unused]] auto i: x){
    output.push_back( b0 + i * m );
  }
  return output;
}

template <typename T>
void Regression<T>::setIsFit(bool value)
{
  this->isFit = value;
}


template <typename T>
void Regression<T>::produceResiduals(const std::vector<T>& x, const std::vector<T>& y)
{

  T x_, y_;
  for(auto tup: boost::combine(x, y)){
    boost::tie(x_, y_) = tup;

    residuals.push_back( (b0 + x_ * m) - y_ ); //predicted value - global
  }
}


template <typename T>
const std::vector<T>& Regression<T>::getResiduals() const
{
  if(!isFit){
    std::cout << "Model must be fit before residuals can be made" << std::endl;
  }
  return residuals;
}


template <typename T>
T Regression<T>::getB0() const
{
  return b0;
}


template <typename T>
T Regression<T>::getM() const
{
  return m;
}

#endif //PCT_BERGEN_REGRESSION_H
