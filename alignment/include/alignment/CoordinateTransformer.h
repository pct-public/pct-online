/// @file   coordinateTransformation.cxx
/// @author Rune Almåsbakk
/// @since  2021-09-02
/// @brief  This class transforms layer, stave, chip... into global x,y,z coordinates


#ifndef COORDINATE_TRANSFORMER_H
#define COORDINATE_TRANSFORMER_H

#include <vector>
#include <cstdint>
#include <cassert>
#include <string>
#include <cmath>
#include <stdexcept>
#include "datamodel/AlpideHit.h"
#include "datamodel/RUDataHeader.h"
#include "../../../geometry/DetectorConstants.h"
#include "datamodel/AliTrack.h"

namespace bpct
{

/*!
  @class CoordinateTransformer
  @brief Get the global hit position from single hit data
*/
class CoordinateTransformer
{
  private:
    // used to transform values from a single hit
    int32_t getX(uint16_t layer, data_model::AlpideHit const* hit);
    int32_t getY(data_model::AlpideHit const* hit);


  public:
    //Constructor initialize with default detector geometry
    CoordinateTransformer();
    ~CoordinateTransformer();

    /*!
     * The used geometry
     */
    DetectorGeometry geo;

    /*!
      @param the local hit information in form of layer, AlpideHit
      @returns the position coordinates of the hit inside the sensor in pixel distance from (0,0,0).
    */
    std::vector<int32_t> localToGlobalHitCoordinatePixel(uint32_t layer, data_model::AlpideHit* hit);

    /*!
      @param the local hit information in form of layer, AlpideHit
      @returns the position coordinates of the hit inside the sensor in millimeters distance from pixel (0,0,0)
    */
    std::vector<double> localToGlobalHitCoordinateMM(uint32_t layer, data_model::AlpideHit* hit);

    /*!
     * @brief transform a global hit into local hit pixel based
     * @param x
     * @param y
     * @param z
     * @return layer, stave, chip, x, y
     */
    std::vector<int> globalToLocalHitCoordinatePixel(uint32_t x, uint32_t y, uint32_t z);

    /*!
     * @brief Transform a global hit into local hit, this conversion may introduce inaccuracy as millimeter does not
     * transform directly into a pixel value.
     *
     * NOTE: This implementation is made for the Monte Carlo simulation data. This data has origin in the middle
     * of the DTC which makes some calculations have extra values added or subtracted to account for this.
     *
     * @param x
     * @param y
     * @param z
     * @return layer, stave, chip, x, y as data_model::AliHit
     */
    data_model::AliHit globalToLocalHitCoordinateMM(double x, double y, double z);
};

} // namespace bpct

#endif //COORDINATE_TRANSFORMER_H
