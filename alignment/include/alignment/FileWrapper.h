//
// Created by Rune Almåsbakk on 07.03.2022.
/// @brief A lot of reading and writing to files makes code less complicated when file handling is done through a class
//

#ifndef PCT_BERGEN_ALIGNMENT_FILEWRAPPER_H
#define PCT_BERGEN_ALIGNMENT_FILEWRAPPER_H

#include <fstream>

using namespace std;

class FileWrapper{
	private:
	public:
	  FileWrapper();
	  ~FileWrapper();

	  fstream openFile(string& filename, ios_base::openmode mode);
};
#endif
