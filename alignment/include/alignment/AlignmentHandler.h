/// @file   AlignmentHandler.h
/// @author Rune Almåsbakk
/// @since  2021-10-18
/// @brief  Offset producer for the alignment module


#ifndef ALIGNMENT_ALIGNMENT_HANDLER_H
#define ALIGNMENT_ALIGNMENT_HANDLER_H

//Uncomment this line to draw a root histogram with values of an element
//#define DRAW_DIAGRAM

#include <iostream>
#include <unordered_map>
#include <vector>
#include <tuple>
#include <numeric>
#include "KeyDefs.h"
#include "datamodel/AliTrack.h"
#include "Regression.h"
#include "CoordinateTransformer.h"
#include "TH1.h"
#include "TF1.h"


#ifdef DRAW_DIAGRAM
  #include <TVectorT.h>
  #include <TVectorD.h>
  #include <TCanvas.h>
  #include <TApplication.h>
#endif

using namespace std;

class AlignmentHandler{
  private:
    /*!
     * For each chip hold all offset parameters that are recorded.
     * Theses will be averaged later to produce the final offset.
     */
    unordered_map<chipKey, vector<double>, KeyHash<chipKey>> offsetsX;

    /*!
     * For each chip hold all offset parameters that are recorded.
     * Theses will be averaged later to produce the final offset.
     */
    unordered_map<chipKey, vector<double>, KeyHash<chipKey>> offsetsY;
  	
    /*!
     * @param track
     * @brief do linear regression on the given values to produce offset values for x and y
     */
    void produceOffsetsXY(const bpct::data_model::AliTrack& track);
    
    /*!
     * @brief Given all residuals from an element, histogram them and use gaussian fit to find 
     * the most likely offset, data should be normalized around the most likely offset.
     * @param residuals from the analyzed element
     */
    static double findMostLikelyOffset(const vector<double>& residuals);

  	/*!
  	 * transform local track data into global mm and pixel values
  	 * index 0 = millimeter, vector<vector<double>
  	 * index 1 = pixel, vector<vector<int>
  	 */
    tuple<vector<vector<double>>,vector<vector<int>>> createGlobalHitLists(const bpct::data_model::AliTrack &track);

    /*!
    * Throw error if lists are unequal length. Rest of algorithm does not work if
    * this does not hold
    */
    static void assertSameLength(const bpct::data_model::AliTrack& track, const vector<double>& x_global, const vector<double>& y_global,
    	const vector<double>& residuals);
    		
  public:
    AlignmentHandler();
    ~AlignmentHandler();

    //TODO: Add possibility to read geometry from file from command-line
    bpct::CoordinateTransformer coordinateTransformer;

    /*
     * A hashmap that should contain the offset parameters produced.
     * the key is a tuple of layer,stave
     * The value is all 6 alignment parameters as tuple.
     */
    unordered_map<staveKey, alignParam, KeyHash<staveKey>> output;

    /*!
     * @brief average the recorded offsets to produce the final offset parameters
     */
    void finalizeTrackOffsets();
 
    /*!
     * @brief produce offsets from each track in the tracks vector, inserted into the offsetsX/Y vectors.
     * @param track
    */
    void analyzeTrackXY(const bpct::data_model::AliTrack& track);
};
#endif //ALIGNMENT_ALIGNMENT_HANDLER_H


