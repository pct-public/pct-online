//
// Created by rune on 07.11.2021.
//

#ifndef PCT_BERGEN_ALIGNMENTHOST_H
#define PCT_BERGEN_ALIGNMENTHOST_H

#include <iostream>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <chrono>
#include <fstream>
#include "datamodel/AliTrack.h"
#include "alignment/FileWrapper.h"

using namespace std;

/*
 * Class representing the host for the policy based implementation of the Alignment module
 */
template<class InputPolicy, class ProcessingPolicy, class OutputPolicy>
class AlignmentHost
{
  private:
    //defining the functions required to run alignment
    // the input/output for each policy can be redefined, but for now they are:

    //Input policy:
    // input: a file with track data
    // output: bpct::data_model::AliTrack

    //processing policy:
    // input: bpct::data_model::AliTrack
    // output: unordered_map<staveKey , tuple<double,double,double,double,double,double>, KeyHash<staveKey>>

    //Output policy:
    // input: unordered_map<staveKey , tuple<double,double,double,double,double,double>, KeyHash<staveKey>>
    // output: stores data in JSON file.

    InputPolicy ip;
    ProcessingPolicy pp;
    OutputPolicy op;
    int numTracks;

    FileWrapper fileWrapper;
    
  public:

    AlignmentHost(InputPolicy ip, ProcessingPolicy pp, OutputPolicy op, int numtracks): ip(ip), pp(pp), op(op)
    {
      this->ip = ip;
      this->pp = pp;
      this->op = op;
      this->numTracks = numtracks;
      this->fileWrapper = FileWrapper();
    }
    /*
     * call this method to execute one run of the alignment procedure.
     */
    void run() {
      auto start = std::chrono::high_resolution_clock::now(); //timing run
      cout << "=======================================================" << endl;
      cout << "=              Input parameters given                 =" << endl;
      cout << "=======================================================" << endl;
      cout << "Input file: " << ip.filePath << endl;
      cout << "Number of tracks to analyze: " << numTracks << endl;
      cout << "=======================================================\n" << endl;


      auto fileStream = fileWrapper.openFile(this->ip.filePath, std::fstream::in);

      cout << "Analyzing tracks..." << endl;
      
      cout << "Generating X and Y offset values." << endl;
      doAnalysisOfTracks(fileStream);

      fileStream.close();
      
      cout << "Finalising offset calculation..." << endl;
      pp.finalizeTrackOffsets();

      cout << "Done analyzing tracks." << endl;

      cout << "Writing a total of " << pp.output.size() << " parameters to output file at: " << op.filePath << endl;
      op.insertValues(pp.output);
      cout << "Done";

      auto finish = std::chrono::high_resolution_clock::now();
      std::cout <<" in " <<std::chrono::duration_cast<std::chrono::seconds>(finish-start).count() << " seconds\n";

    };

    /*!
     * @brief Analyze all input tracks and produce offsets
     * @param fileStream
     */
    void doAnalysisOfTracks(std::fstream &fileStream){

      for(int i = 0; i < numTracks; i++){
        bpct::data_model::AliTrack track = ip.getTrack(fileStream);

        if(track.hits.size() == 0){
          cout << "You requested " + to_string(numTracks) + ", but only " + to_string(i)
                  + " usable tracks where found in input data." << endl;
          break;
        }

        pp.analyzeTrackXY(track);
      }
    }
};

#endif //PCT_BERGEN_ALIGNMENTHOST_H
