//
// Created by Rune Almåsbakk on 07.03.2022.
//

#include "alignment/FileWrapper.h"

FileWrapper::FileWrapper() = default;
FileWrapper::~FileWrapper() = default;

fstream FileWrapper::openFile(string& filename, ios_base::openmode mode){

  fstream file;
  file.open(filename, mode);

  if(!file.is_open())
    throw runtime_error("Could not open file with path: " +
    filename +
    "\n Try running as administrator or make sure the path is correct");
  
  return file;
}
