# The pCT Alignment module

## How to use

This module is part of the pCT project and build together with everything else. Follow instructions from readme in root directory. 
Command inputs and examples can be found under [Commands](#Commands).

## Commands
```
-s          : Run with simulated data. No need to specify input or output path. The program specifies input/output path during run.

-i  PATH    : Specify input PATH for track data.

-o  PATH    : Specify output PATH for where you want the output to be stored. Output file is generated automatically.

-h          : Help

-n  NUMBER  : NUMBER of tracks you want to analyze. Stops at end of file if NUMBER > provided tracks. This command must be included in all runs.

```


Examples:

Without "<>"

(This currently only works for MC data)

```
sudo run-pct-alignment -i <yourInputFile> -o <yourOutputFile.json> -n <NUMBER of tracks>
```

or (for toy simulation)
```
sudo run-pct-alignment -s -n <NUMBER of tracks>
```

## Using your own simulation data

Using the -s command requires a file named simData.csv in the folder alignment/simdata. As default a file with 100 tracks is included such that the command works "out of the box". To generate your own data, head over to the [pru_datasim](https://git.app.uib.no/pct/pru_datasim) repository and follow the instructions. Copy the generated simData.csv file over to the simdata folder under alignment. You can rename the default file or overwrite it.  

## Output

After running the alignment program a JSON file with the name and location you specified is created.
The JSON root object contains all layers found during alignment, each layer has the staves belonging to it as sub objects.
The stave object contains the alignment parameters as numerical values. These values are specified in millimeters.
A positive value means the element had consistent hits more to the right then expected meaning a shift to the left for that element.
Negative values meaning the opposite.


## Benchmarks

Some benchmark tests are for the time commented out because they rely on reading data from file. This makes them fail when using ctest making the pipeline fail. There are no problems with running them localy, simply uncomment, build, and run them like any other benchamrk.  