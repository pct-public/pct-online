//
// Created by Rune Almåsbakk on 09/03/2022.
//

#include "benchmark/benchmark.h"
#include "alignment/AlignmentHandler.h"
#include "../../io-adaptors/include/io-adaptors/AlignmentToySimInputHandler.h"
#include "datamodel/AliTrack.h"
#include "alignment/FileWrapper.h"

namespace bpct
{

  auto ah = AlignmentHandler();

  data_model::AliTrack emulateData(){

    data_model::AliTrack ret;
    auto ih = AlignmentToySimInputHandler("../alignment/simdata/simData.csv");
    FileWrapper fw;

    auto filestream = fw.openFile(ih.filePath, std::fstream::in);

    return ih.getTrack(filestream);
  }

  static void BM_analyzeTrackXY(benchmark::State& state){
    auto data = emulateData();

    for(auto _: state){
      ah.analyzeTrackXY(data);
    }
  }

  // BENCHMARK(BM_analyzeTrackXY);
} // namespace bpct

BENCHMARK_MAIN();