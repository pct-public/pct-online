//
// Created by Rune on 15/03/2022.
//

#include "benchmark/benchmark.h"
#include "../../io-adaptors/include/io-adaptors/AlignmentToySimInputHandler.h"
#include "datamodel/AliTrack.h"
#include "alignment/FileWrapper.h"
#include "alignment/Regression.h"
#include "alignment/CoordinateTransformer.h"
#include "datamodel/AlpideHit.h"

namespace bpct
{

  std::vector<std::vector<double>> emulateData(){

    data_model::AliTrack ret;
    auto ih = AlignmentToySimInputHandler("../alignment/simdata/simData.csv");
    FileWrapper fw;
    CoordinateTransformer transformer;

    auto filestream = fw.openFile(ih.filePath, std::fstream::in);

    auto track = ih.getTrack(filestream);


    std::vector<double> x;
    std::vector<double> y;
    std::vector<double> z;

    for(auto hit: track.hits){

      data_model::AlpideHit ahit;
      ahit.stave = hit.stave;
      ahit.chip = hit.chip;
      ahit.x = hit.x;
      ahit.y = hit.y;

      auto global = transformer.localToGlobalHitCoordinateMM(hit.layer, &ahit);

      x.push_back(global[0]);
      y.push_back(global[1]);
      z.push_back(global[2]);
    }

    std::vector<std::vector<double>> out;
    out.push_back(x);
    out.push_back(y);
    out.push_back(z);

    return out;
  }

  static void BM_regressionFit(benchmark::State& state){
    auto data = emulateData();
    auto reg = Regression<double>();

    for(auto _: state){
      reg.fit(data[2], data[1]);
    }
  }

  static void BM_regressionPredict(benchmark::State& state){
    auto data = emulateData();
    auto reg = Regression<double>();

    //model must be fit before prediction can be done
    reg.fit(data[2], data[1]);

    for(auto _: state){
      auto out = reg.predict(data[2]);
    }
  }

  // BENCHMARK(BM_regressionFit);
  // BENCHMARK(BM_regressionPredict);
} // namespace bpct

BENCHMARK_MAIN();