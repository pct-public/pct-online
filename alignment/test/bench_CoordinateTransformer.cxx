//
// Created by Rune Almåsbakk on 07/03/2022.
//

#include "benchmark/benchmark.h"
#include "datamodel/AlpideHit.h"
#include "../../geometry/DetectorConstants.h"
#include "alignment/CoordinateTransformer.h"
#include "datamodel/AliTrack.h"
#include "alignment/FileWrapper.h"
#include "../../io-adaptors/include/io-adaptors/AlignmentToySimInputHandler.h"

namespace bpct
{

CoordinateTransformer transformer;

std::tuple<int,data_model::AlpideHit> emulateLocalData(){

  data_model::AlpideHit hit;
  DetectorGeometry geo = transformer.geo;

  auto layer = rand() % geo.NUM_LAYERS;
  hit.stave = rand() % geo.NUM_STAVES_PER_LAYER;
  hit.chip = rand() % geo.NUM_CHIPS_PER_STAVE;
  hit.x = rand() % geo.NUM_PIXEL_WIDTH;
  hit.y = rand() % geo.NUM_PIXEL_HEIGHT;

  return std::tuple(layer, hit);

}

std::vector<double> emulateGlobalData(){

  std::vector<double> ret;
  DetectorGeometry geo = transformer.geo;

  ret.push_back(rand() % geo.NUM_PIXEL_WIDTH * geo.NUM_CHIPS_PER_STAVE);
  ret.push_back(rand() % geo.NUM_PIXEL_HEIGHT * geo.NUM_STAVES_PER_LAYER);
  ret.push_back(rand() % geo.NUM_LAYERS);

  return ret;
}

data_model::AliTrack emulateTrackData(){

  data_model::AliTrack ret;
  auto ih = AlignmentToySimInputHandler("../alignment/simdata/simData.csv");
  FileWrapper fw;

  auto filestream = fw.openFile(ih.filePath, std::fstream::in);

  return ih.getTrack(filestream);
}


static void BM_CoordinateTransformerLtoGPixelSingleHit(benchmark::State& state){
  auto data = emulateLocalData();

  for(auto _: state){
    transformer.localToGlobalHitCoordinatePixel(std::get<0>(data), &std::get<1>(data));
  }
}

static void BM_CoordinateTransformerLtoGMMSingleHit(benchmark::State& state){
  auto data = emulateLocalData();

  for(auto _: state){
    transformer.localToGlobalHitCoordinateMM(std::get<0>(data), &std::get<1>(data));
  }
}

static void BM_CoordinateTransformerGtoLPixelSingleHit(benchmark::State& state){
  auto data = emulateGlobalData();

  for(auto _: state){
    transformer.globalToLocalHitCoordinatePixel(data[0], data[1], data[2]);
  }
}

static void BM_CoordinateTransformerGtoLMMSingleHit(benchmark::State& state){
  //Difficult to generate data for this function. Use singular predefined data.
  for(auto _: state){
    transformer.globalToLocalHitCoordinateMM(1.0, 1.0, 366.394012451171875);
  }
}

static void BM_CoordinateTransformerLtoGPixelTrack(benchmark::State& state){
  auto data = emulateTrackData();

  for(auto _: state){
    for(auto hit: data.hits){

      data_model::AlpideHit ahit;
      ahit.stave = hit.stave;
      ahit.chip = hit.chip;
      ahit.x = hit.x;
      ahit.y = hit.y;

      transformer.localToGlobalHitCoordinatePixel(hit.layer, &ahit);
    }
  }
}

static void BM_CoordinateTransformerLtoGMMTrack(benchmark::State& state){
  auto data = emulateTrackData();

  for(auto _: state){
    for(auto hit: data.hits){

      data_model::AlpideHit ahit;
      hit.stave = hit.stave;
      hit.chip = hit.chip;
      hit.x = hit.x;
      hit.y = hit.y;

      transformer.localToGlobalHitCoordinateMM(hit.layer, &ahit);
    }
  }
}


BENCHMARK(BM_CoordinateTransformerLtoGPixelSingleHit);
BENCHMARK(BM_CoordinateTransformerLtoGMMSingleHit);
BENCHMARK(BM_CoordinateTransformerGtoLPixelSingleHit);
BENCHMARK(BM_CoordinateTransformerGtoLMMSingleHit);
// BENCHMARK(BM_CoordinateTransformerLtoGPixelTrack);
// BENCHMARK(BM_CoordinateTransformerLtoGMMTrack);
} // namespace bpct

BENCHMARK_MAIN();