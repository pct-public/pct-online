/**
 * @file test_transformation.cxx
 * @author Rune Almåsbakk 
 * @date 05.09.2021
 * @brief Testing for the transformation from local hit into global position
 */


#define BOOST_TEST_MODULE Test Bergen pCT Alignment
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <iostream>
#include "datamodel/AliTrack.h"
#include "alignment/CoordinateTransformer.h" // Test object


namespace bpct {

  BOOST_AUTO_TEST_CASE(transformationTestX) {


    std::cout << '\n' 
    << "------------- Test 1 -------------" << '\n'
    << "Testing: getX." << '\n';

    // create a buffer holding the header and 1 Alpide hit(s)
    // need to allocate the buffer big enough to hold the actual hit objects
    // because the hits are defined as zero-length array and the size of the
    // RUDataHeader does not include this
    // FIXME: this will simplify with with a generic flat buffer support
    const size_t nofAlpideHits = 1;
    const size_t bufferSize = sizeof(data_model::RUDataHeader) + nofAlpideHits * sizeof(data_model::RUDataHeader::value_type);
    std::vector<uint8_t> buffer(bufferSize);
    auto& testData = *reinterpret_cast<data_model::RUDataHeader*>(buffer.data());
    auto transformer = bpct::CoordinateTransformer{};

    int chips[] = {0,1,2,3,4,5,6,8,9};

    //test for even staves
    for(auto i: chips){
      testData.layer = 0;
      testData.hits[0].chip = i;
      testData.hits[0].stave = 0;
      testData.hits[0].x = 0;
      testData.hits[0].y = 0;

      auto actual = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

      BOOST_TEST(actual[0] == (i > 6 ? 1024*(i-1) : 1024*i));

    }
    //test for odd staves
    for(auto i: chips){
      testData.layer = 0;
      testData.hits[0].chip = i;
      testData.hits[0].stave = 1;
      testData.hits[0].x = 0;
      testData.hits[0].y = 0;

      auto actual = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

      if(i == 0){
        BOOST_TEST(actual[0] == 1023);
      }else{
        BOOST_TEST(actual[0] == (i > 6 ? 1023+1024*(i-1) : 1023+1024*i));
      }

    }
    //test for every stave on chip 0
    for(int i = 0 ; i < transformer.geo.NUM_STAVES_PER_LAYER ; i++){
      testData.layer = 0;
      testData.hits[0].chip = 0;
      testData.hits[0].stave = i;
      testData.hits[0].x = 0;
      testData.hits[0].y = 0;

      auto actual = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

      BOOST_TEST(actual[0] == (i % 2 == 0 ? 0 : 1023));
    }

    //test for every stave non-zero chip
    for(int i = 0 ; i < transformer.geo.NUM_STAVES_PER_LAYER ; i++){
      testData.layer = 0;
      testData.hits[0].chip = 1;
      testData.hits[0].stave = i;
      testData.hits[0].x = 0;
      testData.hits[0].y = 0;

      auto actual = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

      BOOST_TEST(actual[0] == (i % 2 == 0 ? 1024 : 1023+1024));
    }
  }

  BOOST_AUTO_TEST_CASE(LocalToGlobalToLocalTest) {

    std::cout << '\n'
              << "------------- Test 2 -------------" << '\n'
              << "Testing: Local to Global to Local." << '\n';

    // create a buffer holding the header and 1 Alpide hit(s)
    // FIXME: this will simplify with with a generic flat buffer support
    const size_t nofAlpideHits = 1;
    const size_t bufferSize = sizeof(data_model::RUDataHeader) + nofAlpideHits * sizeof(data_model::RUDataHeader::value_type);
    std::vector<uint8_t> buffer(bufferSize);
    auto& testData = *reinterpret_cast<data_model::RUDataHeader*>(buffer.data());
    auto transformer = bpct::CoordinateTransformer{};
    testData.layer = 1;
    testData.hits[0].stave = 2;
    testData.hits[0].chip = 3;
    testData.hits[0].x = 4;
    testData.hits[0].y = 5; // bottom row of odd staves

    auto global = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

    auto local = transformer.globalToLocalHitCoordinatePixel(global[0], global[1], global[2]);

    BOOST_TEST(local[0] == 1);
    BOOST_TEST(local[1] == 2);
    BOOST_TEST(local[2] == 3);
    BOOST_TEST(local[3] == 4);
    BOOST_TEST(local[4] == 5);

  }

  BOOST_AUTO_TEST_CASE(transformationTestY) {
    
    std::cout << '\n' 
    << "------------- Test 2 -------------" << '\n'
    << "Testing: getY." << '\n';

    // create a buffer holding the header and 1 Alpide hit(s)
    // FIXME: this will simplify with with a generic flat buffer support
    const size_t nofAlpideHits = 1;
    const size_t bufferSize = sizeof(data_model::RUDataHeader) + nofAlpideHits * sizeof(data_model::RUDataHeader::value_type);
    std::vector<uint8_t> buffer(bufferSize);
    auto& testData = *reinterpret_cast<data_model::RUDataHeader*>(buffer.data());
    auto transformer = bpct::CoordinateTransformer{};
    testData.layer = 0;
    testData.hits[0].chip = 0;
    testData.hits[0].stave = 1;
    testData.hits[0].x = 0;
    testData.hits[0].y = 511; // bottom row of odd staves

    auto actual = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

    BOOST_TEST(actual[1] == 512);

  }

  BOOST_AUTO_TEST_CASE(transformationTestGetGlobalHitCoordinatePixel) {
    
    std::cout << '\n' 
    << "------------- Test 3 -------------" << '\n'
    << "Testing: localToGlobalHitCoordinatePixel." << '\n';

    // create a buffer holding the header and 1 Alpide hit(s)
    // FIXME: this will simplify with with a generic flat buffer support
    const size_t nofAlpideHits = 1;
    const size_t bufferSize = sizeof(data_model::RUDataHeader) + nofAlpideHits * sizeof(data_model::RUDataHeader::value_type);
    std::vector<uint8_t> buffer(bufferSize);
    auto& testData = *reinterpret_cast<data_model::RUDataHeader*>(buffer.data());
    auto transformer = bpct::CoordinateTransformer{};
    testData.layer = 0;
    testData.hits[0].chip = 0;
    testData.hits[0].stave = 0;
    testData.hits[0].x = 0;
    testData.hits[0].y = 0;

    auto actual = transformer.localToGlobalHitCoordinatePixel(testData.layer, &testData.hits[0]);

    BOOST_TEST(actual[0] == 0);
    BOOST_TEST(actual[1] == 0);
    BOOST_TEST(actual[2] == 0);
  }

  BOOST_AUTO_TEST_CASE(transformationTestGetGlobalHitCoordinatemm) {
    
    std::cout << '\n' 
    << "------------- Test 4 -------------" << '\n'
    << "Testing: localToGlobalHitCoordinatemm." << '\n';

    // create a buffer holding the header and 1 Alpide hit(s)
    // FIXME: this will simplify with with a generic flat buffer support
    const size_t nofAlpideHits = 1;
    const size_t bufferSize = sizeof(data_model::RUDataHeader) + nofAlpideHits * sizeof(data_model::RUDataHeader::value_type);
    std::vector<uint8_t> buffer(bufferSize);
    auto& testData = *reinterpret_cast<data_model::RUDataHeader*>(buffer.data());
    auto transformer = bpct::CoordinateTransformer{};
    //this should be the values for the middle front og the sensor or 0,0,0
    testData.layer = 0;
    testData.hits[0].chip = 0;
    testData.hits[0].stave = 0;
    testData.hits[0].x = 0;
    testData.hits[0].y = 0;

    auto actual = transformer.localToGlobalHitCoordinateMM(testData.layer, &testData.hits[0]);

    BOOST_TEST(actual[0] == 0.0);
    BOOST_TEST(actual[1] == 0.0);
    BOOST_TEST(actual[2] == 0.0);
  }


  BOOST_AUTO_TEST_CASE(transformationTestGlobalToLocalHitCoordinateMM) {
    std::cout << '\n'
    << "------------- Test 5 -------------" << '\n'
    << "Testing: globalToLocalHitCoordinateMM" << '\n';

    auto transformer = bpct::CoordinateTransformer{};

    //Layer tests
    double layer2 = 333.394012451171875;
    data_model::AliHit actual;
    for(int i = 0 ; i < defaultGeometry.NUM_LAYERS-2 ; i++){
      actual = transformer.globalToLocalHitCoordinateMM(0.0, 1.0, (layer2) + (5.5 * i));

      BOOST_TEST(actual.layer == i+2);
    }

    actual = transformer.globalToLocalHitCoordinateMM(0.0, 1.0,  509.394012451171875);

    BOOST_TEST(actual.layer == 34);

    actual = transformer.globalToLocalHitCoordinateMM(0.0, 1.0,  514.89398193359375);

    BOOST_TEST(actual.layer == 35);

    actual = transformer.globalToLocalHitCoordinateMM(0.0, 1.0,  520.39398193359375);

    BOOST_TEST(actual.layer == 36);

    //Stave tests
    for(int i = 0 ; i < defaultGeometry.NUM_STAVES_PER_LAYER/2.0; i++){
      //test positive values
      actual = transformer.globalToLocalHitCoordinateMM(0.0, 0.1 + (defaultGeometry.CHIP_HEIGHT * i), 0.0);

      BOOST_TEST(actual.stave == 6 + i);

      //test negative values
      actual = transformer.globalToLocalHitCoordinateMM(0.0, (0.1 + (defaultGeometry.CHIP_HEIGHT * i))*-1, 0.0);

      BOOST_TEST(actual.stave == 5 - i);
    }



    //Chip tests
    actual = transformer.globalToLocalHitCoordinateMM(0.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 4);

    actual = transformer.globalToLocalHitCoordinateMM(15, -31.0, 207.15);

    BOOST_TEST(actual.chip == 4);

    actual = transformer.globalToLocalHitCoordinateMM(16, -31.0, 207.15);

    BOOST_TEST(actual.chip == 5);

    actual = transformer.globalToLocalHitCoordinateMM(45.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 5);

    actual = transformer.globalToLocalHitCoordinateMM(46.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 6);

    actual = transformer.globalToLocalHitCoordinateMM(75.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 6);

    actual = transformer.globalToLocalHitCoordinateMM(76.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 8);

    actual = transformer.globalToLocalHitCoordinateMM(105.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 8);

    actual = transformer.globalToLocalHitCoordinateMM(106.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 9);

    actual = transformer.globalToLocalHitCoordinateMM(135.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 9);

    actual = transformer.globalToLocalHitCoordinateMM(-0.0, -31.0, 207.15);

    BOOST_TEST(actual.chip == 4);

    actual = transformer.globalToLocalHitCoordinateMM(-15, -31.0, 207.15);

    BOOST_TEST(actual.chip == 4);

    actual = transformer.globalToLocalHitCoordinateMM(-15.1, -31.0, 207.15);

    BOOST_TEST(actual.chip == 3);

    actual = transformer.globalToLocalHitCoordinateMM(-45, -31.0, 207.15);

    BOOST_TEST(actual.chip == 3);

    actual = transformer.globalToLocalHitCoordinateMM(-45.1, -31.0, 207.15);

    BOOST_TEST(actual.chip == 2);

    actual = transformer.globalToLocalHitCoordinateMM(-75, -31.0, 207.15);

    BOOST_TEST(actual.chip == 2);

    actual = transformer.globalToLocalHitCoordinateMM(-75.1, -31.0, 207.15);

    BOOST_TEST(actual.chip == 1);

    actual = transformer.globalToLocalHitCoordinateMM(-105, -31.0, 207.15);

    BOOST_TEST(actual.chip == 1);

    actual = transformer.globalToLocalHitCoordinateMM(-105.1, -31.0, 207.15);

    BOOST_TEST(actual.chip == 0);

    actual = transformer.globalToLocalHitCoordinateMM(-135, -31.0, 207.15);

    BOOST_TEST(actual.chip == 0);
  }

} // namespace bpct
