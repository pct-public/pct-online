// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   AppContext.h
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Context structure for running a controlled app

#ifndef APP_CONTEXT_H
#define APP_CONTEXT_H

#include "app-control/AppSpec.h"
#include <sys/types.h>

namespace appcontrol
{

/// @struct AppContext
/// The structure holds all internal information for an app running in a child
/// process of the main framework process
struct AppContext {
  using OutStream = AppSpec::OutStream;

  enum struct ChildStatus {
    NotStarted,
    Running,
    Paused,
    Finished,
    Error,
  };
  AppContext() = delete;
  AppContext(int _id, AppSpec& _spec)
    : id(_id)
    , spec(_spec)
  {
  }
  int id = -1;      // app id within the framework
  pid_t pid = -1;   // process id
  AppSpec& spec;    // reference to the spec
  int chldin = -1;  // file descriptor of childs stdout
  int chldout = -1; // file descriptor of childs stdout
  int chlderr = -1; // file descriptor of childs stderr
  ChildStatus status = ChildStatus::NotStarted;
  /// callback to get the output stream
  OutStream outStreamCallback = nullptr;
  /// callback to get the error stream
  OutStream errStreamCallback = nullptr;
};

} // namespace appcontrol

#endif // APP_CONTEXT_H
