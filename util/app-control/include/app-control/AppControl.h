// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   AppControl.h
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Worker class to control multiple apps

#ifndef APP_CONTROL_H
#define APP_CONTROL_H

#include <vector>
#include "app-control/AppSpec.h"

namespace appcontrol
{
class AppContext;

/// @AppControl
/// The class is initialized with a list of app declarations. It implemets a functor
/// taking the normal main-fuction arguments.
///
/// Usage:
///   AppControl(std::move(specs)(argc, argv);
///
/// The worker method launches all declared apps each as a separate process, and
/// invokes the poller to check status of childs and monitor the output streams.
class AppControl
{
 public:
  /// constructor
  /// @param list of declared apps
  AppControl(std::vector<AppSpec>&& specs, AppSpec::OutStream cb = nullptr)
    : mSpecs(std::move(specs))
    , mLogStream(cb == nullptr ? AppSpec::defaultStreamCallback(AppSpec::StreamType::Out) : cb)
  {
  }

  // FIXME: log message levels for framework messages, this needs to be moved to
  // a common logging framework
  enum {
    DEBUG = 0,
    INFO,
    ERROR,
  };

  /// the main worker method, it implements
  /// - scanning of command line options of the controller
  /// - launching of the declared apps
  /// - run loop checking status and output pipelines of childs
  /// @param argc  number of command line arguments
  /// @param argv  array of command line arguments
  int operator()(int argc, char* argv[]);

  /// spawn new process for an app
  /// a new process is created by fork, the child process is restarted
  void spawn(AppContext& context, int argc, char* argv[]) const;

  /// monitor all childs, returns when all childs have terminated
  void monitorChilds(std::vector<AppContext>& appinfos);

  /// handle outputs from all childs
  static void handleAppOutput(std::vector<AppContext>& appinfos);

  /// get the log stream for framework messages
  std::ostream& LOG(int level = INFO) const
  {
    if (level < mLogLevel) {
      // FIXME: not quite sure if this is really smart with respect to
      // performance
      return defaultNullStream();
    }
    return mLogStream();
  }

  /// the default logging callback
  static AppSpec::OutStream defaultStreamCallback(AppContext&, std::string = "");

 private:
  static std::ostream& defaultNullStream();

  /// list of declared apps
  std::vector<AppSpec> mSpecs;
  /// framework verbosity
  int mLogLevel = ERROR;
  /// callback for the log stream
  AppSpec::OutStream mLogStream = AppSpec::defaultStreamCallback(AppSpec::StreamType::Out);
};

} // namespace appcontrol

#endif // APP_CONTROL_H
