// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   AppSpec.h
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Specs for running an app

#ifndef APP_SPEC_H
#define APP_SPEC_H

#include <iosfwd> // forward declaration of ostream
#include <functional>
#include <string>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

namespace appcontrol
{

/// @struct AppSpec
/// A struct to declare an app within the app control framework.
/// The hook to be invoked by the framework is provided as a function with signature
/// of the normal main function. A second hook can provide the boost option description
/// of the app and will be used by the framework to filter command line options.
///
/// Customization of output sinks
/// The output sinks for the standard output and error streams of the child process
/// can be configured by adding a function returning the respective stream. The default
/// streams are the output and error streams of the parent process, being equivalent to
///
///     spec.mOutStreamCallback = []() -> std::ostream& { return std::cout; };
struct AppSpec {
  using RunnerT = std::function<int(int /*argc*/, char* /*argv*/[])>;
  using OptionsBuilderT = std::function<boost::program_options::options_description()>;
  // FIXME: not sure if the spec is the right place for the stream setup, or if this
  // is better to be moved to the context. Depending on how much the user is allowed
  // to customize
  using OutStream = std::function<std::ostream&()>;

  enum struct StreamType {
    Out,
    Err,
  };
  /// this method is implemented outside the header to avoid the full iostream header
  static OutStream defaultStreamCallback(StreamType type);

  AppSpec(std::string n, RunnerT&& r)
    : name(n)
    , runner(std::move(r))
  {
  }
  AppSpec(std::string n, RunnerT&& r, OptionsBuilderT&& o)
    : name(n)
    , runner(std::move(r))
    , getOptions(std::move(o))
  {
  }

  /// get the output stream object as reference
  std::ostream& outstream() const
  {
    if (outStreamCallback) {
      return outStreamCallback();
    }
    return defaultStreamCallback(StreamType::Out)();
  }
  /// get the output stream object as reference
  std::ostream& errstream() const
  {
    if (errStreamCallback) {
      return errStreamCallback();
    }
    return defaultStreamCallback(StreamType::Err)();
  }

  std::string name;
  RunnerT runner;
  OptionsBuilderT getOptions = nullptr;
  /// callback to get the output stream
  OutStream outStreamCallback = nullptr;
  /// callback to get the error stream
  OutStream errStreamCallback = nullptr;
};

} // namespace appcontrol

#endif // APP_SPEC_H
