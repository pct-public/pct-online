// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   controller.h
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  The controller implementing customization hook and main function

/// This header is invoking the app control framework and has to be included
/// at the end of the setup file.
/// Note: there is no double include protection because the file should be
/// included only once and we want to provoke an error if something is wrong

#include <vector>
#include "app-control/AppSpec.h"
#include "app-control/AppControl.h"

using AppSpec = appcontrol::AppSpec;
using AppControl = appcontrol::AppControl;

// default is an empty list of apps
void defaultSetup(std::vector<AppSpec>&) {}
// by default, no framework log redirection, std outputs are used
void defaultSetup(AppSpec::OutStream& cb) { cb = nullptr; }

// some template magic to redirect to method 'setapp' if defined, or fall back
// to default method, the trick here is to define the return type through decltype
// and SFINAE idiom. The first version is only compiled if decltype(setapp) is defined.
// The two methods are different because the second parameter is of different type. If
// the first version can not be compiled the compiler silently converts the parameter
// to long int and calls the default
struct SetupHelper {
  template <typename T>
  static auto userDefinedSetup(T& arg, int) -> decltype(setapp(arg))
  {
    setapp(arg);
  }

  template <typename T>
  static auto userDefinedSetup(T& arg, long int) -> decltype(defaultSetup(arg))
  {
    defaultSetup(arg);
  }
};

int main(int argc, char* argv[])
{
  std::vector<AppSpec> specs;
  AppSpec::OutStream streamCallback;
  SetupHelper::userDefinedSetup(specs, 0);
  SetupHelper::userDefinedSetup(streamCallback, 0);
  if (specs.size() == 0) {
    std::cout << "No apps declared, please make sure to include header 'controller.h'\n"
              << "after the implementation of function 'setapp', or at the end.\n";
    return 0;
  }

  return AppControl(std::move(specs), streamCallback)(argc, argv);
}
