// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   AppSpec.cxx
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Non-header file implementation of application spec functionality

#include "app-control/AppSpec.h"
#include <iostream>

namespace appcontrol
{

// Note: the main reason to implement this function outside the header file
// is to avoid dependency on the heaver iostream heraders and use lightweight
// iosfwd instead
AppSpec::OutStream AppSpec::defaultStreamCallback(StreamType type)
{
  if (type == StreamType::Err) {
    return []() -> std::ostream& { return std::cerr; };
  }
  return []() -> std::ostream& { return std::cout; };
}

} // namespace appcontrol
