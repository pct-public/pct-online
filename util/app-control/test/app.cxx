// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   app.cxx
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Simple application to be used as controlled app

// NOTE: all header includes must happen in app.h to have the
// correct namespace when including the app.cxx file into the
// controlled app.
#include "app.h"

int main(int argc, char* argv[])
{
  std::cout << "This is the smart app"
            << " pid " << getpid();
  for (auto i = 0; i < argc; i++) {
    std::cout << " " << argv[i];
  }
  std::cout << std::endl;
  std::cerr << "There was no error" << std::endl;
  return 0;
}
