// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   app.h
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Header file for simple application to be used as controlled app

#ifndef APP_H
#define APP_H

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#endif // APP_H
