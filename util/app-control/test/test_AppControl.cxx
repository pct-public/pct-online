// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file test_AppControl.cxx
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @brief Unit test for class @a AppControl

#define BOOST_TEST_MODULE Unit tests of module appcontrol
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "../include/app-control/AppSpec.h"
#include "../include/app-control/AppControl.h"
#include "../AppContext.h"
#include <unistd.h>
#include <fcntl.h>
#include <thread>
#include <iostream>
#include <string>
#include <sstream>

namespace appcontrol
{

BOOST_AUTO_TEST_CASE(test_handleAppOutput)
{
  int pipefd[2];
  auto piperes = pipe(pipefd);
  BOOST_REQUIRE(piperes != -1);

  auto producer = [writeend = pipefd[1]]() {
    char msg[2] = {'0', '\n'};
    for (int i = 0; i < 10; i++) {
      auto writeres = write(writeend, msg, sizeof(msg));
      BOOST_REQUIRE_MESSAGE(writeres == 2, "Can not write the full message");
      msg[0]++;
      usleep(100000);
    }
    close(writeend);
  };

  std::thread producerThread(producer);

  usleep(500000);
  auto readend = pipefd[0];
  int res = fcntl(readend, F_SETFL, O_NONBLOCK);
  BOOST_REQUIRE_MESSAGE(res != -1, "Error setting socket to non-blocking: " << strerror(errno));

  std::stringstream outstream;
  AppSpec spec{"test app", [](int, char*[]) -> int { return 0; }};
  std::vector<AppContext> appinfos{{0, spec}};
  appinfos[0].chldout = readend;
  appinfos[0].outStreamCallback = [&outstream, &spec]() -> std::ostream& {
    outstream << "[" << spec.name << "]: ";
    return outstream;
  };
  appinfos[0].errStreamCallback = AppControl::defaultStreamCallback(appinfos[0]);

  BOOST_TEST_PASSPOINT();
  int count = 0;
  do {
    AppControl::handleAppOutput(appinfos);
    std::string require;
    if (count == 0) {
      // when reading for the first time, the stream must nevr be empty
      BOOST_REQUIRE(!outstream.str().empty());
      // because of the configured delays, the first 5 lines are
      // available in the same cycle
      require =
        "[test app]: 0\n"
        "[test app]: 1\n"
        "[test app]: 2\n"
        "[test app]: 3\n"
        "[test app]: 4\n";
      count++;
    } else if (count < 6 && !outstream.str().empty()) {
      // each call has one line, but it might also be empty depending on
      // when the producer thread adds new lines
      require = "[test app]: " + std::to_string(count + 4) + "\n";
      count++;
    } else {
      // the output must be empty after all lines from the producer
      // have been read
      if (count > 5) {
        count++;
      }
    }
    BOOST_CHECK(outstream.str() == require);
    std::cout << outstream.str();
    outstream.str(std::string()); // clear stringstream
    usleep(50000);
  } while (count < 20);

  producerThread.join();
}

} // namespace appcontrol
