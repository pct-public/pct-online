// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   controlled-app.cxx
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Example for a controlled app

// This is an example to use the app control framework
//
// The framework is activated by including the header file "app-driver/controller.h"
// *AT THE END* of the compiled file. The header file comes with the main function
// and invokes the framework with the declared apps.
//
// Apps are declared in a specific hook, the function `setapp`, all apps are added
// as AppSpec entries.
//
// Since the main function can only be declared once in a program,
// the app.cxx file with its main function is included within a specific
// namespace. This makes it possible to use the app implementation as
// it is and have the main function of the app controller to be the
// entry point
// To make sure that symbols do not end up within namespace impl,
// the app itself has to add all include directives in its header file
// and this is included outside the namespace
#include "app.h"
namespace impl
{
#include "app.cxx"

}

#include "app-control/AppSpec.h"
#include <iostream>

using AppSpec = appcontrol::AppSpec;

void setapp(std::vector<AppSpec>& specs)
{
  specs.emplace_back("smart app", [](auto argc, auto argv[]) {
    auto result = impl::main(argc, argv);
    int delay = 5;
    std::cout << "Delaying app by " << delay << "s" << std::endl;
    while (delay-- > 0) {
      std::cout << delay << std::endl;
      sleep(1);
    }
    return result;
  });
  specs.emplace_back("dumb app", [](auto argc, auto argv[]) { return impl::main(argc, argv); });
}

void setapp(AppSpec::OutStream& cb)
{
  cb = []() -> std::ostream& {
    std::cout << "[app control] ";
    return std::cout;
  };
}

// the controller.h implements the main function and the hook for the setapp
// method to be used to declare the apps. For the later to work correctly, the
// header needs to be included at the end
#include "app-control/controller.h"
