// Copyright 2020 Matthias Richter. This software is distributed under the
// terms of the GNU General Public License v3 (GPL Version 3).
//
// In applying this license the authors do not submit themself to any
// jurisdiction.
//
// This file is part of cmdlAppControl.
//
// cmdlAppControl is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cmdlAppControl is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cmdlAppControl. If not, see <https://www.gnu.org/licenses/>.

/// @file   AppControl.cxx
/// @author Matthias Richter (freetools@matthias-richter.com)
/// @since  2020-11-23
/// @brief  Controller implementation for multiple defined apps

#include "app-control/AppControl.h"
#include "AppContext.h"
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <string_view>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <ext/stdio_filebuf.h>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>

namespace bpo = boost::program_options;

namespace appcontrol
{

std::condition_variable sigwakeup;

int AppControl::operator()(int argc, char* argv[])
{
  std::vector<AppSpec>& specs = mSpecs;
  std::vector<AppContext> appinfos;

  boost::program_options::options_description od;
  // clang-format off
  od.add_options()
    ("id", bpo::value<int>()->default_value(-1), "application id")
    ("framework-verbosity", bpo::value<int>()->default_value(ERROR), "verbosity level for app control framework")
    ("help,h", "Hilfe!");
  // clang-format on

  // now parse the command line args
  bpo::variables_map varmap;
  try {
    bpo::store(bpo::parse_command_line(argc, argv, od), varmap);
  } catch (std::exception const& e) {
    LOG(ERROR) << "Parsing of command line arguments failed: " << e.what() << std::endl;
    exit(1);
  }

  mLogLevel = varmap["framework-verbosity"].as<int>();
  auto appid = varmap["id"].as<int>();
  if (appid >= 0) {
    if (appid >= (int)specs.size()) {
      throw std::runtime_error("invalid app id " + std::to_string(appid));
    }
    // this is the restarted child process, run the app
    LOG() << "Starting runner of app " << appid << " (" << specs[appid].name << ")"
          << " process id " << getpid() << " of parent " << getppid() << std::endl;

    // the id option is fixed at the end, so we simply drop two argumemts
    // from the vector
    return specs[appid].runner(argc - 2, argv);
  }

  // a set of default arguments for childs, the id is replaced with the application id
  // by the parent process to indicated a child process at start
  std::vector<std::string> childargs = {
    "--id",
    "-1"};

  auto prepareArgs = [&argc, &argv, &childargs](int id) -> std::vector<char*> {
    // the argument array of pointers has to start with the original argv[0] as
    // first element, and is terminated by a nullptr element which is needed in the execvp
    // call. The framework arguments are added at the end and the app id is the last
    // argument and replaced with the id of the current app. This can be done in-place
    // because the spawning of child processes is sequential until the fork, and after that
    // memory space of the child is separate.
    std::vector<char*> args;
    args.insert(args.end(), argv, argv + argc);
    std::for_each(childargs.begin(), childargs.end(), [&args](auto& arg) { args.emplace_back(arg.data()); });
    childargs.back() = std::to_string(id);
    args.emplace_back(nullptr);
    return args;
  };

  // parent process, spawn all childs
  for (auto& spec : specs) {
    appinfos.emplace_back(appinfos.size(), spec);
    auto& context = appinfos.back();

    auto setStreamCallback = [&context, *this](auto& left, auto& right) -> void {
      if (right) {
        std::swap(left, right);
      } else {
        left = defaultStreamCallback(context);
      }
    };

    // take the stream callbacks from the spec or init the default one
    // Note: the callback of the spec will be invalidated
    setStreamCallback(context.outStreamCallback, spec.outStreamCallback);
    setStreamCallback(context.errStreamCallback, spec.errStreamCallback);
    auto args = prepareArgs(context.id);
    spawn(context, args.size(), args.data());
  }

  monitorChilds(appinfos);
  return 0;
}

void AppControl::monitorChilds(std::vector<AppContext>& appinfos)
{
  // return true if all childs are in state 'Finished'
  auto checkReady = [&appinfos]() -> bool {
    for (auto const& context : appinfos) {
      if (context.status != AppContext::ChildStatus::Finished &&
          context.status != AppContext::ChildStatus::Error) {
        return false;
      }
    }
    return true;
  };

  // check the state of the child process by calling waitpid and set the
  // app status
  auto checkChild = [*this](AppContext& context) {
    if (context.status != AppContext::ChildStatus::Running &&
        context.status != AppContext::ChildStatus::Paused) {
      return;
    }
    int processStatus = 0;
    auto waitresult = waitpid(context.pid, &processStatus, WNOHANG | WCONTINUED | WUNTRACED);
    if (waitresult != 0) {
      if (waitresult < 0) {
        auto err = errno;
        LOG(ERROR) << "Error when executing waitpid for application " << context.id
                   << " (pid " << context.pid << ")"
                   << " error " << err << " " << strerror(err) << std::endl;
        context.status = AppContext::ChildStatus::Error;
        return;
      } else {
        if (WIFSTOPPED(processStatus)) {
          context.status = AppContext::ChildStatus::Paused;
          return;
        } else if (WIFCONTINUED(processStatus)) {
          context.status = AppContext::ChildStatus::Running;
          return;
        } else if (WIFSIGNALED(processStatus)) {
          LOG() << "Application " << context.id << " (pid " << context.pid << ")"
                << " terminated by signal " << WTERMSIG(processStatus) << std::endl;
        } else if (WIFEXITED(processStatus)) {
          LOG() << "Application " << context.id << " (pid " << context.pid << ")"
                << " terminated normally with status " << WEXITSTATUS(processStatus) << std::endl;
        }
      }
      context.status = AppContext::ChildStatus::Finished;
    }
  };

  std::mutex mutex;
  std::unique_lock<std::mutex> lock(mutex);
  while (1) {
    handleAppOutput(appinfos);
    for (auto& context : appinfos) {
      checkChild(context);
    }
    if (checkReady()) {
      break;
    }
    sigwakeup.wait_for(lock, std::chrono::milliseconds(100));
  }
  handleAppOutput(appinfos);
}

void AppControl::spawn(AppContext& context, int /*argc*/, char* argv[]) const
{
  // first create pipes for the communication between parent and child process
  int chldin[2];
  int chldout[2];
  int chlderr[2];

  auto create_pipe = [*this](auto& pipes) {
    auto res = pipe(pipes);

    if (res == -1) {
      LOG(ERROR) << "Unable to create pipe: ";
      switch (errno) {
        case EFAULT:
          assert(false && "EFAULT");
          break;
        case EMFILE:
          LOG(ERROR) << "EMFILE: Too many active descriptors";
          break;
        case ENFILE:
          LOG(ERROR) << "ENFILE: system file table is full";
          break;
        default:
          LOG(ERROR) << "Unknown error" << std::endl;
      };
      // Kill immediately both the parent and all the children
      kill(-1 * getpid(), SIGKILL);
    }
  };
  create_pipe(chldin);
  create_pipe(chldout);
  create_pipe(chlderr);

  // now fork, from this moment we continue in two processes
  pid_t pid = 0;
  pid = fork();

  if (pid == 0) {
    // this is the child process,

    // ignore SIGTRAP to allow debugging
    signal(SIGTRAP, SIG_IGN);

    // the pipes have been duplicated and there are file descriptors in parent and
    // child referring to the same resource. The child does not use the write part
    // of the input pipe and the read parts of the output pipes
    close(chldin[1]);
    close(chldout[0]);
    close(chlderr[0]);

    // now close also the standard descriptors of this process and make the read and
    // write ends of the pipes created between parent and child to the new standard
    // descriptors
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    dup2(chldin[0], STDIN_FILENO);
    dup2(chldout[1], STDOUT_FILENO);
    dup2(chlderr[1], STDERR_FILENO);

    // finally reexec the process
    execvp(argv[0], argv);
  }

  context.status = AppContext::ChildStatus::Running;
  context.pid = pid;
  LOG() << "Started app " << context.id << " (" << context.spec.name << ")"
        << " process id " << pid << " from parent " << getpid() << std::endl;

  // handle the parent pipes
  close(chldin[0]);
  close(chldout[1]);
  close(chlderr[1]);

  // keep file descriptors for polling the output pipes of the child
  context.chldin = chldin[1];

  auto setNonBlocking = [*this](int fd) {
    int res = fcntl(fd, F_SETFL, O_NONBLOCK);
    if (res == -1) {
      LOG(ERROR) << "Error setting socket to non-blocking: " << strerror(errno) << std::endl;
    }
  };

  setNonBlocking(chldout[0]);
  context.chldout = chldout[0];
  setNonBlocking(chlderr[0]);
  context.chlderr = chlderr[0];
}

void AppControl::handleAppOutput(std::vector<AppContext>& appinfos)
{
  // TODO: the loop can probably be optimized by using poll fucntion
  // from the system which checks a group of file descriptors for the
  // availability of data or certain features

  char buffer[1024];
  for (auto const& context : appinfos) {
    auto readPipe = [&buffer, &context](int fd) -> size_t {
      // FIXME: maybe we can connect this with some kind of pipe status and make
      // sure, the method is not called unless the pipe is in good state
      if (fd < 0) {
        return 0;
      }
      auto res = read(fd, buffer, sizeof(buffer));
      if (res >= 0) {
        // TODO: 0 has a special meaning, there is no more data available as the
        // other end of the pipe has been closed, we could use this to save the
        // state of the pipe and avoid reading for those where no data is expected
        return res;
      } else {
        // TODO: need to check the actual error, we expect EAGAIN for the pipe
        // which has currently no data
        auto err = errno;
        if (err != EAGAIN) {
          // FIXME: throw
          std::cerr << "Error reading stream for app " << context.id
                    << " " << context.spec.name
                    << ": " << errno << " " << strerror(errno) << std::endl;
        }
      }
      return 0;
    };

    auto writeLines = [&buffer, &context](size_t size, auto& stream) -> size_t {
      if (size == 0) {
        return 0;
      }
      std::string_view lines(buffer, size);
      // TODO: by now the only delimiting character is '\n', can be easily extended
      // or even configured
      const char* delimiters = "\n";
      auto pos = lines.find_first_of(delimiters);
      do {
        auto line = lines.substr(0, pos);
        if (line.length() > 0) {
          stream() << line << std::endl;
        }
        lines.remove_prefix(pos != std::string_view::npos ? pos + 1 : pos);
      } while ((pos = lines.find_first_of(delimiters)) != std::string_view::npos);
      return 0;
    };

    // redirector is checking if the callback is valid, falling back to std out if not
    auto doWrite = [&writeLines](size_t size, auto& stream) -> size_t {
      auto fallback = []() -> std::ostream& { return std::cout; };
      return writeLines(size, stream ? stream : fallback);
    };

    // FIXME: if the full buffer has been read, the last line is potentially broken up
    doWrite(readPipe(context.chldout), context.outStreamCallback);
    doWrite(readPipe(context.chlderr), context.errStreamCallback);
  }
}

std::ostream& AppControl::defaultNullStream()
{
  // use an unopened ofstream, so this will be in error state but we
  // do not care
  static std::ofstream nullstream;
  return nullstream;
}

AppSpec::OutStream AppControl::defaultStreamCallback(AppContext& context, std::string prefix)
{
  return [name = context.spec.name, prefix = prefix]() -> std::ostream& {
    std::cout << "[" << name << "]"
              << (prefix[0] != 0 ? " " : "") << prefix << ": ";
    return std::cout;
  };
}

} // namespace appcontrol
