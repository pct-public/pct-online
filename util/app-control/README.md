# cmdlAppControl - a command-line app controller framework
A controller framework to run a configuration of programs (apps) from one single command.

## General concept
The AppControl framework allows to start multiple processes from the main AppControl process.
Apps are declared using struct AppSpec, the main entry point is the runner function taking
the normal command line arguments of the main function.

All apps are compiled into one executable and can share resources before forking. Each child
will invoke the corresponding runner.

The main process monitors the state and output of the child processes, the target of each
stream can be configured independently. By default, std output of the controller process
is used for output, each line is starting with the source stream name.

Executables can be integrated by implementing a simple hook which declars the apps.

## Declaration of apps
The struct [AppSpec](include/app-control/AppSpec.h) collects all information needed for
declaration of an app.
- [runner hook](#runner-hook)
- [options declarator](#Optional-command-line-options-declaration)
- [output stream customization](#Optional-output-stream-customization)

### Runner hook
A callable object with signature `int(int, char*[])`, usually a lambda function is passed
to the constructor of `AppSpec`.`

### Optional command line options declaration
Optional callable function returning a boost options description.

### Optional output stream customization
Optional callable function return reference to output stream to be used for output from
this process.

## Controller boiler plate
For minimal integration effort, a boiler plate header file [controller.h](include/app-control/controller.h) is provided which implement the main function and setup hooks. The integration consists of
three steps:
- include the `cxx` files of the applications
- implement setup function and add app declarations to list
- include `controller.h` boiler plate

Currently, the boiler plate code is very small, and the underlying `AppControl` class can also be used directly, see [below](#using-appcontrol-class-directly).

An existing program with its mainfunction can be integrated by including the `cxx` file within
an arguments namespace, its main function will appear in that namespace. The only requirement
is to include all necessary app headers in the header file of the app and include this just
before the namespace declaration. All symbols from the header files will be in the namespace
otherwise.
```app
#include "app.h"
namespace impl
{
#include "app.cxx"
}
```

The runner of each app is defined by a lambda function with `int argc, char* argv[]` arguments.
Implement function `setapp` and add all application declarations to the list.
```cpp
void setapp(std::vector<bpct::util::AppSpec>& specs)
{
  // the lambda of the first app calls main of the included program
  specs.emplace_back("dumb app", [](auto argc, auto argv[]) { return impl::main(argc, argv); });
  // the second app implements some simple
  specs.emplace_back("smart app", [](auto argc, auto argv[]) {
    int delay = 5;
    while (delay-- > 0) {
      std::cout << "Only " << delay << "s left" << std::endl;
      sleep(1);
    }
    std::cout << "I'm done" << std::endl;
    return result;
  });
}
```

Finally, include the boiler plate header
```cpp
// the controller.h implements the main function and the hook for the setapp
// method to be used to declare the apps. For the later to work correctly, the
// header needs to be included at the end
#include "app-control/controller.h"
```

## Using `AppControl` class directly

```cpp
int main(int argc, char* argv[])
{
  std::vector<bpct::util::AppSpec> specs;
  // add apps
  return bpct::util::AppControl(std::move(specs))(argc, argv);
}

```

## Next steps/TODO
- signal handling
- support for calling other programs

## Copyright notice
```
Copyright 2020 Matthias Richter. This software is distributed under the
terms of the GNU General Public License v3 (GPL Version 3).
```
