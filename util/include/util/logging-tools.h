#ifndef LOGGING_TOOLS_H
#define LOGGING_TOOLS_H

#include <cstddef>

namespace bpct
{
namespace util
{

/// @brief Print a hexdump of buffer content
/// @param desc     descriptive message to be added in the first line
/// @param buffer   void address to buffer
/// @param size     buffer size in byte
/// @param length   maximum length to be dumped
void hexDump(const char* desc, const void* buffer, size_t size, size_t length);

} // namespace util
} // namespace bpct
#endif // LOGGING_TOOLS_H
