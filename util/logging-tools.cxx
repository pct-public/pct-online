#include "util/logging-tools.h"
#include <cstring> // memset
#include <cstdio>  // printf etc
#include <cstdint> // uint8_t etc

namespace bpct
{
namespace util
{

void hexDump(const char* desc, const void* buffer, size_t size, size_t length)
{
  using byte = uint8_t;
  size_t i;
  unsigned char buff[17]; // stores the ASCII data
  memset(&buff[0], '\0', 17);
  const byte* addr = reinterpret_cast<const byte*>(buffer);

  // Output description if given.
  if (desc != nullptr)
    printf("%s, ", desc);
  printf("%zu bytes:", size);
  if (length > 0 && size > length) {
    size = length; //limit the output if requested
    printf(" output limited to %zu bytes\n", size);
  } else {
    printf("\n");
  }

  // In case of null pointer addr
  if (addr == nullptr) {
    printf("  nullptr, size: %zu\n", size);
    return;
  }

  // Process every byte in the data.
  for (i = 0; i < size; i++) {
    // Multiple of 16 means new line (with line offset).
    if ((i % 16) == 0) {
      // Just don't print ASCII for the zeroth line.
      if (i != 0)
        printf("  %s\n", buff);

      // Output the offset.
      //printf ("  %04x ", i);
      printf("  %p ", &addr[i]);
    }

    // Now the hex code for the specific character.
    printf(" %02x", addr[i]);

    // And store a printable ASCII character for later.
    if ((addr[i] < 0x20) || (addr[i] > 0x7e))
      buff[i % 16] = '.';
    else
      buff[i % 16] = addr[i];
    buff[(i % 16) + 1] = '\0';
    fflush(stdout);
  }

  // Pad out last line if not exactly 16 characters.
  while ((i % 16) != 0) {
    printf("   ");
    fflush(stdout);
    i++;
  }

  // And print the final ASCII bit.
  printf("  %s\n", buff);
  fflush(stdout);
}

} // namespace util
} // namespace bpct
