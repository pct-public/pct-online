#Set CPU to performance mode
#not active if setting power settings in bios
#cpupower frequency-set -g performance
sysctl -w net.core.rmem_max=362144000
#sysctl -w net.core.wmem_max=26214400
sysctl -w net.core.rmem_default=362144000
#sysctl -w net.core.wmem_default=26214400
sysctl -w net.core.optmem_max=362144000
#Increase the length of processor input queues
sysctl -w net.core.netdev_max_backlog=250000
sysctl -w net.core.netdev_budget=50000
#sysctl -w net.ipv4.udp_rmem_min=8192
#sysctl -w net.ipv4.udp_rmem='4096 87380 8388608'
#sysctl -w net.ipv4.udp_wmem='4096 65536 50331648'
sysctl -w net.ipv4.udp_mem="8388608 8388608 50331648"
sysctl -w net.ipv4.route.flush=1
#Dont need GRO(General recive offload) for udp, will steal cpu cycles.
ethtool -K enp21s0f0 gro off
#Set ringbuffer size to maximum
ethtool -G enp21s0f0 rx 4096
ethtool -G enp21s0f1 rx 4096
ethtool -G eno2 rx 4096
#Turn off interupt moderation
ethtool -C enp21s0f0 adaptive-rx off adaptive-tx off rx-usecs 0 tx-usecs 0

