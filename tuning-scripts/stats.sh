#!/bin/bash
#Starting commands
ethtool -S enp21s0f0 | grep rx_packets | grep -v -e "veb" >> stats.txt
ethtool -S enp21s0f0 | grep rx_dropped | grep -v -e "port" >> stats.txt
ethtool -S enp21s0f0 | grep rx_error >> stats.txt

netstat --statistics -une | grep -A 6 Udp: >> stats.txt

