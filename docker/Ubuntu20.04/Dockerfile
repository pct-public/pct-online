# @file   Ubuntu20.04/Dockerfile
# @author Matthias Richter
# @since  2021-03-05
# @brief  Docker configuration for the pct-online environment on Ubuntu20.04

# Build and push docker image with
#    docker build --tag pct-online-env-ubuntu20.04:<version> .
#    docker tag pct-online-env-ubuntu20.04:<version> bpct/ubuntu20.04:full
#    docker push bpct/ubuntu20.04:full
FROM ubuntu:20.04
LABEL Name=pct-online-env-ubuntu20.04 Version=0.0.3
RUN mkdir /home/builder
WORKDIR /home/builder

RUN apt-get update && \
    apt-get -y install apt-utils sudo && \
    apt-get -y upgrade && \
    apt-get clean

ENV TZ 'Europe/Berlin'
RUN echo $TZ > /etc/timezone && \
    apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean

RUN apt-get -y install wget curl git g++ gcc binutils \
                       gfortran cmake \
                       libboost1.71 \
                       libx11-dev libxpm-dev libxft-dev libxext-dev  \
                       gsl-bin libgsl-dbg libgsl-dev gsl-doc-pdf gsl-doc-info  \
                       libglw1-mesa libglw1-mesa-dev  \
                       libafterimage-dev  \
                       libglew-dev libglew2.1  \
                       python3 python3-dev  \
                       python3-pip && \
    python3 -m pip install numpy && \
    apt-get clean

# FIXME: from here, the configuration is common for all pct-online docker setups, would be good
# to find a way to simply include the common configuration

# install ROOT
RUN git clone http://github.com/root-project/root.git root_src && \
    (cd root_src && git checkout v6-20-00-rc1) &&  mkdir root_build && \
    (cd root_build && cmake -DCMAKE_INSTALL_PREFIX=/opt/root -DCMAKE_CXX_STANDARD=17 -DPYTHON_EXECUTABLE=/usr/bin/python3 ../root_src && make -j 8 && make install) &&\
    rm -rf root_src root_build

# install Google benchmark package
RUN mkdir src && cd src && git clone https://github.com/google/benchmark; \
    cd benchmark; \
    mkdir build; \
    (cd build; cmake -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_DOWNLOAD_DEPENDENCIES=TRUE ../;); \
    cmake --build "build" --config Release; \
    cmake --install "build"; \
    cd; rm -rf src

# setup user 'pct'
RUN useradd --create-home --shell /bin/bash -U pct; \
    chown pct:pct /home/pct; \
    chmod 755 /home/pct; \
    usermod -a -G sudo pct; \
    echo "pct     ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

WORKDIR /home/pct
USER pct
SHELL ["/bin/bash", "-c"]
RUN echo "echo Welcome to the Bergen Proton CT project" >> ./.bashrc; \
    echo "test -e /opt/root/bin/thisroot.sh && source /opt/root/bin/thisroot.sh" >> ./.bashrc; \
    echo "echo " >> ./.bashrc
ENTRYPOINT bash
