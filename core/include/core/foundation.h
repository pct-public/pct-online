// Copyright 2021 The Bergen pCT project.
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3).
//
// This file is part of pct-online. In applying this license
// the authors do not submit themself to any jurisdiction.
//
// pct-online is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// pct-online is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with pct-online. If not, see <https://www.gnu.org/licenses/>.

/// @file   foundation.h
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-03-15
/// @brief  Dependency-free c++ foundations, declarations and implementations which
///         only depend on C++ standard

#ifndef FOUNDATION_H
#define FOUNDATION_H

namespace bpct
{
namespace foundation
{

/// @struct Value
/// @brief A helper struct to define distinct data types for different configuration
/// parameters. The integral selector makes the types unique, of course the selectors
/// must be unique
template <unsigned char Selector, typename T>
struct Value {
  using value_type = T;
  Value() {}
  Value(value_type v)
    : value(v)
  {
  }
  operator value_type() const { return value; }
  value_type operator()() const { return value; }
  value_type value;
};

} // namespace foundation

// some general purpose definitions
using DefaultVerbosity = foundation::Value<1, int>;
using DefaultFileName = foundation::Value<2, const char*>;
using DefaultDirectory = foundation::Value<3, const char*>;
using DefaultWordSize = foundation::Value<4, unsigned int>;
} // namespace bpct

#endif // FOUNDATION_H
