// Copyright 2021 The Bergen pCT project.
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3).
//
// This file is part of pct-online. In applying this license
// the authors do not submit themself to any jurisdiction.
//
// pct-online is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// pct-online is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with pct-online. If not, see <https://www.gnu.org/licenses/>.

/// @file   test_foundation.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-03-15
/// @brief  Unit test for pct coding foundations

#define BOOST_TEST_MODULE Test Bergen pCT core
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "core/foundation.h"

namespace bpct
{

BOOST_AUTO_TEST_CASE(test_foundation)
{
  // compile time check of the predefined configuration objects
  static_assert(std::is_same_v<DefaultVerbosity, foundation::Value<1, int>>);
  static_assert(std::is_same_v<DefaultFileName, foundation::Value<2, const char*>>);
  static_assert(std::is_same_v<DefaultDirectory, foundation::Value<3, const char*>>);
  static_assert(std::is_same_v<DefaultWordSize, foundation::Value<4, unsigned int>>);

  // check the basic Value struct, instantiate Value of type identified by 255 (arbitrary)
  // with value 42, type conversion operator used to compare value
  BOOST_CHECK_EQUAL((foundation::Value<255, int>(42)), 42);

  // callable object used to compare value
  BOOST_CHECK_EQUAL((foundation::Value<255, int>(42)()), 42);

  // system-wide available configuration object for default verbosity
  BOOST_CHECK_EQUAL(DefaultVerbosity(3), 3);

  // system-wide available configuration object for file names
  BOOST_CHECK(strcmp(DefaultFileName("testfile.data"), "testfile.data") == 0);

  // system-wide available configuration object for directories
  BOOST_CHECK(strcmp(DefaultDirectory("targetdir"), "targetdir") == 0);
}

} // namespace bpct
