/* Local Variables:  */
/* mode: c++         */
/* End:              */

/**
 * A configurable index grid to allow optimized access of data in quantized
 * cells in multi dimensionional manifolds.
 *
 * Data is sorted into the grid according to sorting rules, and can be access
 * using the cell quantization.
 * The grid handles data objects by index in an object store, or allocates
 * underlying memory and copies the data into a linearized representation.
 */
