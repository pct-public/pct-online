#ifndef PDTPHELPERS_H
#define PDTPHELPERS_H

#include "datamodel/pDTP-protocol.h"
#include <ostream>

// TODO: decide whether that should be utilities
namespace bpct
{
namespace data_model
{
struct pDTPHelpers {
  enum struct ClientStringID : unsigned {
    CLIENT_OPCODES = 0,
    CLIENT_RQR = CLIENT_OPCODES,
    CLIENT_RQT,
    CLIENT_RQS,
    CLIENT_RQFS,
    CLIENT_ERR,
    CLIENT_ACK,
    CLIENT_ABRT,
    CLIENT_GS,
    FLAGS,
    NO_ACK = FLAGS,
    MIN_REQ,
    UNINTERPRETABLE,
    MAZIMIZE,
    NO_WAIT,
    TIMEOUT,
    RESEND,
    INVALID,
  };

  enum struct ServerStringID : unsigned {
    SERVER_OPCODES = 0,
    SERVER_WRITE = SERVER_OPCODES,
    SERVER_STREAM,
    SERVER_ERROR,
    SERVER_EOS,
    SERVER_STATUS,
    FLAGS,
    FULL,
    INVALID_RQ,
    ALMOST_FULL,
    MIN_RQ,
    EMPTY,
    TIMEOUT,
    INVALID,
  };

  static const char* ClientStrings[];
  static const char* ServerStrings[];
};

} // namespace data_model
} // namespace bpct

inline std::ostream& operator<<(std::ostream& stream, bpct::data_model::pDTPHelpers::ClientStringID id)
{
  stream << bpct::data_model::pDTPHelpers::ClientStrings[static_cast<unsigned>(id)];
  return stream;
}

inline std::ostream& operator<<(std::ostream& stream, bpct::data_model::pDTPHelpers::ServerStringID id)
{
  stream << bpct::data_model::pDTPHelpers::ServerStrings[static_cast<unsigned>(id)];
  return stream;
}

std::ostream& operator<<(std::ostream& stream, bpct::data_model::pDTPClientRequest code);

std::ostream& operator<<(std::ostream& stream, bpct::data_model::pDTPServerReply code);

#endif
