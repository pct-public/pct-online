#ifndef PRU_FORMAT_H
#define PRU_FORMAT_H

#include <cstdint> // uint64_t etc

namespace bpct
{
namespace data_model
{

// Masks
const unsigned char pRUTypeMask = 0b11000000; // 0b11000000

// Flags TODO: Same as PRUFormat? remove?
const unsigned char pRUTHeaderWord = 0b01000000;
const unsigned char pRUTDataWord = 0b00000000;
const unsigned char pRUTTrailerWord = 0b10000000;

struct PRUFormat {
  enum struct WordType {
    DATA_WORD = 0x0,
    TAG_HEADER_WORD = 0x1,
    TAG_TRAILER_WORD = 0x2,
    TAG_EMPTY_WORD = 0x3, // can be also DELIMITER_WORD
  };

  uint64_t lowerAlpide;
  union {
    uint64_t msbs64;
    struct {
      uint64_t upperAlpide : 48;
      uint64_t chipID : 4;
      uint64_t staveID : 4;
      uint64_t readoutUnitID : 6;
      uint64_t wordType : 2;
    };
  };

  /* TODO: check this alternative
  union {
    uint64_t raw[2];
    struct {
      uint64_t upperAlpide : 112;
      uint64_t chipID : 4;
      uint64_t staveID : 4;
      uint64_t readoutUnitID : 6;
      uint64_t wordType : 2;
    };
  };
  */
};
} // namespace data_model
} // namespace bpct
#endif // PRU_FORMAT_H
