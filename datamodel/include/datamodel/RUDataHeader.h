#ifndef RUDATAHEADER_H
#define RUDATAHEADER_H

#include <cstdint> // uint32_t etc
#include "AlpideHit.h"

namespace bpct
{
namespace data_model
{

// TODO: Realign elements
// Ellements wil be aligned in to 32 or 64 bit memmory blocks by the compiler.
// Non 32 bit blocks wil create memmory aereas with no data. 
// - Wastes memory. 
// - Issue when using direct memmory access. An empty aerea where data was expected.

/// @struct RUDataHeader
/// Header to preceed an array of AlpideHit objects
/// Assuming a 10% occupancy, each Alpide has about 550000 hits and a full layer
/// less than 6*10^6 hits, which fits well in a 32 bit number
struct RUDataHeader {
  using value_type = AlpideHit;
  union {
    uint32_t propertyfield = 0xff;
    struct {
      // 40 layers
      uint32_t layer : 8;
      // bunch crossing counter in the Alpide chip is 7 bit, use 8 for better readability
      uint32_t bc : 8;
      // busy information, needs to be clarified
      uint32_t busy : 8;
      // reserved
      uint32_t reserved : 8;
    };
  };
  // a frame counter produced by the RU (32 bit)
  uint32_t frameid = 0;
  // a frame counter produced by the RU (32 bit)
  uint32_t time = 0;
  // number of hits
  uint32_t nofHits = 0;
  // this member has length 0 and can be used to acces the array
  value_type hits[];
};
// ths size of RUDataHeader should be 128bit
static_assert(sizeof(RUDataHeader) == 16);

} // namespace data_model
} // namespace bpct

#endif // RUDATAHEADER_H
