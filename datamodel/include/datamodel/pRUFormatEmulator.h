/// @file   pRUFormatEmulator.h
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-21
/// @brief  Emulator for the pRU data format

#ifndef PRUFORMAT_EMULATOR_H
#define PRUFORMAT_EMULATOR_H

#include <functional>
#include <array>
#include <type_traits>

namespace bpct
{
namespace data_model
{

namespace internal
{
// just a helper struct to define distinct data types for the different distribution
// functions
template <unsigned char Selector>
struct Value {
  using value_type = unsigned char;
  Value(value_type v)
    : value(v)
  {
  }
  operator value_type() const { return value; }
  unsigned char value;
};
} // namespace internal

namespace pRU
{
constexpr size_t PRUWORD_SIZE = 16;
using Word = std::array<unsigned char, PRUWORD_SIZE>;
} // namespace pRU

/// @class pRUEmulatorDevice
/// A stateful emulator device, pRU words are inserted into the stream according
/// to executed state. A device has a 3-dimensional identifier {id, row, column},
/// corresponding to {PRU id, stave id and chip id}
///
/// If selected, the device runs actions depending on its state and parameter
/// distributions. The ALPIDE word types are inserted into the data stream,
/// CHIP header and trailer are set according to specifications, and data words
/// are inserted according to the statistical distributions for burst length and
/// framesize, but the ALPIDE data words do not contain any meaningful data.
///
/// The state processing is executed according to the following parameters:
///   - DataLength:  number of octets in a data word
///   - BurstLength: the minimum number of octets in the data stream in the
///                  current processing
///   - FrameSize:   the minimum number of pRU words in the pRU frame
///   - WaitCount:   number of invocations to stay in IDLE/BUSY
///
/// Every parameter is configured by a corresponding callback. The callback
/// can be used to implement probability distributions.
///
/// States:
///   - START start emulator device, configure parameters
///     transition to state HEADER, and restarts state processing
///   - HEADER create and insert pRU header word, init the pRU data word and set
///     ALPIDE_CHIP_HEADER and ALPIDE_REGION_HEADER
///     transition to state DATA, restart state processing
///   - DATA  create data words, ALPIDE long and short data words are selected
///     by callback DataLengthDistribution, the number of inserted data words
///     depends on parameters BurstLength, the number of octets in one processing
///     cycle, and FrameSize, describing the number of pRU words of the pRU frame.
///     stops after reaching number of octets returned by the BurstLengthDistribution
///     callback
///     transition to state TRAILER if frame size returned by FrameSizeDistribution
///     is reached, and restarts state processing; stays in state DATA otherwise,
///     no restart of the state processing
///   - TRAILER insert ALPIDE_CHIP_TRAILER, flush the current pRU word and create
///     and insert the pRU trailer word
///     transition to state IDLE and set a wait count according to WaitDistribution
///   - IDLE ignore a number of invocations before starting new frame
///     transition to state HEADER if wait count reached 0, restart state processing
///     stay in IDLE and leave processing otherwise
///   - BUSY
///   - STOP finish and insert current pRU word and add pRU trailer (see state TRAILER)
///     transition to DEAD
///   - BREAK do not insert neither current pRU word nor pRU trailer
///   - transition to DEAD
///   - DEAD no action
///
/// Format errors can be provoked by means of forceState, to set a state and continue
/// from this state, and the stream will lack some data and trailer information.
///
/// Some notes for further development:
/// For simulating a whole layer with 108 devices, some optimizations might be
/// appropriate:
/// - each device needs its state, the current pRU word, and the position within,
///   however its not necessary that each devices has its own copy of the distribution
///   callback. Though, being a first prototype, this class might be better transformed
///   into a struct holding the state and all other unique values, while the methods
///   go to a worker class processing the stateful objects
/// - the original idea of having an m x n matrix with the emulator devices corresponding
///   to the stave/chip layout of one detector layer will come with a lot of overhead
///   if the data is only sparse, i.e. many of the devices are just idling and occupying
///   memory. A solution with a queue of active devices with configurable identifier can
///   be more efficient
class pRUEmulatorDevice
{
 public:
  enum struct State {
    START,
    HEADER,
    DATA,
    TRAILER,
    BUSY,
    IDLE,
    STOP,
    BREAK,
    DEAD,
  };
  constexpr static size_t PRUWORD_SIZE = 16;
  using Word = pRU::Word;
  using Inserter = std::function<void(Word const&)>;

  // defining distinct data types for return types
  using DataLength = internal::Value<1>;
  using BurstLength = internal::Value<2>;
  using FrameSize = internal::Value<3>;
  using WaitCount = internal::Value<5>;

  // distribution functions
  using DataLengthDistribution = std::function<DataLength()>;
  using BurstLengthDistribution = std::function<BurstLength()>;
  using FrameSizeDistribution = std::function<FrameSize()>;
  using WaitDistribution = std::function<WaitCount()>;

  pRUEmulatorDevice();

  template <typename... Args>
  pRUEmulatorDevice(unsigned char id, unsigned char row, unsigned char column, Args&&... args)
    : mId(id)
    , mRow(row)
    , mColumn(column)
    , mState(pRUEmulatorDevice::State::START)
  {
    if constexpr (sizeof...(args) > 0) {
      scanArgs(std::forward<Args>(args)...);
    }
  }

  template <typename Arg, typename... Args>
  void scanArgs(Arg&& arg, Args&&... args)
  {
    using T = std::decay_t<Arg>;
    static_assert(std::is_void_v<T> == false, "invalid void type");
    // the original idea was to assign the callback by move, making the
    // function empty at the callers side, this was found to be counter-
    // intuitive, so we make a copy here
    if constexpr (std::is_same_v<T, DataLengthDistribution>) {
      mDataLengthDistribution = arg;
    } else if constexpr (std::is_same_v<T, BurstLengthDistribution>) {
      mBurstLengthDistribution = arg;
    } else if constexpr (std::is_same_v<T, FrameSizeDistribution>) {
      mFrameSizeDistribution = arg;
    } else if constexpr (std::is_same_v<T, WaitDistribution>) {
      mWaitDistribution = arg;
    } else {
      // the type-dependent trick, its always false, but we need this type-dependent
      // argument for the static_assert
      static_assert(std::is_void_v<T>, "invalid argument type");
    }

    if constexpr (sizeof...(args) > 0) {
      scanArgs(std::forward<Args>(args)...);
    }
  }

  bool operator==(State const& rhs) const
  {
    return mState == rhs;
  }

  bool operator!=(State const& rhs) const
  {
    return mState != rhs;
  }

  // execute state and stream pRU words to target
  template <typename T, typename Alloc>
  friend auto& operator<<(std::vector<T, Alloc>& target, pRUEmulatorDevice& device)
  {
    static_assert(sizeof(T) == 1, "currently only char types are supported");
    Inserter inserter = [&target](Word const& word) -> void {
      target.insert(target.end(), word.begin(), word.end());
    };

    while (device.executeState(inserter)) {
    }
    return target;
  }

  /// execute state action, action might also include a state transition
  /// @return true if the executor should be called again
  bool executeState(Inserter& inserter);

  /// force a specific state, this can be used to stop the device as well as
  /// to disturb the emulation sequence and introduce errors in the data stream
  void forceState(State forcedStae);

 private:
  /// insert a pRU word to the inserter and increment frame size
  void insert(Inserter& inserter, pRU::Word const& words);

  /// push data words to the stream
  /// data words are either 2 or 3 octets long, the length is chosen on a
  /// statistical basis
  /// data is first inserted in the current pRU word and if this is full,
  /// it is inserted into the stream
  void pushData(Inserter& inserter);

  /// set octet in the current pRU word
  size_t setOctet(unsigned char octet);

  /// get data length
  size_t dataLength() const;

  /// get burst length, a burst is a number of data words sent together
  size_t burstLength() const;

  /// get frame size, i.e. number of pRU words for the current frame
  size_t frameSize() const;

  /// reset current pRU word
  /// the first two bytes are pRU format specific and describe the pRU
  /// word type, matrix id, and row and column of device in the matrix,
  /// data words beginning from third octet are set to 0x23 which can
  /// be easily recognized in the byte stream.
  /// If there is an overflow from the data word in the last pRU word,
  /// the position is incremented by the overflow value and the octets
  /// corresponding to the overflow set to 0x69.
  void resetWord(size_t overflow = 0);

  /// return current word and reset
  void flush(Inserter&);

  /// return pRU header
  Word header() const;

  /// return pRU header
  Word trailer() const;

  /// get the number of wait cycles for IDLE or BUSY
  int getWait() const;

  /// id of the matrix
  unsigned char mId = 0;
  /// position of this device in the matrix
  unsigned char mRow = 0;
  /// position of this device in the matrix
  unsigned char mColumn = 0;
  /// state of the device
  State mState = State::START;
  /// counter for staying in IDLE
  int mWaitCount = 0;
  /// minimum number of pRU words in the frame
  int mMinFrameSize = 0;
  /// the actual size of the frame
  int mFrameSize = 0;
  /// total number of data octets
  int mDataOctets = 0;
  /// the current data word
  Word mWord = {0};
  /// position within the current word
  size_t mPosition = 16;
  /// configurable distribution for the data length
  DataLengthDistribution mDataLengthDistribution = []() { return DataLength{2}; };
  /// configurable distribution for the burst length
  BurstLengthDistribution mBurstLengthDistribution = []() { return BurstLength{3}; };
  /// configurable distribution for the frame size
  FrameSizeDistribution mFrameSizeDistribution = []() { return FrameSize{3}; };
  /// configurable distribution for busy/wait time
  WaitDistribution mWaitDistribution = []() { return WaitCount{0}; };
};

} // namespace data_model
} // namespace bpct

#endif // PRUFORMAT_EMULATOR_H
