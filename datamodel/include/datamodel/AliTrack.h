#include <vector>
#include "RUDataHeader.h"
#include <cstdint>

#ifndef PCT_BERGEN_ALITRACK_H
#define PCT_BERGEN_ALITRACK_H

namespace bpct
{
namespace data_model
{


/// @struct AliTrack
/// A data structure containing particle hit points for the Alignment module.
/// This structure does not guarantee an ordered list, it only holds the points using a vector.
/// Its up to the creator/user of the data structure to ensure ordered list.
///
/// The alignment algorithm needs information about the layer, stave, chip id to apply correct
/// offset values to each chip.

struct AliHit {
  int layer;
  int stave;
  int chip;
  int x;
  int y;
};

struct AliTrack {
  std::vector<AliHit> hits;
};


struct globalHit{
  double x;
  double y;
  double z;
};

} // namespace data_model
} // namespace bpct

#endif //PCT_BERGEN_ALITRACK_H