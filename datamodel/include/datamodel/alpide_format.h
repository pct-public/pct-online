#ifndef ALPIDE_FORMAT_H
#define ALPIDE_FORMAT_H

/* 
   Enum class for ALPIDE word prefixes and their masks.
   Note that the words have separate lengths and this must be handled separately

   // TODO: Bring it in to line with the other format classes.
*/

#include <cstdint> // uint 

namespace bpct {
namespace data_model  // parser
{

//using octet = std::uint8_t; // Can potentially give endianness problems
using octet = unsigned char;


//------------ ALPIDE WORDS EXPLAINED ------------------//

/*
 * Bunch Counter: This ﬁeld contains the last sample of the Bunch Crossing counter. 
 * The FROMU samples the Bunch Crossing counter shortly after it receives a trigger pulse.
 */

/* Chip Header:  Name/value: |1010 chip_id | BUNCH_COUNTER_FOR_FRAME|
 * (16 bit):           Bits: |15:12  11:8  |          7:0           |
 */// alpide chip header =   {0b1010 xxxx,  0bxxxxxxxx};

/*
* Region_id is one of the 32 readout regions of the ALPIDE chip.
* Each region contains (512 x 32 pixels )(16 double coullomns).
* The double coullumns are read out sequencially while the regions are read out in parallel.
* Region headers are sent in acending order!
* 
* TODO: Check statement for correctness: 
* readout from double coullumns and regions happens sequentially in acending order?
*/


/* Region Header: Name/value: |110 region_id|
 * (8 bits):            Bits: |7:5    4:0   |
 */// alpide region Header =  {0b110 xxxxx};

/*
 * The encoder_id field (4.2 in the Alpide maual(p 88-91))
 * Looking at the chip with the digital periphery on the bottom, the leftmost
 * region is region 0 and the rightmost region is region 31 (see Figure 4.3).
 * Each region contains 16 double columns. Double column 0 is the leftmost and
 * double column 15 is the rightmost (see Figure 4.4). 
 * The matrix of pixels is read out by an array of 512 Priority Encoder blocks (2x512 = 1024). 
 * The pixels are arranged in double columns and the regions at the middle of each
 * double column are occupied by the Priority Encoders. 
 * The indexing of the pixels in the readout data words is deﬁned by the Priority Encoders. 
 * The indexing of the pixels in each double column is illustrated in Figure 4.5.
 */

/* Data Short: Name/value: |01 encoder_id address|address(hit adress)|
 * (16 bit):         Bits: |15:14  13:10    9:8  |       7:0         |
 */// alpide data short =  {0b01 xxxx xx,         0bxxxxxxxx};

/*
 * A bit in the hit map is set for any active pixel among the 7 immediately
 * after (based on PE pixel addreses) the one indicated by the addr[9:0] ﬁeld.
 * The LSB of hit map corresponds to ﬁrst subsequent pixel and bit 6 to the 7th.
 * Figure 3.11 illustrates this for a sample cluster.
 */

/* Data Long: Name/value: |00   encoder_id address| address(hit adress) | 0  hit_map | 
 * (24 bits):       Bits: |23:22   21:18    17:16 |         15:8        | 7     6:0  |
 */// alpide data long =  {0b00 xxxx xx,            0bxxxxxxxx,           0b0xxxxxxx};

/*
 * The <readout ﬂags[3:0]> data ﬁeld consists of 4 ﬂags, 
 * which transmit information about the event in question and the state of the chip. 
 * The data composition is as follows:
 *  
 * readout flags[3 : 0] >=
 * {< BUSY V IOLATION >< FLUSHED INCOMPLETE > < STROBE EXTENDED >< BUSY TRANSITION >}
 * 
 * The ﬂags outlined above are generated as follows:
 *   BUSY VIOLATION – indication that the chip is replying with an empty data packet due to saturation of data processing capabilities.
 *   FLUSHED INCOMPLETE – indication that a MEB slice was ﬂushed in order to ensure that the MATRIX always has a free memory bank for storing new events. Observed in Continuous mode only.
 *   STROBE EXTENDED – indication that the framing window for the event of question was extended due to the reception of an external trigger.
 *   BUSY TRANSITION – indication that the BUSY was asserted during the readout of the frame in question.
 *  
 * Note that the ﬂags descirbed above can be overridden in special circumstances:
 * 
 * 1. If the FATAL bit was asserted, the <readout ﬂags[3:0]> ﬁeld is set to a constant 4’b1110, regardless of whether any of the standard ﬂags were asserted for the event in question.
 * 2. If FATAL is not asserted but the event is transmitted when the chip is in DATA OVERRUN MODE, the standard ﬂags are overridden by a constant 4’b1100 pattern.
 */

/* Chip Trailer: Name/value: |1011 readout_flags|
 * (8 bits):           Bits: |7:4      3:0      |
 */// alpide chip trailer =  {0b1011'xxxx};

//------------ PREFIX STRUCTS ------------------//

// Alpide word prefixes with data section masked. 
// The ' indicates end of prefix
//enum struct Headers : octet {
const octet ALPIDE_CHIP_HEADER = {0b1010'0000};
const octet ALPIDE_CHIP_TRAILER = {0b1011'0000};
const octet ALPIDE_REGION_HEADER = {0b110'00000};
const octet ALPIDE_DATA_SHORT = {0b01'000000};
const octet ALPIDE_DATA_LONG = {0b00'000000};
const octet ALPIDE_PADDING = {0b11111111};
const octet ALPIDE_BUSY_ON = {0b11110001};
const octet ALPIDE_BUSY_OFF = {0b11110000};
//}

// Prefix bit masks. The ' denotes the quad mark
const octet MASK_CHIP = 0b1111'0000;           /// Chip Header & Trailer mask
const octet MASK_REGION_HEADER = 0b1110'0000;  /// Region header mask
const octet MASK_DATA = 0b1100'0000;           /// Data short/long mask

}  // namespace readout
}  // end namespace bpct

#endif  // ALPIDE_FORMAT_H
