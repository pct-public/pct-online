#ifndef ALPIDEHIT_H
#define ALPIDEHIT_H

#include <cstdint> // uint32_t etc

namespace bpct
{
namespace data_model
{

/// @struct AlpideHit
/// The fundamental raw coordinates of an Alpide hit within a detector layer
/// width of the fields is given by the dimension of the Alpide with 1024 x 512 pixels,
/// and the design of the detector layer with 12 staves and 9 chips each
/// The raw position can be stored in a 32 bit number.
struct AlpideHit {
  AlpideHit()
    : positionfield(0)
  {
  }
  AlpideHit(uint32_t setX, uint32_t setY, uint32_t setChip = 0, uint32_t setStave = 0)
    : x(setX)
    , y(setY)
    , chip(setChip)
    , stave(setStave)
  {
  }
  union {
    uint32_t positionfield;
    struct {
      // range 1024 columns, 12 bit for better readability
      uint32_t x : 12;
      // range 512 rows, 12 bit for better readability
      uint32_t y : 12;
      // 9 chips per stave
      uint32_t chip : 4;
      // 12 staves per layer
      uint32_t stave : 4;
    };
  };
};
// ths size of AlpideHit should be 32bit
static_assert(sizeof(AlpideHit) == 4);

/// @struct AlpideHitTimed
/// Like AlpideHit with an aaditional 32 bit timing information, consisting of the
/// 8 bit bunch crossing counter of the Alpide and a time value provided by the RU.
/// A 24 bit time value can represent a roughly 15 s with a 1 microsec clock tick.
struct AlpideHitTimed {
  AlpideHitTimed()
    : positionfield(0)
    , timefield(0)
  {
  }
  AlpideHitTimed(uint32_t setX, uint32_t setY, uint32_t setChip = 0, uint32_t setStave = 0, uint32_t setBc = 0, uint32_t setTime = 0)
    : x(setX)
    , y(setY)
    , chip(setChip)
    , stave(setStave)
    , bc(setBc)
    , time(setTime)
  {
  }
  union {
    uint32_t positionfield;
    struct {
      // range 1024 columns, 12 bit for better readability
      uint32_t x : 12;
      // range 512 rows, 12 bit for better readability
      uint32_t y : 12;
      // 9 chips per stave
      uint32_t chip : 4;
      // 12 staves per layer
      uint32_t stave : 4;
    };
  };
  union {
    uint32_t timefield;
    struct {
      // bunch crossing counter in the Alpide chip is 7 bit, use 8 for better readability
      uint32_t bc : 8;
      // use remaining 24 bit for a time stamp
      uint32_t time : 24;
    };
  };
};
// ths size of AlpideHit should be 64bit
static_assert(sizeof(AlpideHitTimed) == 8);

} // namespace data_model
} // namespace bpct

#endif // ALPIDEHIT_H
