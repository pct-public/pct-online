#ifndef PDTP_PROTOCOL_H
#define PDTP_PROTOCOL_H

#include <cstdint> // uint64_t etc

namespace bpct
{
namespace data_model
{

/// @section pDTP protocol
/// The pCT Data Transfer Protocol (pDTP) defines the communication between the
/// pCT Readout Unit and a client PC
/// The RU unit acts as a server
///
/// \par Byte and bit order
/// This definition of the protocol headers is for little endian hosts. A nice summary of the byte and
/// bit order game is given in https://www.linuxjournal.com/article/6788.
/// Because endianess is a tricky subject, a few examples follow to illustrate the fields of the defined
/// headers. In principle, we are free to define our byte and bit order, we only have to agree on it.
/// Byte order is somehow clear, bit order can make the things more difficult. There is not only endianess
/// for bytes but also for bit order. Network byte order is big endian, and for UDP also bit order follows
/// big endian. In a 4 bit field with big endian bit order, the most significant bit is stored at position
/// zero. In the left to right reading that means
///      0 1 2 3           and not   0 1 2 3
///      1 0 1 0 = 0xa               0 1 0 1
///
/// This applies for a value with bit width smaller than 8 like the opcode. In addition the flags act as
/// individual bits, not as a 4 bit field. That has implications on a stored number. A RQR with no_ack flag
/// becomes in hexadecimal with the field width in brackets
///
///       2[4] 1[1] 0[1] 0[1] 0[1] 0[16] 1[8]
///
/// And in bit representation
///
///       |       0       |       1       |       2       |       3       |
///       |0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|
///
///        0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
///
/// The remaining confusion is that the corresponding hexadecimal number for byte 0 will be 28, however
/// having bit 4 in mind that would be something like 10 because, bit 4 is the lowest bit of the upper nibble.
///
/// In the end its a matter of defining the protocol header in software. This is easy to adjust, so we stick
/// to the hardware implementation.

/// @class pDTPClient_header
/// @brief Header for the client requests to the server
/// In raw network format, the header as defined here is preceded by the IPv4 and
/// UDP headers, in total 20 + 8 = 28 bytes
struct pDTPClientRequest {
  // TODO: consider making this a real class which then allows to implement convenience operators
  // at the same time as keeping type-safety
  // see https://stackoverflow.com/questions/12753543/is-it-possible-to-manually-define-a-conversion-for-an-enum-class
  enum struct ClientOpcode {
    /// read request: readout packet with n pRU words
    CLIENT_RQR = 0x00,
    /// test request: test packet with n pRU words
    CLIENT_RQT = 0x01,
    /// stream request request: readout stream of m packts with n pRU words each
    CLIENT_RQS = 0x02,
    /// full stream request request: continous readout stream
    CLIENT_RQFS = 0x03,
    /// client error: timeout while waiting for packet
    CLIENT_ERR = 0x04,
    /// acknowledge for received packet
    CLIENT_ACK = 0x05,
    /// abort current operation
    CLIENT_ABRT = 0x06,
    /// require the server status
    CLIENT_GS = 0x07,
    /// The number of clock cycles between each stream packet transmitted from the server.
    CLIENT_THR = 0x08, ///Use nofpackets and sizeofpacket to create 24 bit field WAIT_CYCLES
  };

  /// Client flags are in the lower nibble of the first byte. Because of the big
  /// endian bit order, bits appear swapped in the host representation.
  enum struct ClientFlag : uint8_t {
    NO_ACK = 0x8,          // Note: bit number 4 in the big endian bit stream!
    MIN_REQ = 0x4,         // bit number 4, and so forth
    UNINTERPRETABLE = 0x4, // Note: same as above, depends on opcode
    TIMEOUT = 0x2,
    MAXIMIZE = 0x2, // Note: same as above, depends on opcode
    RESEND = 0x1,
    NO_WAIT = 0x1, // Note: same as above, depends on opcode
  };

  /// The definition of the individual fields in a 32 bit raw value
  /// Note:
  /// - since bit order is big endian, the two nibbles of the first byte are
  ///   swapped and we have first flags and then the opcode
  /// - the 16 bit packet size needs to get byte-swapped before transmission
  ///   for the sake of network byte order (big endian)
  union {
    uint32_t raw = 0;
    struct {
      /// flag validity depending on opcode            RQR RQT RQS RQFS ERR ACK ABRT GS THR
      ///
      /// flags depending on opcode                     x   -   x   x    -   -   -   -   -
      uint32_t flags : 4;
      /// requested operation
      uint32_t opcode : 4;
      /// number of packets                             -   -   x   -    -   -   -   -   -
      uint32_t nofpackets : 16;
      /// packet size in pRU words                      x   x   x   x    -   -   -   -   -
      uint32_t sizeofpacket : 8;
    };
    struct {
      /// The number of clock cycles between each
      /// stream packet transmitted from the server.    -   -   -   -   -    -   -   -   x
      uint32_t unused_msb_thr : 8;
      uint32_t wait_cycles : 24;
    };
    // this struct defines the special fields for read requests at bit 4 and 5
    // Note: the most significant byte in network byte order becomes the lowest part in the
    // 32 bit little endian representation, since also the bit order is big endian in
    // the hardware implementation, also the two nibbles are swapped in the first byte
    struct {
      ///Server will not wait for buffer to be filled
      /// with data if buffer is empty or is filled
      /// with less than MIN_RQ. Result is EOS in both
      /// RQS and RQFS.                                 -   -   x   x    -   -   -   -   -
      uint32_t no_wait : 1;
      /// Maximize the number of pRU words transmitted,
      /// based on the data available. Can be used
      /// together with MIN_RQ.                         x   -   x   x    -   -   -   -   -
      uint32_t maxi : 1;
      /// min size in packets                           x   -   x   x    -   -   -   -   -
      uint32_t min_req : 1;
      /// no acknowledge flag                           x   x   -   -    -   -   -   -   -
      uint32_t no_ack : 1;
      /// alignment field, unused
      uint32_t unused_lsb_rr : 28;
    };
    // this struct defines the special fields for the CLIENT_ERROR command
    struct {
      /// resend flag                                   -   -   -   -    x   -   -   -   -
      uint32_t resend : 1;
      /// timeout flag                                  -   -   -   -    x   -   -   -   -
      uint32_t timeout : 1;
      /// uninterpretable server response               -   -   -   -    x   -   -   -   -
      uint32_t uninterpretable : 1;
      /// alignment field, unused
      uint32_t unused_lsb_error : 29;
    };
    // This structs defines the special field for the CLIENT_THROTTLE op code
  };
};
static_assert(sizeof(pDTPClientRequest) == 4);

/// @class pDTPServer_header
/// @brief Header for the server replies to the client's requests
/// In raw network format, the header as defined here is preceded by the IPv4 and
/// UDP headers, in total 20 + 8 = 28 bytes
///
/// The payload directly follows the header
struct pDTPServerReply {
  enum struct ServerOpcode : unsigned {
    SERVER_WRITE = 0x0,
    SERVER_STREAM = 0x1,
    SERVER_ERROR = 0x2,
    SERVER_EOS = 0x3,
    SERVER_STATUS = 0x4,
  };

  /// Server flags are in the lower nibble of the first byte. Because of the big
  /// endian bit order, bits appear swapped in the host representation.
  enum struct ServerFlags : unsigned {
    FULL = 0x8,
    INVALID_RQ = 0x8, // same as above, depending on opcode
    ALMOST_FULL = 0x4,
    MIN_RQ = 0x4, // same as above, depending on opcode
    EMPTY = 0x2,
    TIMEOUT = 0x1,
  };

  union {
    uint32_t raw = 0;
    struct {
      /// field validity depending on operation: WRITE  STREAM  ERROR  EOS  STATUS
      ///
      /// flags depending on opcode                x      x       x     x     x
      uint32_t flags : 4;
      /// operation
      uint32_t opcode : 4;
      /// packet id                                x      x       -     -     -
      uint32_t packetid : 16;
      /// payload size in pRU words                x      x       -     -     -
      uint32_t size : 8;
    };
    // this unnamed struct defines the special fields for the server status reply
    // Note: the most significant byte in network byte order becomes the lowest part in the
    // 32 bit little endian representation, since also the bit order is big endian in
    // the hardware implementation, also the two nibbles are swapped in the first byte
    struct {
      /// flag: timeout, belongs to server error, but we can define the alias here
      /// as this is the only meaning of bit 7
      uint32_t timeout : 1;
      /// flag: readout buffer is empty
      uint32_t empty : 1;
      /// flag: readout buffer is almost full
      uint32_t almost_full : 1;
      /// flag: readout buffer is full
      uint32_t full : 1;
      /// unused alignment filed
      uint32_t status_opcode : 4;
      /// number of pRU words in the buffer
      uint32_t fill_count : 16;
      /// version number
      uint32_t version : 8;
    };
    // this unnamed struct defines the special fields for the server error reply
    struct {
      /// alignment field, aliases for bit 6 and 7 are defined in the status struct
      uint32_t unused_msb_error_reply : 2;
      /// flag: min_req
      uint32_t min_req : 1;
      /// flag: invalid request
      uint32_t invalid_rq : 1;
      /// unused alignment filed
      uint32_t error_opcode : 4;
      /// alignment field, aliases for bit 6 and 7 are defined in the status struct
      uint32_t unused_lsb_error_reply : 24;
    };
  };
  uint32_t timestamp = 0;
};
static_assert(sizeof(pDTPServerReply) == 8);

} // namespace data_model
} // namespace bpct
#endif
