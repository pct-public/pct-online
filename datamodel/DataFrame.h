/* Local Variables:  */
/* mode: c++         */
/* End:              */

/**
 * Frame holding a set of data annotated with a constant property
 *
 * The property is initialized when instantiating the object and is constant
 * over its lifetime.
 */
template <typename T, typename PropertyT>
class DataFrame
{
 public:
  using value_type = T;
  using property_type = PropertyT;
  using set_type = std::vector<value_type>;

  DataFrame(property_type property) : mProperty(property);
  ~DataFrame();

  set_type& data() { return mData; }
  set_type const& data() const { return mData; }

 private:
  property_type mProperty;
  set_type mData;
};
