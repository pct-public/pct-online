#include "datamodel/pDTP-helpers.h"
#include <iomanip>
#include <sstream>

namespace bpct
{
namespace data_model
{
const char* pDTPHelpers::ClientStrings[] = {
  "CLIENT_RQR",
  "CLIENT_RQT",
  "CLIENT_RQS",
  "CLIENT_RQFS",
  "CLIENT_ERR",
  "CLIENT_ACK",
  "CLIENT_ABRT",
  "CLIENT_GS",
  "CLIENT_THROTTLE",
  "NO_ACK",
  "MIN_REQ",
  "MAXIMIZE",
  "NO_WAIT",
  "UNINTERPRETABLE",
  "TIMEOUT",
  "RESEND",
  "",
};

const char* pDTPHelpers::ServerStrings[] = {
  "SERVER_WRITE",
  "SERVER_STREAM",
  "SERVER_ERROR",
  "SERVER_EOS",
  "SERVER_STATUS",
  "FULL",
  "INVALID_RQ",
  "ALMOST_FULL",
  "MIN_RQ",
  "EMPTY",
  "TIMEOUT",
  "",
};

} // namespace data_model

namespace format
{
/// format bit pattern of n-bit value in big-endian byte and bit order
/// TODO: move to logging utilities
template <typename T>
std::stringstream formatBitpattern(T const& value)
{
  // need to format the bitpattern because we want to have blanks between the bits
  // could use std::bitset otherwise
  std::stringstream bitpattern;
  for (int byteno = 0, lastbyte = sizeof(typename std::remove_reference<decltype(value)>::type); byteno < lastbyte; byteno++) {
    for (int bitno = 7; bitno >= 0; bitno--) {
      bitpattern << " ";
      bitpattern << ((value & (0x1 << (byteno * 8 + bitno))) != 0);
    }
  }
  return bitpattern;
}

} // namespace format

} // namespace bpct

std::ostream& operator<<(std::ostream& stream, bpct::data_model::pDTPClientRequest code)
{
  using pDTPHelpers = bpct::data_model::pDTPHelpers;
  // clang-format off
  std::stringstream hexformatted;
  hexformatted << "0x" << std::setw(8) << std::setfill('0') << std::hex << code.raw;
  stream << "pDTP client request header: " << static_cast<pDTPHelpers::ClientStringID>(code.opcode)
         << "\n  " << "|       0       |       1       |       2       |       3       |"
         << "\n  " << "|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|"
         << "\n  " << bpct::format::formatBitpattern(code.raw).str()
         << "\n  " << " O      |N M M N|          nof packets          | packet size   |"
         << "\n  " << "  P     |O I A O|"
         << "\n  " << "   C    |A N X W|"
         << "\n  " << "    D   |C R I A|                       ________________________"
         << "\n  " << "     E  |K Q M I|                      | raw value: " << " "
         << hexformatted.str() << " |"
         << std::endl << std::endl;
  // clang-format on

  return stream;
}

std::ostream& operator<<(std::ostream& stream, bpct::data_model::pDTPServerReply code)
{
  using pDTPHelpers = bpct::data_model::pDTPHelpers;
  // clang-format off
  std::stringstream hexformatted;
  hexformatted << "0x" << std::setw(8) << std::setfill('0') << std::hex << code.raw;
  stream << "pDTP server reply header: " << static_cast<pDTPHelpers::ServerStringID>(code.opcode)
         << "  ( raw value " << hexformatted.str() << ")"
         << "\n  " << "|       0       |       1       |       2       |       3       |"
         << "\n  " << "|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|0 1 2 3 4 5 6 7|"
         << "\n  " << bpct::format::formatBitpattern(code.raw).str()
         << std::endl << std::endl;
  // clang-format on

  return stream;
}
