/* Local Variables:  */
/* mode: c++         */
/* End:              */

/**
 * A data point in XYZ coordinates and time
 *
 * TODO: think about making the index type optional, with default void
 * and inheriting from a simple index structure if non-void
 */
time<typename Rep = float, typename IndexT = unsigend int>
struct PointXYZT {
  using value_type = Rep;
  using index_type = IndexT;

  value_type x;
  value_type y;
  value_type z;
  value_type t;
};
