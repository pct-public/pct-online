#define BOOST_TEST_MODULE Test Bergen pCT datamodel
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <boost/endian/conversion.hpp>
#include "datamodel/pDTP-protocol.h"
#include "datamodel/pDTP-helpers.h"

namespace bpct
{
namespace data_model
{

BOOST_AUTO_TEST_CASE(test_pDTP_protocol)
{
  pDTPClientRequest clientRequest;
  pDTPServerReply serverReply;
}

BOOST_AUTO_TEST_CASE(test_pDTP_helpers_rqr)
{
  pDTPClientRequest clientRequest;
  clientRequest.opcode = static_cast<decltype(clientRequest.opcode)>(pDTPClientRequest::ClientOpcode::CLIENT_RQR);
  clientRequest.no_ack = 1;
  clientRequest.sizeofpacket = 20;
  BOOST_TEST(boost::endian::endian_reverse(clientRequest.raw) == 0b00001000000000000000000000010100);
  std::cout << clientRequest << std::endl;
}

BOOST_AUTO_TEST_CASE(test_pDTP_helpers_rqs_maxi)
{
  pDTPClientRequest clientRequest;
  clientRequest.opcode = static_cast<decltype(clientRequest.opcode)>(pDTPClientRequest::ClientOpcode::CLIENT_RQS);
  clientRequest.no_ack = 1;
  clientRequest.nofpackets = boost::endian::endian_reverse(uint16_t(1));
  clientRequest.sizeofpacket = 3;
  clientRequest.no_wait = 0;
  clientRequest.maxi = 1;
  BOOST_TEST(boost::endian::endian_reverse(clientRequest.raw) == 0b00101010000000000000000100000011);
  std::cout << clientRequest << std::endl;
}
BOOST_AUTO_TEST_CASE(test_pDTP_helpers_rqs_no_wait)
{
  pDTPClientRequest clientRequest;
  clientRequest.opcode = static_cast<decltype(clientRequest.opcode)>(pDTPClientRequest::ClientOpcode::CLIENT_RQS);
  clientRequest.no_ack = 1;
  clientRequest.nofpackets = boost::endian::endian_reverse(uint16_t(1));
  clientRequest.sizeofpacket = 3;
  clientRequest.no_wait = 1;
  clientRequest.maxi = 0;
  BOOST_TEST(boost::endian::endian_reverse(clientRequest.raw) == 0b00101001000000000000000100000011);
  std::cout << clientRequest << std::endl;
}

BOOST_AUTO_TEST_CASE(test_pDTP_helpers_rqfs)
{
  pDTPClientRequest clientRequest;
  clientRequest.opcode = static_cast<decltype(clientRequest.opcode)>(pDTPClientRequest::ClientOpcode::CLIENT_RQFS);
  clientRequest.no_ack = 1;
  clientRequest.sizeofpacket = 3;
  BOOST_TEST(boost::endian::endian_reverse(clientRequest.raw) == 0b00111000000000000000000000000011);
  std::cout << clientRequest << std::endl;
}

BOOST_AUTO_TEST_CASE(test_pDTP_helpers_thr)
{
  pDTPClientRequest clientRequest;
  clientRequest.opcode = static_cast<decltype(clientRequest.opcode)>(pDTPClientRequest::ClientOpcode::CLIENT_THR);
  clientRequest.wait_cycles = boost::endian::endian_reverse(int32_t(2) << 8);
  BOOST_TEST(boost::endian::endian_reverse(clientRequest.raw) == 0b10000000000000000000000000000010);
  std::cout << clientRequest << std::endl;
}

} // namespace data_model
} // namespace bpct
