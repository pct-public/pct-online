#define BOOST_TEST_MODULE Test Bergen pCT datamodel
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include "datamodel/pRU-format.h"

namespace bpct
{
namespace data_model
{

BOOST_AUTO_TEST_CASE(test_PRUFormat)
{
  [[maybe_unused]] PRUFormat word;
}

} // namespace data_model
} // namespace bpct
