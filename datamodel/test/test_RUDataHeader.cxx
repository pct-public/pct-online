#define BOOST_TEST_MODULE Test Bergen pCT datamodel
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include "datamodel/RUDataHeader.h"

namespace bpct
{
namespace data_model
{

BOOST_AUTO_TEST_CASE(test_AlpideHits_container)
{
  // create a buffer holding the header and 10 Alpide hits
  const size_t nofAlpideHits = 10;
  const size_t bufferSize = sizeof(RUDataHeader) + nofAlpideHits * sizeof(AlpideHit);
  std::vector<uint8_t> buffer(bufferSize);
  RUDataHeader& data = *reinterpret_cast<RUDataHeader*>(buffer.data());
  data.layer = 21;
  data.frameid = 42;
  data.nofHits = nofAlpideHits;
  for (size_t hitidx = 0; hitidx < data.nofHits; hitidx++) {
    data.hits[hitidx].x = 0x1 << hitidx;
    data.hits[hitidx].y = 0x1 << hitidx;
    data.hits[hitidx].chip = hitidx % 4;
    data.hits[hitidx].stave = 5;
  }

  // test one of the hits
  size_t offset = sizeof(RUDataHeader) + (nofAlpideHits / 2) * sizeof(AlpideHit);
  auto const& hit5 = *reinterpret_cast<AlpideHit*>(buffer.data() + offset);
  BOOST_CHECK(hit5.x == 32);
  BOOST_CHECK(hit5.chip == 1);
  BOOST_CHECK(hit5.stave == 5);
}

} // namespace data_model
} // namespace bpct
