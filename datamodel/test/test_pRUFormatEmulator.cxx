#define BOOST_TEST_MODULE Test Bergen pCT datamodel
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "datamodel/pRUFormatEmulator.h"

namespace bpct
{
namespace data_model
{

BOOST_AUTO_TEST_CASE(test_single_device)
{
  using State = pRUEmulatorDevice::State;
  pRUEmulatorDevice emulator;
  std::vector<unsigned char> buffer;
  while (emulator != State::IDLE) {
    buffer << emulator;
  }

  BOOST_REQUIRE(buffer.size() == 48);
  BOOST_CHECK(buffer[0] == 0x40);
  BOOST_CHECK(buffer[16] == 0x00);
  BOOST_CHECK(buffer[buffer.size() - 16] == 0x80);
  BOOST_CHECK(buffer[buffer.size() - 1] == 0x08);
}

BOOST_AUTO_TEST_CASE(test_emulator_distributions)
{
  int nDataWords = 0;
  using State = pRUEmulatorDevice::State;
  pRUEmulatorDevice::DataLengthDistribution dataLengthDistribution = [&nDataWords]() {
    // a simple alternating distribution
    return (++nDataWords % 2) + 2;
  };
  pRUEmulatorDevice::FrameSizeDistribution frameSizeDistribution = []() {
    return 10;
  };
  BOOST_CHECK((bool)dataLengthDistribution == true); // have a function
  pRUEmulatorDevice emulator(1, 2, 3, dataLengthDistribution, frameSizeDistribution);
  // the original idea was to assign the callback by move inside the emulator constructor,
  // making the function here, this was found to be counter intuitive, so we assign by
  // copy, and the callback remains valid here
  BOOST_CHECK((bool)dataLengthDistribution == true);
  BOOST_CHECK((bool)frameSizeDistribution == true);

  std::vector<unsigned char> buffer;
  while (emulator != State::IDLE) {
    buffer << emulator;
  }

  BOOST_REQUIRE(buffer.size() == 160);
  BOOST_CHECK(buffer[0] == 0x41);
  BOOST_CHECK(buffer[16] == 0x01);
  BOOST_CHECK(buffer[buffer.size() - 16] == 0x81);
  size_t expectedAlpideWords = 3 + 1 + (nDataWords / 2) * 5 + (nDataWords % 2 ? 3 : 0);
  BOOST_CHECK(buffer[buffer.size() - 1] = expectedAlpideWords);
}

BOOST_AUTO_TEST_CASE(test_multiple_devices)
{
  int nDataWords = 0;
  using State = pRUEmulatorDevice::State;
  pRUEmulatorDevice::DataLengthDistribution dataLengthDistribution = [&nDataWords]() {
    // a simple alternating distribution
    return (++nDataWords % 2) + 2;
  };
  pRUEmulatorDevice::FrameSizeDistribution frameSizeDistribution = []() {
    return 5;
  };
  pRUEmulatorDevice::BurstLengthDistribution burstLengthDistribution = []() {
    return 10;
  };
  pRUEmulatorDevice emulator1(1, 2, 3, dataLengthDistribution, frameSizeDistribution);
  BOOST_REQUIRE((bool)dataLengthDistribution == true);
  BOOST_REQUIRE((bool)frameSizeDistribution == true);
  pRUEmulatorDevice emulator2(1, 5, 7, dataLengthDistribution, frameSizeDistribution, burstLengthDistribution);
  BOOST_CHECK((bool)burstLengthDistribution == true);

  std::vector<unsigned char> buffer;
  int cycle = 10;
  bool active = true;
  do {
    active = false;
    if (emulator1 != State::IDLE || cycle-- > 0) {
      buffer << emulator1;
      active = true;
    }
    if (emulator2 != State::IDLE || cycle-- > 5) {
      buffer << emulator2;
      active = true;
    }
  } while (active);

  BOOST_REQUIRE(buffer.size() == 40 * 16);
  BOOST_CHECK(buffer[0] == 0x41);
  BOOST_CHECK(buffer[1] == 0x23);
  BOOST_CHECK(buffer[16] == 0x41);
  BOOST_CHECK(buffer[17] == 0x57);
}

} // namespace data_model
} // namespace bpct
