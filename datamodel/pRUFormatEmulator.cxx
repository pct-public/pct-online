/// @file   pRUFormatEmulator.cxx
/// @author Matthias Richter (sw@matthias-richter.com)
/// @since  2021-01-21
/// @brief  Emulator for the pRU data format

#include "datamodel/pRUFormatEmulator.h"
#include "datamodel/alpide_format.h"

#include <cstring>
#include <stdexcept>
#include <string>

namespace bpct
{
namespace data_model
{

pRUEmulatorDevice::pRUEmulatorDevice()
  : mId(0)
  , mRow(0)
  , mColumn(0)
  , mState(pRUEmulatorDevice::State::START)
{
}

bool pRUEmulatorDevice::executeState(Inserter& inserter)
{
  if (mState == State::START) {
    //start();
    mState = State::HEADER;
    return true;
  } else if (mState == State::HEADER) {
    insert(inserter, header());
    mDataOctets = 0;
    resetWord();
    setOctet(ALPIDE_CHIP_HEADER);
    setOctet(0x33); // chip header has two octets
    setOctet(ALPIDE_REGION_HEADER);
    mMinFrameSize = frameSize();
    mFrameSize = 1;
    mState = State::DATA;
    return true;
  } else if (mState == State::DATA) {
    pushData(inserter);
    // we need to add two words, one for the current pRU word containing data words,
    // and one for the pRU trailer
    if (mFrameSize + 2 >= mMinFrameSize) {
      setOctet(ALPIDE_CHIP_TRAILER);
      flush(inserter);
      mState = State::TRAILER;
    }
    return false;
  } else if (mState == State::TRAILER) {
    insert(inserter, trailer());
    mWaitCount = getWait();
    mState = State::IDLE;
    return false;
  } else if (mState == State::BUSY) {
    throw std::runtime_error("state 'BUSY' not yet suported");
  } else if (mState == State::IDLE) {
    if (mWaitCount > 0 && --mWaitCount == 0) {
      return false;
    }
    mState = State::HEADER;
    return true;
  } else if (mState == State::STOP) {
    setOctet(ALPIDE_CHIP_TRAILER);
    flush(inserter);
    insert(inserter, trailer());
    mState = State::DEAD;
  } else if (mState == State::BREAK) {
    resetWord();
    mState = State::DEAD;
  } else if (mState == State::DEAD) {
  }
  return false;
}

void pRUEmulatorDevice::insert(Inserter& inserter, pRU::Word const& word)
{
  inserter(word);
  mFrameSize++;
}

void pRUEmulatorDevice::forceState(State /*forcedStae*/)
{
  // TODO
  throw std::runtime_error("forced state changes not yet suported");
}

void pRUEmulatorDevice::pushData(Inserter& inserter)
{
  unsigned int count = 0;
  auto burstl = burstLength();
  do {
    auto length = dataLength();
    if (length < 2 || length > 3) {
      throw std::runtime_error("invalid data length " + std::to_string(length));
    }
    unsigned char octet = ALPIDE_DATA_SHORT;
    if (length == 3) {
      octet = ALPIDE_DATA_LONG;
    }
    auto overflow = mPosition + length - 16;
    if (setOctet(octet) >= 16 || (mPosition += length - 1) >= 16) {
      insert(inserter, mWord);
      resetWord(overflow);
    }
    count += length;
    mDataOctets += length - 1; // incremented once in setOctet already
  } while (count < burstl);
}

size_t pRUEmulatorDevice::setOctet(unsigned char octet)
{
  if (mPosition >= 16) {
    throw std::logic_error("missing flush");
  }
  mWord[mPosition++] = octet;
  mDataOctets++;
  return mPosition;
}

size_t pRUEmulatorDevice::dataLength() const
{
  return mDataLengthDistribution ? mDataLengthDistribution() : DataLength{2};
}

size_t pRUEmulatorDevice::burstLength() const
{
  return mBurstLengthDistribution ? mBurstLengthDistribution() : BurstLength{0};
}

size_t pRUEmulatorDevice::frameSize() const
{
  return mFrameSizeDistribution ? mFrameSizeDistribution() : FrameSize{0};
}

void pRUEmulatorDevice::resetWord(size_t overflow)
{
  mPosition = 0;
  mWord[mPosition++] = mId;
  mWord[mPosition++] = mRow << 4 | mColumn;
  // we simply set the octets of the overflow to some other
  // value to mark those in the stream
  std::memset(mWord.data() + mPosition, 0x69, overflow);
  mPosition += overflow;
  // all remaining are set to the padding value
  std::memset(mWord.data() + mPosition, 0x23, 16 - mPosition);
}

void pRUEmulatorDevice::flush(Inserter& inserter)
{
  // if the pRU word contains data already, all remaining octets are set to
  // the padding value
  if (mPosition > 2) {
    std::memset(mWord.data() + mPosition, 0xff, 16 - mPosition);
    insert(inserter, mWord);
    resetWord();
  }
}

pRU::Word pRUEmulatorDevice::header() const
{
  return Word{(unsigned char)(0x40 | mId), (unsigned char)(mRow << 4 | mColumn), 0x0};
}

pRU::Word pRUEmulatorDevice::trailer() const
{
  Word trailer{(unsigned char)(0x80 | mId), (unsigned char)(mRow << 4 | mColumn), 0x0};
  trailer[15] = mDataOctets & 0xff;
  trailer[14] = (mDataOctets >> 8) & 0xff;
  trailer[13] = (mDataOctets >> 16) & 0xff;
  trailer[12] = (mDataOctets >> 24) & 0xff;
  return trailer;
}

int pRUEmulatorDevice::getWait() const
{
  return mWaitDistribution ? mWaitDistribution() : WaitCount{0};
}

} // namespace data_model
} // namespace bpct
