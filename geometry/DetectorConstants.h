//
// Created by Rune Almåsbakk on 17/11/2021.
//

#ifndef PCT_BERGEN_DETECTOR_CONSTANTS_H
#define PCT_BERGEN_DETECTOR_CONSTANTS_H

/*!
 * Constants are defined in millimeters
 *
 * Every value needed to reconstruct actual values from the detector are defined here.
 * Example: the physical width and height of a pixel
 *
 * every constant which contains "OFFSET" are values which denotes gaps between some parts
 * that should not really be there e.g gaps between each pixel.
 */

inline struct DetectorGeometry {
    //tracking layers
    double LAYER_0_TO_1_GAP = 52.4;  // this value is not certain
    double LAYER_1_TO_2_GAP = 55.75; // this value is not certain
    int NUM_TRACKING_LAYERS = 2;
    //Calorimeter layers
    double CHIP_WIDTH            = 30;        //Width
    double CHIP_OFFSET          = 0.0001;
    double CHIP_HEIGHT          = 15;        //Height
    double CHIP_HEIGHT_OFFSET   = 0.000001;
    double CHIP_DEPTH           = 0.1;
    double CHIP_GAP             = 0.02912;   //Between each chip on a stave
    double CHIP_GAP_OFFSET      = 0.000001;
    double STAVE_SIZE_HEIGHT    = 27.1;
    double STAVE_SIZE_WIDTH     = 273.35;
    double STAVE_SIZE_DEPTH     = 0.344;
    double STAVE_SIZE_OFFSET    = 0.00001;
    double STAVE_GAP            = 0.61872;   //Between even and odd staves
    double STAVE_GAP_OFFSET     = 0.000001;
    double LAYER_DEPTH          = 3;
    double LAYER_DEPTH_OFFSET   = 0.00001;
    double LAYER_GAP            = 0.2;       //Between layers
    double LAYER_GAP_OFFSET     = 0.00001;
    double ABSORBER_LAYER       = 2.3;  //should be 3.5
    double ABSORBER_LAYER_OFFSET= 0.00001;

    int NUM_LAYERS              = 50; //should be 43 but simulation uses 50?
    int NUM_STAVES_PER_LAYER    = 12;
    int NUM_CHIPS_PER_STAVE     = 9;
    int TOTAL_NUM_CHIPS               = NUM_LAYERS * NUM_STAVES_PER_LAYER * NUM_CHIPS_PER_STAVE;

    int NUM_PIXEL_WIDTH        = 1024;
    int NUM_PIXEL_HEIGHT       = 512;

    double PIXEL_SIZE_X         = 0.02924;
    double PIXEL_SIZE_OFFSET_X  = 0.00000000001;
    double PIXEL_SIZE_Y         = 0.02924;  //should be 0.02688?;
    double PIXEL_SIZE_OFFSET_Y  = 0.00000000001;
} defaultGeometry;

#endif //PCT_BERGEN_DETECTOR_CONSTANTS_H
