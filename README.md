# The pCT Bergen on-line software prototype

## Introduction
This is a prototype package for thr online software for the Bergen proton CT project.
The package also defines data formats for processing and storage which can be used inside
the package and by additional packages like the reconstruction building on top of this.

## Installation
### Requirements
- cmake > 3.5
- boost > 1.70
- devtoolset-7 or higher, gcc 7.3 or higher
- ROOT (currently required but will be optional again)

### Optional packages
#### Fair packages
FairLogger and FairMQ packages are needed in order to build the services, the
corresponding executables are skipped if not available.
- https://github.com/FairRootGroup/FairLogger (required by FairMQ)
- https://github.com/FairRootGroup/FairMQ

#### ROOT
ROOT is supposed to be an optional package for building ROOT I/O bindings. Currently,
the parser build needs ROOT, but once the I/O is disentangled we will go back to make
ROOT an optional package.

ROOT needs to be compiled with c++17 standard by enabling flag -DCMAKE_CXX_STANDARD=17
This is because ROOT comes with its own string_view implementation which conflicts to the
standard implementation.

#### Google benchmark
Specific benchmark test applications are using the Google benchmark framework
https://github.com/google/benchmark. The package is optional, all targets depending
on it will be conditionally built based on CMake switch `benchmark_Found`.

Non-system package can be enabled via switch
`-Dbenchmark_DIR=<patch_to_googlebenchmark>/lib/cmake/benchmark`

### Build
    mkdir build
    cd build
    cmake ..
    make


#### Custom install target
Use `-DCMAKE_INSTALL_PREFIX=<path>` to define a custom install target, e.g. `-DCMAKE_INSTALL_PREFIX=../install`

#### Custom version of boost
If the version of boost is outside the system directories, the custom install prefix can
be passed to cmake using the switch `-DBOOST_ROOT=/path/to/boost`. **Note**: after cmake
has reported an incompatible version, the internal cache variables are already set and there
is not new find process triggered if `BOOST_ROOT` is provided later. Clear the cache to fix this,
or brute force delete all content of the build directory and start from scratch.

The flag `-DBoost_NO_SYSTEM_PATHS` can be passed to cmake to avoid searching system directories
for boost, but does not work either if the cache is already set.

### Testing
In build folder (or any subfolder to run subset of tests):

    make test

or

    ctest

Specific tests can be run using selection by regular expression

    ctest -V -R specific_test

### Gitlab Continuous Integration (CI)
Gitlab triggers CI pipelines for
- changes in branche `dev`, and
- merge requests

Pipelines connect to runners, the project is using the gitlab runner in docker mode. That
means it loads docker images and executes the job within a docker container. The following
images from docker hub are used for the CI:
- bpct/centos7:full
- bpct/centos7:basic

The basic image does not contain any optional package and is used for a second build in
merge request pipelines.

The CI configuration is stored in file [.gitlab-ci.yml](.gitlab-ci.yml) and applies for
the parent project and all forks.

#### Specific runner on dedicated machine
As the default shared runner has stopped working for the project, a specific runner is set up
for the parent project on the pCT lab machine. The runner itsself can be started from a docker
image, thus reducing the effort to a minimum.
Description can be found in https://docs.gitlab.com/runner/install/docker.html

#### CI for forks
Specific runners for the parent project are not working for forks. This is somehow cumbersome, as
the CI configuration file is part of the fork. Gitlab will trigger pipelines for changes in the
dev branch but those are stuck because there is no runner connected. There is still discussion
ongoing among users and gitlab developers. For the moment these are the options to avoid stuck
pipelines in forks:
- do not push to the dev branch of the fork but use feature branches with dedicated names
- disable CI for the fork by by specifying custom CI configuration file which does not exist,
  e.g. in `Settings` -> `CI/CD` -> `General Pipelines` -> `Custom CI configuration` path
  specify '.disable-ci.yml'. Ugly but working.
- set up a runner for the fork

## Modules:
The package is in prototype phase, the different software modules are in the
first draft are as such:
- datamodel: defines the primitive data types and their relation
- datastore: implements abstract (in-memory) storage (might be core)
- core: core functionality
- utilities: common utilities
- io-adapters: collection of data adaptors to file, network, external formats
- readout: pRU readout
- hardware-support: hardware spec headers and board support packages
- geometry: geometry abstraction of detector (and phantom)
- visual, control: front-ends to provide the user interfaces for control and visualization
- back-ends: back-end implementations like VTK rendering and ITK algorithms

The current version implements the following modules:

### Module [readout](readout)
Contains the pDTP readout client and pTB readout software and a binary pRU format parser.
In addition some other utilities for the readout.

### Module [datamodel](datamodel)
Definition of primitive protocol structures like the pRU format and pDTP format.

### Module [io-adaptors](io-adaptors)
Input/output adaptors like ASCII to binary converters, file reader and file writer policies.
The sub-module [rootio](io-adaptors/rootio) contains exporter tools to the ROOT format.

## Docker images
A ready-to-go docker image for compiling the software on the CentOS7 reference system can
be downloaded from docker hub https://hub.docker.com/u/bpct. The configuration can be found
in this [Dockerfile](docker/CentOS7/Dockerfile).

Other Configurations will be added.
