/// @file test_NetworkSession
/// @brief Unit test for class @a NetworkSession

#define BOOST_TEST_MODULE Test Bergen pCT project
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/endian/conversion.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <thread>
#include "network/NetworkSession.h"
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/read.hpp>
#include "util/logging-tools.h"

namespace bpct
{
namespace network
{

namespace testclient
{

template <typename Protocol>
void run(std::string host, int port)
{
  boost::asio::io_service ioservice;
  typename Protocol::socket socket{ioservice};

  auto connect_handler = [&socket](const boost::system::error_code& ec) {
    if (!ec) {
      std::string r = "hello!\r\n";
      boost::asio::write(socket, boost::asio::buffer(r));
    }
  };

  auto resolve_handler = [connect_handler, &socket](const boost::system::error_code& ec, typename Protocol::resolver::iterator it) {
    if (!ec) {
      socket.async_connect(*it, connect_handler);
    }
  };

  typename Protocol::resolver r{ioservice};
  typename decltype(r)::query q{host, std::to_string(port)};
  r.async_resolve(q, resolve_handler);
  ioservice.run();
}

} // namespace testclient

namespace testserver
{

template <typename Protocol>
void run(int port)
{
  boost::asio::io_service ioservice;
  auto socket = std::make_shared<typename Protocol::socket>(ioservice, typename Protocol::endpoint(Protocol::v4(), port));
  auto remote = std::make_shared<typename Protocol::endpoint>();
  auto rb = std::make_shared<std::array<char, 1024>>();

  auto handle_send = [](const boost::system::error_code& /*ec*/, std::size_t /*length*/) {
    // TODO: can check that the write op was successful
  };

  std::function<void(const boost::system::error_code&, std::size_t)> handle_receive = [socket, remote, rb, &handle_receive, &handle_send](const boost::system::error_code& ec, std::size_t length) {
    if (length == 0) {
      // this is in principle obsolete, the handler only gets called when there is something
      // received, or some thing went completely wrong which is indicated in the error
      sleep(1);
      socket->async_receive_from(boost::asio::buffer(*rb), *remote, handle_receive);
    } else if (!ec || ec == boost::asio::error::message_size) {
      socket->async_send_to(boost::asio::buffer(*rb, length), *remote, handle_send);
    }
  };

  auto schedule_listen = [socket, remote, rb, &handle_receive]() {
    socket->async_receive_from(boost::asio::buffer(*rb), *remote, handle_receive);
  };

  schedule_listen();
  ioservice.run();
}

} // namespace testserver

// TODO: further tests
// - test the binding session on an address which is already in use by adding another test case which opens a server before
// - right now, the test client thread crashes if the address is blocked by a different server, e.g. 'ncat -e /bin/cat -k -l 23042'
//   #5  0x00005555555699f1 in std::thread::~thread (this=0x7fffffffbe00, __in_chrg=<optimized out>) at /usr/include/c++/7/thread:135
//   #6  0x000055555556518c in bpct::readout::test_NetworkSession_bind::test_method (this=0x7fffffffbf5e) at readout/test/test_NetworkSession.cxx:109
//   #7  0x00005555555648d8 in bpct::readout::test_NetworkSession_bind_invoker () at readout/test/test_NetworkSession.cxx:94

BOOST_AUTO_TEST_CASE(test_Types)
{
  // check the type implementaions depending on protocols
  static_assert(std::is_same<NetworkSession<boost::asio::ip::tcp>::socket_type, boost::asio::ip::tcp::socket>::value);
  static_assert(std::is_same<NetworkSession<boost::asio::ip::tcp>::acceptor_type, boost::asio::ip::tcp::acceptor>::value);
  static_assert(std::is_same<NetworkSession<boost::asio::ip::tcp>::resolver_type, boost::asio::ip::tcp::resolver>::value);
  static_assert(std::is_same<NetworkSession<boost::asio::ip::udp>::socket_type, boost::asio::ip::udp::socket>::value);
  static_assert(std::is_same<NetworkSession<boost::asio::ip::udp>::acceptor_type, void>::value);
  static_assert(std::is_same<NetworkSession<boost::asio::ip::udp>::resolver_type, boost::asio::ip::udp::resolver>::value);
}

BOOST_AUTO_TEST_CASE(test_NetworkSession_bind)
{
  // unit test for a binding network session, the binding happens to a part on the local machine
  // Using the TCP testclient for establishing some communication
  // Note: will wait for an established connection before the the notifier is called
  // Another simple test with netcat command
  // echo -n "hello!" | nc localhost 23042
  constexpr int port = 23042;

  NetworkSession<boost::asio::ip::tcp> session;
  auto notifier = [port]() {
    std::cout << "bind to port " << port << " and established connection" << std::endl;
  };

  // start the test client
  std::thread testclient(testclient::run<decltype(session)::protocol>, "localhost", port);

  auto result = session.open(port, notifier);
  BOOST_REQUIRE(result);

  // now some communication can be done via the socket:
  std::vector<char> buffer = {'n', 'o', 't', 'h', 'i', 'n', 'g', 0};
  boost::system::error_code error;
  auto len = session.getSocket().read_some(boost::asio::buffer(buffer.data(), buffer.size() * sizeof(decltype(buffer)::value_type)), error);
  BOOST_REQUIRE(len <= buffer.size());
  BOOST_CHECK_MESSAGE(std::string(buffer.data(), buffer.size()).find("hello!") != std::string::npos, "No one saying hello to me, got " << std::string(buffer.data(), (len > 0 ? len : buffer.size())));

  testclient.join();
  // the network session is automatically closed when the object goes out of scope
}

BOOST_AUTO_TEST_CASE(test_NetworkSession_connect)
{
  // unit test for a connecting network session, the binding happens to a part on the specified machine
  constexpr int port = 24042;

  NetworkSession<boost::asio::ip::udp> session;
  // start the test server, wait a bit because there is no synchronization in UDP
  std::thread testserver(testserver::run<decltype(session)::protocol>, port);
  sleep(2);

  auto notifier = [port]() {
    std::cout << "connected to port " << port << std::endl;
  };
  // Note that there is no check in UDP whether the port is really open/has an open endpoint
  // That's why the method will always succeed.
  auto result = session.open("localhost", port, notifier);
  BOOST_REQUIRE(result);

  // now some communication can be done via the socket, we expect an echo server

  // we expect an echo server
  std::string callout = "Anybody out there?\r\n";
  std::vector<char> reply(callout.size() + 1);
  boost::system::error_code ec;
  session.getSocket().send(boost::asio::buffer(callout));
  auto len = session.getSocket().receive(boost::asio::buffer(reply.data(), reply.size() * sizeof(decltype(reply)::value_type)), 0, ec);
  BOOST_CHECK_MESSAGE(!ec, "receive operation returned error " << ec);
  BOOST_CHECK(std::string(reply.data(), len) == callout);

  // TODO: implement timeout
  testserver.join();
  // the network session is automatically closed when the object goes out of scope
}

} // namespace network
} // namespace bpct
