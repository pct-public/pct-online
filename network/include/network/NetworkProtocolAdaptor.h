#ifndef NETWORKPROTOCOLADAPTOR_H
#define NETWORKPROTOCOLADAPTOR_H
/// @file NetworkProtocolAdaptor.h
/// @brief Selector proxy for network protocol

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <type_traits>

namespace bpct
{
namespace network
{

namespace protocol
{
/// @struct TCP
/// @brief identifier type for the TCP protocol to be used as template parameter
struct TCP : std::integral_constant<size_t, 0> {
};
/// @struct UDP
/// @brief identifier type for the UDP protocol to be used as template parameter
struct UDP : std::integral_constant<size_t, 1> {
};

// a type trait to get the acceptor type from the protocol, e.g. udp does not come
// with an acceptor
template <typename T, typename _ = void>
struct AcceptorTraits {
  using acceptor = void;
};
// this is a SFINAE specialization, the std::conditional as second template parameter
// is ill-formed if the type does not define `T::acceptor`, though the actual type of
// this is ignored because the switch is always false
template <typename T>
struct AcceptorTraits<T, std::conditional_t<false, typename T::acceptor, void>> {
  using acceptor = typename T::acceptor;
};
} // namespace protocol

/// @struct NetworkProtocolAdaptor
/// @brief Adaptor to the different protocols
/// The adaptor defines types for socket, acceptor, and resolver. The default implementation
/// defines all types as `void`. Specializations for the allowed protocols define the concrete
/// types.
/// The generic type definitions can be used in generic host classes to specify the corresponding
/// types. The compiler will build the code depending on the protocol template parameter.
template <typename Protocol>
struct NetworkProtocolAdaptor {
  using socket_type = void;
  using acceptor_type = void;
  using resolver_type = void;
};

/// @struct NetworkProtocolAdaptor
/// @brief specialization for the TCP protocol
template <>
struct NetworkProtocolAdaptor<boost::asio::ip::tcp> {
  using socket_type = boost::asio::ip::tcp::socket;
  using acceptor_type = boost::asio::ip::tcp::acceptor;
  using resolver_type = boost::asio::ip::tcp::resolver;
};

/// @struct NetworkProtocolAdaptor
/// @brief specialization for the UDP protocol
/// @note  UDP protocol does not define an acceptor
template <>
struct NetworkProtocolAdaptor<boost::asio::ip::udp> {
  using socket_type = boost::asio::ip::udp::socket;
  using acceptor_type = void;
  using resolver_type = boost::asio::ip::udp::resolver;
};

// some checks
static_assert(std::is_same<protocol::AcceptorTraits<boost::asio::ip::tcp>::acceptor, boost::asio::ip::tcp::acceptor>::value);
static_assert(std::is_same<protocol::AcceptorTraits<boost::asio::ip::udp>::acceptor, void>::value);

} // namespace network
} // namespace bpct

#endif // NETWORKPROTOCOLADAPTOR_H
