#ifndef NETWORKSESSION_H
#define NETWORKSESSION_H
/// @file NetworkSession.h
/// @brief Stateful object holding a network session

#include <iostream>
#include <iomanip>
#include <boost/asio/io_service.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/read.hpp>
#include "network/NetworkProtocolAdaptor.h"

namespace bpct
{
namespace network
{

/// @class NetworkSession
/// @brief Handler for a network socket
/// This class handles a network connection session by asynchronously establishing the
/// socket connection and leaving it open for communication.
///
/// Protocol: boost::asio::ip::tcp, boost::asio::ip::udp
template <typename Protocol>
class NetworkSession
{
 public:
  NetworkSession()
    : mIoService()
    , mSocket(mIoService){};
  ~NetworkSession()
  {
    close();
  }

  // this serves the interface of boost asio tcp/udp
  using protocol = Protocol;
  using socket_type = typename NetworkProtocolAdaptor<Protocol>::socket_type;
  using acceptor_type = typename NetworkProtocolAdaptor<Protocol>::acceptor_type;
  using resolver_type = typename NetworkProtocolAdaptor<Protocol>::resolver_type;

  /// Open a session
  ///
  /// TODO: get rid of the acceptor to support also UDP
  template <typename Callback>
  bool open(int port, Callback&& notifier)
  {
    bool connected = false;
    acceptor_type connection(mIoService, typename Protocol::endpoint(Protocol::v4(), port));
    connection.listen();

    auto acceptHandler = [port, &connected, notifier(std::move(notifier))](const boost::system::error_code& errorCode) {
      if (!errorCode) {
        connected = true;
        notifier();
      } else {
        // TODO: error policy
        std::cerr << "accept handler called with error " << errorCode << std::endl;
      }
    };
    connection.async_accept(this->mSocket, acceptHandler);
    mIoService.run();
    return connected;
  }

  /// Open a session
  /// Resolves a host and connect to the specified port
  /// TODO: handle port with no bound endpoint, support retry
  template <typename Callback>
  bool open(const char* host, int port, Callback&& notifier)
  {
    bool connected = false;
    typename resolver_type::query hostquery{host, std::to_string(port)};

    auto connectHandler = [&connected, notifier(std::move(notifier))](const boost::system::error_code& errorCode) {
      if (!errorCode) {
        connected = true;
        notifier();
      } else {
        // TODO: error policy
        std::cerr << "connect handler called with error " << errorCode << std::endl;
      }
    };
    auto resolveHandler = [connectHandler, this](const boost::system::error_code& errorCode, typename resolver_type::iterator it) {
      if (!errorCode) {
        this->mSocket.async_connect(*it, connectHandler);
      } else {
        // TODO: error policy
        std::cerr << "resolve handler called with error " << errorCode << std::endl;
      }
    };
    resolver_type resolver{mIoService};
    resolver.async_resolve(hostquery, resolveHandler);
    mIoService.run();
    return connected;
  }

  /// Close session
  /// Shutdown socket to terminate a pending operation
  void close()
  {
    boost::system::error_code ec;
    mSocket.shutdown(socket_type::shutdown_both, ec);
    // TODO: remove the debug output
    std::cout << "shutdown session: " << ec << std::endl;
    mSocket.close();
    mIoService.stop();
  }

  socket_type& operator()()
  {
    return mSocket;
  }

  socket_type& getSocket()
  {
    return (*this)();
  }

 private:
  /// the asynchronous io service
  boost::asio::io_service mIoService;
  /// the socket
  socket_type mSocket;
};

} // namespace network
} // namespace bpct

#endif // NETWORKSESSION_H
